package com.bt.kotlindemo.global

import android.app.Application
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.di.*
import com.bt.kotlindemo.model.app_menu.Menu
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.ThemeHelper
import com.google.firebase.FirebaseApp
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import timber.log.Timber
import timber.log.Timber.DebugTree


class App : Application() {

    private var menu = arrayListOf<Menu>()

    override fun onCreate() {
        super.onCreate()
        instance = this
        ThemeHelper.applyTheme(ThemeHelper.lightMode)
        FirebaseApp.initializeApp(this@App)
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            modules(
                listOf(
                    appModule,
                    networkModule,
                    repositoryModule,
                    viewModelModule,
                    dataBaseModule
                )
            )
        }
        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }


    }

    fun getAppMenu(): ArrayList<Menu> {
        if (menu.isEmpty()) {
            menu = AppPreferences(this).getMenu()
        }
        return menu
    }

    fun setAppMenu(menu: ArrayList<Menu>) {
        this@App.menu.clear()
        this@App.menu.addAll(menu)
    }

    companion object {
        lateinit var instance: App
            private set
    }

}