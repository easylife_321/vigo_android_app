/*
 * *
 *  * Created by Surajit on 10/4/20 3:08 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 5:21 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_history

import android.text.format.DateUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.repository.DeviceRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.device_history.History
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.DateTimeUtil
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.bt.kotlindemo.utils.toUtc
import com.google.gson.JsonObject
import java.util.*

class DeviceHistoryViewModel(
    val deviceId: Int,
    var preference: AppPreferences,
    private var deviceRepository: DeviceRepository
) : BaseViewModel() {

    val selectDate = MutableLiveData<Boolean>()
    val date = MutableLiveData<String>()
    val dateTimeStamp = MutableLiveData<Long>()
    val dateText = MutableLiveData<String>()
    val timeList = MutableLiveData<ArrayList<Long>>()
    val toTime = MutableLiveData<Long>()
    val fromTime = MutableLiveData<Long>()
    val day = MutableLiveData<Int>()
    var showMessage = MutableLiveData<String>()
    var showProgress = MutableLiveData<Boolean>()
    val locationList = MutableLiveData<ArrayList<History>>()
    var isPlaying = MutableLiveData<Boolean>()
    var showControls = MutableLiveData<Boolean>().apply { value = false }
    var changeView = MutableLiveData<Boolean>()
    var playPause = MutableLiveData<Boolean>()
    var speed = MutableLiveData<Int>().apply { value = 1 }
    val currentLocation = MutableLiveData<History>()
    var fetch = MutableLiveData<Boolean>()
    var route = SingleLiveEvent<Void?>()


    fun fetchHistory(): LiveData<Boolean> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("fromTimestamp", fromTime.value!!.toUtc())
        obj.addProperty("toTimestamp", toTime.value!!.toUtc())
        return deviceRepository.getHistory(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    false
                }
                else -> {
                    locationList.value = it.data!!.history as ArrayList<History>
                    false
                }
            }

        }

    }

    fun searchData() {
        if (DateTimeUtil.isValidHistoryRange(fromTime.value!!, toTime.value!!)) {
            fetch.value = true
        } else {
            showMessage.value = "You can view history of max 24 hours at a time."
        }
    }

    fun selectDate() {
        selectDate.value = true
    }

    fun playPause() {
        playPause.value = true
    }

    fun changeMapView() {
        changeView.value = true
    }

    fun changeSpeed() {
        if (speed.value == 4) {
            speed.value = 1
        } else {
            speed.value = speed.value!! + 1
        }
    }


    fun arrangeDate() {
        when {
            DateUtils.isToday(dateTimeStamp.value!!) -> {
                val date: Calendar = Calendar.getInstance()
                date.set(Calendar.HOUR_OF_DAY, 0)
                date.set(Calendar.MINUTE, 0)
                date.set(Calendar.SECOND, 0)
                date.set(Calendar.MILLISECOND, 0)
                //date.set(Calendar., 0)
                val start = date.timeInMillis - DateUtils.DAY_IN_MILLIS
                val end = System.currentTimeMillis()
                //setDateArray(startTimestamp,endTimeStamp)
                dateText.value =
                    DateTimeUtil.formatSharableTime(start) + "   " + DateTimeUtil.formatSharableTime(
                        end
                    )
                setDateArray(start, end, TODAY)
            }
            DateUtils.isToday(dateTimeStamp.value!! + DateUtils.DAY_IN_MILLIS) -> {
                val date: Calendar = Calendar.getInstance()
                date.timeInMillis = dateTimeStamp.value!!
                date.set(Calendar.HOUR_OF_DAY, 0)
                date.set(Calendar.MINUTE, 0)
                date.set(Calendar.SECOND, 0)
                date.set(Calendar.MILLISECOND, 0)
                val start = date.timeInMillis - DateUtils.DAY_IN_MILLIS
                val end = System.currentTimeMillis()
                dateText.value =
                    DateTimeUtil.formatSharableTime(start) + "   " + DateTimeUtil.formatSharableTime(
                        end
                    )
                setDateArray(start, end, YESTERDAY)
            }
            else -> {
                val date: Calendar = Calendar.getInstance()
                date.timeInMillis = dateTimeStamp.value!!
                date.set(Calendar.HOUR_OF_DAY, 0)
                date.set(Calendar.MINUTE, 0)
                date.set(Calendar.SECOND, 0)
                date.set(Calendar.MILLISECOND, 0)
                val start = date.timeInMillis - DateUtils.DAY_IN_MILLIS
                val end = date.timeInMillis + DateUtils.DAY_IN_MILLIS * 2 - 1000
                dateText.value =
                    DateTimeUtil.formatSharableTime(start) + "   " + DateTimeUtil.formatSharableTime(
                        end
                    )
                setDateArray(start, end, EARLIER)
            }
        }
    }

    private fun setDateArray(startTimestamp: Long, endTimeStamp: Long, day: Int) {
        val list = arrayListOf<Long>()
        var temp = startTimestamp
        while (temp < endTimeStamp) {
            if ((endTimeStamp - temp) >= DateUtils.MINUTE_IN_MILLIS * 10) {
                list.add(temp)
            } else {
                list.add(endTimeStamp)
            }
            temp += DateUtils.MINUTE_IN_MILLIS * 10
        }
        this.day.value = day
        timeList.value = list

    }

    fun route() {
        route.call()
    }

    fun saveRoute(name: String): LiveData<Boolean> {
        val obj = JsonObject()
        obj.addProperty("userId", preference.getUserId())
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("fromts", fromTime.value!!.toUtc())
        obj.addProperty("tots", toTime.value!!.toUtc())
        obj.addProperty("route_name", name)
        return deviceRepository.saveRoute(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    false
                }
                else -> {
                    showMessage.value = it.data?.result?.msg
                    false
                }
            }

        }
    }


    companion object {
        const val TODAY = 1
        const val YESTERDAY = 2
        const val EARLIER = 3
    }


}