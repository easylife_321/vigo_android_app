/*
 * *
 *  * Created by Surajit on 25/4/20 12:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/4/20 6:18 PM
 *  *
 */

package com.bt.kotlindemo.ui.profile.update_profile

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.R
import com.bt.kotlindemo.data.repository.SettingsRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Country
import com.bt.kotlindemo.model.State
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.MiscUtil
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import java.io.File

class UpdateProfileViewModel(
    private val preference: AppPreferences,
    private var repository: SettingsRepository,
    private val appContext: Application
) : BaseViewModel() {


    var isLoaded = ObservableBoolean()
    var address = ObservableField<String>()
    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var showToast = MutableLiveData<String>()
    var stateList = MutableLiveData<List<State>>()
    var countrylist = MutableLiveData<List<Country>>()
    var name = MutableLiveData<String>()
    var mobile = MutableLiveData<String>()
    var email = MutableLiveData<String>()
    var address1 = MutableLiveData<String>()
    var landmark = MutableLiveData<String>()
    var state = MutableLiveData<String>()
    var country = MutableLiveData<String>()
    var pin = MutableLiveData<String>()
    var pickImage = SingleLiveEvent<Boolean?>()
    var update = SingleLiveEvent<Boolean?>()
    var updated = MutableLiveData<Boolean>()
    var isCameraPermissionAllowed = MutableLiveData<Boolean>()
    var file = MutableLiveData<File>()
    val src = MutableLiveData<Any>()
    var pos = MutableLiveData<Int>()
    var posCountry = MutableLiveData<Int>()
    var stateId = MutableLiveData<Int>()
    var countryId = MutableLiveData<Int>()
    var city = MutableLiveData<String>()

    init {
        name.value = preference.getUserName()
        mobile.value = "+${preference.getCountry().countryCode}${preference.getMobile()}"
        email.value = preference.getEmail()
        if (!preference.getProfileImage().isNullOrEmpty()) {
            src.value = preference.getProfileImage()
        }else{
            src.value= R.drawable.user_icon
        }
        state.value=preference.getStateName()
        stateId.value=preference.getStateId()
        pin.value=preference.getPin()
        address1.value=preference.getAddress1()
        landmark.value=preference.getAddress2()
        city.value = preference.getCity()
        countryId.value = preference.getCountryId()

    }


    fun getStateList(): LiveData<Unit> {
        val obj = JsonObject()
        obj.addProperty("countryId",countryId.value)
        return repository.getStateList(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message?:""
                    isLoading.value = false
                }
                else -> {
                    if (it.data == null) {
                        isLoading.value = false
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"
                    } else {
                        isLoading.value = false
                        val list = it.data.state?.toMutableList()
                        list?.add(0, State(0,  "Select State/Province"))
                        stateList.value = list?: emptyList()
                        if (preference.getStateId() != 0) {
                            setState()
                        }
                    }
                }
            }


        }

    }

    fun getCountryList(): LiveData<Unit> {
        return repository.getCountryList().map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message?:""
                    isLoading.value = false
                }
                else -> {
                    if (it.data == null) {
                        isLoading.value = false
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"
                    } else {
                        isLoading.value = false
                        val list = it.data.state?.toMutableList()
                        list?.add(0, Country(-1, "Select Country"))
                        countrylist.value = list?: emptyList()
                        if (preference.getCountryId() != 0) {
                            setCountry()
                        }
                    }
                }
            }


        }

    }

    private fun setState() {
        stateList.value?.onEachIndexed { index, it ->
            if (it.stateCode == preference.getStateId()) {
                pos.value = index
                return@onEachIndexed
            }
        }
    }

    private fun setCountry() {
        countrylist.value?.onEachIndexed { index, it ->
            if (it.countryCode == preference.getCountryId()) {
                posCountry.value = index
                return@onEachIndexed
            }
        }
    }


    fun updateData(): LiveData<Unit> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("fullName", name.value)
        jsonObject.addProperty("emailId", email.value)
        jsonObject.addProperty("addressLine1", address1.value)
        jsonObject.addProperty("landmark", landmark.value)
        jsonObject.addProperty("stateId", stateId.value)
        jsonObject.addProperty("stateName", state.value)
        jsonObject.addProperty("countryId", countryId.value)
        jsonObject.addProperty("cityName", city.value)
        jsonObject.addProperty("pinCode", pin.value)

        return repository.updateProfile(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message?:""
                    isLoading.value = false
                }
                else -> {
                    isLoading.value = false
                    if (it.data == null) {
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"

                    } else {
                        if (it.data.result.code == 1) {
                            preference.setUserName(name.value ?: "")
                            preference.setEmail(email.value ?: "")
                            preference.setAddress1(address1.value ?: "")
                            preference.setAddress2(landmark.value ?: "")
                            preference.setState(state.value ?: "")
                            preference.setStateId(stateId.value ?: 0)
                            preference.setCountryId(countryId.value ?: 0)
                            preference.setPin(pin.value ?: "")
                            preference.setCity(city.value?:"")
                            showToast.value = it.data.result.msg
                            updated.value=true

                        } else {
                            showMessage.value = it.data.result.msg
                        }

                    }
                }
            }


        }
    }

    fun uploadProfilePic(): LiveData<Unit> {
        val parts: MultipartBody.Part = MiscUtil.prepareFilePart("file", file.value!!)
        val userId = MiscUtil.prepareBodyPart(preference.getUserId().toString())
        return repository.updateProfileImage(userId, parts).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message?:""
                    isLoading.value = false
                }
                else -> {
                    if (it.data == null) {
                        isLoading.value = false
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"

                    } else {
                        preference.setProfileImage(it.data.url)
                        src.value = "${preference.getBaseImageUrl()}${it.data.url}"
                        isLoading.value = false
                        showToast.value = it.data.result.msg
                        updated.value=true
                    }
                }
            }


        }

    }

    fun pickImage() {
        pickImage.call()

    }

    fun update() {
        update.call()
    }


}