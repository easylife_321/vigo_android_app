/*
 * *
 *  * Created by Surajit on 27/5/20 11:18 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 8:16 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.shareable_link.add_new_link

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.repository.DeviceRepository
import com.bt.kotlindemo.data.repository.ShareableLinkRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Poi
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.DateTimeUtil
import com.bt.kotlindemo.utils.toUtc
import com.google.gson.JsonArray
import com.google.gson.JsonObject

class AddShareableLinkViewModel(
    var preference: AppPreferences,
    private var linkRepository: ShareableLinkRepository,
    private val deviceRepository: DeviceRepository
) : BaseViewModel() {

    var poi = MutableLiveData<ArrayList<Poi>>()

    var isLoading = MutableLiveData<Boolean>()
    var devices = MutableLiveData<ArrayList<Device>>()
    var selectedDevices = MutableLiveData<ArrayList<Device>>()
    var showMessage = MutableLiveData<String>()
    var showMessageRetry = MutableLiveData<String>()
    var isEmpty = MutableLiveData<Boolean>()
    private var jsonObject: JsonObject = JsonObject()

    val addDevice = MutableLiveData<Boolean>()
    val validate = MutableLiveData<Boolean>()
    val fromTime = MutableLiveData<Boolean>()
    val toTime = MutableLiveData<Boolean>()
    val name = ObservableField<String>()
    val toTimeSelected = ObservableField<String>()
    val fromTimeSelected = ObservableField<String>()
    val isEdit = ObservableField<Boolean>()
    val id = ObservableField<Int>()


    init {
        jsonObject.addProperty("userId", preference.getUserId())
        fromTimeSelected.set("Set Date & time")
        toTimeSelected.set("Set Date & time")

    }

    fun getDevices() = deviceRepository.getDevicesAdmin()


    fun addLink(): LiveData<ApiResult<BaseResponse>> {
        val obj = JsonObject()
        obj.addProperty("trackingLinkName", name.get())
        obj.addProperty(
            "validfrom",
            (DateTimeUtil.formatSharableTimeToTimeStamp(fromTimeSelected.get()!!)!!).toUtc()
        )
        obj.addProperty(
            "validtill",
            (DateTimeUtil.formatSharableTimeToTimeStamp(toTimeSelected.get()!!)!!.toUtc())
        )
        obj.addProperty("userId", preference.getUserId())
        obj.add("shareDevicesList", getSelectedDevices())
        if (isEdit.get()!!) {
            obj.addProperty("trackingLinkId", id.get())
        }
        return linkRepository.addLink(obj)
    }

    private fun getSelectedDevices(): JsonArray {
        val arr = JsonArray()
        selectedDevices.value!!.forEach {
            val id = JsonObject()
            id.addProperty("deviceId", it.deviceId)
            arr.add(id)
        }
        return arr
    }

    fun checkSelected() {
        selectedDevices.value = devices.value!!.filter {
            it.isSelected
        } as ArrayList<Device>
        isEmpty.value = selectedDevices.value!!.isEmpty()
    }


    fun addDevices() {
        addDevice.value = true
    }

    fun done() {
        validate.value = true
    }

    fun toTime() {
        toTime.value = true
    }

    fun fromTime() {
        fromTime.value = true
    }

    fun removeDevice(device: Device) {
        val temp = devices.value
        temp?.forEach {
            if (it.deviceId == device.deviceId) {
                it.isSelected = false
                return@forEach
            }
        }
        temp.apply {
            devices.value = this
        }

        checkSelected()
    }

    fun setDevices(devices: List<Int>) {
        val temp = this@AddShareableLinkViewModel.devices.value!!
        temp.forEach {
            if (devices.contains(it.deviceId)) {
                it.isSelected = true
            }
        }
        this@AddShareableLinkViewModel.devices.value = temp
    }

}