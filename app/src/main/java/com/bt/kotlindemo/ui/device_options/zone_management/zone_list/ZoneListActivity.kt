/*
 * *
 *  * Created by Surajit on 25/5/20 7:43 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:43 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.zone_management.zone_list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.databinding.ActivityZoneListBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.PoiRecyclerClickListener
import com.bt.kotlindemo.interfaces.ZoneRecyclerClickListener
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.zone.Zone
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.device_options.zone_management.add_zone.AddNewZoneActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.ferfalk.simplesearchview.SimpleSearchView
import org.koin.androidx.viewmodel.ext.android.viewModel

class ZoneListActivity : BaseActivity() {

    private lateinit var binding: ActivityZoneListBinding
    private val viewModel: ZoneListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        setStatusBarColor(R.color.colorPrimary)
        super.onCreate(savedInstanceState)

    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_zone_list)
        binding.apply {
            lifecycleOwner=this@ZoneListActivity
            viewModel=this@ZoneListActivity.viewModel
        }
    }

    override fun initViews() {
        addObservers()
        fetchList()
        setSearchView()
    }

    private fun setSearchView() {
        binding.searchView.setOnSearchViewListener(object : SimpleSearchView.SearchViewListener {
            override fun onSearchViewShownAnimation() {
            }

            override fun onSearchViewClosed() {
                if (binding.adapter != null)
                    binding.adapter!!.filter.filter("")
                binding.toolbar.visibility = View.VISIBLE
                binding.searchView.visibility = View.INVISIBLE
            }

            override fun onSearchViewClosedAnimation() {

            }

            override fun onSearchViewShown() {

            }

        })

        binding.searchView.setOnQueryTextListener(object : SimpleSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (binding.adapter != null) {
                    binding.adapter!!.filter.filter(newText)
                }
                return false
            }

            override fun onQueryTextCleared(): Boolean {
                return false
            }
        })

    }


    private fun addObservers() {
        viewModel.isLoading.observe(this, Observer {
            if (it) {
                showDialog()
            } else {
                dismissDialog()
            }
        })

        viewModel.showMessage.observe(this, Observer {
            AlertHelper.showOkAlert(this,it)

        })

        viewModel.list.observe(this, Observer {
            setList(it as ArrayList)
        })

        viewModel.search.observe(this, Observer { openSearch() })
        viewModel.onBackPressed.observe(this, Observer { finish() })
        viewModel.addZone.observe(this, Observer {
            startActivityForResult(
                Intent(this@ZoneListActivity, AddNewZoneActivity::class.java),
                IntentRequestConstants.ADD_ZONE
            )
        })
    }

    fun openSearch() {
        binding.toolbar.visibility = View.INVISIBLE
        binding.searchView.visibility = View.VISIBLE
        binding.searchView.showSearch(true)
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            slideToRight()
        }
    }

    override fun onBackPressed() {
        if (binding.searchView.isSearchOpen) {
            binding.searchView.closeSearch(true)
        } else {
            super.onBackPressed()
        }

    }


    private fun setList(it: ArrayList<Zone>) {
        binding.adapter = ZoneListAdapter(it, object : ZoneRecyclerClickListener {
            override fun onClick(data: Any, action: Int) {
                when (action) {
                    PoiRecyclerClickListener.VIEW -> {
                        val intent =
                            Intent(this@ZoneListActivity, AddNewZoneActivity::class.java).apply {
                                val bundle = Bundle().apply {
                                    putSerializable(IntentConstants.ZONE, data as Zone)
                                    putBoolean(IntentConstants.EDIT, true)
                                }
                                putExtras(bundle)
                            }
                        startActivityForResult(intent, IntentRequestConstants.EDIT_POI)
                    }
                    PoiRecyclerClickListener.DELETE -> {
                        AlertHelper.showOKCancelAlert(this@ZoneListActivity,
                            "Are you sure you want to delete this zone? ",
                            object : AlertCallback {
                                override fun action(isOk: Boolean, a: Any?) {
                                    if (isOk) {
                                        deleteAndObserve(data as Zone)
                                    }
                                }
                            })
                    }
                }
            }
        })
    }

    private fun deleteAndObserve(zone: Zone) {
        viewModel.deleteZone(zone.zoneId).observe(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(this@ZoneListActivity, it.message!!, null)
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(
                        this@ZoneListActivity,
                        it.data!!.result.msg,
                        object : AlertCallback {
                            override fun action(isOk: Boolean, data: Any?) {
                                binding.adapter!!.removeDevice(zone)
                            }
                        })
                }
                else -> return@Observer
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            fetchList()
        }
    }

    private fun fetchList() {
        viewModel.getZoneList().observe(this, Observer { })
    }
}
