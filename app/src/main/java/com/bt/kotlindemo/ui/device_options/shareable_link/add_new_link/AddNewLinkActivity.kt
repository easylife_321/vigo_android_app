/*
 * *
 *  * Created by Surajit on 27/5/20 10:27 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/5/20 10:27 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.shareable_link.add_new_link


import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.ActivityAddNewLinkBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.shareable_link.ShareableLink
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.select_device.SelectDeviceActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.DateTimeUtil
import com.bt.kotlindemo.utils.toLocalTime
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class AddNewLinkActivity : BaseActivity() {

    private val viewModel: AddShareableLinkViewModel by viewModel()
    private lateinit var binding: ActivityAddNewLinkBinding
    private var link: ShareableLink? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColor(R.color.colorPrimary)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_new_link)
        binding.apply {
            lifecycleOwner = this@AddNewLinkActivity
            viewModel = this@AddNewLinkActivity.viewModel
        }
    }

    override fun initViews() {
        addObservers()
        viewModel.getDevices()
    }

    private fun addObservers() {
        viewModel.getDevices().observe(this) {
            if (it.isEmpty()) {
                AlertHelper.showOkAlertFinish(
                    this,
                    "You are not admin of any device. To add a sharable link you need to have at-least one as an admin"
                )
            } else {
                viewModel.devices.value = it as ArrayList<Device>?
                setIfEdit()
            }
        }

        viewModel.devices.observe(this) {
            viewModel.checkSelected()
        }

        viewModel.selectedDevices.observe(this) {
            setList(it)
        }

        viewModel.onBackPressed.observe(this) {
            finish()
        }


        viewModel.toTime.observe(this) {
            pickDateTime(false)
        }

        viewModel.fromTime.observe(this) {
            pickDateTime(true)
        }

        viewModel.validate.observe(this) {
            validate()
        }

        viewModel.addDevice.observe(this) {
            startActivityForResult(
                Intent(this@AddNewLinkActivity, SelectDeviceActivity::class.java).apply {
                    putExtras(Bundle().apply {
                        putSerializable(IntentConstants.DATA, viewModel.devices.value!!)
                    })
                },
                IntentRequestConstants.SELECT_DEVICES
            )
        }


    }

    private fun setIfEdit() {
        if (intent.getBooleanExtra(IntentConstants.EDIT, false)) {
            link = intent.getSerializableExtra(IntentConstants.DATA) as? ShareableLink
            viewModel.isEdit.set(true)
            viewModel.name.set(link?.trackingLinkName)
            viewModel.fromTimeSelected.set(DateTimeUtil.formatSharableTime(link?.validfromTimestamp!!.toLocalTime()))
            viewModel.toTimeSelected.set(DateTimeUtil.formatSharableTime(link?.validtillTimestamp!!.toLocalTime()))
            viewModel.id.set(link?.trackingLinkId)
            viewModel.setDevices(link?.devices!!)
        } else {
            viewModel.isEdit.set(false)
        }
    }

    private fun validate() {
        if (binding.etName.text.toString().isEmpty()) {
            binding.etName.error = "Please enter link name"
            return
        }
        if (!DateTimeUtil.isValidShareableDate(
                binding.txtValidFrom.text.toString(),
                binding.txtValidTo.text.toString()
            )
        ) {
            showSnackBar(
                binding.root,
                "To date cannot be before from date",
            )
            return
        }
        if (binding.adapter!!.itemCount == 0) {
            showSnackBar(
                binding.root,
                "Please add at-least one device to continue"
            )
            return
        }
        addAndObserve()
    }

    private fun addAndObserve() {
        viewModel.addLink().observe(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showSnackBar(
                        binding.root,
                        it.message!!
                    )
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(
                        this, it.data?.result?.msg!!, object : AlertCallback {
                            override fun action(isOk: Boolean, data: Any?) {
                                setResult(Activity.RESULT_OK)
                                finish()
                            }
                        }
                    )
                }
                else -> return@Observer

            }
        })
    }

    private fun setList(devices: ArrayList<Device>) {
        if (binding.adapter == null) {
            binding.adapter = SelectedDeviceAdapter(devices)
        } else {
            binding.adapter!!.setDevices(devices)
        }

    }


    private fun pickDateTime(isFrom: Boolean) {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        val startHour = currentDateTime.get(Calendar.HOUR_OF_DAY)
        val startMinute = currentDateTime.get(Calendar.MINUTE)

        DatePickerDialog(this, { _, year, month, day ->
            TimePickerDialog(this, { _, hour, minute ->
                val pickedDateTime = Calendar.getInstance()
                pickedDateTime.set(year, month, day, hour, minute)
                if (isFrom) {
                    binding.txtValidFrom.text =
                        DateTimeUtil.formatSharableTime(pickedDateTime.timeInMillis)
                } else {
                    binding.txtValidTo.text =
                        DateTimeUtil.formatSharableTime(pickedDateTime.timeInMillis)
                }
            }, startHour, startMinute, false).show()
        }, startYear, startMonth, startDay).show()
    }


    @Suppress("UNCHECKED_CAST")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            viewModel.devices.value =
                data?.getSerializableExtra(IntentConstants.DATA) as? ArrayList<Device>
        }
    }

    fun remove(device: Device) {
        viewModel.removeDevice(device)
    }
}
