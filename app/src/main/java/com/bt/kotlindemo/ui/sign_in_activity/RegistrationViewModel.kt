package com.bt.kotlindemo.ui.sign_in_activity

import android.app.Application
import android.content.Context
import android.os.Build
import android.os.CountDownTimer
import android.provider.Settings
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.data.network.netwrok_responses.AppMenuResponse
import com.bt.kotlindemo.data.network.netwrok_responses.RegistrationResponse
import com.bt.kotlindemo.data.repository.AppDataRepository
import com.bt.kotlindemo.data.repository.RegistrationRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.ConnectivityUtil
import com.bt.kotlindemo.utils.CryptoUtil
import com.bt.kotlindemo.utils.MiscUtil
import com.bt.kotlindemo.utils.nullToEmpty
import com.google.gson.JsonObject
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.get
import timber.log.Timber
import java.util.*

const val COUNT_DOWN_TIME: Long = 60000
const val ONE_SEC: Long = 1000

class RegistrationViewModel(
    val context: Application,
    val preferences: AppPreferences,
    val connectivityUtil: ConnectivityUtil,
    val repository: RegistrationRepository,
    val appDataRepository: AppDataRepository
) :
    BaseViewModel(), KoinComponent {

    var loginWithOtp: Boolean = false
    val token = MutableLiveData<String>()
    private lateinit var jsonObjectReg: JsonObject
    private lateinit var jsonObjectOtp: JsonObject
    lateinit var otp: String
    var counter = MutableLiveData<Int>()
    var resendEnabled = MutableLiveData<Boolean>()
    private var countDownTimer: CountDownTimer? = null
    var fetchMenu = MutableLiveData<Boolean>()

    private val countries: ArrayList<CountryList> = get()
    val cryptoUtil: CryptoUtil = get()
     val phoneNumberUtil: PhoneNumberUtil = get()


    fun getCountry(): CountryList {
        var countryCode: String? = MiscUtil.getDeviceCountryCode(context)
        if (countryCode == null) {
            countryCode = "in"
        }
        val country = countries.find {
            it.isoAlpha.toLowerCase(Locale.getDefault()) == countryCode
        }
        preferences.setPhoneLength(phoneNumberUtil.getExampleNumber(country!!.isoAlpha).nationalNumber.toString().length)
        preferences.setCountry(country)
        return country
    }


    fun verifyOtp(
        name: String,
        mobile: String,
        email: String,
        countryList: CountryList,
        password: String,
        token: String
    ) {
        //object to register user

        jsonObjectReg = JsonObject()
        jsonObjectReg.addProperty("Name", name)
        jsonObjectReg.addProperty("MobileNo", mobile)
        jsonObjectReg.addProperty("EmailId", email)
        jsonObjectReg.addProperty("Password", cryptoUtil.encrypt(password))
        jsonObjectReg.addProperty("accountId", BuildConfig.ACCOUNT_ID)
        jsonObjectReg.addProperty("PhoneType", 1)
        jsonObjectReg.addProperty("phoneToken", token)
        jsonObjectReg.addProperty("Countrycode", countryList.countryCode)
        jsonObjectReg.addProperty("appVersion",BuildConfig.VERSION_CODE)
       // jsonObjectReg.addProperty("PhoneToken", Settings.Secure.ANDROID_ID)
      //  jsonObjectReg.addProperty("UDID", token)
        jsonObjectReg.addProperty("phoneMake", Build.MANUFACTURER)
        jsonObjectReg.addProperty("phoneModel", Build.MODEL)
        jsonObjectReg.addProperty("OS", "${Build.VERSION.CODENAME} (${Build.VERSION.RELEASE})")

        //object to send otp
        jsonObjectOtp = JsonObject()
        jsonObjectOtp.addProperty("Countrycode", countryList.countryCode)
        jsonObjectOtp.addProperty("MobileNo", mobile)
        jsonObjectOtp.addProperty("EmailId", email)
        jsonObjectOtp.addProperty("OTP", generateOtp())
        jsonObjectOtp.addProperty("accountId", BuildConfig.ACCOUNT_ID)


        preferences.setMobile(mobile)
        preferences.setCountry(countryList)
        // switchOtp.value = true


    }

    private fun generateOtp(): String {
        otp = ((Math.random() * 9000).toInt() + 1000).toString()
        return otp
    }


    fun resendCounterStart() {
        resendEnabled.postValue(false)
        countDownTimer?.cancel()
        countDownTimer = object : CountDownTimer(COUNT_DOWN_TIME, ONE_SEC) {
            override fun onTick(millisUntilFinished: Long) {
                Timber.i("seconds remaining: %s", millisUntilFinished / ONE_SEC)
                counter.postValue((millisUntilFinished / ONE_SEC).toInt())
            }

            override fun onFinish() {
                Timber.i("done")
                resendEnabled.postValue(true)
            }
        }
        countDownTimer?.start()
    }

    private fun resendCounterStop() {
        countDownTimer?.cancel()
    }


    fun register() = repository.registerUser(jsonObjectReg)

    fun sendOtp() = repository.sendOtp(jsonObjectOtp)

    fun getToken() {
        viewModelScope.launch {
            val token = appDataRepository.getToken()
            this@RegistrationViewModel.token.value = token ?: ""
        }
    }

    fun login(
        mobile: String,
        password: String,
        countryList: CountryList,
        token: String
    ): LiveData<ApiResult<RegistrationResponse>> {
        preferences.setPassword(cryptoUtil.encrypt(password).nullToEmpty())
        val jsonObject = JsonObject()
        jsonObject.addProperty("Countrycode", countryList.countryCode)
        jsonObject.addProperty("MobileNo", mobile)
        jsonObject.addProperty("Password", cryptoUtil.encrypt(password))
        jsonObject.addProperty("phoneToken", token)
        jsonObject.addProperty("accountId", BuildConfig.ACCOUNT_ID)
        jsonObject.addProperty("PhoneType", 1)
        jsonObject.addProperty("phoneMake", Build.MANUFACTURER)
        jsonObject.addProperty("phoneModel", Build.MODEL)
        jsonObject.addProperty("OS", "${Build.VERSION.CODENAME} (${Build.VERSION.RELEASE})")
        jsonObject.addProperty("appVersion",BuildConfig.VERSION_CODE)
        return repository.login(jsonObject)
    }

    fun loginWithOtp(token: String): LiveData<ApiResult<RegistrationResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("Countrycode", preferences.getCountry().countryCode)
        jsonObject.addProperty("MobileNo", preferences.getMobile())
        jsonObject.addProperty("OTP", generateOtp())
        jsonObject.addProperty("accountId", BuildConfig.ACCOUNT_ID)
        jsonObject.addProperty("phoneToken", token)
        jsonObject.addProperty("PhoneType", 1)
        jsonObject.addProperty("phoneMake", Build.MANUFACTURER)
        jsonObject.addProperty("phoneModel", Build.MODEL)
        jsonObject.addProperty("OS", "${Build.VERSION.CODENAME} (${Build.VERSION.RELEASE})")
        jsonObject.addProperty("appVersion",BuildConfig.VERSION_CODE)
        return repository.loginWithOtp(jsonObject)
    }

    fun setPassword(): LiveData<ApiResult<RegistrationResponse>> {
        val jsonObject = JsonObject()
        preferences.setPassword(cryptoUtil.encrypt(preferences.getPassword().nullToEmpty()).nullToEmpty())
        jsonObject.addProperty("Userid", preferences.getUserId())
        jsonObject.addProperty("Password", preferences.getPassword())
        jsonObject.addProperty("accountId", BuildConfig.ACCOUNT_ID)
        return repository.setPassword(jsonObject)
    }

    fun getAppMenu(): LiveData<ApiResult<AppMenuResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", preferences.getUserId())
        return appDataRepository.getAppMenu(jsonObject)
    }


    override fun onCleared() {
        super.onCleared()
        resendCounterStop()
    }


}