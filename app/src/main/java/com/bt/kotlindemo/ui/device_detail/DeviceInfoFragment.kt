/*
 * *
 *  * Created by Surajit on 23/4/20 8:35 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 21/4/20 5:27 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_detail

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.FragmentDeviceInfoBinding
import com.bt.kotlindemo.ui.base.BaseFragment
import com.bt.kotlindemo.utils.gone
import com.bt.kotlindemo.utils.visible
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.util.*


class DeviceInfoFragment : BaseFragment() {

    private val viewModel: DeviceDetailViewModel by sharedViewModel()
    private lateinit var binding: FragmentDeviceInfoBinding

    companion object {
        fun newInstance() = DeviceInfoFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_device_info, container, false)
        binding.apply {
            viewModel = this@DeviceInfoFragment.viewModel
            lifecycleOwner = this@DeviceInfoFragment
            adapter = DeviceFeaturesAdapter()
        }

        if(viewModel.deviceDetail.value?.odometer==0L){
            binding.tickerView.gone()
        }else{
            binding.tickerView.visible()
            binding.tickerView.typeface =
                Typeface.createFromAsset(activity.assets, "fonts/digital.ttf");
            binding.tickerView.text = "0000${viewModel.deviceDetail.value?.odometer}"
        }

        binding.speedView.setSpeed(0.0, 0, 0)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addObservers()
        addOdometerTimer()
    }

    private fun addOdometerTimer() {

    }

    private fun addObservers() {
        viewModel.deviceDetail.observe(viewLifecycleOwner, Observer {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                if(it.odometer>0){
                    binding.tickerView.text = "0000${it.odometer}"
                }
            }
        })

        viewModel.accessories.observe(viewLifecycleOwner, Observer {
            if (it == null) return@Observer
            binding.adapter!!.features = it
        })

        viewModel.speed.observe(viewLifecycleOwner, Observer {
            if (it == null) return@Observer
            binding.speedView.setSpeed(it / 10)
        })

    }





    override fun onDestroyView() {
        super.onDestroyView()

    }


}



