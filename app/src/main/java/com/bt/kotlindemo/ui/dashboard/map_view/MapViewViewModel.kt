/*
 * *
 *  * Created by Surajit on 5/4/20 11:57 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 5/4/20 11:57 AM
 *  *
 */

package com.bt.kotlindemo.ui.dashboard.map_view

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.data.repository.DeviceRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*
import kotlin.collections.ArrayList

class MapViewViewModel(
    private val deviceRepository: DeviceRepository,
     preference: AppPreferences
) : BaseViewModel() {

     val devices = MutableLiveData<List<Device>>()
    val devicesToShow = MutableLiveData<List<Device>>()
    val groups = MutableLiveData<List<Group>>()
    val selectedGroup = MutableLiveData<Group>()
    private var jsonObject: JsonObject = JsonObject()
    var showMessage = MutableLiveData<String>()
    private val timer = Timer()
    var fetchData = MutableLiveData<Boolean>()

    var moving = MutableLiveData<Boolean>()
    var standing = MutableLiveData<Boolean>()
    var idle = MutableLiveData<Boolean>()
    var offline = MutableLiveData<Boolean>()

    var movingCount = MutableLiveData<Int>()
    var standingCount = MutableLiveData<Int>()
    var idleCount = MutableLiveData<Int>()
    var offlineCount = MutableLiveData<Int>()

    var isSearchOpen = MutableLiveData<Boolean>()
    var isFilterOpen = MutableLiveData<Boolean>()

    var searchText = MutableLiveData<String>()


    init {
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("groupId", 0)
        jsonObject.addProperty("deviceId", 0)
        isSearchOpen.value = false
        searchText.value = ""
        isFilterOpen.value=false


    }

    companion object {
        const val INITIAL_DELAY = 0L
        const val REPEAT_INTERVAL = 20000L
    }

    fun initValues() {
        moving.value = true
        standing.value = true
        idle.value = true
        offline.value = true
        movingCount.value = 0
        idleCount.value = 0
        standingCount.value = 0
        offlineCount.value = 0
    }

    fun search() {
        isSearchOpen.value = !isSearchOpen.value!!
    }


    fun filter() {
        isFilterOpen.value = !isFilterOpen.value!!
    }


    private fun getDevices() =
        deviceRepository.getDeviceList(jsonObject).map { apiResult ->
            when (apiResult.status) {
                ApiResult.Status.LOADING -> {
                    true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = apiResult.message?:""
                    false

                }
                else -> {
                    devices.value = apiResult.data?.devices?:ArrayList()
                    if (groups.value != null) {
                        val grp = apiResult.data?.groups?.toMutableList()
                        grp?.add(0, Group(0, "All Groups", 0))
                        groups.value?.let {groupList->
                            grp?.let {
                                if (!listsEqual(it, groupList)) {
                                    selectedGroup.value = grp[0]
                                    groups.value = it
                                }
                            }

                        }

                    } else {
                        val grp = apiResult.data?.groups?.toMutableList()?:ArrayList()
                        grp.add(0, Group(0, "All Groups", 0))
                        selectedGroup.value = grp[0]
                        groups.value = grp
                    }
                    filterDevices()
                    false

                }
            }


        }

    private fun listsEqual(list1: List<Any>, list2: List<Any>): Boolean {

        if (list1.size != list2.size)
            return false

        val pairList = list1.zip(list2)

        return pairList.all { (elt1, elt2) ->
            elt1 == elt2
        }
    }

    fun filterDevices() {
        var offlineCountTemp = 0
        var movingCountTemp = 0
        var standingCountTemp = 0
        var idleCountTemp = 0
        val status = mutableListOf<String>()
        if (moving.value!!) {
            status.add("MV")
        }
        if (standing.value!!) {
            status.add("ST")
        }
        if (idle.value!!) {
            status.add("ID")
        }
        if (idle.value!!) {
            status.add("OF")
        }
        devicesToShow.value = devices.value!!.filter {
            if (selectedGroup.value!!.id == 0) {
                if(searchText.value.isNullOrEmpty()) {
                    status.contains(it.movingStatusShort)
                }else{
                    status.contains(it.movingStatusShort) && it.deviceName.lowercase(
                        Locale.ROOT
                    ).contains(searchText.value!!.lowercase(Locale.ROOT))
                }
            } else {
                if(searchText.value.isNullOrEmpty()){
                    it.groupid == selectedGroup.value!!.id && status.contains(it.movingStatusShort)
                }else{
                    it.groupid == selectedGroup.value!!.id && status.contains(it.movingStatusShort) && it.deviceName.lowercase(
                        Locale.ROOT
                    ).contains(searchText.value?.lowercase(Locale.ROOT)?:"")
                }

            }
        }
        devicesToShow.value?.forEach {
            if (it.movingStatusShort == "MV") {
                movingCountTemp++
            }
            if (it.movingStatusShort == "ST") {
                standingCountTemp++
            }
            if (it.movingStatusShort == "ID") {
                idleCountTemp++
            }
            if (it.movingStatusShort == "OF") {
                offlineCountTemp++
            }
        }
        movingCount.value = movingCountTemp
        standingCount.value = standingCountTemp
        idleCount.value = idleCountTemp
        offlineCount.value = offlineCountTemp
    }

    fun startTimer() {
        timer.schedule(object : TimerTask() {
            override fun run() {
                viewModelScope.launch {
                    withContext(Dispatchers.Main) {
                        fetchData.value = true
                    }
                }
            }

        }, INITIAL_DELAY, REPEAT_INTERVAL)

    }

    fun getDeviceData() = fetchData.switchMap {
        getDevices()
    }


    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }

}
