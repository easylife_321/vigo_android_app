package com.bt.kotlindemo.ui.sign_in_activity


import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.FragmentOtpBinding
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.ui.base.BaseFragment
import com.bt.kotlindemo.utils.hideKeyboard
import com.bt.kotlindemo.utils.reObserver
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class OtpFragment : BaseFragment() {


    private lateinit var binding: FragmentOtpBinding
    private val viewModel: RegistrationViewModel by sharedViewModel()
    private var whoHasFocus = 0
    private val code = CharArray(4)

    companion object {
        @JvmStatic
        fun newInstance() =
            OtpFragment()
    }

    private fun setViews() {
        binding.etCode1.setOnKeyListener(onKeyListener)
        binding.etCode2.setOnKeyListener(onKeyListener)
        binding.etCode3.setOnKeyListener(onKeyListener)
        binding.etCode4.setOnKeyListener(onKeyListener)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_otp, container, false)
        binding.apply {
            lifecycleOwner = this@OtpFragment
            handler = ClickHandler()
            resendEnabled = false
            mobile = viewModel.preferences.getMobile()
            code = viewModel.preferences.getCountry().countryCode
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
        addObservers()
        if (!viewModel.loginWithOtp)
            sendAndObserverOtp() else viewModel.getToken()

    }

    private fun sendAndObserverLoginWithOtp(token: String) {
        val otp = viewModel.loginWithOtp(token)
        otp.observe(viewLifecycleOwner, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    otp.removeObservers(this)
                    dismissDialog()
                    showToastRetry(
                        binding.root,
                        it.message!!, object : ToastRetry {
                            override fun retry() {
                                sendAndObserverLoginWithOtp(token)
                            }

                        }
                    )


                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    otp.removeObservers(this)
                    viewModel.resendCounterStart()
                }

                else -> {}
            }
        })
    }


    private fun addObservers() {
        observeResend()
    }

    private fun observeResend() {
        viewModel.counter.observe(viewLifecycleOwner, Observer {
            binding.counter = it
        })
        viewModel.resendEnabled.observe(viewLifecycleOwner, Observer {
            binding.resendEnabled = it
        })
        if (viewModel.loginWithOtp) {
            viewModel.token.reObserver(viewLifecycleOwner, Observer {
                if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED)
                    sendAndObserverLoginWithOtp(it)
            })
        }
    }

    private fun sendAndObserverOtp() {
        val otp = viewModel.sendOtp()
        otp.reObserver(viewLifecycleOwner, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showToastRetry(
                        binding.root,
                        it.message!!, object : ToastRetry {
                            override fun retry() {
                                sendAndObserverOtp()
                            }

                        }
                    )
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    viewModel.resendCounterStart()
                }

                else -> {}
            }
        })
    }

    private fun registerAndObserve() {
        viewModel.register().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showSnackBar(binding.root, it.message!!)
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    // redirectToHome()
                    viewModel.fetchMenu.value = true
                }

                else -> {}
            }
        })
    }


    var onKeyListener = View.OnKeyListener { v, keyCode, event ->
        if (event.action == KeyEvent.ACTION_DOWN) {
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                when (v.id) {
                    R.id.etCode2 -> {
                        binding.etCode2.setText("")
                        binding.etCode1.requestFocus()
                    }
                    R.id.etCode3 -> {
                        binding.etCode3.setText("")
                        binding.etCode2.requestFocus()
                    }
                    R.id.etCode4 -> {
                        binding.etCode4.setText("")
                        binding.etCode3.requestFocus()
                    }

                }
            }
        }

        false
    }


    inner class ClickHandler() {
        fun edit() {
            activity?.supportFragmentManager?.popBackStack()
        }

        fun resend() {
            if (!viewModel.loginWithOtp)
                sendAndObserverOtp()
            else
                if (viewModel.token.value.isNullOrEmpty()) viewModel.getToken() else {
                    if (viewModel.loginWithOtp) {
                        sendAndObserverLoginWithOtp(viewModel.token.value!!)
                    } else {
                        registerAndObserve()
                    }
                }
        }

        var onFocusChangeListener =
            OnFocusChangeListener { view, isFocused ->
                when (view.id) {
                    R.id.etCode1 -> if (isFocused) whoHasFocus = 1
                    R.id.etCode2 -> if (isFocused) whoHasFocus = 2
                    R.id.etCode3 -> if (isFocused) whoHasFocus = 3
                    R.id.etCode4 -> if (isFocused) whoHasFocus = 4
                    else -> {
                    }
                }
            }


        fun textChange(text: CharSequence) {
            if (text.length == 1) {
                when (whoHasFocus) {
                    1 -> {
                        code[0] = text[0]
                        binding.etCode2.requestFocus()
                    }
                    2 -> {
                        code[1] = text[0]
                        binding.etCode3.requestFocus()
                    }
                    3 -> {
                        code[2] = text[0]
                        binding.etCode4.requestFocus()
                    }
                    4 -> {
                        code[3] = text[0]
                        hideKeyboard()
                    }

                }
            }
        }


        fun verify() {
            if (viewModel.connectivityUtil.isNetworkAvailable()) {
                if (viewModel.otp != String(code)) {
                    showSnackBar(
                        binding.root,
                        "Please enter valid otp!!"
                    )
                } else {
                    if (viewModel.loginWithOtp) {
                        (context as SignInActivity).changeFragment(ChangePasswordFragment.newInstance())
                    } else {
                        registerAndObserve()
                    }

                }
            } else {
                showSnackBar(
                    binding.root,
                    "No internet connectivity!!"
                )
            }
        }


    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.handler = null


    }


}
