package com.bt.kotlindemo.ui.sign_in_activity


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.FragmentWelcomeBinding
import com.bt.kotlindemo.ui.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class WelcomeFragment : BaseFragment() {


    private lateinit var binding: FragmentWelcomeBinding
    private val viewModel: RegistrationViewModel by sharedViewModel()


    companion object {
        @JvmStatic
        fun newInstance() =
            WelcomeFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_welcome, container, false)
        return binding.root
    }

    private fun setViews() {
        binding.btnSignIn.setOnClickListener {
            (activity as SignInActivity).changeFragment(SignInFragment.newInstance())
        }
        binding.btnSignUp.setOnClickListener {
            (activity as SignInActivity).changeFragment(SignUpFragment.newInstance())
        }
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
    }


}
