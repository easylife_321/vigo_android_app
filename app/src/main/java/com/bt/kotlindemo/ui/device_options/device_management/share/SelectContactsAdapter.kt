/*
 * *
 *  * Created by Surajit on 23/5/20 2:50 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 4:30 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.share

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.data.database.entity.Contacts
import com.bt.kotlindemo.databinding.ListItemDaysBinding
import com.bt.kotlindemo.databinding.ListItemSelectedContactsBinding
import com.bt.kotlindemo.databinding.ListItemSelectedTimeBinding
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.model.Days
import com.bt.kotlindemo.model.SelectedTime
import java.util.*

class SelectContactsAdapter(
) : RecyclerView.Adapter<SelectContactsAdapter.ViewHolder>() {

     var numbers = ArrayList<Contacts>()


    override fun getItemCount(): Int {
        return numbers.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemSelectedContactsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_selected_contacts,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.data = numbers[position]
        holder.binding.layout.setOnClickListener {
            numbers.removeAt(holder.adapterPosition)
            notifyItemRemoved(holder.adapterPosition)
            notifyItemRangeChanged(position, itemCount);

        }

    }

    fun add(number: Any) {
        if (number is String) {
            val contacts = numbers.find {
                it.mobileNo == number
            }
            if (contacts == null) {
                numbers.add(Contacts(phoneName = "+$number",mobileNo = number))
                notifyItemInserted(numbers.size - 1)
            }

        } else {
            val contacts = numbers.find {
                it.mobileNo == (number as Contacts).mobileNo
            }
            if (contacts == null) {
                numbers.add(number as Contacts)
                notifyItemInserted(numbers.size - 1)
            }
        }

    }


    inner class ViewHolder(
        val binding: ListItemSelectedContactsBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }


}

