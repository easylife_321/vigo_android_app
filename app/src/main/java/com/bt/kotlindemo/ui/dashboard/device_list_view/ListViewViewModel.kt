/*
 * *
 *  * Created by Surajit on 5/4/20 11:57 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 5/4/20 11:57 AM
 *  *
 */

package com.bt.kotlindemo.ui.dashboard.device_list_view

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.data.repository.DeviceRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.ui.dashboard.map_view.MapViewViewModel.Companion.INITIAL_DELAY
import com.bt.kotlindemo.ui.dashboard.map_view.MapViewViewModel.Companion.REPEAT_INTERVAL
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.Timer
import java.util.TimerTask

class ListViewViewModel(
    private val deviceRepository: DeviceRepository,
    private val preference: AppPreferences
) : BaseViewModel() {

    val devices = MutableLiveData<List<Device>>()
    val devicesToShow = MutableLiveData<List<Device>>()
    val groups = MutableLiveData<ArrayList<Group>>()
    val selectedGroup = MutableLiveData<Group>()
    private var jsonObject: JsonObject = JsonObject()
    var showMessage = MutableLiveData<String>()
    var fetchData = MutableLiveData<Boolean>()

    var moving = MutableLiveData<Boolean>()
    var standing = MutableLiveData<Boolean>()
    var idle = MutableLiveData<Boolean>()
    var offline = MutableLiveData<Boolean>()

    var movingCount = MutableLiveData<Int>()
    var standingCount = MutableLiveData<Int>()
    var idleCount = MutableLiveData<Int>()
    var offlineCount = MutableLiveData<Int>()
    private val timer = Timer()

    var searchText = MutableLiveData<String>()

    init {
        initValues()
        viewModelScope.launch(Dispatchers.IO) {
          //  devices.postValue(deviceRepository.getDevices())
            withContext(Dispatchers.Main) {
                filterDevices()
            }

        }

    }


    fun initValues() {
        moving.value = true
        standing.value = true
        idle.value = true
        offline.value = true
        movingCount.value = 0
        idleCount.value = 0
        standingCount.value = 0
        offlineCount.value = 0
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("groupId", 0)
        jsonObject.addProperty("deviceId", 0)
    }


    fun filterDevices() {
        var offlineCountTemp = 0
        var movingCountTemp = 0
        var standingCountTemp = 0
        var idleCountTemp = 0
        val status = mutableListOf<String>()
        if (moving.value!!) {
            status.add("MV")
        }
        if (standing.value!!) {
            status.add("ST")
        }
        if (idle.value!!) {
            status.add("ID")
        }
        if (idle.value!!) {
            status.add("OF")
        }
        val list = devices.value?.filter {
            if (selectedGroup.value!!.id == 0) {
                //  if (searchText.value.isNullOrEmpty()) {
                status.contains(it.movingStatusShort)
                //  } else {
                //     status.contains(it.movingStatusShort) && it.deviceName.toLowerCase(
                //         Locale.ROOT
                //     ).contains(searchText.value!!.toLowerCase(Locale.ROOT))
                // }
            } else {
                // if (searchText.value.isNullOrEmpty()) {
                it.groupid == selectedGroup.value!!.id && status.contains(it.movingStatusShort)
                // } else {
                //    it.groupid == selectedGroup.value!!.id && status.contains(it.movingStatusShort) && it.deviceName.toLowerCase(
                //        Locale.ROOT
                //   ).contains(searchText.value!!.toLowerCase(Locale.ROOT))
                // }

            }
        }
        devicesToShow.value = list ?: emptyList()
        devicesToShow.value?.forEach {
            if (it.movingStatusShort == "MV") {
                movingCountTemp++
            }
            if (it.movingStatusShort == "ST") {
                standingCountTemp++
            }
            if (it.movingStatusShort == "ID") {
                idleCountTemp++
            }
            if (it.movingStatusShort == "OF") {
                offlineCountTemp++
            }
        }
        movingCount.value = movingCountTemp
        standingCount.value = standingCountTemp
        idleCount.value = idleCountTemp
        offlineCount.value = offlineCountTemp
    }

    fun startTimer() {
        timer.schedule(object : TimerTask() {
            override fun run() {
                viewModelScope.launch {
                    withContext(Dispatchers.Main) {
                        fetchData.value = true
                    }
                }
            }

        }, INITIAL_DELAY, REPEAT_INTERVAL)

    }

    private fun getDevices() =
        deviceRepository.getDeviceList(jsonObject).map { apiResult ->
            when (apiResult.status) {
                ApiResult.Status.LOADING -> {
                    true
                }

                ApiResult.Status.ERROR -> {
                    showMessage.value = apiResult.message ?: ""
                    false

                }

                else -> {
                    devices.value = apiResult.data?.devices ?: ArrayList()
                    filterDevices()
                    false

                }
            }


        }

    fun getDeviceData() = fetchData.switchMap {
        getDevices()
    }


    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }


}
