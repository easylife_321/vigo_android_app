package com.bt.kotlindemo.ui.base

import androidx.annotation.CallSuper
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bt.kotlindemo.utils.SingleLiveEvent
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    private val disposables = CompositeDisposable()
    var onBackPressed = MutableLiveData<Boolean>()
    var requestPermission = SingleLiveEvent<Void?>()
    fun launch(job: () -> Disposable) {
        disposables.add(job())
    }

    fun back() {
        onBackPressed.value = true
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }
}