/*
 * *
 *  * Created by Surajit on 25/4/20 12:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/4/20 6:18 PM
 *  *
 */

package com.bt.kotlindemo.ui.notifications

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.constants.NotificationType
import com.bt.kotlindemo.data.database.entity.Notification
import com.bt.kotlindemo.data.repository.NotificationRepository
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.DateTimeUtil
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.bt.kotlindemo.utils.toLocalTime
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NotificationViewModel(
    private var repository: NotificationRepository,
) : BaseViewModel() {


    var isLoaded = ObservableBoolean()
    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var showToast = MutableLiveData<String>()
    var rawNotificationList = MutableLiveData<List<Notification>>()
    var notifications = MutableLiveData<List<Notification>>()
    var notificationType = MutableLiveData<List<String>>()
    var selectedNotificationType = MutableLiveData<String?>()
    var select = SingleLiveEvent<Void?>()
    var deleteConfirmation = SingleLiveEvent<Void?>()
    private var distinctType = mutableSetOf<String>()


    fun getNotifications(id: Int) =
        if (id == 0) repository.getNotificationsFromDb() else repository.getNotificationsFromDb(id)


    fun filterData(setType: Boolean = false) {
        var date = ""
        val list = arrayListOf<Notification>()
        distinctType.clear()
        distinctType.add(NotificationType.ALL)
        rawNotificationList.value?.onEach {
            distinctType.add(it.type)
            if (selectedNotificationType.value == NotificationType.ALL || selectedNotificationType.value == it.type) {
                val temp = DateTimeUtil.getDate(it.timeStamp.toLocalTime())
                if (date == temp) {
                    list.add(it)
                } else {
                    date = temp
                    list.add(Notification(isHeader = true, headerText = date))
                    list.add(it)
                }
            }
        }

        this@NotificationViewModel.notifications.value = list
        if (setType) {
            notificationType.value = distinctType.toList()
            selectedNotificationType.value = notificationType.value?.get(0)
        }
    }


    fun select() {
        select.call()
    }


    fun deleteConfirmation() {
        deleteConfirmation.call()
    }


    fun delete(deviceId: Int = 0) {
        viewModelScope.launch(Dispatchers.IO) {
            if (deviceId == 0 && selectedNotificationType.value == NotificationType.ALL) {
                repository.deleteAllNotification()
            } else if (deviceId == 0 && selectedNotificationType.value != NotificationType.ALL) {
                repository.deleteAllTypeNotification(selectedNotificationType.value ?: "")
            } else if (deviceId != 0 && selectedNotificationType.value == NotificationType.ALL) {
                repository.deleteAllDeviceNotification(deviceId)
            } else {
                repository.deleteSelectedTypeNotification(
                    selectedNotificationType.value ?: "",
                    deviceId
                )
            }


        }
    }


}