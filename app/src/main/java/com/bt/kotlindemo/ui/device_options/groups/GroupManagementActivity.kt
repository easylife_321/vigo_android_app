/*
 * *
 *  * Created by Surajit on 12/4/20 10:50 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 12/4/20 10:50 AM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.groups

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.databinding.ActivityGroupManagementBinding
import com.bt.kotlindemo.databinding.FragmentGroupOptionsBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.device_options.notification_settings.NotificationSettingsActivity
import com.bt.kotlindemo.utils.ActivityUtils
import com.bt.kotlindemo.utils.AlertHelper
import com.ferfalk.simplesearchview.SimpleSearchView
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.koin.androidx.viewmodel.ext.android.viewModel


class GroupManagementActivity : BaseActivity() {

    private lateinit var binding: ActivityGroupManagementBinding
    private val viewModel: GroupManagementActivityViewModel by viewModel()
    private var groups: ArrayList<Group>? = null
    private var selectedGroup: Group? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            groups = savedInstanceState.getSerializable(IntentConstants.DATA) as ArrayList<Group>?
        }
        super.onCreate(savedInstanceState)
        setStatusBarColor(R.color.colorPrimary)

    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_group_management)
        binding.apply {
            lifecycleOwner = this@GroupManagementActivity
            handler = ClickHandler()
        }
    }

    override fun initViews() {
        setSupportActionBar(binding.toolbar)
        setSearchView()
        if (groups != null) {
            setUpList()
        } else {
            fetchGroup()

        }
        addObservers()
    }

    private fun addObservers() {
        viewModel.delete.observe(this, Observer {
            if (selectedGroup?.deviceCount ?: 0 > 0) {
                AlertHelper.showOkAlert(
                    this@GroupManagementActivity,
                    resources.getString(R.string.group_delete_error)
                )
            } else {
                AlertHelper.showOKCancelAlert(
                    this@GroupManagementActivity,
                    resources.getString(R.string.are_you_sure),
                    object : AlertCallback {
                        override fun action(isOk: Boolean, data: Any?) {
                            if (isOk) deleteGroup(selectedGroup?.id ?: 0)
                        }

                    }
                )

            }
        })

        viewModel.edit.observe(this, Observer {
            ActivityUtils.replaceFragmentWithAnimFromBottom(
                supportFragmentManager,
                binding.container.id,
                AddGroupFragment.newInstance(selectedGroup),
                AddGroupFragment::class.java.simpleName
            )
        })

        viewModel.notification.observe(this, Observer {
            val intent = Intent(
                this@GroupManagementActivity,
                NotificationSettingsActivity::class.java
            )
            intent.putExtra(IntentConstants.DATA, selectedGroup?.id)
            startActivity(intent)
        })


        viewModel.share.observe(this, Observer {

        })


        viewModel.create.observe(this, Observer {
            createGroup(it)
        })

        viewModel.update.observe(this, Observer {
            updateGroup(it, selectedGroup?.id ?: 0)
        })

    }

    private fun setSearchView() {
        binding.searchView.setOnSearchViewListener(object : SimpleSearchView.SearchViewListener {
            override fun onSearchViewShownAnimation() {
            }

            override fun onSearchViewClosed() {
                if (binding.adapter != null) {
                    binding.adapter!!.filter.filter("")
                }
                binding.toolbar.visibility = View.VISIBLE
                binding.searchView.visibility = View.INVISIBLE
            }

            override fun onSearchViewClosedAnimation() {

            }

            override fun onSearchViewShown() {

            }

        })

        binding.searchView.setOnQueryTextListener(object : SimpleSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (binding.adapter != null) {
                    binding.adapter!!.filter.filter(newText)
                }
                return false
            }

            override fun onQueryTextCleared(): Boolean {
                return false
            }
        })

    }


    private fun fetchGroup() {
        viewModel.getGroups().observe(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showToastRetry(
                        binding.root,
                        it.message!!,
                        retry = object : ToastRetry {
                            override fun retry() {
                                fetchGroup()
                            }
                        }
                    )
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    groups = it.data?.groups as ArrayList<Group>
                    setUpList()
                }

                else -> {}
            }
        })
    }

    private fun setUpList() {
        groups?.let {
            binding.adapter =
                GroupListAdapter(it, object : GroupRecyclerClickListener {
                    override fun onClick(data: Any, action: Int) {
                        selectedGroup = data as Group
                        showOptions()
                    }
                })

        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(IntentConstants.DATA, groups)
    }


    private fun deleteGroup(id: Int) {
        viewModel.deleteGroup(id).observe(this, Observer {
            if (it != null) {
                AlertHelper.showOkAlert(
                    this@GroupManagementActivity,
                    it,
                    object : AlertCallback {
                        override fun action(isOk: Boolean, data: Any?) {
                            fetchGroup()
                        }
                    })
            }
        })
    }


    private fun createGroup(name: String) {
        viewModel.createGroup(name).observe(this, Observer {
            if (it != null) {
                AlertHelper.showOkAlert(
                    this@GroupManagementActivity,
                    it,
                    object : AlertCallback {
                        override fun action(isOk: Boolean, data: Any?) {
                            fetchGroup()
                        }
                    })
            }
        })
    }


    private fun updateGroup(name: String, id: Int) {
        viewModel.updateGroup(name, id).observe(this, Observer {
            if (it != null) {
                AlertHelper.showOkAlert(
                    this@GroupManagementActivity,
                    it,
                    object : AlertCallback {
                        override fun action(isOk: Boolean, data: Any?) {
                            fetchGroup()
                        }
                    })
            }
        })
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            slideToRight()
        }
    }

    inner class ClickHandler {

        fun addGroup() {
            ActivityUtils.replaceFragmentWithAnimFromBottom(
                supportFragmentManager,
                binding.container.id,
                AddGroupFragment.newInstance(null),
                AddGroupFragment::class.java.simpleName
            )
        }

        fun back() {
            finish()
        }

        fun openSearch() {
            binding.toolbar.visibility = View.INVISIBLE
            binding.searchView.visibility = View.VISIBLE
            binding.searchView.showSearch(true)
        }
    }


    private fun showOptions() {
        try {
            val binding: FragmentGroupOptionsBinding = DataBindingUtil.inflate(
                layoutInflater,
                R.layout.fragment_group_options,
                null,
                false
            )
            val mBottomDialogNotificationAction =
                BottomSheetDialog(this, R.style.bottomSheetDialogTheme)
            mBottomDialogNotificationAction.setContentView(binding.root)
            mBottomDialogNotificationAction.show()
            binding.imgShare.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.share.call()
            }
            binding.imgEdit.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.edit.call()
            }

            binding.imgDelete.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.delete.call()
            }

            binding.imgNoti.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.notification.call()
            }

            /*  sheetView.layout.setOnClickListener {
                  mBottomDialogNotificationAction.dismiss()
              }*/
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}
