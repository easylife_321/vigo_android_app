/*
 * *
 *  * Created by Surajit on 23/5/20 2:50 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 4:30 PM
 *  *
 */

package com.bt.kotlindemo.ui.notifications

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.data.database.entity.Notification
import com.bt.kotlindemo.databinding.ListItemDeviceManagermntBinding
import com.bt.kotlindemo.databinding.ListItemHeaderBinding
import com.bt.kotlindemo.databinding.ListItemNotificationBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import java.util.*

class NotificationAdapter(
    private val notifications: List<Notification>,
    private val mListener: RecyclerClickListener? = null
) : RecyclerView.Adapter<NotificationAdapter.BaseViewHolder>() {


    override fun getItemCount(): Int {
        return notifications.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        if (viewType == 0) {
            return HeaderViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.list_item_header,
                    parent,
                    false
                )
            )

        } else {
            return ViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.list_item_notification,
                    parent,
                    false
                )
            )
        }

    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
            if(holder is HeaderViewHolder){
                    holder.binding.data=notifications[position].headerText
            }else if(holder is ViewHolder){
                holder.binding.data=notifications[position]
            }

    }


    override fun getItemViewType(position: Int): Int {
        return if (notifications[position].isHeader) 0 else 1
    }

    inner class HeaderViewHolder(
        val binding: ListItemHeaderBinding
    ) : BaseViewHolder(binding.root)


    inner class ViewHolder(
        val binding: ListItemNotificationBinding
    ) : BaseViewHolder(binding.root)


    abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view)


}

