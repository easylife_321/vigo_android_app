/*
 * *
 *  * Created by Surajit on 25/4/20 12:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/4/20 6:18 PM
 *  *
 */

package com.bt.kotlindemo.ui.profile.change_mobile_number

import android.app.Application
import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.data.repository.SettingsRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.ui.sign_in_activity.COUNT_DOWN_TIME
import com.bt.kotlindemo.ui.sign_in_activity.ONE_SEC
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.CryptoUtil
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.google.gson.JsonObject
import timber.log.Timber

class ChangeMobileNumberViewModel(
    val preference: AppPreferences,
    private var repository: SettingsRepository,
    private val appContext: Application,
    private val cryptoUtil: CryptoUtil
) : BaseViewModel() {


    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var showMessageFinish = MutableLiveData<String>()
    var number = MutableLiveData<String>()
    val code = CharArray(4)
    val send = SingleLiveEvent<Void?>()
    val isSent = MutableLiveData<Boolean>().apply { value = false }
    lateinit var otp: String
    var counter = MutableLiveData<Int>()
    var resendEnabled = MutableLiveData<Boolean>()
    private var countDownTimer: CountDownTimer? = null
    var inputCode = MutableLiveData<CharSequence>()
    var resend = MutableLiveData<Boolean>()
    var selectCountry = SingleLiveEvent<Void?>()
    var country = MutableLiveData<CountryList>()
    var password = MutableLiveData<String>()

    init {
        resendEnabled.value = false
        country.value = preference.getCountry()
    }

    fun sendOtp(pwd: String): LiveData<Unit> {
        password.value=pwd
        val jsonObject = JsonObject()
        jsonObject.addProperty("Userid", preference.getUserId())
        jsonObject.addProperty("Newmobileno", number.value)
        jsonObject.addProperty("CountryCode", country.value?.countryCode)
        jsonObject.addProperty("OTP", generateOtp())
        jsonObject.addProperty("Password",cryptoUtil.encrypt( password.value?.trim()?:""))
        jsonObject.addProperty("accountId",BuildConfig.ACCOUNT_ID)
        return repository.sendOtpNumberChange(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message?:""
                    isLoading.value = false
                }
                else -> {
                    if (it.data == null) {
                        isLoading.value = false
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"

                    } else {
                        isLoading.value = false
                        resendCounterStart()
                        resendEnabled.value = true
                        isSent.value = true

                    }
                }
            }


        }
    }

    fun changeAccountMobileNo(): LiveData<Unit> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("Userid", preference.getUserId())
        jsonObject.addProperty("Newmobileno",number.value)
        jsonObject.addProperty("CountryCode", country.value?.countryCode)
        jsonObject.addProperty("OTP", generateOtp())
        jsonObject.addProperty("accountId",BuildConfig.ACCOUNT_ID)
        return repository.changeAccountMobileNo(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message?:""
                    isLoading.value = false
                }
                else -> {
                    if (it.data == null) {
                        isLoading.value = false
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"

                    } else {
                        isLoading.value = false
                        preference.setMobile(number.value ?: "")
                        preference.setCountry(country.value!!)
                        showMessageFinish.value = it.data.result.msg

                    }
                }
            }


        }
    }


    private fun generateOtp(): String {
        otp = ((Math.random() * 9000).toInt() + 1000).toString()
        return otp
    }


    private fun resendCounterStart() {
        resendEnabled.postValue(false)
        countDownTimer?.cancel()
        countDownTimer = object : CountDownTimer(COUNT_DOWN_TIME, ONE_SEC) {
            override fun onTick(millisUntilFinished: Long) {
                Timber.i("seconds remaining: %s", millisUntilFinished / ONE_SEC)
                counter.postValue((millisUntilFinished / ONE_SEC).toInt())
            }

            override fun onFinish() {
                Timber.i("done")
                resendEnabled.postValue(true)
            }
        }
        countDownTimer?.start()
    }

    fun textChange(text: CharSequence) {
        if (text.length == 1) {
            inputCode.value = text
        }
    }

    fun send() {
        send.call()
    }


    fun resend() {
        resend.value = true
    }

    fun selectCountry() {
        selectCountry.call()
    }

}