package com.bt.kotlindemo.ui.splash_activity

import android.content.Intent
import android.os.Bundle
import androidx.core.splashscreen.SplashScreen
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.R
import com.bt.kotlindemo.data.network.netwrok_responses.BaseUrlResponse
import com.bt.kotlindemo.databinding.ActivitySplashBinding
import com.bt.kotlindemo.di.selectCountryModule
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.dashboard.DashboardActivity
import com.bt.kotlindemo.ui.sign_in_activity.RegistrationViewModel
import com.bt.kotlindemo.ui.sign_in_activity.SignInActivity
import com.bt.kotlindemo.utils.WorkHelper
import com.bt.kotlindemo.utils.nullToEmpty
import com.bt.kotlindemo.utils.reObserver
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules

class SplashActivity : BaseActivity() {

    private lateinit var binding: ActivitySplashBinding
    private val viewModel: SplashActivityViewModel by viewModel()
    private lateinit var splashScreen: SplashScreen
    private val userViewModel: RegistrationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        loadKoinModules(selectCountryModule)
        // splashScreen = installSplashScreen()
        //splashScreen.setKeepOnScreenCondition { true }
        transparentStatusAndNavigation()
        super.onCreate(savedInstanceState)

    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

    }


    override fun initViews() {
        getBaseAndObserve()
        viewModel.token.observe(this) {
            login(token = it)
        }
    }


    private fun getBaseAndObserve() {
        val baseUrl = viewModel.getBaseUrl()
        baseUrl.observe(this, Observer {
            when (it.status) {
                ApiResult.Status.ERROR -> {
                    baseUrl.removeObservers(this)
                    showToastRetry(
                        binding.root,
                        it.message!!,
                        retry = object : ToastRetry {
                            override fun retry() {
                                getBaseAndObserve()
                            }
                        }
                    )
                }

                ApiResult.Status.SUCCESS -> {
                    baseUrl.removeObservers(this)
                    if (checkForUpdate(it.data)) {
                        return@Observer
                    }
                    if (viewModel.isRegistered()) {
                        WorkHelper.startWork()
                        viewModel.getToken()
                    } else {
                        startActivity(Intent(this, SignInActivity::class.java))
                        finish()
                    }

                }

                ApiResult.Status.LOADING -> {
                }

                else -> {}
            }
        })
    }

    private fun getMenu() {
        val appMenu = viewModel.getAppMenu()
        appMenu.reObserver(this, Observer {
            when (it.status) {
                ApiResult.Status.ERROR -> {

                    showToastRetry(
                        binding.root,
                        it.message!!,
                        retry = object : ToastRetry {
                            override fun retry() {
                                getBaseAndObserve()
                            }
                        }
                    )
                }

                ApiResult.Status.SUCCESS -> {
                    //App.instance.menu = it.data?.menu!!
                    it.data?.let { it1 -> viewModel.preference.setDnd(it1.isDndOn) }
                    finish()
                    startActivity(Intent(this, DashboardActivity::class.java))
                }

                ApiResult.Status.LOADING -> {

                }

                else -> {}
            }
        })


    }

    private fun checkForUpdate(data: BaseUrlResponse?): Boolean {
        if (data?.isUnderMaintenance!!) {
            //show maintaninece screen
            return true
        } else {
            return if (data.androidVer > BuildConfig.VERSION_CODE) {
                if (data.isMandatoryUpdate) {
                    //show update screen
                    true
                } else {
                    viewModel.preference.setUpdateAvl(true)
                    false
                }
            } else {
                viewModel.preference.setUpdateAvl(false)
                false
            }
        }
    }

    private fun login(token: String) {
        val login = userViewModel.login(
            userViewModel.preferences.getMobile().nullToEmpty(),
            userViewModel.cryptoUtil.decrypt(userViewModel.preferences.getPassword().nullToEmpty())
                .nullToEmpty(),
            userViewModel.preferences.getCountry(),
            token
        )
        login.reObserver(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> {}
                ApiResult.Status.ERROR -> {
                   showSnackBar(binding.root,it.message?:getString(R.string.server_error))
                }
                ApiResult.Status.CUSTOM -> {
                    startActivity(Intent(this, SignInActivity::class.java))
                    finish()
                }
                ApiResult.Status.SUCCESS -> {
                    getMenu()
                }

            }
        })
    }
    override fun onDestroy() {
        super.onDestroy()
        unloadKoinModules(selectCountryModule)

    }

}
