/*
 * *
 *  * Created by Surajit on 9/4/20 11:57 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 11:56 AM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.add_device

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.ActivityAddDevciceNewBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.DeviceIcon
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.reObserver
import org.koin.androidx.viewmodel.ext.android.viewModel

class AddDeviceActivity : BaseActivity() {

    private val viewModel: AddDeviceActivityViewModel by viewModel()
    private var groupList: ArrayList<Group>? = null
    private var device: Device? = null
    private var selectedGroup: Group? = null
    private var icon: DeviceIcon? = null;
    private lateinit var binding: ActivityAddDevciceNewBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        setStatusBarColor(R.color.colorPrimaryDark)
        setStatusBarTint(false)
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_devcice_new)
        binding.apply {
            lifecycleOwner = this@AddDeviceActivity
            viewModel = this@AddDeviceActivity.viewModel
            handler = ClickHandler()
        }
        setSupportActionBar(binding.toolbar)

    }

    @Suppress("UNCHECKED_CAST")
    override fun initViews() {
        groupList = intent.getSerializableExtra(IntentConstants.GROUP) as? ArrayList<Group>
        if (intent.getBooleanExtra(IntentConstants.EDIT, false)) {
            device = intent.getSerializableExtra(IntentConstants.DEVICE) as? Device
            selectedGroup = groupList?.find {
                it.id == device?.groupid
            }
            icon = viewModel.getIcons().find {
                it.id == device?.deviceIcon
            }
        }

        viewModel.setData(
            intent.getBooleanExtra(IntentConstants.EDIT, false),
            device,
            selectedGroup,
            icon
        )

        if (groupList == null) {
            fetchGroup()
        } else {
            bindGroups()
        }
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            slideToRight()
        }
    }

    private fun fetchGroup() {
        viewModel.getGroups().observe(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    viewModel.getGroups().removeObservers(this)
                    showToastRetry(
                        binding.root,
                        it.message!!,
                        retry = object : ToastRetry {
                            override fun retry() {
                                fetchGroup()
                            }
                        }
                    )
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    groupList = it.data?.groups as ArrayList
                    if (device != null && intent.getBooleanExtra(IntentConstants.EDIT, false)) {
                        selectedGroup = groupList?.find {
                            it.id == device?.groupid
                        }
                        viewModel.groupName.set(selectedGroup?.name)
                    }
                    bindGroups()
                }

                else -> {}
            }
        })
    }

    private fun bindGroups() {
        if (groupList.isNullOrEmpty()) {
            AlertHelper.showOKCancelAlert(
                this@AddDeviceActivity,
                "You don't have any group. To add devices you need to have at-least one group. To create a group please click OK button.",
                object : AlertCallback {
                    override fun action(isOk: Boolean, data: Any?) {
                        if (isOk) {
                            showCreateAlert()
                        } else {
                            finish()
                        }
                    }

                }
            )
        }
    }

    private fun showCreateAlert() {
        AlertHelper.showCreateGroupAlert(
            context = this@AddDeviceActivity,
            callBack = object : AlertCallback {
                override fun action(isOk: Boolean, data: Any?) {
                    if (isOk) {

                    } else {
                        finish()
                    }
                }

            }
        )
    }

    private fun saveAndObserve() {
        viewModel.addDevice(
            binding.etDeviceName.text.toString().trim(),
            binding.etNickName.text.toString().trim(),
            binding.etImei.text.toString().trim(),
            selectedGroup?.id!!,
            icon?.id!!,
            binding.etDriverName.text.toString().trim(),
            binding.etDriverMobile.text.toString().trim(),
            device?.deviceId
        ).reObserver(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showSnackBar(
                        binding.root,
                        it.message!!
                    )
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(this@AddDeviceActivity, it.data?.result?.msg!!,
                        object : AlertCallback {
                            override fun action(isOk: Boolean, data: Any?) {
                                if (viewModel.isEdit.get()) {
                                    device?.groupid = selectedGroup?.id!!
                                    device?.deviceIcon = icon?.icon!!
                                    device?.deviceName = binding.etDeviceName.text.toString().trim()
                                    device?.driverName = binding.etDriverName.text.toString().trim()
                                    device?.driverNumber =
                                        binding.etDriverMobile.text.toString().trim()
                                    device?.deviceNickName =
                                        binding.etNickName.text.toString().trim()
                                    val intent = Intent()
                                    intent.putExtra(IntentConstants.DEVICE, device)
                                    setResult(Activity.RESULT_OK, intent)
                                    finish()
                                } else {
                                    setResult(Activity.RESULT_OK)
                                    finish()
                                }

                            }

                        })
                }

                else -> {}
            }
        })
    }


    inner class ClickHandler {

        fun save() {
            if (selectedGroup == null) {
                showSnackBar(binding.root, "Select group")
                return
            }
            if (binding.etDeviceName.text.toString().isBlank()) {
                binding.etDeviceName.error = "Enter Device Name"
                return
            }
            if (binding.etImei.text.toString().isBlank()) {
                binding.etImei.error = "Enter Device IMEI"
                return
            }
            if (icon == null) {
                showSnackBar(binding.root, "Select device icon")
                return
            }

            if (!binding.etDriverMobile.text.toString()
                    .isBlank() && binding.etDriverMobile.text.toString().trim().length < 7
            ) {
                binding.etDriverMobile.error = "Enter valid mobile no."
                return
            }
            saveAndObserve()

        }

        fun openDialog() {
            AlertHelper.showGroupListAlert(
                this@AddDeviceActivity,
                groupList!!,
                object : AlertCallback {
                    override fun action(isOk: Boolean, data: Any?) {
                        selectedGroup = data as Group
                        binding.txtGroupNameText.text = selectedGroup?.name
                    }

                })
        }


        fun selectIcon() {
            AlertHelper.showIconListAlert(
                this@AddDeviceActivity,
                viewModel.getIcons(),
                object : AlertCallback {
                    override fun action(isOk: Boolean, data: Any?) {
                        icon = (data as DeviceIcon)
                        binding.txtDeviceIconText.setText(icon?.text)
                    }

                })
        }

        fun back() {
            finish()
        }
    }


}
