/*
 * *
 *  * Created by Surajit on 14/4/20 6:33 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 13/4/20 7:10 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.notification_settings

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.databinding.ActivityNotificationSettingsBinding
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.NotificationSettings
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AlertHelper
import org.koin.androidx.viewmodel.ext.android.viewModel

class NotificationSettingsActivity : BaseActivity() {

    private lateinit var binding: ActivityNotificationSettingsBinding
    private val viewModel: NotificationSettingsActivityViewModel by viewModel()
    private var notificationList: ArrayList<NotificationSettings>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStatusBarColor(R.color.colorPrimary)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification_settings);
        binding.handler = ClickHandler()

        setSupportActionBar(binding.toolbar)
    }

    override fun initViews() {
        fetchSettings()
    }


    private fun fetchSettings() {
        viewModel.getNotificationSettings(intent.getIntExtra(IntentConstants.DATA, 0))
            .observe(this, Observer {
                when (it.status) {
                    ApiResult.Status.LOADING -> showDialog()
                    ApiResult.Status.ERROR -> {
                        dismissDialog()
                        showToastRetry(
                            binding.root,
                            it.message!!,
                            retry = object : ToastRetry {
                                override fun retry() {
                                    fetchSettings()
                                }
                            }
                        )
                    }
                    ApiResult.Status.SUCCESS -> {
                        dismissDialog()
                        notificationList = it.data?.ntf as ArrayList<NotificationSettings>?
                        setUpList()
                    }

                    else -> {}
                }
            })
    }

    private fun save() {
        viewModel.saveNotificationSettings(
            notificationList!!,
            intent.getIntExtra(IntentConstants.DATA, 0)
        ).observe(this@NotificationSettingsActivity, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showSnackBar(
                        binding.root,
                        it.message!!
                    )
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(
                        this@NotificationSettingsActivity,
                        it.data?.result?.msg!!
                    )
                }
                else -> {}
            }
        })
    }


    fun show() {
        binding.show = true
        binding.isALL = viewModel.checkAll(notificationList!!)
    }

    fun showSave() {
        binding.show = true
    }

    private fun setUpList() {
        notificationList?.let {
            binding.showViews = false
            binding.adapter = NotificationSettingsAdapter(it)
            binding.isALL = viewModel.checkAll(notificationList!!)
        }
    }

    inner class ClickHandler {


        fun saveSettings() {
            save()
        }

        fun back() {
            finish()
        }

        fun applyToALL() {
            binding.show = true
            if (binding.isALL!!) {
                viewModel.selectAll(false, notificationList)
            } else {
                viewModel.selectAll(true, notificationList)
            }
            binding.adapter!!.notifyDataSetChanged()
            binding.isALL = viewModel.checkAll(notificationList!!)
        }

    }


}
