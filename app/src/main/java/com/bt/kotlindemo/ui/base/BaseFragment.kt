package com.bt.kotlindemo.ui.base


import android.content.Context
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bt.kotlindemo.R
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.utils.ProgressHelper
import com.google.android.material.snackbar.Snackbar
import io.github.rupinderjeet.kprogresshud.KProgressHUD


abstract class
BaseFragment : Fragment() {

    lateinit var activity: AppCompatActivity
    private var progress: KProgressHUD? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as AppCompatActivity
    }

    protected fun showDialog() {
        if (progress == null)
            progress = ProgressHelper.showProgress(activity)
        if (!progress!!.isShowing)
            progress?.show()
    }

    protected fun dismissDialog() {
        ProgressHelper.dismissProgress(progress)
    }

    protected fun showSnackBar(view: View, msg: String) {
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
            .setBackgroundTint(ContextCompat.getColor(activity, R.color.colorPrimary)).show()
    }

    protected fun showToastRetry(view: View, msg: String, retry: ToastRetry) {
        Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
            .setAction("Retry") {
                retry.retry()
            }
            .setBackgroundTint(ContextCompat.getColor(view.context, R.color.colorPrimary)).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
       /* if (snackbar != null)
            snackbar?.dismissNow()*/
    }

}
