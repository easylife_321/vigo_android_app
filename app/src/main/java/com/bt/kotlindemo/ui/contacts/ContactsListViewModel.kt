/*
 * *
 *  * Created by Surajit on 25/4/20 12:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/4/20 6:18 PM
 *  *
 */

package com.bt.kotlindemo.ui.contacts

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.preference.PreferenceManager
import com.bt.kotlindemo.constants.OnBoardingConstants
import com.bt.kotlindemo.data.repository.ContactRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Contact
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.ContactUtil
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.launch
import timber.log.Timber

class ContactsListViewModel(
    private val preference: AppPreferences,
    private var repository: ContactRepository,
    private val appContext: Application
) : BaseViewModel() {


    var isLoaded = ObservableBoolean()
    var address = ObservableField<String>()
    var deviceDetail = MutableLiveData<Device>()
    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var showToast = MutableLiveData<String>()
    var contacts = MutableLiveData<List<Contact>>()
    var isContactPermissionAllowed = MutableLiveData<Boolean>()
    var isContactPermissionAllowedAnim = MutableLiveData<Boolean>().apply { value = true }
    var syncContacts = SingleLiveEvent<Void?>()
    var select = SingleLiveEvent<Void?>()


    private fun sync(contacts: List<Contact>): LiveData<Unit> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", preference.getUserId())
        val arr = JsonArray()
        contacts.forEach {
            val obj = JsonObject()
            obj.addProperty("mobileNo", it.contactNumber)
            obj.addProperty("fullName", it.contactName)
            arr.add(obj)
        }
        jsonObject.add("contacts", arr)
        return repository.syncContact(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message?:""
                    isLoading.value = false
                }
                else -> {
                    if (it.data == null) {
                        isLoading.value = false
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"

                    } else {
                        if (it?.data.friends == null) {
                            showMessage.value =
                                "(Error code:101)We encountered a small glitch. If the problem please contact support"

                        } else {
                            PreferenceManager.getDefaultSharedPreferences(appContext).edit()
                                .putBoolean(OnBoardingConstants.IS_CONTACT_SYNCED, true).apply()
                            isLoading.value = false
                            showToast.value = "Contacts successfully synced."
                        }
                    }
                }
            }


        }
    }


    fun syncContacts() = contacts.switchMap {
        sync(it)
    }

    fun requestPermission() {
        requestPermission.call()
    }

    fun fetchContacts() {
        isLoading.value = true
        viewModelScope.launch {
            val contact = ContactUtil.getAllContacts(
                preference.getMobile()?:"",
                appContext,
                preference.getCountry().countryCode,
                preference.getPhoneLength()
            )
            contacts.value = contact
            Timber.d("contact size ${contact.size}")
        }

    }

    fun startSync() {
        syncContacts.call()
    }

    fun select() {
        select.call()
    }
    fun getContacts() = repository.getFriends()


    override fun onCleared() {
        super.onCleared()
    }


}