/*
 * *
 *  * Created by Surajit on 7/7/20 4:07 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 7/7/20 4:07 PM
 *  *
 */

package com.bt.kotlindemo.ui.profile.update_profile

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Parcelable
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.databinding.ActivityUpdateProfileBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.IPermissionGrantedDeniedCallback
import com.bt.kotlindemo.model.Country
import com.bt.kotlindemo.model.State
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.*
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File


class UpdateProfileActivity : BaseActivity() {

    private lateinit var binding: ActivityUpdateProfileBinding
    private val viewModel: UpdateProfileViewModel by viewModel()
    private var imgPath: String = ""
    private var imageUri: Uri? = null
    private var queryImageUrl: String = ""
    private lateinit var permissionHelper: PermissionHelper
    private val PHOTO = "photo"
    private val CAMERA = "camera"
    private val cameraHelper by lazy {
        CameraHelper(this)
    }
    private val pickImageLauncher: ActivityResultLauncher<String> =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            if (uri != null) {
                viewModel.viewModelScope.launch(Dispatchers.IO) {
                    handleImage(uri)


                }
            }
        }
    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                handleImage(result.data?.data)

            }
        }


    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_update_profile)
        binding.apply {
            lifecycleOwner = this@UpdateProfileActivity
            viewModel = this@UpdateProfileActivity.viewModel

        }
    }


    override fun initViews() {
        attachObservers()
        initPermission()
        setSpinnerClick()
    }


    private fun setSpinnerClick() {
        binding.spnCountry.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                if (viewModel.countrylist.value?.get(position)?.countryCode != -1) {
                    viewModel.countryId.value =
                        viewModel.countrylist.value?.get(position)?.countryCode
                    viewModel.country.value =
                        viewModel.countrylist.value?.get(position)?.countryName
                    viewModel.getStateList().observe(this@UpdateProfileActivity) { }
                } else {
                    viewModel.countryId.value = -1
                    viewModel.country.value = ""
                }
            }
        }
        binding.spnState.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                if (viewModel.stateList.value?.get(position)?.stateCode != 0) {
                    viewModel.stateId.value = viewModel.stateList.value?.get(position)?.stateCode
                    viewModel.state.value = viewModel.stateList.value?.get(position)?.stateName
                }
            }
        }
    }

    private fun setList(list: List<State>) {
        binding.spnState.adapter = StateSpinnerAdapter(list)
    }

    private fun setCountryList(list: List<Country>) {
        binding.spnCountry.adapter = CountrySpinnerAdapter(list)
    }


    private fun initPermission() {
        permissionHelper =
            PermissionHelper(this).setListener(object : PermissionHelper.PermissionsListener {
                override fun onPermissionGranted(requestCode: Int) {
                    viewModel.isCameraPermissionAllowed.value = true
                }

                override fun onPermissionRejectedManyTimes(
                    rejectedPerms: List<String>,
                    request_code: Int
                ) {
                    viewModel.isCameraPermissionAllowed.value = false
                }

                override fun onPermissionDenied(request_code: Int) {
                    viewModel.isCameraPermissionAllowed.value = false
                }

                override fun showRationale() {

                }
            })
    }

    private fun attachObservers() {
        viewModel.onBackPressed.observe(this) {
            finish()
        }

        viewModel.showMessage.observe(this, Observer {
            AlertHelper.showOkAlertFinish(this, it)
        })

        viewModel.showToast.observe(this) {
            showToast(it)
        }

        viewModel.stateList.observe(this) {
            setList(it)
        }

        viewModel.countrylist.observe(this) {
            setCountryList(it)
        }


        viewModel.pos.observe(this) {
            binding.spnState.setSelection(it)
        }

        viewModel.posCountry.observe(this) {
            binding.spnCountry.setSelection(it)
        }

        viewModel.isLoading.observe(this) {
            if (it) showDialog() else dismissDialog()
        }

        viewModel.requestPermission.observe(this) {
            permissionHelper.requestPermission(
                arrayOf(Manifest.permission.CAMERA),
                IntentRequestConstants.CAMERA_PERMISSION
            )
        }
        viewModel.update.observe(this) {
            verify()
        }

        viewModel.pickImage.observe(this) {
            checkPermission(PHOTO)
        }

        viewModel.updated.observe(this) {
            setResult(RESULT_OK)
            finish()
        }

        viewModel.isCameraPermissionAllowed.observe(this) {
            if (it) {
                chooseImage()
            } else {

                AlertHelper.showOKCancelAlert(this@UpdateProfileActivity,
                    getString(R.string.camera_permission),
                    object : AlertCallback {
                        override fun action(isOk: Boolean, data: Any?) {
                            if (isOk)
                                viewModel.requestPermission.call()
                        }
                    })

            }
        }

        viewModel.getCountryList().observe(this) { }


    }

    private fun checkPermissions() {
        permissionHelper.checkPermission(
            arrayOf(Manifest.permission.CAMERA),
            IntentRequestConstants.CAMERA_PERMISSION
        )
    }

    private fun verify() {
        if (viewModel.name.value.isNullOrEmpty()) {
            binding.etName.error = "Required"
            return
        }
        if (viewModel.email.value.isNullOrEmpty()) {
            binding.etEmail.error = "Required"
            return
        }
        if (!viewModel.email.value.isValidEmail()) {
            binding.etEmail.error = "Invalid Email"
            return
        }

        if (viewModel.address1.value.isNullOrEmpty()) {
            binding.etAddress1.error = "Required"
            return
        }

        if (viewModel.countryId.value == -1) {
            showSnackBar(binding.root, "Select Country")
            return
        }

        if (viewModel.stateId.value == 0) {
            showSnackBar(binding.root, "Select State")
            return
        }


        if (viewModel.pin.value.isNullOrEmpty()) {
            binding.etPinCode.error = "Required"
            return
        }


        viewModel.updateData().observe(this) {

        }


    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            IntentRequestConstants.PICK_IMAGE -> {
                if (resultCode == Activity.RESULT_OK) {
                    handleImageRequest(data)
                }
            }
        }
    }

    private fun chooseImage() {
        getPickImageIntent()?.let { startActivityForResult(it, IntentRequestConstants.PICK_IMAGE) }
    }

    private fun getPickImageIntent(): Intent? {
        var chooserIntent: Intent? = null

        var intentList: MutableList<Intent> = ArrayList()

        val pickIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, setImageUri())

        intentList = ActivityUtils.addIntentsToList(this, intentList, pickIntent)
        intentList = ActivityUtils.addIntentsToList(this, intentList, takePhotoIntent)

        if (intentList.size > 0) {
            chooserIntent = Intent.createChooser(
                intentList.removeAt(intentList.size - 1),
                getString(R.string.app_name)
            )
            chooserIntent!!.putExtra(
                Intent.EXTRA_INITIAL_INTENTS,
                intentList.toTypedArray<Parcelable>()
            )
        }

        return chooserIntent
    }


    private fun setImageUri(): Uri {
        val folder = File("${getExternalFilesDir(Environment.DIRECTORY_DCIM)}")
        folder.mkdirs()

        val file = File(folder, "Image_Tmp.jpg")
        if (file.exists())
            file.delete()
        file.createNewFile()
        imageUri = FileProvider.getUriForFile(
            this,
            "${BuildConfig.APPLICATION_ID}.fileProvider",
            file
        )
        imgPath = file.absolutePath
        return imageUri!!
    }


    private fun handleImageRequest(data: Intent?) {
        val exceptionHandler = CoroutineExceptionHandler { _, t ->
            t.printStackTrace()
            dismissDialog()
            showToast(
                t.localizedMessage ?: getString(R.string.server_error)
            )
        }

        lifecycleScope.launch(Dispatchers.Main + exceptionHandler) {
            showDialog()

            if (data?.data != null) {     //Photo from gallery
                imageUri = data.data
                queryImageUrl = imageUri?.path!!
                queryImageUrl = compressImageFile(queryImageUrl, false, imageUri!!)
            } else {
                queryImageUrl = imgPath ?: ""
                compressImageFile(queryImageUrl, uri = imageUri!!)
            }
            viewModel.file.value = File(queryImageUrl)
            imageUri = Uri.fromFile(File(queryImageUrl))

            uploadImageInServer()
            // if (queryImageUrl.isNotEmpty()) {
            //     binding.src = imageUri
            // }
            // dismissDialog()
        }

    }

    private fun uploadImageInServer() {
        viewModel.uploadProfilePic().observe(this) { }
    }

    private fun checkPermission(mediaType: String) {
        if (mediaType == PHOTO) {
            val permission = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                android.Manifest.permission.READ_MEDIA_IMAGES
            } else {
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            }
            PermissionUtil.askPermissionFor(
                permission,
                object : IPermissionGrantedDeniedCallback {
                    override fun onPermissionGrantedOrDenied(
                        permission: String,
                        isGranted: Boolean
                    ) {
                        if (isGranted) {
                            pickImageLauncher.launch("image/*")
                        } else {
                            checkPermission(mediaType)
                        }
                    }

                    override fun afterCompletePermissionDenied() {
                        showToast("Necessary permissions are required to choose photo")
                    }
                },
                this,
                this
            )
        } else if (mediaType == CAMERA) {
            PermissionUtil.askPermissionFor(
                android.Manifest.permission.CAMERA, object : IPermissionGrantedDeniedCallback {
                    override fun onPermissionGrantedOrDenied(
                        permission: String,
                        isGranted: Boolean
                    ) {
                        if (isGranted) {
                            startForResult.launch(cameraHelper.getPickImageIntent(true))
                        } else {
                            checkPermission(mediaType)
                        }
                    }

                    override fun afterCompletePermissionDenied() {
                        showToast("Necessary permissions are required to capture photo")
                    }
                }, this, this
            )
        }
    }

    private fun handleImage(result: Uri?) {
        val exceptionHandler = CoroutineExceptionHandler { _, t ->
            t.printStackTrace()
        }

        lifecycleScope.launch {
            val file = cameraHelper.handleImageRequest(
                result,
                this@UpdateProfileActivity,
                exceptionHandler,
                this@UpdateProfileActivity
            )
            viewModel.file.value = file
            uploadImageInServer()
        }

    }


}