/*
 * *
 *  * Created by Surajit on 1/24/21 11:25 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/24/21 11:25 PM
 *  *
 */

package com.bt.kotlindemo.ui.webview_activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.net.MailTo
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.databinding.ActivityWebViewBinding
import com.bt.kotlindemo.ui.base.BaseActivity


class WebViewActivity : BaseActivity() {

    private lateinit var binding: ActivityWebViewBinding


    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun initViews() {
        binding.imgBack.setOnClickListener {
            finish()
        }
        binding.title = intent.getStringExtra(IntentConstants.TITLE)
        showDialog()
        binding.webView.loadUrl(intent.getStringExtra(IntentConstants.DATA) ?: "")
        binding.webView.settings.javaScriptEnabled = true
        binding.webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                showDialog()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                dismissDialog()
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
                dismissDialog()
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (request?.url?.toString()?.startsWith("tel:") == true) {
                    val intent = Intent(Intent.ACTION_DIAL, request.url)
                    startActivity(intent)
                    return true
                }
                if (request?.url?.toString()?.startsWith("mailto:") == true) {
                    val mt: MailTo = MailTo.parse(request.url!!)
                    val i: Intent = newEmailIntent(
                        mt.to,
                        mt.subject,
                        mt.body,
                        mt.cc
                    )
                    startActivity(i)
                    return true
                }
                return false
            }
        }
        binding.webView.setBackgroundColor(Color.TRANSPARENT)
    }

    private fun newEmailIntent(
        address: String?,
        subject: String?,
        body: String?,
        cc: String?
    ): Intent {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(address))
        intent.putExtra(Intent.EXTRA_TEXT, body)
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        intent.putExtra(Intent.EXTRA_CC, cc)
        intent.setType("message/rfc822")
        return intent
    }
}