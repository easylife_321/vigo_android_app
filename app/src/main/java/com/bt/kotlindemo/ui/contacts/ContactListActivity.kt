/*
 * *
 *  * Created by Surajit on 7/7/20 4:07 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 7/7/20 4:07 PM
 *  *
 */

package com.bt.kotlindemo.ui.contacts

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.preference.PreferenceManager
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.constants.OnBoardingConstants
import com.bt.kotlindemo.data.database.entity.Contacts
import com.bt.kotlindemo.databinding.ActivityContactListBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.PermissionHelper
import org.koin.androidx.viewmodel.ext.android.viewModel

class ContactListActivity : BaseActivity() {

    private lateinit var binding: ActivityContactListBinding
    private val viewModel: ContactsListViewModel by viewModel()
    private lateinit var permissionHelper: PermissionHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_contact_list)
        binding.apply {
            lifecycleOwner = this@ContactListActivity
            viewModel = this@ContactListActivity.viewModel
            res = R.mipmap.denied
        }
    }

    override fun initViews() {
        initPermission()
        attachObservers()
        if (!PreferenceManager.getDefaultSharedPreferences(this)
                .getBoolean(OnBoardingConstants.IS_CONTACT_SYNCED, false)
        ) {
            checkContactPermission()
        }
    }

    fun showContactPermissionPopup() {
        showContactPermissionRationale {
            if (it) {
                viewModel.requestPermission.call()
            } else {
                finish()
            }
        }
    }

    private fun attachObservers() {
        viewModel.requestPermission.observe(this, Observer {
            permissionHelper.requestPermission(
                arrayOf(Manifest.permission.READ_CONTACTS),
                IntentRequestConstants.CONTACT_PERMISSION
            )
        })
        viewModel.onBackPressed.observe(this, Observer {
            finish()
        })

        viewModel.isContactPermissionAllowed.observe(this, Observer {
            if (it) {
                viewModel.isContactPermissionAllowedAnim.value = true
                viewModel.fetchContacts()
            } else {
                if (!PreferenceManager.getDefaultSharedPreferences(this)
                        .getBoolean(OnBoardingConstants.IS_CONTACT_SYNCED, false)
                ) {
                    viewModel.isContactPermissionAllowedAnim.value = false
                } else {
                    AlertHelper.showOKCancelAlert(this@ContactListActivity,
                        getString(R.string.contact_permission),
                        object : AlertCallback {
                            override fun action(isOk: Boolean, data: Any?) {
                                if (isOk)
                                    viewModel.requestPermission.call()
                            }
                        })
                }
            }
        })
        viewModel.isLoading.observe(this, Observer {
            if (it) showDialog() else dismissDialog()
        })

        viewModel.showToast.observe(this, Observer {
            showToast(it)
        })
        viewModel.showMessage.observe(this, Observer {
            AlertHelper.showOkAlert(this@ContactListActivity, it)
        })
        viewModel.getContacts().observe(this, Observer {
            setList(it)
        })

        viewModel.syncContacts.observe(this, Observer {
            checkContactPermission()
        })

        viewModel.select.observe(this, Observer {
            checkSelected()
        })

        viewModel.syncContacts().observe(this, Observer { })

    }

    private fun checkSelected() {
        val selected = binding.adapter!!.getSelected()
        if (selected.isEmpty()) {
            showSnackBar(binding.root, "Please select at-least one contact to continue")
            return
        }
        setResult(Activity.RESULT_OK, Intent().apply {
            putExtra(IntentConstants.DATA, selected)
        })
        finish()
    }

    private fun setList(it: List<Contacts>?) {
        it?.apply {
            if (it.isNotEmpty())
                binding.isSelectable = intent.getBooleanExtra(IntentConstants.IS_SELECTABLE, false)
            binding.adapter =
                ContactsListAdapter(
                    intent.getBooleanExtra(IntentConstants.IS_SELECTABLE, false),
                    this as ArrayList<Contacts>,
                    object : RecyclerClickListener {
                        override fun onClick(objects: Any) {

                        }
                    })
        }
    }

    private fun initPermission() {
        permissionHelper =
            PermissionHelper(this).setListener(object : PermissionHelper.PermissionsListener {
                override fun onPermissionGranted(requestCode: Int) {
                    viewModel.isContactPermissionAllowed.value = true
                }

                override fun onPermissionRejectedManyTimes(
                    rejectedPerms: List<String>,
                    request_code: Int
                ) {
                    viewModel.isContactPermissionAllowed.value = false
                }

                override fun onPermissionDenied(request_code: Int) {
                    showContactPermissionPopup()
                }

                override fun showRationale() {

                }
            })
    }

    private fun checkContactPermission() {
        permissionHelper.checkPermission(
            arrayOf(Manifest.permission.READ_CONTACTS),
            IntentRequestConstants.CONTACT_PERMISSION
        )
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IntentRequestConstants.CONTACT_PERMISSION) {
            viewModel.requestPermission.call()
        }

    }
}