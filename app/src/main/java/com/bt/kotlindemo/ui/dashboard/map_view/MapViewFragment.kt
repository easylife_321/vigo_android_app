/*
 * *
 *  * Created by Surajit on 5/4/20 11:57 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 5/4/20 11:57 AM
 *  *
 */

package com.bt.kotlindemo.ui.dashboard.map_view

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.transition.Fade
import android.transition.Transition
import android.transition.TransitionManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.CustomMarkerVehiclesNewBinding
import com.bt.kotlindemo.databinding.FragmentMapViewBinding
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.ui.base.BaseFragment
import com.bt.kotlindemo.ui.dashboard.DashboardActivity
import com.bt.kotlindemo.ui.device_detail.DeviceDetailActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class MapViewFragment : BaseFragment() {


    private lateinit var binding: FragmentMapViewBinding
    private lateinit var mMap: GoogleMap
    private val viewModel: MapViewViewModel by viewModel()

    private var setBound = true

    companion object {
        fun newInstance() = MapViewFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map_view, container, false)
        binding.apply {
            lifecycleOwner = this@MapViewFragment
            viewModel = this@MapViewFragment.viewModel

        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setMap()
        addObservers()
       // setBottomSheet()
    }

    private fun setBottomSheet() {
        val bottomSheetBehavior: BottomSheetBehavior<*> =
            BottomSheetBehavior.from<View>(binding.filterLayout)
        bottomSheetBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(
                bottomSheet: View,
                newState: Int
            ) {
            }

            override fun onSlide(
                bottomSheet: View,
                slideOffset: Float
            ) {
                binding.fab.animate().scaleX(1 - slideOffset).scaleY(1 - slideOffset).setDuration(0)
                    .start()
            }
        })
    }

    private fun addObservers() {
        binding.fab.setOnClickListener {
            viewModel.groups.value?.let {
                ((activity as DashboardActivity)).openList(it as ArrayList<Group>)
            }

        }
        viewModel.getDeviceData().observe(viewLifecycleOwner, Observer {
            return@Observer
        })
        viewModel.groups.observe(viewLifecycleOwner, Observer {
            if (binding.adapter == null) {
                setAdapter(it)
            }
        })
        viewModel.devicesToShow.observe(viewLifecycleOwner, Observer {
            try {
                (activity as DashboardActivity).showRightMenu()
                addToMap(it)
            } catch (ex: Exception) {
                AlertHelper.showOkAlert(requireContext(), ex.message!!, null)
            }
        })

        viewModel.isSearchOpen.observe(viewLifecycleOwner, Observer {
            toggle(it, binding.searchLayout)
            if (!it) {
                viewModel.searchText.value = ""
            }

        })
        viewModel.isFilterOpen.observe(viewLifecycleOwner, Observer {
            toggle(it, binding.filterLayout)

        })

        viewModel.searchText.observe(viewLifecycleOwner, Observer {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED)
                viewModel.filterDevices()
        })


    }

    private fun toggle(show: Boolean, view: View) {
        val transition: Transition = Fade()
        transition.duration = 300
        transition.addTarget(view)
        TransitionManager.beginDelayedTransition(binding.parentView, transition)
        view.visibility = if (show) View.VISIBLE else View.GONE
    }


    private fun setAdapter(group: List<Group>) {
        binding.adapter =
            SelectGroupAdapter(group as ArrayList<Group>)
        binding.spnGroup.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.selectedGroup.value = binding.adapter?.getItem(position)
                viewModel.filterDevices()
            }

        }

    }


    private fun addToMap(devices: List<Device>) {
        mMap.clear()
        val builder = LatLngBounds.Builder()
        devices.forEach { device ->
            val latLng = LatLng(device.lat, device.lang)
            val markerIcon = getMarkerBitmapFromView(
                device.deviceName,
                device.movingStatusShort,
                device.deviceIcon
            )

            mMap.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .icon(
                        markerIcon?.let { BitmapDescriptorFactory.fromBitmap(it) }
                    )

            )?.tag = device.deviceId


            /* mMap.addMarker(
                 MarkerOptions()
                     .icon(ImageUtil.bitmapFromVector(requireContext(), R.drawable.car_icon))
                     .position(latLng)
                     .anchor(.5f, .5f)
             )!!*/

            builder.include(latLng)


        }
        if (devices.isNotEmpty() && setBound) {
            setBound = false
            val bounds = builder.build()
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50))
        }
    }


    private fun getMarkerBitmapFromView(name: String, status: String, deviceIcon: Int): Bitmap? {
        val binding: CustomMarkerVehiclesNewBinding =
            DataBindingUtil.inflate(
                layoutInflater,
                R.layout.custom_marker_vehicles_new,
                null,
                false
            )
        binding.name = name
        binding.status = status
        binding.type = deviceIcon
        val customMarkerView: View = binding.root
        binding.executePendingBindings()
        customMarkerView.measure(
            View.MeasureSpec.UNSPECIFIED,
            View.MeasureSpec.UNSPECIFIED
        )
        customMarkerView.layout(
            0,
            0,
            customMarkerView.measuredWidth,
            customMarkerView.measuredHeight
        )

        customMarkerView.buildDrawingCache()
        val returnedBitmap = Bitmap.createBitmap(
            customMarkerView.measuredWidth, customMarkerView.measuredHeight,
            Bitmap.Config.ARGB_8888
        )

        val canvas = Canvas(returnedBitmap)
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN)
        val drawable = customMarkerView.background
        drawable?.draw(canvas)
        customMarkerView.draw(canvas)
        return returnedBitmap
    }


    private fun setMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            mMap = it
            viewModel.initValues()
            viewModel.startTimer()
            addMarkerClick()

        }
    }

    private fun addMarkerClick() {
        mMap.setOnMarkerClickListener {
            startActivity(Intent(activity, DeviceDetailActivity::class.java).apply {
                putExtra(IntentConstants.DEVICE_ID, it.tag as Int)//it.tag as Int
            })
            true
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        try {
            val mapFragment =
                childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
            if (mapFragment != null) {
                childFragmentManager.beginTransaction().remove(mapFragment).commit()
            }
        } catch (ignored: Exception) {
            ignored.printStackTrace()
        }

    }


    fun setListCallback() {
        //((activity as DashboardActivity)).openList()
    }


}



