/*
 * *
 *  * Created by Surajit on 10/4/20 3:08 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 5:21 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.add_device

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.repository.DeviceRepository
import com.bt.kotlindemo.data.repository.GroupRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.DeviceIcon
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject

class AddDeviceActivityViewModel(
    var preference: AppPreferences,
    private var groupRepository: GroupRepository,
    private var deviceRepository: DeviceRepository
) : BaseViewModel() {

    val isEdit = ObservableBoolean()
    val isAdmin = ObservableBoolean()
    val deviceName = ObservableField<String>()
    val deviceNickName = ObservableField<String>()
    val groupName = ObservableField<String>()
    val imei = ObservableField<String>()
    val driverName = ObservableField<String>()
    val driverMobile = ObservableField<String>()
    val iconName = ObservableField<String>()


    fun getGroups() = groupRepository.getGroups()

    fun getIcons() = deviceRepository.getIcons()

    fun addDevice(
        name: String,
        nickName: String,
        imei: String,
        groupId: Int,
        icon: Int,
        driverName: String,
        driverMobile: String,
        deviceId: Int?
    ): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()

        jsonObject.addProperty("GroupId", groupId)
        jsonObject.addProperty("UserId", preference.getUserId())
        jsonObject.addProperty("DeviceName", name)
        jsonObject.addProperty("DeviceNickName", nickName)
        jsonObject.addProperty("DeviceIMEI", imei)
        jsonObject.addProperty("IconType", icon)
        jsonObject.addProperty("DriverName", driverName)
        jsonObject.addProperty("DriverNo", driverMobile)
        if (isEdit.get())
            jsonObject.addProperty("DeviceId", deviceId)

        if (isEdit.get()) return deviceRepository.updateDevice(jsonObject) else return deviceRepository.addDevice(
            jsonObject
        )
    }


    fun setData(edit: Boolean, device: Device?, group: Group?, icon: DeviceIcon?) {
        if (edit) {
            isAdmin.set(device?.isAdmin == true)
            isEdit.set(true)
            groupName.set(group?.name)
            iconName.set(icon?.text)
            imei.set(device?.imei)
            driverMobile.set(device?.driverNumber)
            driverName.set(device?.driverName)
            deviceName.set(device?.deviceName)
            deviceNickName.set(device?.deviceNickName)
        } else {
            isAdmin.set(true)
            isEdit.set(false)
            groupName.set(" Select Group Name")
            iconName.set("Select Icon")
            imei.set("")
            driverMobile.set("")
            driverName.set("")
            deviceName.set("")
            deviceNickName.set("")
        }
    }

}