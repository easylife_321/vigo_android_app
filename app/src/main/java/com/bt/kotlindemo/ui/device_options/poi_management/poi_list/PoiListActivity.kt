/*
 * *
 *  * Created by Surajit on 25/5/20 7:43 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:43 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.poi_management.poi_list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.databinding.ActivityPoiListBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.PoiRecyclerClickListener
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Poi
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.device_options.poi_management.add_edit_poi.AddPoiActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.ferfalk.simplesearchview.SimpleSearchView
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class PoiListActivity : BaseActivity() {

    private lateinit var binding: ActivityPoiListBinding
    private val viewModel: PoiListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        setStatusBarColor(R.color.colorPrimary)
        super.onCreate(savedInstanceState)

    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_poi_list)
        binding.apply {
            lifecycleOwner = this@PoiListActivity
            viewModel = this@PoiListActivity.viewModel
        }
        setSearchView()
    }

    override fun initViews() {
        addObservers()
        fetchList()
    }

    private fun addObservers() {
        viewModel.isLoading.observe(this, Observer {
            if (it) {
                showDialog()
            } else {
                dismissDialog()
            }
        })

        viewModel.showMessageRetry.observe(this, Observer {
            showToastRetry(binding.root, it, object : ToastRetry {
                override fun retry() {
                    fetchList()
                }
            })

        })

        viewModel.poi.observe(this, Observer {
            setList(it)
        })

        viewModel.onSearch.observe(this, Observer {
            openSearch()
        })

        viewModel.onBackPressed.observe(this, Observer {
            finish()
        })
        viewModel.addPoi.observe(this, Observer {
            val intent =
                Intent(this@PoiListActivity, AddPoiActivity::class.java).apply {
                    val bundle = Bundle().apply {
                        putBoolean(IntentConstants.EDIT, false)
                    }
                    putExtras(bundle)
                }
            startActivityForResult(intent, IntentRequestConstants.ADD_POI)
        })
    }

    fun openSearch() {
        binding.toolbar.visibility = View.INVISIBLE
        binding.searchView.visibility = View.VISIBLE
        binding.searchView.showSearch(true)
    }

    private fun setSearchView() {
        binding.searchView.setOnSearchViewListener(object : SimpleSearchView.SearchViewListener {
            override fun onSearchViewShownAnimation() {
            }

            override fun onSearchViewClosed() {
                if (binding.adapter != null)
                    binding.adapter!!.filter.filter("")
                binding.toolbar.visibility = View.VISIBLE
                binding.searchView.visibility = View.INVISIBLE
            }

            override fun onSearchViewClosedAnimation() {

            }

            override fun onSearchViewShown() {

            }

        })

        binding.searchView.setOnQueryTextListener(object : SimpleSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (binding.adapter != null) {
                    binding.adapter!!.filter.filter(newText)
                }
                return false
            }

            override fun onQueryTextCleared(): Boolean {
                return false
            }
        })

    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            slideToRight()
        }
    }

    override fun onBackPressed() {
        if (binding.searchView.isSearchOpen) {
            binding.searchView.closeSearch(true)
        } else {
            super.onBackPressed()
        }

    }


    private fun setList(it: ArrayList<Poi>) {
        binding.adapter = PoiListAdapter(it, object : PoiRecyclerClickListener {
            override fun onClick(data: Any, action: Int) {
                when (action) {
                    PoiRecyclerClickListener.VIEW -> {
                        val intent =
                            Intent(this@PoiListActivity, AddPoiActivity::class.java).apply {
                                val bundle = Bundle().apply {
                                    putSerializable(IntentConstants.DATA, data as Poi)
                                    putBoolean(IntentConstants.EDIT, true)
                                }
                                putExtras(bundle)
                            }
                        startActivityForResult(intent, IntentRequestConstants.EDIT_POI)
                    }
                    PoiRecyclerClickListener.DELETE -> {
                        AlertHelper.showOKCancelAlert(this@PoiListActivity,
                            "Are you sure you want to delete this place? ",
                            object : AlertCallback {
                                override fun action(isOk: Boolean, a: Any?) {
                                    if (isOk) {
                                        deleteAndObserve(data as Poi)
                                    }
                                }
                            })
                    }
                }
            }
        })
    }

    private fun deleteAndObserve(poi: Poi) {
        viewModel.deletePoi(poi.id).observe(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(this@PoiListActivity, it.message!!, null)
                }
                ApiResult.Status.SUCCESS -> {
                    AlertHelper.showOkAlert(
                        this@PoiListActivity,
                        it.data!!.result.msg,
                        object : AlertCallback {
                            override fun action(isOk: Boolean, data: Any?) {
                                binding.adapter!!.removeDevice(poi)
                            }
                        })
                }
                else -> return@Observer
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            fetchList()
        }
    }

    private fun fetchList() {
        viewModel.getPoi().observe(this, Observer { })
    }
}
