package com.bt.kotlindemo.ui.sign_in_activity


import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.databinding.FragmentSignUpBinding
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.ui.base.BaseFragment
import com.bt.kotlindemo.ui.select_country.SelectCountryActivity
import com.bt.kotlindemo.utils.isValidEmail
import com.bt.kotlindemo.utils.isValidMobile
import com.bt.kotlindemo.utils.observeOnce
import com.bt.kotlindemo.utils.onRightDrawableClicked
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class SignUpFragment : BaseFragment() {


    private lateinit var binding: FragmentSignUpBinding
    private val viewModel: RegistrationViewModel by sharedViewModel()
    private var countryList: CountryList? = null

    companion object {
        @JvmStatic
        fun newInstance() =
            SignUpFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false)
        binding.handler = ClickHandler()
        binding.country = viewModel.getCountry()
        binding.maxLenghth = viewModel.preferences.getPhoneLength()
        setViews()
        return binding.root
    }

    private fun setViews() {

        binding.etPassword.onRightDrawableClicked {
            if (it.tag == 1) {
                it.tag = 2
                it.transformationMethod = null
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_visible, 0);
            } else {
                it.tag = 1
                it.transformationMethod = PasswordTransformationMethod()
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_hidden, 0);
            }
        }
        binding.etConfirmPassword.onRightDrawableClicked {
            if (it.tag == 1) {
                it.tag = 2
                it.transformationMethod = null
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_visible, 0);
            } else {
                it.tag = 1
                it.transformationMethod = PasswordTransformationMethod()
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_hidden, 0);
            }
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addTokenListener()
    }

    private fun addTokenListener() {
        viewModel.token.observeOnce(viewLifecycleOwner, Observer {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED) {
                navigateToOtp(it)
            }

        })
    }

    private fun navigateToOtp(token: String) {
        viewModel.verifyOtp(
            binding.etName.text.toString().trim(),
            binding.etMobile.text.toString().trim(),
            binding.etEmail.text.toString().trim(),
            binding.country!!,
            binding.etPassword.text.toString().trim(),
            token
        )
        (context as SignInActivity).changeFragment(OtpFragment.newInstance())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            countryList = data!!.getSerializableExtra(IntentConstants.DATA) as CountryList
            binding.country = countryList
            viewModel.preferences.setCountry(countryList!!)
            viewModel.preferences.setPhoneLength(
                viewModel.phoneNumberUtil.getExampleNumber(
                    countryList!!.isoAlpha
                ).nationalNumber.toString().length
            )
            binding.maxLenghth = viewModel.preferences.getPhoneLength()
            binding.etMobile.setText("")
        }
    }


    inner class ClickHandler() {


        fun selectCountry() {
            startActivityForResult(
                Intent(activity, SelectCountryActivity::class.java),
                IntentRequestConstants.SELECT_COUNTRY
            )
        }

        fun switchSignIn() {
            (context as SignInActivity).changeFragment(SignInFragment.newInstance())
        }


        fun signUp() {

            if (TextUtils.isEmpty(binding.etName.text.toString().trim())) {
                showSnackBar(
                    binding.root,
                    "Please enter your name"
                )
                binding.etName.error = "Required"
                return
            }


            if (TextUtils.isEmpty(binding.etMobile.text.toString().trim())) {
                showSnackBar(
                    binding.root,
                    "Please enter your mobile number"
                )
                binding.etMobile.error = "Required"
                return
            }

            if (!binding.etMobile.text.toString().trim().isValidMobile()) {
                showSnackBar(
                    binding.root,
                    "Please enter valid mobile number"
                )
                binding.etMobile.error = "Invalid"
                return
            }


            if (!binding.etEmail.text.toString().trim().isValidEmail()) {
                showSnackBar(
                    binding.root,
                    "Please enter valid email id"
                )
                binding.etEmail.error = "Invalid"
                return
            }

            if (TextUtils.isEmpty(binding.etPassword.text.toString().trim())) {
                showSnackBar(
                    binding.root,
                    "Please enter your password"
                )
                binding.etPassword.error = "Required"
                return
            }

            if (TextUtils.isEmpty(binding.etConfirmPassword.text.toString().trim())) {
                showSnackBar(
                    binding.root,
                    "Please enter confirm password"
                )
                binding.etConfirmPassword.error = "Required"
                return
            }


            if (binding.etPassword.text.toString().trim() !=
                binding.etConfirmPassword.text.toString().trim()
            ) {
                showSnackBar(
                    binding.root,
                    "Password and confirm password does not match"
                )
                binding.etConfirmPassword.error = "Mismatch"
                return
            }
            viewModel.loginWithOtp = false
            if (!viewModel.token.value.isNullOrEmpty()) {
                navigateToOtp(viewModel.token.value ?: "")
            } else {
                viewModel.getToken()
            }
        }
    }


}
