/*
 * *
 *  * Created by Surajit on 25/4/20 12:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/4/20 6:18 PM
 *  *
 */

package com.bt.kotlindemo.ui.profile.profile_detail

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.R
import com.bt.kotlindemo.data.repository.RegistrationRepository
import com.bt.kotlindemo.data.repository.SettingsRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Contact
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.CryptoUtil
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.koin.core.KoinComponent
import org.koin.core.get

class ProfileDetailViewModel(
    val preference: AppPreferences,
    private var repository: SettingsRepository,
    private var registrationRepository: RegistrationRepository,
    private val appContext: Application
) : BaseViewModel(),KoinComponent {


    var isLoaded = ObservableBoolean()
    var address = ObservableField<String>()
    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var showToast = MutableLiveData<String>()
    var contacts = MutableLiveData<List<Contact>>()
    var name = MutableLiveData<String>()
    var mobile = MutableLiveData<String>()
    var email = MutableLiveData<String>()
    var feedback = SingleLiveEvent<Boolean?>()
    var src = MutableLiveData<Any>()
    var logout = SingleLiveEvent<Void?>()
    var updateView = SingleLiveEvent<Void?>()
    val passwordChanged = MutableLiveData<Boolean>()
    private val cryptoUtil: CryptoUtil = get()
    init {
        initData()
    }

    fun initData() {
        name.value = preference.getUserName()
        mobile.value = "+${preference.getCountry().countryCode}${preference.getMobile()}"
        email.value = preference.getEmail()
        src.value = if (preference.getProfileImage()
                .isNullOrEmpty()
        ) R.drawable.user_icon else "${preference.getProfileImage()}"

    }


    fun logout(): LiveData<Unit> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", preference.getUserId())
        return repository.logout(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    isLoading.value = false
                }
                else -> {
                    isLoading.value = false
                    if (it.data == null) {
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"
                    } else {
                        clearData()
                    }
                }
            }
        }
    }

    fun dnd(): LiveData<Unit> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("active", !preference.getDnd())

        return repository.doNotDisturb(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    isLoading.value = false
                }
                else -> {
                    isLoading.value = false
                    if (it.data == null) {
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"
                    } else {
                        preference.setDnd(!preference.getDnd())
                        updateView.call()
                        showMessage.value = it.data.result.msg
                    }
                }
            }
        }
    }


    fun changePassword(password: String): LiveData<Unit> {
        val jsonObject = JsonObject()
        preference.setPassword(cryptoUtil.encrypt(password)!!)
        jsonObject.addProperty("Userid", preference.getUserId())
        jsonObject.addProperty("Password", preference.getPassword())
        jsonObject.addProperty("accountId", BuildConfig.ACCOUNT_ID)
        return registrationRepository.setPassword(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    isLoading.value = false
                }
                else -> {
                    isLoading.value = false
                    if (it.data == null) {
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"
                    } else {
                        passwordChanged.postValue(true)
                    }
                }
            }
        }
    }


    fun openFeedback() {
        feedback.call()
    }

    private fun clearData() {
        viewModelScope.launch(Dispatchers.IO) {
            preference.clear()
            repository.deleteAllTable()
            withContext(Dispatchers.Main) {
                logout.call()

            }


        }


    }


}