/*
 * *
 *  * Created by Surajit on 29/4/20 6:20 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 28/4/20 8:07 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.share_device

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ActivityShareDevciceBinding
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.observeOnce
import org.koin.androidx.viewmodel.ext.android.viewModel

class ShareDeviceActivity : BaseActivity() {

    private val viewModel: ShareDeviceActivityViewModel by viewModel()
    private var deviceList: ArrayList<Device>? = null
    private var device: Device? = null

    private lateinit var binding: ActivityShareDevciceBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        setStatusBarColor(R.color.colorPrimaryDark)
        setStatusBarTint(false)
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_share_devcice)
        binding.apply {
            lifecycleOwner = this@ShareDeviceActivity
            viewModel = this@ShareDeviceActivity.viewModel
        }
        setSupportActionBar(binding.toolbar)

    }

    @Suppress("UNCHECKED_CAST")
    override fun initViews() {
        addObservers()

    }

    private fun addObservers() {
        viewModel.onBackPressed.observeOnce(this, Observer {
            finish()
        })

    }



}
