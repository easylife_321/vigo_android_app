/*
 * *
 *  * Created by Surajit on 23/5/20 2:53 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/5/20 4:35 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.poi_management.add_edit_poi

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.repository.PoiRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Poi
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AddressUtil
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class AddPoiViewModel(
    private val poiId: String?,
    private val preference: AppPreferences,
    private var poiRepository: PoiRepository,
    private val context: Application
) : BaseViewModel() {

    var poiName = ObservableField<String>()
    var lat = ObservableField<String>()
    var lng = ObservableField<String>()
    var deviceName = ObservableField<String>()
    var address = ObservableField<String>()
    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var openDeviceList = MutableLiveData<Boolean>()
    var validate = MutableLiveData<Boolean>()
    var isEdit = ObservableField<Boolean>()
    var plotMarker = MutableLiveData<Boolean>()


    init {
        address.set("Fetching address. Please wait...")
        isEdit.set(false)
    }

    fun setData(poi: Poi) {
        isEdit.set(true)
        poiName.set(poi.name)
        lat.set(poi.lat.toString())
        lng.set(poi.lng.toString())
        fetchAddress()
        plotMarker.value = true
    }


    fun fetchAddress() {
        viewModelScope.launch {
            address.set(
                AddressUtil.getAddress(
                    lat.get()!!.toDouble(),
                    lng.get()!!.toDouble(),
                    context
                )
            )
        }
    }


    fun openDeviceList() {
        openDeviceList.value = true
    }

    fun validate() {
        validate.value = true
    }


    fun savePoi(): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("name", poiName.get()!!)
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("lat", lat.get()!!)
        jsonObject.addProperty("lng", lng.get()!!)
        if (isEdit.get()!!) {
            jsonObject.addProperty("favPlaceId", poiId)
        }

        return if (isEdit.get()!!) poiRepository.updatePoi(jsonObject) else poiRepository.addPoi(
            jsonObject
        )
    }


}