/*
 * *
 *  * Created by Surajit on 23/5/20 2:50 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 4:30 PM
 *  *
 */

package com.bt.kotlindemo.ui.dashboard.device_list_view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.ListItemDeviceListBinding
import com.bt.kotlindemo.databinding.ListItemDeviceListNewBinding
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.ui.device_detail.DeviceFeaturesAdapter
import com.bt.kotlindemo.utils.AddressUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ListViewDevicesAdapter(
    private val mListener: GroupRecyclerClickListener? = null
) : RecyclerView.Adapter<ListViewDevicesAdapter.ViewHolder>() {

    private lateinit var context: Context
    private val diffUtil = object : DiffUtil.ItemCallback<Device>() {
        override fun areItemsTheSame(oldItem: Device, newItem: Device):
                Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Device, newItem: Device):
                Boolean {
            return oldItem == newItem
        }
    }


    private val asyncListDiffer = AsyncListDiffer(this, diffUtil)

    override fun getItemCount(): Int {
        return asyncListDiffer.currentList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val binding: ListItemDeviceListNewBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_device_list_new,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.device = asyncListDiffer.currentList[position]

        asyncListDiffer.currentList[position]?.accessories?.filter {
            it.datatypeOfReceivingValue == 1
        }?.let {
            val adapter = DeviceFeaturesAdapter()
            holder.binding.featuresList.adapter = adapter
            adapter.features = it
        }

        if (asyncListDiffer.currentList[position].address.isEmpty()) {
            fetchAddress(asyncListDiffer.currentList[position].lat, asyncListDiffer.currentList[position].lang, holder, position)
        } else {
            holder.binding.txtAddress.text = asyncListDiffer.currentList[position].address
        }


        holder.binding.layout.setOnClickListener {
            mListener?.onClick(asyncListDiffer.currentList[position], 0)
        }


    }

    private fun fetchAddress(lat: Double, lang: Double, holder: ViewHolder, position: Int) {
        holder.job = GlobalScope.launch(Dispatchers.IO) {
            val address = AddressUtil.getAddress(lat, lang, context)
            withContext(Dispatchers.Main) {
                asyncListDiffer.currentList[position].address = address?:""
                notifyItemChanged(position)
            }
        }
    }


     fun updateList(devices: List<Device>) {
         asyncListDiffer.submitList(devices)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.job?.cancel()
    }


    inner class ViewHolder(
        val binding: ListItemDeviceListNewBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        var job: Job? = null
    }

}

