/*
 * *
 *  * Created by Surajit on 8/6/20 9:43 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 31/5/20 2:31 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.zone_management.add_zone

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.transition.Slide
import android.transition.Transition
import android.transition.TransitionManager
import android.view.Gravity
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.AppConstants
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.ZoneHelper
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.ActivityAddZoneBinding
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.zone.Zone
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.google_map.Shape
import com.bt.kotlindemo.utils.google_map.ShapeHelper
import com.bt.kotlindemo.utils.google_map.ShapeListener
import com.bt.kotlindemo.utils.reObserver
import com.bt.kotlindemo.utils.roundTo
import com.bt.kotlindemo.utils.toListLatLng
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.bt.kotlindemo.utils.quickpermissions_kotlin.runWithPermissions
import com.bt.kotlindemo.utils.location.AirLocation
import org.koin.androidx.viewmodel.ext.android.viewModel


class AddNewZoneActivity : BaseActivity(), ShapeListener {


    private val viewModel: AddZoneViewModel by viewModel()
    private lateinit var binding: ActivityAddZoneBinding
    private var mMap: GoogleMap? = null
    private lateinit var marker: Marker
    private var devices: ArrayList<Device>? = null
    private lateinit var shapeHelper: ShapeHelper


    private var airLocation: AirLocation? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        makeStatusBarTransparent()
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_zone)
        binding.apply {
            viewModel = this@AddNewZoneActivity.viewModel
            lifecycleOwner = this@AddNewZoneActivity

        }
        toggle(false, binding.doneLayout)
    }

    @Suppress("UNCHECKED_CAST")
    override fun initViews() {
        devices = intent.getSerializableExtra(IntentConstants.DATA) as? ArrayList<Device>
        setSpinner()
        setMap()
        setObservers()

    }

    private fun setSpinner() {
        binding.etNotifyWhen.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View,
                position: Int,
                id: Long
            ) {
                if (position == 0) {
                    viewModel.notifyWhen.value = null
                } else {
                    viewModel.notifyWhen.value = binding.etNotifyWhen.selectedItem as String
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        }

        binding.etNotifyOn.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parentView: AdapterView<*>?,
                selectedItemView: View,
                position: Int,
                id: Long
            ) {
                if (position == 0) {
                    viewModel.notifyOn.value = null
                } else {
                    viewModel.notifyOn.value = binding.etNotifyOn.selectedItem as String
                }
            }

            override fun onNothingSelected(parentView: AdapterView<*>?) {
                // your code here
            }
        }
    }


    private fun requestPermission() =
        runWithPermissions(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            options = quickPermissionsOption
        ) {
            fetchLocation()
        }


    private fun setMap() {
        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            mMap = it
            setMapLoadedCallback()

        }
    }

    private fun setMapLoadedCallback() {
        mMap!!.setOnMapLoadedCallback {
            shapeHelper = ShapeHelper(mMap!!, this@AddNewZoneActivity, this)
            if (intent.getBooleanExtra(IntentConstants.EDIT, false)) {
                viewModel.setData(intent.getSerializableExtra(IntentConstants.ZONE) as Zone)
                if (viewModel.zone.value!!.shape == 0) {
                    viewModel.circle.value = true
                    viewModel.polygon.value = false
                    shapeHelper.drawCircle(
                        LatLng(
                            viewModel.zone.value!!.loc[0].lat,
                            viewModel.zone.value!!.loc[0].lng
                        ), viewModel.zone.value!!.radius
                    )
                } else {
                    viewModel.polygon.value = true
                    viewModel.circle.value = false
                    shapeHelper.drawPolygon(
                        shapeHelper.getShapeId(),
                        viewModel.zone.value!!.loc.toListLatLng(),
                        viewModel.zone.value!!.loc.toListLatLng()
                    )
                }
                binding.etNotifyOn.setSelection(
                    resources.getStringArray(R.array.notify_on).toList().indexOf(
                        (ZoneHelper.getNotifyOnText(
                            viewModel.zone.value?.notifyWith ?: 0
                        ))
                    )
                )
                binding.etNotifyWhen.setSelection(
                    resources.getStringArray(R.array.notify_on).toList().indexOf(
                        (ZoneHelper.getNotifyWhenText(
                            viewModel.zone.value?.notifyAction ?: 0
                        ))
                    )
                )


            } else {
                viewModel.circle.value = true
                if (intent.getDoubleExtra(IntentConstants.LAT, 0.0) != 0.0
                    && intent.getDoubleExtra(IntentConstants.LNG, 0.0) != 0.0
                ) {
                    viewModel.lat.set(intent.getDoubleExtra(IntentConstants.LAT, 0.0).toString())
                    viewModel.lng.set(intent.getDoubleExtra(IntentConstants.LNG, 0.0).toString())
                    addMarker()

                } else {
                    requestPermission()
                }
            }
        }
    }


    private fun setObservers() {
        viewModel.openDeviceList.observe(this, Observer {

        })

        viewModel.validate.observe(this, Observer {
            if (viewModel.zoneName.get().isNullOrEmpty()) {
                binding.etName.error = "Please enter zone name"
                return@Observer
            }

            if (viewModel.notifyWhen.value.isNullOrEmpty()) {
                showSnackBar(binding.root, "Please select when you want to get notified")
                return@Observer
            }
            if (viewModel.notifyOn.value.isNullOrEmpty()) {
                showSnackBar(binding.root, "Please select where you want to get notified")
                return@Observer
            }
            val latLng = arrayListOf<LatLng>()
            var radius = 0.0
            if (viewModel.circle.value!!) {
                radius = shapeHelper.getCircle()!!.radius.roundTo(2)
                latLng.add(shapeHelper.getCircle()!!.center)
            } else {
                latLng.addAll(shapeHelper.getPolygon()!!.points)
            }

            saveAndObserve(radius, latLng)

        })

        viewModel.done.observe(this, Observer {
            toggle(true, binding.infoLayout)
            toggle(false, binding.doneLayout)
        })

        viewModel.modify.observe(this, Observer {
            toggle(false, binding.infoLayout)
            toggle(true, binding.doneLayout)
        })

        viewModel.clear.observe(this, Observer {
            shapeHelper.clearAll()
            toggle(false, binding.doneLayout)
        })
        viewModel.pattern.observe(this, Observer {
            toggle(false, binding.doneLayout)
            toggle(false, binding.infoLayout)
            shapeHelper.isDrawing = false
            if (viewModel.circle.value!!) {
                shapeHelper.shape = Shape.POLYGON
                viewModel.circle.value = false
                viewModel.polygon.value = true
            } else {
                shapeHelper.shape = Shape.CIRCLE
                viewModel.circle.value = true
                viewModel.polygon.value = false
            }

        })

        viewModel.onBackPressed.observe(this, Observer {
            finish()
        })

    }

    private fun toggle(show: Boolean, view: View) {
        val transition: Transition = Slide(Gravity.BOTTOM)
        transition.duration = 600
        transition.addTarget(view)
        TransitionManager.beginDelayedTransition(binding.parentView, transition)
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun saveAndObserve(
        radius: Double,
        latLng: ArrayList<LatLng>
    ) {
        viewModel.addZone(radius, latLng).reObserver(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(this@AddNewZoneActivity, it.message!!, null)
                }
                ApiResult.Status.SUCCESS -> {
                    setResult(Activity.RESULT_OK)
                    AlertHelper.showOkAlertFinish(this@AddNewZoneActivity, it.data!!.result.msg)
                }
                else -> return@Observer
            }
        })
    }


    private fun addMarker() {
        mMap!!.clear()
        mMap?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(viewModel.lat.get()!!.toDouble(), viewModel.lng.get()!!.toDouble()),
                AppConstants.MAP_DEFAULT_ZOOM
            )
        )
    }


    private fun fetchLocation() {
        airLocation = AirLocation(this, object : AirLocation.Callback {

            override fun onSuccess(locations: ArrayList<Location>) {
                viewModel.lat.set(locations[0].latitude.toString())
                viewModel.lng.set(locations[0].longitude.toString())
                addMarker()
            }

            override fun onFailure(locationFailedEnum: AirLocation.LocationFailedEnum) {
                showSnackBar(
                    binding.root,
                    "Could not fetch your current location"
                )
            }

        }, true)
        airLocation?.start()


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        airLocation?.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        airLocation?.onActivityResult(requestCode, resultCode, data)
    }


    override fun completed() {
        toggle(true, binding.doneLayout)
    }

    override fun updated(areaInSqFt: Double) {

    }


}
