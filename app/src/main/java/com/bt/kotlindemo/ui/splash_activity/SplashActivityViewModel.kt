package com.bt.kotlindemo.ui.splash_activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.data.network.netwrok_responses.AppMenuResponse
import com.bt.kotlindemo.data.network.netwrok_responses.BaseUrlResponse
import com.bt.kotlindemo.data.repository.AppDataRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.launch

class SplashActivityViewModel(
    var preference: AppPreferences,
    private var repository: AppDataRepository
) : BaseViewModel() {

    val token = MutableLiveData<String>()

    fun getBaseUrl(): LiveData<ApiResult<BaseUrlResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("accountId", BuildConfig.ACCOUNT_ID)
        return repository.getBaseUrl(jsonObject)
    }

    fun isRegistered(): Boolean {
        return preference.isRegistered()
    }

    fun getAppMenu(): LiveData<ApiResult<AppMenuResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("deviceToken", token.value)
        return repository.getAppMenu(jsonObject)
    }

    fun getToken() {
        viewModelScope.launch {
            val token = repository.getToken()
            this@SplashActivityViewModel.token.value = token ?: ""
        }
    }


}