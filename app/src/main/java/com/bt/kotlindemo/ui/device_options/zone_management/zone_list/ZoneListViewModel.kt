/*
 * *
 *  * Created by Surajit on 23/5/20 2:53 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/5/20 4:35 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.zone_management.zone_list

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.repository.ZoneRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.zone.Zone
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject

class ZoneListViewModel(
    private val preference: AppPreferences,
    private var repository: ZoneRepository,
    private val context: Context
) : BaseViewModel() {

    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var addZone = MutableLiveData<Boolean>()
    var isEdit = ObservableField<Boolean>()
    var list = MutableLiveData<List<Zone>>()
    var search = MutableLiveData<Boolean>()
    var isEmpty = MutableLiveData<Boolean>()

    init {
        isEmpty.value=false
    }

    fun search() {
        if (list.value != null)
            search.value = true
    }
    fun addZone(){
        addZone.value=true
    }


    fun getZoneList() = repository.getZoneList().map {
        if (it.status == ApiResult.Status.LOADING) {
            isLoading.value = true

        } else if (it.status == ApiResult.Status.ERROR || it.status == ApiResult.Status.CUSTOM) {
            if (it.status == ApiResult.Status.ERROR)
                showMessage.value = it.message?:"" else showMessage.value = it.message?:""
            isLoading.value = false

        } else {
            isLoading.value = false
            list.value = it.data!!.zones
            isEmpty.value=it.data.zones.isEmpty()
        }


    }

    fun deleteZone(id: String): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("zoneId", id)
        return repository.deleteZone(jsonObject)
    }


}