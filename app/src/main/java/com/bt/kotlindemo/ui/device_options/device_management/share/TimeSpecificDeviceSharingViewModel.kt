/*
 * *
 *  * Created by Surajit on 10/4/20 3:08 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 5:21 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.share

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.database.entity.Contacts
import com.bt.kotlindemo.data.repository.DeviceRepository
import com.bt.kotlindemo.data.repository.GroupRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.model.Days
import com.bt.kotlindemo.model.SelectedTime
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.DateTimeUtil
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.google.gson.JsonArray
import com.google.gson.JsonObject

class TimeSpecificDeviceSharingViewModel(
    var preference: AppPreferences,
    private var groupRepository: GroupRepository,
    private var deviceRepository: DeviceRepository
) : BaseViewModel() {


    val openContacts = SingleLiveEvent<Boolean?>()
    val addNumber = SingleLiveEvent<Boolean?>()
    val showPickerTo = SingleLiveEvent<Boolean?>()
    val showPickerFrom = SingleLiveEvent<Boolean?>()
    val selectCountry=SingleLiveEvent<Boolean?>()
    val verify = SingleLiveEvent<Boolean?>()
    val addTime = SingleLiveEvent<Boolean?>()
    val toTimeSelected = ObservableField<String>()
    val fromTimeSelected = ObservableField<String>()
    val country = ObservableField<CountryList>()
    val isFullTime = MutableLiveData<Boolean>().apply { value=true }
    val mobile = ObservableField<String>()
    val from = ObservableField<Long>()
    val to = ObservableField<Long>()
    var showMessage = MutableLiveData<String>()
    var showMessageSuccess = MutableLiveData<String>()
    val showShared = SingleLiveEvent<Void?>()


    init {
        country.set( preference.getCountry())
        from.set(0)
        to.set(0)
    }


    fun shareDevice(
        contacts: ArrayList<Contacts>,
        deviceId: Int,
        days: ArrayList<Days>?=null,
        time: ArrayList<SelectedTime>?=null
    ): LiveData<Boolean> {
        val jsonObject = JsonObject()

        jsonObject.addProperty("sharedbyUserId", preference.getUserId())
        val users = JsonArray()
        contacts.onEach {
            //val user = JsonObject()
            //user.add("mobileNo", it.mobileNo)
            users.add( it.mobileNo)
        }
        jsonObject.add("ShareUsersList", users)

        val devices = JsonArray()
        val device = JsonObject()
        device.addProperty("deviceId", deviceId)
        devices.add(device)
        jsonObject.add("ShareDevicesList", devices)
        if (isFullTime.value == false) {
            jsonObject.addProperty("mon", days!![0].selected)
            jsonObject.addProperty("tue", days[1].selected)
            jsonObject.addProperty("wed", days[2].selected)
            jsonObject.addProperty("thu", days[3].selected)
            jsonObject.addProperty("fri", days[4].selected)
            jsonObject.addProperty("sat", days[5].selected)
            jsonObject.addProperty("sun", days[6].selected)

            val timeSlot=JsonArray()
            time?.onEach {
                val t=JsonObject()
                t.addProperty("fromtime",DateTimeUtil.formatDeviceShare24Hour(it.startTime))
                t.addProperty("totime",DateTimeUtil.formatDeviceShare24Hour(it.endTime))
                timeSlot.add(t)
            }
            jsonObject.add("TimeList",timeSlot)
        }

        return  (if (isFullTime.value == true) deviceRepository.shareDevice(jsonObject) else deviceRepository.shareDeviceTimeSpecific(
            jsonObject
        )).map{
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message?:""
                   false
                }
                else -> {
                    if (it.data == null) {
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"

                    } else {
                       showMessageSuccess.value=it.data.result.msg
                    }
                    false
                }
            }

        }
    }


    fun getDevices() = deviceRepository.getDevicesAdmin()


    fun openContacts() {
        openContacts.call()
    }


    fun changeMode(isFullTime: Boolean) {
        this.isFullTime.value = isFullTime
    }

    fun addTime() {
        addTime.call()
    }

    fun addNumber() {
        addNumber.call()
    }

    fun showPickerFrom() {
        showPickerFrom.call()
    }

    fun showPickerTo() {
        showPickerTo.call()
    }
    fun selectCountry(){
        selectCountry.call()
    }

    fun verify(){
        verify.call()
    }

    fun showShared(){
        showShared.call()
    }


}