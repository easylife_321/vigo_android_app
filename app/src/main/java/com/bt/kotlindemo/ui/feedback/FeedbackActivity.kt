/*
 * *
 *  * Created by Surajit on 7/7/20 4:07 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 7/7/20 4:07 PM
 *  *
 */

package com.bt.kotlindemo.ui.feedback

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.databinding.ActivityFeedbackBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.model.Item
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.PermissionHelper
import com.bt.kotlindemo.utils.compressImageFile
import com.bt.kotlindemo.utils.isValidEmail
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.File

class FeedbackActivity : BaseActivity() {

    private lateinit var binding: ActivityFeedbackBinding
    private val viewModel: FeedbackViewModel by viewModel()
    private lateinit var permissionHelper: PermissionHelper
    private var imageUri: Uri? = null
    private var queryImageUrl: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_feedback)
        binding.apply {
            lifecycleOwner = this@FeedbackActivity
            viewModel = this@FeedbackActivity.viewModel
        }
    }

    override fun initViews() {
        binding.hintText = "Please let us know about your issue"
        attachObservers()
        setRadioGroupEvents()
        setSpinnerClick()
        setList()
    }

    private fun setSpinnerClick() {
        binding.spnType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                viewModel.itemId.value = viewModel.itemsToShow.value?.get(position)?.id
            }
        }
    }

    private fun setList() {
        viewModel.files.value?.apply {
            binding.adapter = SelectedImagesAdapter(this)
        }

    }

    private fun setRadioGroupEvents() {
        binding.rdGroup.setOnCheckedChangeListener { _, _ ->
            viewModel.fetchItem()
            if (binding.radio0.isChecked) {
                viewModel.typeId.value = 1
                binding.hintText = "Please let us know about your issue"
            } else {
                viewModel.typeId.value = 2
                binding.hintText = "Please help us improve by giving your valuable feedback"
            }
        }
    }

    private fun attachObservers() {
        viewModel.requestPermission.observe(this, Observer {
            permissionHelper.requestPermission(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                IntentRequestConstants.STORAGE_PERMISSION
            )
        })
        viewModel.onBackPressed.observe(this, Observer {
            finish()
        })
        viewModel.isLoading.observe(this, Observer {
            if (it) showDialog() else dismissDialog()
        })

        viewModel.showMessage.observe(this, Observer {
            AlertHelper.showOkAlert(this@FeedbackActivity, it)
        })

        viewModel.showMessageFinish.observe(this, Observer {
            AlertHelper.showOkAlertFinish(this@FeedbackActivity, it)
        })

        viewModel.pickImage.observe(this, Observer {
            //checkStoragePermission()
            if (viewModel.files.value?.size ?: 0 >= 3) {
                showSnackBar(binding.root, "One 3 images are allowed")
            } else {
                chooseImage()
            }
        })

        viewModel.isStoragePermissionAllowed.observe(this, Observer {
            if (it) {
                chooseImage()
            } else {
                AlertHelper.showOKCancelAlert(this@FeedbackActivity,
                    getString(R.string.storage_permission),
                    object : AlertCallback {
                        override fun action(isOk: Boolean, data: Any?) {
                            if (isOk)
                                viewModel.requestPermission.call()
                        }
                    })
            }

        })

        viewModel.itemsToShow.observe(this, Observer {
            it?.apply {
                setList(it)
            }

        })

        viewModel.submit.observe(this, Observer {
            verify()
        })

        viewModel.getFeedBackItems().observe(this, Observer {

        })


    }

    private fun verify() {
        if (viewModel.message.value.isNullOrEmpty()) {
            showSnackBar(binding.root, "Please enter a message before you submit")
            return
        }
        if (viewModel.typeId.value == 0) {
            showSnackBar(binding.root, "Please select a feature")
            return
        }

        if (viewModel.email.value?.isValidEmail() == false) {
            showSnackBar(binding.root, "Please enter your valid email id")
            return
        }

        viewModel.uploadData().observe(this, Observer {

        })

    }

    private fun setList(items: List<Item>) {
        binding.spnType.adapter = ItemsSpinnerAdapter(items)
    }

    private fun checkStoragePermission() {
        permissionHelper.checkPermission(
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
            IntentRequestConstants.STORAGE_PERMISSION
        )
    }

    private fun chooseImage() {
        startActivityForResult(getPickImageIntent(), 100)
    }


    private fun getPickImageIntent(): Intent {
        return Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

    }

    private fun handleImageRequest(data: Intent?) {
        val exceptionHandler = CoroutineExceptionHandler { _, t ->
            t.printStackTrace()
            dismissDialog()
            showToast(t.localizedMessage ?: getString(R.string.server_error))

        }

        GlobalScope.launch(Dispatchers.Main + exceptionHandler) {
            showDialog()
            if (data?.data != null) {     //Photo from gallery
                imageUri = data.data
                queryImageUrl = imageUri?.path!!
                queryImageUrl = compressImageFile(queryImageUrl, false, imageUri!!)
                File(queryImageUrl).apply {
                    viewModel.files.value!!.add(this)
                    binding.adapter!!.notifyItemInserted(viewModel.files.value!!.size - 1)
                }
            } else {
                showToast(getString(R.string.server_error))
            }
            dismissDialog()
        }
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == IntentRequestConstants.STORAGE_PERMISSION) {
            viewModel.requestPermission.call()
        } else {
            handleImageRequest(data)
        }

    }
}