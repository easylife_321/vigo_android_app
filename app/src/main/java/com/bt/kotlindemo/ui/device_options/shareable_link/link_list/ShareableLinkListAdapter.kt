/*
 * *
 *  * Created by Surajit on 25/5/20 8:01 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:26 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.shareable_link.link_list

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemShareableLinkBinding
import com.bt.kotlindemo.interfaces.LinkRecyclerClickListener
import com.bt.kotlindemo.model.shareable_link.ShareableLink
import java.util.*

class ShareableLinkListAdapter(
    val link: ArrayList<ShareableLink>,
    val mListener: LinkRecyclerClickListener
) : RecyclerView.Adapter<ShareableLinkListAdapter.ViewHolder>(), Filterable {


    private var mLink: ArrayList<ShareableLink> = link

    override fun getItemCount(): Int {
        return mLink.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemShareableLinkBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_shareable_link,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.link = mLink[position]
        if (position == mLink.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }
        holder.binding.switchShare.setOnClickListener {
            mListener.onClick(mLink[position], LinkRecyclerClickListener.ENABLE_DISABLE)
        }
        holder.binding.layout.setOnClickListener {
            // setUpPopMenu(holder.binding.imgOptions, mLink[position])
            mListener.onClick(link[position], 0)
        }

    }


    private fun setUpPopMenu(imgOptions: ImageView, link: ShareableLink) {
        val popup = PopupMenu(imgOptions.context, imgOptions, Gravity.END);
        popup.inflate(R.menu.shareable_link_menu);
        if (link.isExpired) {
            popup.menu.findItem(R.id.share).isVisible = false
        }
        popup.setOnMenuItemClickListener {

            false
        }

        popup.show();
    }


    inner class ViewHolder(
        val binding: ListItemShareableLinkBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                mLink = filterResults.values as ArrayList<ShareableLink>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val queryString = charSequence.toString().toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString.isEmpty())
                    link
                else
                    link.filter {
                        it.trackingLinkName.toLowerCase(Locale.getDefault()).contains(queryString)
                    }
                return filterResults
            }
        }
    }

    fun removeDevice(link: ShareableLink) {
        this@ShareableLinkListAdapter.link.remove(link)
        notifyDataSetChanged()
    }

    fun updateLink(link: ShareableLink, isSuccess: Boolean = true) {
        val temp = this@ShareableLinkListAdapter.link.indexOf(link)
        if (isSuccess) {
            this@ShareableLinkListAdapter.link[temp].isActive =
                !this@ShareableLinkListAdapter.link[temp].isActive
        }
        notifyItemChanged(temp)
    }
}




