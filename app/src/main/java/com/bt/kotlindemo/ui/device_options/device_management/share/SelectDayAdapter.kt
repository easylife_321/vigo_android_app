/*
 * *
 *  * Created by Surajit on 23/5/20 2:50 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 4:30 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.share

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemDaysBinding
import com.bt.kotlindemo.databinding.ListItemSelectedTimeBinding
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.model.Days
import com.bt.kotlindemo.model.SelectedTime
import java.util.*

class SelectDayAdapter(
) : RecyclerView.Adapter<SelectDayAdapter.ViewHolder>() {

     var days = ArrayList<Days>()

    init {
        days.add(Days("M",false))
        days.add(Days("T",false))
        days.add(Days("W",false))
        days.add(Days("T",false))
        days.add(Days("F",false))
        days.add(Days("S",false))
        days.add(Days("S",false))
    }


    override fun getItemCount(): Int {
        return days.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemDaysBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_days,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.day = days[position]
        holder.binding.layout.setOnClickListener {
            days[position].selected = !days[position].selected
            notifyItemChanged(position)
        }

    }



    inner class ViewHolder(
        val binding: ListItemDaysBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }


    fun isAnySelected():Boolean{
        days.onEach {
            if(it.selected) return true
        }
        return false
    }


}

