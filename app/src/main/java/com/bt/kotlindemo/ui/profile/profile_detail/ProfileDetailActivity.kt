/*
 * *
 *  * Created by Surajit on 7/7/20 4:07 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 7/7/20 4:07 PM
 *  *
 */

package com.bt.kotlindemo.ui.profile.profile_detail

import android.content.Intent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.constants.ProfileOption
import com.bt.kotlindemo.databinding.ActivityProfileDetailBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.ProfileOptions
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.contacts.ContactListActivity
import com.bt.kotlindemo.ui.feedback.FeedbackActivity
import com.bt.kotlindemo.ui.profile.change_mobile_number.ChangeMobileNumberActivity
import com.bt.kotlindemo.ui.profile.update_profile.UpdateProfileActivity
import com.bt.kotlindemo.ui.splash_activity.SplashActivity
import com.bt.kotlindemo.utils.AlertHelper
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileDetailActivity : BaseActivity() {

    private lateinit var binding: ActivityProfileDetailBinding
    private val viewModel: ProfileDetailViewModel by viewModel()


    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_detail)
        binding.apply {
            lifecycleOwner = this@ProfileDetailActivity
            viewModel = this@ProfileDetailActivity.viewModel

        }
    }

    override fun initViews() {
        binding.adapter =
            ProfileOptionsListAdapter(
                ProfileOption.getOption(viewModel.preference.getDnd()),
                object : RecyclerClickListener {
                    override fun onClick(objects: Any) {
                        when ((objects as ProfileOptions).id) {
                            ProfileOption.FEEDBACK -> {
                                startActivity(
                                    Intent(
                                        this@ProfileDetailActivity,
                                        FeedbackActivity::class.java
                                    )
                                )
                            }

                            ProfileOption.EDIT_PROFILE -> {
                                startActivityForResult(
                                    Intent(
                                        this@ProfileDetailActivity,
                                        UpdateProfileActivity::class.java
                                    ),
                                    IntentRequestConstants.UPDATE_PROFILE
                                )
                            }

                            ProfileOption.CHANGE_MOBILE_NO -> {
                                startActivityForResult(
                                    Intent(
                                        this@ProfileDetailActivity,
                                        ChangeMobileNumberActivity::class.java
                                    ),
                                    IntentRequestConstants.CHANGE_MOBILE_NUMBER
                                )


                            }

                            ProfileOption.LOGOUT -> {
                                AlertHelper.showOKCancelAlert(
                                    this@ProfileDetailActivity,
                                    "Are you sure you want to logout?",
                                    object : AlertCallback {
                                        override fun action(isOk: Boolean, data: Any?) {
                                            if (isOk)
                                                viewModel.logout()
                                                    .observe(this@ProfileDetailActivity) {}
                                        }
                                    })

                            }

                            ProfileOption.DO_NOT_DISTURB -> {
                                val msg =
                                    if (viewModel.preference.getDnd()) "Are you sure you want to turn of DND for your account?" else "Do you want to set your account to Do Not Disturb mode? You will not be notified for any event. To get notifications again please turn off this feature. "
                                AlertHelper.showOKCancelAlert(
                                    this@ProfileDetailActivity,
                                    msg,
                                    object : AlertCallback {
                                        override fun action(isOk: Boolean, data: Any?) {
                                            if (isOk)
                                                viewModel.dnd()
                                                    .observe(this@ProfileDetailActivity) {
                                                    }
                                        }
                                    })

                            }

                            ProfileOption.MY_CONTACTS -> {
                                startActivityForResult(
                                    Intent(
                                        this@ProfileDetailActivity,
                                        ContactListActivity::class.java
                                    ),
                                    IntentRequestConstants.CHANGE_MOBILE_NUMBER
                                )


                            }

                            ProfileOption.SHARE_APP -> {

                            }

                            ProfileOption.CHANGE_PASSWORD -> {
                                AlertHelper.showOKCancelAlert(
                                    this@ProfileDetailActivity,
                                    "Changing your password will log you out of your account. Are you sure you want to proceed?",
                                    object : AlertCallback {
                                        override fun action(isOk: Boolean, data: Any?) {
                                            if (isOk) {
                                                AlertHelper.showChangePasswordAlert(this@ProfileDetailActivity,
                                                    callBack = object : AlertCallback {
                                                        override fun action(
                                                            isOk: Boolean,
                                                            data: Any?
                                                        ) {
                                                            if (isOk) {
                                                                viewModel.changePassword(data.toString())
                                                                    .observe(this@ProfileDetailActivity) {}
                                                            }
                                                        }
                                                    }
                                                )
                                            }
                                        }
                                    }
                                )
                            }
                        }
                    }
                })
        attachObservers()

    }

    private fun attachObservers() {
        viewModel.onBackPressed.observe(this, Observer {
            finish()
        })

        viewModel.showMessage.observe(this) {
            AlertHelper.showOkAlert(this, it)
        }

        viewModel.isLoading.observe(this) {
            if (it) showDialog() else dismissDialog()
        }

        viewModel.logout.observe(this) {
            finishAffinity()
            startActivity(Intent(this, SplashActivity::class.java))
        }

        viewModel.updateView.observe(this) {
            binding.adapter!!.setDnd(viewModel.preference.getDnd())
        }

        viewModel.passwordChanged.observe(this) {
            if (it) {
                viewModel.logout()
                    .observe(this@ProfileDetailActivity) {}
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                IntentRequestConstants.CHANGE_MOBILE_NUMBER -> {
                    viewModel.mobile.value =
                        "+${viewModel.preference.getCountry().countryCode}${viewModel.preference.getMobile()}"
                }
            }
            when (requestCode) {
                IntentRequestConstants.UPDATE_PROFILE -> {
                    viewModel.initData()
                }
            }
        }
    }


}