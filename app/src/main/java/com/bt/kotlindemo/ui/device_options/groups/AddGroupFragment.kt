/*
 * *
 *  * Created by Surajit on 12/9/20 11:46 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 12/9/20 11:46 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.groups

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.databinding.FragmentAddGroupBinding
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.ui.base.BaseFragment
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class AddGroupFragment : BaseFragment() {

    private val viewModel: GroupManagementActivityViewModel by sharedViewModel()
    private lateinit var binding: FragmentAddGroupBinding
    private var group: Group? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_add_group,
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        group = arguments?.getSerializable(IntentConstants.GROUP) as? Group
        group?.apply {
            binding.txtTitle.text = resources.getString(R.string.edit_group)
            binding.etGroupName.setText(name)
            binding.btnOk.text = resources.getString(R.string.update)
        }
        setViews()
        addObservers()
    }

    private fun addObservers() {
        viewModel.closeFragment.observe(viewLifecycleOwner, Observer {
            parentFragmentManager.popBackStack()
        })
    }

    private fun setViews() {
        binding.btnCancel.setOnClickListener {
            parentFragmentManager.popBackStack()
        }

        binding.btnOk.setOnClickListener {
            if (binding.etGroupName.text.toString().trim().isNotEmpty()) {
                if (group == null) {
                    viewModel.create(binding.etGroupName.text.toString().trim())
                } else {
                    viewModel.update(binding.etGroupName.text.toString().trim())
                }
                parentFragmentManager.popBackStack()
            } else {
                Snackbar.make(binding.root, "Please enter group name", Snackbar.LENGTH_SHORT).show()
            }
        }

    }

    companion object {
        @JvmStatic
        fun newInstance(group: Group?) =
            AddGroupFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(IntentConstants.GROUP, group)
                }
            }
    }


}