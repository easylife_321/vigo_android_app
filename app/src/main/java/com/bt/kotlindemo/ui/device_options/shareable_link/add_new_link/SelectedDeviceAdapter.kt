/*
 * *
 *  * Created by Surajit on 31/5/20 5:35 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 31/5/20 11:47 AM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.shareable_link.add_new_link

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemSelectedDevicesBinding
import com.bt.kotlindemo.data.database.entity.Device
import java.util.*

class SelectedDeviceAdapter(
    private var devices: ArrayList<Device>
) : RecyclerView.Adapter<SelectedDeviceAdapter.ViewHolder>() {



    override fun getItemCount(): Int {
        return devices.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemSelectedDevicesBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_selected_devices,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.data = devices[position]
        if (position == devices.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }


        holder.binding.deleteLayout.setOnClickListener {
            ((it.context) as AddNewLinkActivity).remove(devices[position])
        }


    }


    override fun onViewRecycled(holder: ViewHolder) {
        holder.itemView.setOnLongClickListener(null)
        super.onViewRecycled(holder)
    }

    fun setDevices(devices: ArrayList<Device>) {
        this@SelectedDeviceAdapter.devices.clear()
        this@SelectedDeviceAdapter.devices = devices
        notifyDataSetChanged()
    }


    inner class ViewHolder(
        val binding: ListItemSelectedDevicesBinding
    ) : RecyclerView.ViewHolder(binding.root)


}

