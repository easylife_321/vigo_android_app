/*
 * *
 *  * Created by Surajit on 25/4/20 12:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/4/20 6:18 PM
 *  *
 */

package com.bt.kotlindemo.ui.feedback

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.repository.SettingsRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Item
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.MiscUtil
import com.bt.kotlindemo.utils.SingleLiveEvent
import okhttp3.MultipartBody
import java.io.File


class FeedbackViewModel(
    private val preference: AppPreferences,
    private var repository: SettingsRepository,
    private val appContext: Application
) : BaseViewModel() {


    var isLoaded = ObservableBoolean()

    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var showMessageFinish = MutableLiveData<String>()
    var files = MutableLiveData<ArrayList<File>>().apply { value = arrayListOf() }
    var message = MutableLiveData<String>()
    var email = MutableLiveData<String>()
    var pickImage = SingleLiveEvent<Void?>()
    var typeId = MutableLiveData<Int>().apply { value = 0 }
    var itemId = MutableLiveData<Int>().apply { value = 0 }
    var isStoragePermissionAllowed = MutableLiveData<Boolean>()
    var submit = SingleLiveEvent<Void?>()
    var items = MutableLiveData<List<Item>>()
    var itemsToShow = MutableLiveData<List<Item>>()


    init {
        email.value = preference.getEmail()
    }


    fun getFeedBackItems(): LiveData<Unit> {
        return repository.getFeedBackItems().map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    isLoading.value = false
                }
                else -> {
                    if (it.data == null) {
                        isLoading.value = false
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"

                    } else {
                        isLoading.value = false
                        items.value = it.data.item ?: emptyList()
                        fetchItem()
                    }
                }
            }


        }

    }


    fun uploadData(): LiveData<Unit> {

        val parts: ArrayList<MultipartBody.Part> = ArrayList()
        // Multiple Images
        for (i in files.value!!.indices) {
            parts.add(MiscUtil.prepareFilePart("file_$i", files.value!![i]))
        }
        val userId = MiscUtil.prepareBodyPart(preference.getUserId().toString())
        val typeId = MiscUtil.prepareBodyPart(preference.getUserId().toString())
        val message = MiscUtil.prepareBodyPart(preference.getUserId().toString())
        val itemId = MiscUtil.prepareBodyPart(preference.getUserId().toString())
        return repository.feedback(userId, typeId, message, itemId, parts).map{
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    isLoading.value = false
                }
                else -> {
                    if (it.data == null) {
                        isLoading.value = false
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"

                    } else {
                        isLoading.value = false
                        showMessageFinish.value = it.data.result.msg
                    }
                }
            }


        }

    }

    fun fetchItem() {
        val list = items.value?.filter {
            it.typeId == typeId.value
        }?.toMutableList()
        list?.add(Item(0, "Select Feature", 0))
        list?.apply {
            itemsToShow.value = this
        }

    }

    fun pickImage() {
        pickImage.call()
    }

    fun submit() {
        submit.call()
    }


    override fun onCleared() {
        super.onCleared()
    }


}