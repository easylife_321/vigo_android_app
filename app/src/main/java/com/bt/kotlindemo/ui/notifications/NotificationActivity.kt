/*
 * *
 *  * Created by Surajit on 7/7/20 4:07 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 7/7/20 4:07 PM
 *  *
 */

package com.bt.kotlindemo.ui.notifications

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.data.database.entity.Notification
import com.bt.kotlindemo.databinding.ActivityNotificationsBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.model.NotificationTypeModel
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.PermissionHelper
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class NotificationActivity : BaseActivity() {

    private lateinit var binding: ActivityNotificationsBinding
    private val viewModel: NotificationViewModel by viewModel()
    private lateinit var permissionHelper: PermissionHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_notifications)
        binding.apply {
            lifecycleOwner = this@NotificationActivity
            viewModel = this@NotificationActivity.viewModel
        }

    }

    override fun initViews() {
        attachObservers()
        setSpinner()

    }

    private fun setSpinner() {
        binding.spnType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                viewModel.selectedNotificationType.value = viewModel.notificationType.value?.get(p2)
                viewModel.filterData(false)
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {

            }
        }
    }

    private fun attachObservers() {

        viewModel.onBackPressed.observe(this) {
            finish()
        }


        viewModel.isLoading.observe(this) {
            if (it) showDialog() else dismissDialog()
        }

        viewModel.showToast.observe(this) {
            showToast(it)
        }
        viewModel.showMessage.observe(this) {
            AlertHelper.showOkAlert(this@NotificationActivity, it)
        }


        viewModel.notifications.observe(this) {
            it?.apply {
                setList(this)
            }

        }

        viewModel.notificationType.observe(this) {
            it?.apply {
                setType(it)
            }
        }

        viewModel.getNotifications(intent.getIntExtra(IntentConstants.DEVICE_ID, 0)).observe(this) {
            viewModel.rawNotificationList.value = it
            viewModel.filterData(true)
        }

        viewModel.deleteConfirmation.observe(this) {
            AlertHelper.showOKCancelAlert(
                this@NotificationActivity,
                "Are you sure you want to completely delete all the notifications of the selected type. Once deleted the notification will permanently lost. Do you want to proceed?",
                object : AlertCallback {
                    override fun action(isOk: Boolean, data: Any?) {
                        if (isOk) {

                            viewModel.delete(intent.getIntExtra(IntentConstants.DEVICE_ID, 0))

                        }
                    }
                })

        }

    }

    private fun setType(list: List<String>) {
        binding.typeAdapter = NotificationTypeAdapter(list)
    }


    private fun setList(it: List<Notification>) {
        binding.adapter = NotificationAdapter(it)
    }


}