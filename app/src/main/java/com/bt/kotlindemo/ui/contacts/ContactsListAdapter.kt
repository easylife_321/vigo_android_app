/*
 * *
 *  * Created by Surajit on 25/5/20 8:01 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:26 PM
 *  *
 */

package com.bt.kotlindemo.ui.contacts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.data.database.entity.Contacts
import com.bt.kotlindemo.databinding.ListItemContactsBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import java.util.*
import kotlin.collections.ArrayList

class ContactsListAdapter(
    val isSelectable: Boolean,
    val contacts: ArrayList<Contacts>,
    val mListener: RecyclerClickListener
) : RecyclerView.Adapter<ContactsListAdapter.ViewHolder>(), Filterable {


    private var mContacts: ArrayList<Contacts> = contacts

    override fun getItemCount(): Int {
        return mContacts.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemContactsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_contacts,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.contact = mContacts[position]
        holder.binding.txtName.text = mContacts[position].phoneName
        holder.binding.txtMobile.text = "+${mContacts[position].mobileNo}"
        holder.binding.isSelectable=isSelectable
        if (position == mContacts.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }
        holder.binding.layout.setOnClickListener {
            mListener.onClick(mContacts[position])
        }
        holder.binding.chck.setOnClickListener {
            mContacts[position].isSelected=! mContacts[position].isSelected
           /*  contacts.find {
                it.mobileNo==mContacts[position].mobileNo
            }*/
            notifyItemChanged(position)
        }


    }

    inner class ViewHolder(
        val binding: ListItemContactsBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                mContacts = filterResults.values as ArrayList<Contacts>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val queryString = charSequence.toString().toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString.isEmpty())
                    contacts
                else
                    contacts.filter {
                        it.phoneName.toLowerCase(Locale.getDefault()).contains(queryString)
                    }
                return filterResults
            }
        }
    }


    fun getSelected():ArrayList<Contacts>{
        val list= arrayListOf<Contacts>()
        mContacts.onEach {
            if(it.isSelected) list.add(it)
        }
        return list
    }


}




