/*
 * *
 *  * Created by Surajit on 23/5/20 2:53 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/5/20 4:35 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.zone_management.add_zone

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.constants.ZoneHelper
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.repository.ZoneRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.zone.Zone
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AddressUtil
import com.bt.kotlindemo.utils.AppPreferences
import com.google.android.gms.maps.model.LatLng
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

class AddZoneViewModel(
    private val preference: AppPreferences,
    private var repository: ZoneRepository,
    private val context: Context
) : BaseViewModel() {

    var zoneName = ObservableField<String>()
    var lat = ObservableField<String>()
    var lng = ObservableField<String>()
    var deviceName = ObservableField<String>()
    var address = ObservableField<String>()
    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var openDeviceList = MutableLiveData<Boolean>()
    var validate = MutableLiveData<Boolean>()
    var isEdit = ObservableField<Boolean>()
    var modify = MutableLiveData<Boolean>()
    var clear = MutableLiveData<Boolean>()
    var done = MutableLiveData<Boolean>()
    var circle = MutableLiveData<Boolean>()
    var polygon = MutableLiveData<Boolean>()
    var pattern = MutableLiveData<Boolean>()
    var notifyWhen = MutableLiveData<String>()
    var notifyOn = MutableLiveData<String>()
    var zone = MutableLiveData<Zone>()


    init {
        address.set("Fetching address. Please wait...")
        isEdit.set(false)
        circle.value = true
        polygon.value = false
        zoneName.set("")
        notifyWhen.value = ""
        notifyOn.value = ""
    }

    fun setData(zone: Zone) {
        this.zone.value = zone
        isEdit.set(true)
        zoneName.set(zone.name)
        notifyWhen.value = ZoneHelper.getNotifyWhenText(zone.notifyAction)
        notifyOn.value = ZoneHelper.getNotifyOnText(zone.notifyWith)
    }


    fun fetchAddress() {
        viewModelScope.launch {
            address.set(
                AddressUtil.getAddress(
                    lat.get()!!.toDouble(),
                    lng.get()!!.toDouble(),
                    context
                )
            )
        }
    }


    fun openDeviceList() {
        openDeviceList.value = true
    }

    fun validate() {
        validate.value = true
    }

    fun clear() {
        clear.value = true
    }

    fun modify() {
        modify.value = true
    }

    fun done() {
        done.value = true
    }

    fun patternChange() {
        pattern.value = true
    }


    override fun onCleared() {
        super.onCleared()

    }

    fun addZone(
        radius: Double,
        latLng: ArrayList<LatLng>
    ): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("name", zoneName.get()!!)
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("notifyAction", ZoneHelper.getNotifyWhen(notifyWhen.value!!))
        jsonObject.addProperty("notifyWith", ZoneHelper.getNotifyOn(notifyOn.value!!))
        jsonObject.addProperty("shape", if (circle.value!!) 0 else 1)
        jsonObject.addProperty("radius", radius.roundToInt())
        val arr = JsonArray()
        latLng.forEach {
            val obj = JsonObject()
            obj.addProperty("lat", it.latitude)
            obj.addProperty("lng", it.longitude)
            arr.add(obj)
        }
        jsonObject.add("geolocations", arr)
        if (isEdit.get()!!) {
            jsonObject.addProperty("zoneId", zone.value!!.zoneId)
        }

        return if (isEdit.get()!!) repository.updateZone(jsonObject) else repository.addZone(
            jsonObject
        )
    }


}