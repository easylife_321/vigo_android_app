package com.bt.kotlindemo.ui.base

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowInsets
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bt.kotlindemo.R
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.utils.ContactSyncPopupListener
import com.bt.kotlindemo.utils.CustomContactSyncPopup
import com.bt.kotlindemo.utils.ProgressHelper
import com.bt.kotlindemo.utils.quickpermissions_kotlin.util.QuickPermissionsOptions
import com.bt.kotlindemo.utils.quickpermissions_kotlin.util.QuickPermissionsRequest
import com.bt.kotlindemo.utils.showPermanentlyDeniedDialog
import com.google.android.material.snackbar.Snackbar
import io.github.rupinderjeet.kprogresshud.KProgressHUD
import java.lang.ref.WeakReference


abstract class BaseActivity : AppCompatActivity() {

    lateinit var activity: WeakReference<Activity>

    private var isFullScreen = false;
    private val progress: KProgressHUD by lazy {
        ProgressHelper.showProgress(this)
    }
    val quickPermissionsOption by lazy {
        QuickPermissionsOptions(
            handleRationale = true,
            rationaleMessage = "Custom rational message",
            permanentlyDeniedMessage = "Custom permanently denied message",
            rationaleMethod = { req -> rationaleCallback(req) },
            permanentDeniedMethod = { req -> permissionsPermanentlyDenied(req) }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = WeakReference(this@BaseActivity)
        if (isUsingNightModeResources() && !isFullScreen) {
            setStatusBarTint(false)
        }
        setBinding()
        initViews()
        //adjustFontScale(resources.configuration)
    }


    abstract fun setBinding()
    abstract fun initViews()
    protected open fun transparentStatusAndNavigation() { //make full transparent statusBar
        isFullScreen = true
        setWindowFlag(
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
            true
        ) //| WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION
        window.decorView.systemUiVisibility = (
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN // | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                )
        setWindowFlag(
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, false
        )
        window.statusBarColor = Color.TRANSPARENT
        //getWindow().setNavigationBarColor(Color.TRANSPARENT);
    }

    private fun setWindowFlag(bits: Int, on: Boolean) {
        val win: Window = window
        val winParams: WindowManager.LayoutParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }


    protected open fun setStatusBarColor(color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.statusBarColor = resources.getColor(color, this.theme)
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = resources.getColor(color)
        }
    }

    protected open fun fullScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }
    }


    @Suppress("SameParameterValue")
    protected open fun setStatusBarTint(shouldChangeStatusBarTintToDark: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val decor = window.decorView
            if (shouldChangeStatusBarTintToDark) {
                decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            } else {
                // We want to change tint color to white again.
                // You can also record the flags in advance so that you can turn UI back completely if
                // you have set other flags before, such as translucent or full screen.
                decor.systemUiVisibility = 0
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }

        return super.onOptionsItemSelected(item)
    }

    protected fun showDialog() {
        if (!progress.isShowing)
            progress.show()
    }

    protected fun dismissDialog() {
        ProgressHelper.dismissProgress(progress)
    }

    protected fun setNoLimits() {
        isFullScreen = true
        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        ) // will remove all window bounds of our activity

    }

    private fun isUsingNightModeResources(): Boolean {
        return when (resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK) {
            Configuration.UI_MODE_NIGHT_YES -> true
            Configuration.UI_MODE_NIGHT_NO -> false
            Configuration.UI_MODE_NIGHT_UNDEFINED -> false
            else -> false
        }
    }

    protected fun slideFromRight() {
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    protected fun slideToRight() {
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    protected fun slideToBottom() {
        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_down);
    }


    fun makeStatusBarTransparent() {
        isFullScreen = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.apply {
                clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    decorView.systemUiVisibility =
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                } else {
                    decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                }
                statusBarColor = Color.TRANSPARENT
            }
        }

    }

    private fun rationaleCallback(req: QuickPermissionsRequest) {

    }

    private fun permissionsPermanentlyDenied(req: QuickPermissionsRequest) {
        showPermanentlyDeniedDialog()
    }

    protected fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    protected fun showSnackBar(view: View, msg: String) {
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT)
            .setBackgroundTint(ContextCompat.getColor(this, R.color.colorPrimary)).show()
    }

    protected fun showToastRetry(view: View, msg: String, retry: ToastRetry) {
        Snackbar.make(view, msg, Snackbar.LENGTH_INDEFINITE)
            .setAction("Retry") {
                retry.retry()
            }
            .setBackgroundTint(ContextCompat.getColor(view.context, R.color.colorPrimary)).show()
    }

    private fun adjustFontScale(configuration: Configuration?) {
        configuration?.let {
            it.fontScale = .8F
            val metrics: DisplayMetrics = resources.displayMetrics
            val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
            wm.defaultDisplay.getMetrics(metrics)
            metrics.scaledDensity = configuration.fontScale * metrics.density

            baseContext.applicationContext.createConfigurationContext(it)
            baseContext.resources.displayMetrics.setTo(metrics)

        }
    }

    fun showContactPermissionRationale(callback: (action: Boolean) -> Unit) {
        val dialog = CustomContactSyncPopup(this, object : ContactSyncPopupListener {
            override fun onContinueBtnPressed() {
                callback(true)
            }

            override fun notNowBtnPressed() {
                callback(false)
            }

            override fun onPolicyBtnPressed() {


            }
        })
        dialog.show()
    }


}
