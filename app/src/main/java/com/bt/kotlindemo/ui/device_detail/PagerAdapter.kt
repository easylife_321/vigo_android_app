/*
 * *
 *  * Created by Surajit on 23/4/20 8:49 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/4/20 8:49 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_detail

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.bt.kotlindemo.custom.SDViewPager

class PagerAdapter(fragmentManager: FragmentManager, private val fragments: ArrayList<Fragment>) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var mCurrentPosition = -1

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun setPrimaryItem(container: ViewGroup, position: Int, `object`: Any) {
        super.setPrimaryItem(container, position, `object`)
        if (position != mCurrentPosition) {
            val fragment =
                `object` as Fragment
            val pager: SDViewPager = container as SDViewPager
            mCurrentPosition = position;
            pager.measureCurrentView(fragment.view)

        }
    }
}