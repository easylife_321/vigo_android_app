/*
 * *
 *  * Created by Surajit on 1/3/21 1:30 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/3/21 1:30 AM
 *  *
 */

package com.bt.kotlindemo.ui.feedback

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemFeedbackItemsBinding
import com.bt.kotlindemo.model.Item

class ItemsSpinnerAdapter(private val items: List<Item>) : BaseAdapter() {
    override fun getItem(p0: Int): Any {
        return items.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val viewHolder: ViewHolder
        val retView: View
        if (convertView == null) {
            val binding: ListItemFeedbackItemsBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_feedback_items, parent, false
            )
            retView = binding.root
            viewHolder = ViewHolder(binding)
            retView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
            retView=convertView
        }
        viewHolder.binding.item=items[position]
        return retView
    }

    private class ViewHolder(var binding: ListItemFeedbackItemsBinding)
}