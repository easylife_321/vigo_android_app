/*
 * *
 *  * Created by Surajit on 14/5/20 11:01 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/4/20 12:05 PM
 *  *
 */

package com.bt.kotlindemo.ui.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemLeftNavMenuBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.app_menu.Menu


class LeftDrawerAdapter(
    private val options: ArrayList<Menu>,
    private val recyclerClickListener: RecyclerClickListener,
     val baseUrl:String
) :
    RecyclerView.Adapter<LeftDrawerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_left_nav_menu,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(options[position],baseUrl)
        holder.binding.layout.setOnClickListener {
            recyclerClickListener.onClick(options[position])
        }

    }

    override fun getItemCount(): Int {
        return options.size
    }

    //the class is hodling the list view
    class ViewHolder(val binding: ListItemLeftNavMenuBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItems(menu: Menu, baseUrl: String) {
            binding.menu = menu
            binding.url=baseUrl
        }
    }
}