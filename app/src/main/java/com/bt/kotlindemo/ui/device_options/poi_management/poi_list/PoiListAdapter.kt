/*
 * *
 *  * Created by Surajit on 25/5/20 8:01 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:26 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.poi_management.poi_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemPoiBinding
import com.bt.kotlindemo.interfaces.PoiRecyclerClickListener
import com.bt.kotlindemo.model.Poi
import com.chauthai.swipereveallayout.ViewBinderHelper
import java.util.*


class PoiListAdapter(
    val poi: ArrayList<Poi>,
    val mListener: PoiRecyclerClickListener
) : RecyclerView.Adapter<PoiListAdapter.ViewHolder>(), Filterable {


    private var mPoi: ArrayList<Poi> = poi
    private val viewBinderHelper = ViewBinderHelper()

    override fun getItemCount(): Int {
        return mPoi.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemPoiBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_poi,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        viewBinderHelper.bind(holder.binding.reveallayout, mPoi[position].id)
        holder.binding.poi = mPoi[position]
        if (position == mPoi.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }
        holder.binding.layout.setOnClickListener {
            mListener.onClick(mPoi[position], PoiRecyclerClickListener.VIEW)
        }
        holder.binding.imgDelete.setOnClickListener {
            mListener.onClick(mPoi[position], PoiRecyclerClickListener.DELETE)
        }

    }

    inner class ViewHolder(
        val binding: ListItemPoiBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                mPoi = filterResults.values as ArrayList<Poi>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val queryString = charSequence.toString().toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString.isEmpty())
                    poi
                else
                    poi.filter {
                        it.name.toLowerCase(Locale.getDefault()).contains(queryString)
                    }
                return filterResults
            }
        }
    }

    fun removeDevice(poi: Poi) {
        this@PoiListAdapter.poi.remove(poi)
        notifyDataSetChanged()
    }
}




