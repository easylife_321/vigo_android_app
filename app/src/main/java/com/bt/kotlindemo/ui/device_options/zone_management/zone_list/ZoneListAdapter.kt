/*
 * *
 *  * Created by Surajit on 25/5/20 8:01 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:26 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.zone_management.zone_list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemZoneBinding
import com.bt.kotlindemo.interfaces.PoiRecyclerClickListener
import com.bt.kotlindemo.interfaces.ZoneRecyclerClickListener
import com.bt.kotlindemo.model.Poi
import com.bt.kotlindemo.model.zone.Zone
import java.util.*

class ZoneListAdapter(
    val zone: ArrayList<Zone>,
    val mListener: ZoneRecyclerClickListener
) : RecyclerView.Adapter<ZoneListAdapter.ViewHolder>(), Filterable {


    private var mZone: ArrayList<Zone> = zone

    override fun getItemCount(): Int {
        return mZone.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemZoneBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_zone,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.zone = mZone[position]
        if (position == mZone.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }
        holder.binding.layout.setOnClickListener {
            mListener.onClick(mZone[position], ZoneRecyclerClickListener.VIEW)
        }
        holder.binding.imgDelete.setOnClickListener {
            mListener.onClick(mZone[position], ZoneRecyclerClickListener.DELETE)
        }

    }

    inner class ViewHolder(
        val binding: ListItemZoneBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                mZone = filterResults.values as ArrayList<Zone>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val queryString = charSequence.toString().toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString.isEmpty())
                    zone
                else
                    zone.filter {
                        it.name.toLowerCase(Locale.getDefault()).contains(queryString)
                    }
                return filterResults
            }
        }
    }

    fun removeDevice(zone: Zone) {
        this@ZoneListAdapter.zone.remove(zone)
        notifyDataSetChanged()
    }
}




