/*
 * *
 *  * Created by Surajit on 31/5/20 11:47 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 31/5/20 11:47 AM
 *  *
 */

package com.bt.kotlindemo.ui.select_device

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.databinding.ActivitySelectDeviceBinding
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.ui.base.BaseActivity
import com.ferfalk.simplesearchview.SimpleSearchView

class SelectDeviceActivity : BaseActivity() {

    private lateinit var binding: ActivitySelectDeviceBinding
    private var devices: ArrayList<Device>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_device)
    }

    override fun initViews() {
        setList()
        setClickListener()
        setSearch()
    }

    private fun setSearch() {
        binding.searchView.setOnSearchViewListener(object : SimpleSearchView.SearchViewListener {
            override fun onSearchViewShownAnimation() {
            }

            override fun onSearchViewClosed() {
                if (binding.adapter != null)
                    binding.adapter!!.filter.filter("")
                binding.toolbar.visibility = View.VISIBLE
                binding.searchView.visibility = View.INVISIBLE
            }

            override fun onSearchViewClosedAnimation() {

            }

            override fun onSearchViewShown() {

            }

        })

        binding.searchView.setOnQueryTextListener(object : SimpleSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (binding.adapter != null) {
                    binding.adapter!!.filter.filter(newText)
                }
                return false
            }

            override fun onQueryTextCleared(): Boolean {
                return false
            }
        })
    }

    override fun onBackPressed() {
        if (binding.searchView.isSearchOpen) {
            binding.searchView.closeSearch(true)
        } else {
            super.onBackPressed()
        }

    }


    private fun setClickListener() {
        binding.btn.setOnClickListener {
            setResult(Activity.RESULT_OK, Intent().apply {
                putExtras(Bundle().apply {
                    putSerializable(IntentConstants.DATA, devices)
                })
            })
            finish()
        }

        binding.imgBack.setOnClickListener {
            finish()
        }
    }

    @Suppress("UNCHECKED_CAST")
    private fun setList() {
        devices = intent.getSerializableExtra(IntentConstants.DATA) as? ArrayList<Device>
        devices?.apply {
            binding.adapter = DeviceAdapter(this)
        }


    }
}
