/*
 * *
 *  * Created by Surajit on 23/5/20 2:50 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 10:03 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.device_list


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.ActivityDeviceManagementNewBinding
import com.bt.kotlindemo.databinding.FragmentDeviceOptionsBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.device_options.device_management.add_device.AddDeviceActivity
import com.bt.kotlindemo.ui.device_options.device_management.share.TimeSpecificDeviceSharingActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.reObserver
import com.ferfalk.simplesearchview.SimpleSearchView
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.koin.androidx.viewmodel.ext.android.viewModel

class DeviceManagementActivity : BaseActivity() {

    private val viewModel: DeviceManagementViewModel by viewModel()
    private var groupList: ArrayList<Group>? = null
    private var selectedGroup: Group? = null
    private var selectedDevice: Device? = null
    private lateinit var binding: ActivityDeviceManagementNewBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        setStatusBarColor(R.color.colorPrimaryDark)
        setStatusBarTint(false)
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_device_management_new)
        binding.apply {
            lifecycleOwner = this@DeviceManagementActivity
            isMyDeviceSelected = true
            viewModel = this@DeviceManagementActivity.viewModel
            handler = ClickHandler()
        }
        setSupportActionBar(binding.toolbar)
        setSearchView()
    }

    override fun initViews() {
        viewModel.isLoaded.observe(this@DeviceManagementActivity, Observer {
            return@Observer
        })
        viewModel.devicesToShow.observe(this, Observer {
            bindDevices(it)
        })
        viewModel.isLoading.observe(this, Observer {
            if (!it)
                dismissDialog()
        })
        viewModel.showMessageRetry.observe(this, Observer {
            showToastRetry(binding.root, it, object : ToastRetry {
                override fun retry() {
                    if (groupList.isNullOrEmpty()) {
                        fetchGroups()
                    } else {
                        viewModel.getAllDevices()
                    }
                }

            })
        })
        viewModel.showMessage.observe(this, Observer {
            showToastRetry(binding.root, it, object : ToastRetry {
                override fun retry() {
                    finish()
                }

            })
        })


        viewModel.showSelected.observe(this, Observer {
            binding.adapter!!.showSelected(it)
        })


        viewModel.delete.observe(this, Observer {
            deleteDevice(
                "Are you sure you want to delete ${selectedDevice?.deviceName} from your account?",
                selectedDevice?.deviceId ?: 0
            )

        })

        viewModel.edit.observe(this, Observer {
            val intent = Intent(this@DeviceManagementActivity, AddDeviceActivity::class.java)
            val bun = Bundle()
            bun.putSerializable(IntentConstants.GROUP, groupList)
            bun.putSerializable(IntentConstants.DEVICE, selectedDevice)
            bun.putBoolean(IntentConstants.EDIT, true)
            intent.putExtras(bun)
            startActivityForResult(intent, IntentRequestConstants.EDIT_DEVICE)
        })

        viewModel.move.observe(this, Observer {
            moveDevice(
                deviceId = selectedDevice?.deviceId ?: 0
            )
        })


        viewModel.share.observe(this, Observer {
            val intent =
                Intent(this@DeviceManagementActivity, TimeSpecificDeviceSharingActivity::class.java)
            val bun = Bundle()
            bun.putSerializable(IntentConstants.DEVICE_ID, selectedDevice?.deviceId)
            bun.putBoolean(IntentConstants.EDIT, true)
            intent.putExtras(bun)
            startActivityForResult(intent, IntentRequestConstants.SHARE_DEVICE)
        })



        fetchGroups()
    }

    private fun setSearchView() {
        binding.searchView.setOnSearchViewListener(object : SimpleSearchView.SearchViewListener {
            override fun onSearchViewShownAnimation() {
            }

            override fun onSearchViewClosed() {
                if (binding.adapter != null)
                    binding.adapter!!.filter.filter("")
                binding.toolbar.visibility = View.VISIBLE
                binding.searchView.visibility = View.INVISIBLE
            }

            override fun onSearchViewClosedAnimation() {

            }

            override fun onSearchViewShown() {

            }

        })

        binding.searchView.setOnQueryTextListener(object : SimpleSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (binding.adapter != null) {
                    binding.adapter!!.filter.filter(newText)
                }
                return false
            }

            override fun onQueryTextCleared(): Boolean {
                return false
            }
        })

    }

    private fun bindDevices(devices: List<Device>) {
        if (binding.adapter == null) {
            binding.adapter =
                DeviceListAdapter(
                    devices = devices as ArrayList,
                    mListener = object : GroupRecyclerClickListener {
                        override fun onClick(data: Any, action: Int) {
                            selectedDevice = data as Device
                            showOptions()
                        }
                    })
        } else {
            if (binding.searchView.isSearchOpen)
                binding.searchView.closeSearch(true)
            binding.adapter!!.filter.filter("")
            binding.adapter!!.updateData(devices)
        }
    }

    private fun handleOptionsClicked(device: Device, action: Int) {
        when (action) {
            GroupRecyclerClickListener.DELETE -> deleteDevice(
                "Are you sure you want to delete ${device.deviceName} from your account?",
                device.deviceId
            )
            GroupRecyclerClickListener.EDIT -> {
                val intent = Intent(this@DeviceManagementActivity, AddDeviceActivity::class.java)
                val bun = Bundle()
                bun.putSerializable(IntentConstants.GROUP, groupList)
                bun.putSerializable(IntentConstants.DEVICE, device)
                bun.putBoolean(IntentConstants.EDIT, true)
                intent.putExtras(bun)
                startActivityForResult(intent, IntentRequestConstants.EDIT_DEVICE)
            }
            GroupRecyclerClickListener.MOVE -> moveDevice(
                deviceId = device.deviceId
            )
        }
    }

    fun fetchGroups() {
        viewModel.getGroup()?.observe(this, Observer {
            if (it == null) return@Observer
            groupList = it as ArrayList
            selectedGroup = it[0]
            binding.grpName = selectedGroup?.name
            viewModel.getAllDevices()
        })

    }

    fun checkAll() {
        viewModel.checkIsAllDevicesChecked()
    }


    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            slideToRight()
        }
    }

    override fun onBackPressed() {
        if (binding.searchView.isSearchOpen) {
            binding.searchView.closeSearch(true)
        } else {
            super.onBackPressed()
        }

    }


    private fun deleteAndObserve(deviceId: Int = 0) {
        viewModel.deleteDevice(deviceId).reObserver(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showSnackBar(
                        binding.root,
                        it.message!!

                    )
                }
                ApiResult.Status.SUCCESS -> {
                    viewModel.getAllDevices(selectedGroup?.id!!, binding.isMyDeviceSelected!!)
                    showSnackBar(
                        binding.root,
                        it.data!!.result.msg

                    )

                }
                else -> {
                }
            }
        })
    }

    private fun deleteDevice(
        msg: String = resources.getString(R.string.delete_devices),
        deviceId: Int = 0
    ) {
        AlertHelper.showOKCancelAlert(
            this@DeviceManagementActivity,
            msg,
            object : AlertCallback {
                override fun action(isOk: Boolean, data: Any?) {
                    if (isOk)
                        deleteAndObserve(deviceId)
                }

            })
    }

    private fun moveDevice(
        msg: String = resources.getString(R.string.move_devices),
        deviceId: Int = 0
    ) {

        AlertHelper.showGroupListAlert(
            this@DeviceManagementActivity,
            groupList!!,
            object : AlertCallback {
                override fun action(isOk: Boolean, data: Any?) {
                    showConfirmAlertAndSave(msg, deviceId, (data as Group).id)

                }

            })
    }

    private fun showConfirmAlertAndSave(msg: String, deviceId: Int = 0, groupId: Int) {
        AlertHelper.showOKCancelAlert(
            this@DeviceManagementActivity,
            msg,
            object : AlertCallback {
                override fun action(isOk: Boolean, data: Any?) {
                    if (isOk) {
                        moveDeviceAndObserve(deviceId, groupId)
                    }

                }

            })
    }

    private fun moveDeviceAndObserve(deviceId: Int = 0, groupId: Int) {
        viewModel.moveDevice(deviceId, groupId).reObserver(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showSnackBar(
                        binding.root,
                        it.message!!

                    )
                }
                ApiResult.Status.SUCCESS -> {
                    viewModel.getAllDevices(selectedGroup?.id!!, binding.isMyDeviceSelected!!)
                    showSnackBar(
                        binding.root,
                        it.data?.result?.msg!!

                    )

                }

                else -> {}
            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IntentRequestConstants.ADD_DEVICE) {
                viewModel.isLoading.value = true
                viewModel.isLoaded.value = false
                viewModel.getAllDevices()
            } else {
                viewModel.updateList(
                    data?.getSerializableExtra(IntentConstants.DEVICE) as Device,
                    selectedGroup?.id!!,
                    binding.isMyDeviceSelected!!
                )

            }

        }
    }


    inner class ClickHandler {
        fun addDevice() {
            val intent = Intent(this@DeviceManagementActivity, AddDeviceActivity::class.java)
            val bun = Bundle()
            bun.putSerializable(IntentConstants.GROUP, groupList)
            intent.putExtras(bun)
            startActivityForResult(intent, IntentRequestConstants.ADD_DEVICE)
        }

        fun openGroup() {
            AlertHelper.showGroupListAlert(
                this@DeviceManagementActivity,
                groupList!!,
                object : AlertCallback {
                    override fun action(isOk: Boolean, data: Any?) {
                        selectedGroup = data as Group
                        binding.grpName = selectedGroup?.name
                        viewModel.selectAll(false)
                        viewModel.getDevicesByGroup(
                            groupId = selectedGroup?.id!!,
                            isMyDeviceSelected = binding.isMyDeviceSelected!!
                        )
                    }

                })
        }

        fun deleteDevices() {
            deleteDevice()
        }

        fun moveDevices() {
            moveDevice()
        }

        fun setMyDeviceSelected() {
            if (binding.isMyDeviceSelected!!) return
            binding.isMyDeviceSelected = true
            viewModel.selectAll(false)
            viewModel.getDevicesByGroup(
                groupId = selectedGroup?.id!!,
                isMyDeviceSelected = binding.isMyDeviceSelected!!
            )

        }

        fun setSharedDeviceSelected() {
            if (!binding.isMyDeviceSelected!!) return
            binding.isMyDeviceSelected = false
            viewModel.selectAll(false)
            viewModel.getDevicesByGroup(
                groupId = selectedGroup?.id!!,
                isMyDeviceSelected = binding.isMyDeviceSelected!!
            )
        }

        fun back() {
            finish()
        }

        fun openSearch() {
            binding.toolbar.visibility = View.INVISIBLE
            binding.searchView.visibility = View.VISIBLE
            binding.searchView.showSearch(true)
        }

    }


    private fun showOptions() {
        try {
            val binding: FragmentDeviceOptionsBinding = DataBindingUtil.inflate(
                layoutInflater,
                R.layout.fragment_device_options,
                null,
                false
            )
            val mBottomDialogNotificationAction =
                BottomSheetDialog(this, R.style.bottomSheetDialogTheme)
            mBottomDialogNotificationAction.setContentView(binding.root)
            mBottomDialogNotificationAction.show()
            if (selectedDevice?.isAdmin == false) {
                binding.imgShare.visibility = View.GONE
                binding.txtShare.visibility = View.GONE
            }
            binding.imgShare.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.share.call()
            }
            binding.imgEdit.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.edit.call()
            }

            binding.imgDelete.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.delete.call()
            }

            binding.imgMove.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.move.call()
            }

            binding.layout.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}
