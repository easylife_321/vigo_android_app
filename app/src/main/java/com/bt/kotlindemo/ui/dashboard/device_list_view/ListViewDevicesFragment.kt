/*
 * *
 *  * Created by Surajit on 24/12/20 12:01 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 24/12/20 12:00 AM
 *  *
 */

package com.bt.kotlindemo.ui.dashboard.device_list_view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.FragmentListViewDevicesBinding
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.ui.base.BaseFragment
import com.bt.kotlindemo.ui.dashboard.DashboardActivity
import com.bt.kotlindemo.ui.dashboard.map_view.SelectGroupAdapter
import com.bt.kotlindemo.ui.device_detail.DeviceDetailActivity
import org.koin.androidx.viewmodel.ext.android.viewModel


class ListViewDevicesFragment : BaseFragment() {

    private val viewModel: ListViewViewModel by viewModel()
    private lateinit var binding: FragmentListViewDevicesBinding
    private var adapter: ListViewDevicesAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_list_view_devices, container, false)
        binding.apply {
            viewModel = this@ListViewDevicesFragment.viewModel
            lifecycleOwner = this@ListViewDevicesFragment
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }


    fun initViews() {
        viewModel.groups.value =
            arguments?.getSerializable(IntentConstants.GROUP) as ArrayList<Group>
        viewModel.selectedGroup.value = (viewModel.groups.value as ArrayList<Group>)[0]
        adapter =
            ListViewDevicesAdapter(
                object : GroupRecyclerClickListener {
                    override fun onClick(data: Any, action: Int) {
                        startActivity(Intent(activity, DeviceDetailActivity::class.java).apply {
                            putExtra(
                                IntentConstants.DEVICE_ID,
                                (data as Device).deviceId
                            )//it.tag as Int
                        })
                    }
                })
        binding.list.adapter = adapter
        binding.imgBack.setOnClickListener {
            ((activity as DashboardActivity)).onBackPressed()
        }
        viewModel.devicesToShow.observe(viewLifecycleOwner) {
            adapter?.updateList(it)
        }
        viewModel.groups.observe(viewLifecycleOwner, Observer {
            if (binding.adapter == null) {
                setAdapter(it)
            }
        })
        viewModel.getDeviceData().observe(viewLifecycleOwner, Observer {
            return@Observer
        })

        viewModel.startTimer()
    }

    private fun setAdapter(group: List<Group>) {
        binding.adapter =
            SelectGroupAdapter(group as java.util.ArrayList<Group>)
        binding.spnGroup.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.selectedGroup.value = binding.adapter?.getItem(position)
                viewModel.filterDevices()
            }

        }
    }

    companion object {

        @JvmStatic
        fun newInstance(devices: ArrayList<Group>) =
            ListViewDevicesFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(IntentConstants.GROUP, devices)
                }
            }
    }
}