/*
 * *
 *  * Created by Surajit on 24/4/20 6:16 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 11/4/20 12:59 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemDeviceDetailFeaturesBinding
import com.bt.kotlindemo.interfaces.DiffUtil
import com.bt.kotlindemo.model.device.Accessory
import kotlin.properties.Delegates

class DeviceFeaturesAdapter() :
    RecyclerView.Adapter<DeviceFeaturesAdapter.ViewHolder>(), DiffUtil {


    var features: List<Accessory> by Delegates.observable(emptyList()) { prop, old, new ->
        autoNotify(old, new) { o, n -> o.assetsName == n.assetsName || o.currentValue1==n.currentValue1 }
    }

    override fun getItemCount(): Int {
        return features.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemDeviceDetailFeaturesBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_device__detail_features,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.fearures = features[position]

    }

    inner class ViewHolder(
        val binding: ListItemDeviceDetailFeaturesBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

}

