/*
 * *
 *  * Created by Surajit on 25/4/20 12:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/4/20 6:18 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_detail

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.*
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.data.repository.AddressRepository
import com.bt.kotlindemo.data.repository.DeviceRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.device.Accessory
import com.bt.kotlindemo.model.device_command.Commands
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AddressUtil
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class DeviceDetailViewModel(
    deviceId: Int,
    preference: AppPreferences,
    private var deviceRepository: DeviceRepository,
    private var addressRepository: AddressRepository,
    private val context: Application
) : BaseViewModel() {


    companion object {
        const val INITIAL_DELAY = 0L
        const val REPEAT_INTERVAL = 20000L
    }

    var isLoaded = ObservableBoolean()
    var address = ObservableField<String>()
    var deviceDetail = MutableLiveData<Device>()
    var accessories = MutableLiveData<List<Accessory>>()
    var commands = MutableLiveData<List<Commands>>()
    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var fetchData = MutableLiveData<Boolean>()
    var speed = MutableLiveData<Double>()
    private val timer = Timer()
    var fetchCurrentLocation = MutableLiveData<Boolean>()
    var changeView = MutableLiveData<Boolean>()
    var history = MutableLiveData<Boolean>()
    var showTraffic = MutableLiveData<Boolean>()


    private var jsonObject: JsonObject = JsonObject()


    init {
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("groupId", 0)
        jsonObject.addProperty("deviceId", deviceId)
        isLoaded.set(false)
    }

    fun refresh() {
        fetchData.value = true
    }

    fun changeView() {
        changeView.value = true
    }

    fun openHistory() {
        history.value = true
    }


    fun getIcons() = deviceRepository.getIcons()

    fun getDevices(): LiveData<Device?> =
        deviceRepository.getDeviceList(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    isLoading.value = false
                    null
                }
                else -> {
                    isLoading.value = false
                    if (it.data == null) {
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support "
                        null
                    } else {
                        if (it?.data.devices.isNullOrEmpty()) {
                            showMessage.value =
                                "(Error code:101)We encountered a small glitch. If the problem please contact support"
                            null
                        } else {
                            accessories.value =
                                it.data.devices[0].accessories?.filter { accessory ->
                                    accessory.datatypeOfReceivingValue == 1
                                }
                            if (!isLoaded.get()) {
                                isLoaded.set(true)
                            }
                            commands.value = it?.data.commands
                            deviceDetail.value = it?.data.devices[0]
                            fetchAddress(deviceDetail.value?.lat!!, deviceDetail.value?.lang!!)
                            // speed.value = it.data.devices[0].speed.toDouble()
                            it?.data.devices[0]
                        }
                    }

                }
            }


        }


    /* fun getDeviceData() = Transformations.switchMap(fetchData) {
             getDevices()
     }*/

    fun startTimer() {
        timer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                viewModelScope.launch {
                    withContext(Dispatchers.Main) {
                        fetchData.value = true
                    }
                }
            }

        }, INITIAL_DELAY, REPEAT_INTERVAL)

    }


    private fun fetchAddress(lat: Double, lng: Double) {
        viewModelScope.launch {
            val address:String? = AddressUtil.getAddress(lat, lng, context,true)
            address?.let {
                this@DeviceDetailViewModel.address.set(it)
            } ?: kotlin.run {
                val jsonObject = JsonObject().apply {
                    addProperty("lat", lat)
                    addProperty("lng", lng)
                    addProperty("accountId", BuildConfig.ACCOUNT_ID)
                }
                addressRepository.getAddress(jsonObject).asFlow().collect {
                    when (it.status) {
                        ApiResult.Status.SUCCESS -> {
                            it.data?.address?.address?.let { data ->
                                this@DeviceDetailViewModel.address.set(data)
                            }
                        }
                        else -> {}
                    }
                }
            }

        }

    }

    fun navigateToLocation() {
        fetchCurrentLocation.value = true
    }

    fun showTraffic(){
        showTraffic.value = !(showTraffic.value?:false)
    }


    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }


}