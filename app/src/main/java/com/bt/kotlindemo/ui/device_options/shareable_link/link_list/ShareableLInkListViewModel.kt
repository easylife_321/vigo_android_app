/*
 * *
 *  * Created by Surajit on 25/5/20 8:07 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/5/20 3:08 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.shareable_link.link_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.repository.ShareableLinkRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.shareable_link.ShareableLink
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.google.gson.JsonObject

class ShareableLInkListViewModel(
    var preference: AppPreferences,
    private var repository: ShareableLinkRepository
) : BaseViewModel() {

    var link = MutableLiveData<ArrayList<ShareableLink>>()

    var isLoading = MutableLiveData<Boolean>()

    var showMessage = MutableLiveData<String>()
    var showMessageRetry = MutableLiveData<String>()
    var isEmpty = MutableLiveData<Boolean>()
    private var jsonObject: JsonObject = JsonObject()
    var add = MutableLiveData<Boolean>()
    var search = MutableLiveData<Boolean>()


    val edit = SingleLiveEvent<Boolean?>()
    val delete = SingleLiveEvent<Boolean?>()
    val share = SingleLiveEvent<Boolean?>()


    init {
        jsonObject.addProperty("userId", preference.getUserId())

    }


    fun getLink() = repository.getLinkList(jsonObject).map {
        if (it.status == ApiResult.Status.LOADING) {
            isLoading.value = true

        } else if (it.status == ApiResult.Status.ERROR || it.status == ApiResult.Status.CUSTOM) {
            if (it.status == ApiResult.Status.ERROR)
                showMessageRetry.value = it.message?:"" else showMessage.value = it.message?:""
            isLoading.value = false

        } else {
            isLoading.value = false
            link.value =
                if (it.data!!.details == null) arrayListOf() else it.data.details as ArrayList<ShareableLink>
        }


    }

    fun addLink() {
        add.value = true
    }


    fun openSearch() {
        search.value = true
    }


    fun deleteLink(link: ShareableLink): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("trackingLinkId", link.trackingLinkId)
        jsonObject.addProperty("userId", preference.getUserId())
        return repository.deleteLink(jsonObject)
    }

    fun toggle(link: ShareableLink): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("trackingLinkId", link.trackingLinkId)
        jsonObject.addProperty("action", !link.isActive)
        jsonObject.addProperty("userId", preference.getUserId())

        return repository.toggle(jsonObject)
    }

    fun edit() {
        edit.call()
    }

    fun delete() {
        delete.call()
    }

    fun share() {
        share.call()
    }



}