/*
 * *
 *  * Created by Surajit on 12/4/20 4:30 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 11/4/20 12:59 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.groups

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.databinding.ListItemGroupItemBinding
import com.bt.kotlindemo.model.Group
import java.util.*

class GroupListAdapter(
    val group: ArrayList<Group>,
    private val mListener: GroupRecyclerClickListener? = null
) : RecyclerView.Adapter<GroupListAdapter.ViewHolder>(), Filterable {


    var groupFilterList = ArrayList<Group>()

    init {
        groupFilterList = group
    }


    override fun getItemCount(): Int {
        return groupFilterList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemGroupItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_group_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.group = groupFilterList[position]
        if (position == groupFilterList.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }
        /* holder.binding.imgDelete.setOnClickListener {
             mListener?.onClick(groupFilterList[position], GroupRecyclerClickListener.DELETE)
         }
         holder.binding.imgEdit.setOnClickListener {
             mListener?.onClick(groupFilterList[position], GroupRecyclerClickListener.EDIT)
         }
         holder.binding.imgShare.setOnClickListener {
             mListener?.onClick(groupFilterList[position], GroupRecyclerClickListener.SHARE)
         }
         holder.binding.imgNotification.setOnClickListener {
             mListener?.onClick(groupFilterList[position], GroupRecyclerClickListener.NOTIFICATION)
         }*/

        holder.binding.layout.setOnClickListener {
            mListener?.onClick(groupFilterList[position], 0)
        }


    }

    inner class ViewHolder(
        val binding: ListItemGroupItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                groupFilterList = if (charSearch.isEmpty()) {
                    group
                } else {
                    val resultList = ArrayList<Group>()
                    for (row in group) {
                        if (row.name.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = groupFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                groupFilterList = results?.values as ArrayList<Group>
                notifyDataSetChanged()
            }

        }
    }

}

