/*
 * *
 *  * Created by Surajit on 25/5/20 7:43 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:43 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.trips_management

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ActivityTripsListBinding
import com.bt.kotlindemo.interfaces.*
import com.bt.kotlindemo.model.Trips
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.DatePickerHelper
import com.bt.kotlindemo.utils.reObserver
import org.koin.androidx.viewmodel.ext.android.viewModel

class TripsListActivity : BaseActivity() {

    private lateinit var binding: ActivityTripsListBinding
    private val viewModel: TripsListViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        setStatusBarColor(R.color.colorPrimary)
        super.onCreate(savedInstanceState)

    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trips_list)
        binding.apply {
            lifecycleOwner = this@TripsListActivity
            viewModel = this@TripsListActivity.viewModel
        }
    }

    override fun initViews() {
        addObservers()
        fetchList()
        setSearchView()
    }

    private fun setSearchView() {

    }


    private fun addObservers() {
        viewModel.onBackPressed.observe(this, {
            finish()
        })
        viewModel.isLoading.observe(this, {
            if (it) {
                showDialog()
            } else {
                dismissDialog()
            }
        })

        viewModel.showMessage.observe(this, {
            AlertHelper.showOkAlert(this, it)
        })

        viewModel.list.observe(this, {
            setList(it as ArrayList)
        })

        viewModel.fetch.observe(this, {
            fetchList()
        })

        viewModel.setFromDate.observe(this, {
            setDate(FROM_DATE)
        })

        viewModel.setToDate.observe(this, {
            setDate(TO_DATE)
        })


    }

    private fun setDate(setFor: Int) {
        DatePickerHelper.showDatePicker(this, object : DateSelectionCallback {
            override fun dateSelected(milis: Long) {
                if (setFor == TO_DATE) {
                    viewModel.to.value = milis
                } else {
                    viewModel.from.value = milis
                }
                fetchList()
            }
        })
    }


    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            slideToRight()
        }
    }


    private fun setList(it: ArrayList<Trips>) {
        binding.adapter = TripsListAdapter(it, object : RecyclerClickListener {
            override fun onClick(data: Any) {

                AlertHelper.showOKCancelAlert(this@TripsListActivity,
                    "Are you sure you want to end this trip? ",
                    object : AlertCallback {
                        override fun action(isOk: Boolean, a: Any?) {
                            if (isOk) {
                                stopAndObserve(data as Trips)
                            }
                        }
                    })


            }
        })
    }

    private fun stopAndObserve(trip: Trips) {
        viewModel.endTrip(trip.tripId ?: "").observe(this, {})
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            fetchList()
        }
    }


    private fun fetchList() {
        viewModel.getTripList().reObserver(this, { })
    }

    companion object {
        private const val TO_DATE = 1
        private const val FROM_DATE = 2
    }
}
