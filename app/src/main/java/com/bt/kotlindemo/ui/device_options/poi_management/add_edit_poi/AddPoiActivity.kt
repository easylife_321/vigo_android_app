/*
 * *
 *  * Created by Surajit on 23/5/20 2:51 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/5/20 4:35 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.poi_management.add_edit_poi

import android.app.Activity
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.AppConstants
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.ActivityAddPoiBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Poi
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.bt.kotlindemo.utils.location.AirLocation
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class AddPoiActivity : BaseActivity() {


    private val viewModel: AddPoiViewModel by viewModel {
        parametersOf(
            if (intent.getBooleanExtra(IntentConstants.EDIT, true)) {
                (intent.getSerializableExtra(IntentConstants.DATA) as Poi).id
            } else {
                null
            }
        )
    }
    private lateinit var binding: ActivityAddPoiBinding
    private var mMap: GoogleMap? = null
    private lateinit var marker: Marker
    private var devices: ArrayList<Device>? = null

    private var airLocation: AirLocation? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        makeStatusBarTransparent()
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_poi)
        binding.apply {
            viewModel = this@AddPoiActivity.viewModel
            lifecycleOwner = this@AddPoiActivity

        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun initViews() {
        devices = intent.getSerializableExtra(IntentConstants.DATA) as? ArrayList<Device>
        addFocusListener()
        setMap()
        setObservers()

    }

    private fun addFocusListener() {
        binding.etLat.setOnFocusChangeListener { _: View, b: Boolean ->
            if (!b) {
                if (viewModel.lat.get()!!.isValidLat()) {
                    addMarker()
                } else {
                    binding.etLat.error = "Invalid Latitude"
                }

            }
        }

        binding.etLng.setOnFocusChangeListener { _: View, b: Boolean ->
            if (!b) {
                if (viewModel.lng.get()!!.isValidLng()) {
                    addMarker()
                } else {
                    binding.etLng.error = "Invalid Longitude"
                }

            }
        }
    }


    private fun setMap() {
        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            mMap = it
            addMapLoadedListener()
        }
    }

    private fun addMapLoadedListener() {
        mMap!!.setOnMapLoadedCallback {
            if (intent.getBooleanExtra(IntentConstants.EDIT, true)) {
                viewModel.setData(intent.getSerializableExtra(IntentConstants.DATA) as Poi)
            } else {
                fetchLocation()
            }
            addMapClickListener()
        }
    }

    private fun addMapClickListener() {
        mMap!!.setOnMapClickListener {
            plotMarker(it)
        }
    }

    private fun plotMarker(it: LatLng) {
        viewModel.lat.set(it.latitude.toString())
        viewModel.lng.set(it.longitude.toString())
        addMarker()
    }

    private fun setObservers() {
        viewModel.openDeviceList.observe(this, Observer {
            showDevices()
        })
        /* viewModel.poiName.addOnPropertyChangedCallback(object :
             Observable.OnPropertyChangedCallback() {
             override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                 if (!binding.et.error.isNullOrEmpty()) {
                     binding.nameLayout.error = ""
                 }
             }
         })*/
        viewModel.validate.observe(this, Observer {
            if (!viewModel.lat.get()!!.isValidLat()) {
                binding.etLat.error = "Invalid Latitude"
                return@Observer
            }
            if (!viewModel.lng.get()!!.isValidLng()) {
                binding.etLng.error = "Invalid Longitude"
                return@Observer
            }
            if (viewModel.poiName.get() == null) {
                binding.etName.error = "Name Required"
                return@Observer
            }
            if (viewModel.poiName.get()!!.isEmpty()) {
                binding.etName.error = "Name Required"
                return@Observer
            }
            saveAndObserve()

        })

        viewModel.onBackPressed.observe(this, { finish() })
        viewModel.plotMarker.observe(this, { addMarker() })


    }

    private fun saveAndObserve() {
        viewModel.savePoi().reObserver(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(this@AddPoiActivity, it.message!!, null)
                }
                ApiResult.Status.SUCCESS -> {
                    setResult(Activity.RESULT_OK)
                    AlertHelper.showOkAlertFinish(this@AddPoiActivity, it.data!!.result.msg)
                }
                else -> return@Observer
            }
        })
    }

    private fun showDevices() {
        AlertHelper.showDeviceList(this, devices!!, object : AlertCallback {
            override fun action(isOk: Boolean, data: Any?) {
                plotMarker(LatLng((data as Device).lat, data.lang))
            }
        })
    }


    private fun addMarker() {
        mMap!!.clear()
        viewModel.fetchAddress()
        marker = mMap?.addMarker(
            MarkerOptions()
                .icon(ImageUtil.bitmapFromVector(this, R.drawable.car_icon))
                .position(
                    LatLng(
                        viewModel.lat.get()!!.toDouble(),
                        viewModel.lng.get()!!.toDouble()
                    )
                )
                .anchor(.5f, .5f)
        )!!
        mMap?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(viewModel.lat.get()!!.toDouble(), viewModel.lng.get()!!.toDouble()),
                AppConstants.MAP_DEFAULT_ZOOM
            )
        )
    }


    private fun fetchLocation() {
        airLocation = AirLocation(this, object : AirLocation.Callback {

            override fun onSuccess(locations: ArrayList<Location>) {
                if (viewModel.lat.get().isNullOrEmpty()) {
                    viewModel.lat.set(locations[0].latitude.toString())
                    viewModel.lng.set(locations[0].longitude.toString())
                    addMarker()
                }
            }

            override fun onFailure(locationFailedEnum: AirLocation.LocationFailedEnum) {
                showSnackBar(
                    binding.root,
                    "Could not fetch your current location"
                )
            }

        })
        airLocation?.start()


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        airLocation?.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        airLocation?.onActivityResult(requestCode, resultCode, data)
    }


}
