/*
 * *
 *  * Created by Surajit on 15/5/20 3:57 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/4/20 1:50 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_detail.device_comands

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.repository.DeviceCommandRepository
import com.bt.kotlindemo.data.repository.TripRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.device_command.Commands
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject

class DeviceCommandViewModel(
    private val deviceId: Int,
    private val preference: AppPreferences,
    private var repository: DeviceCommandRepository,
    private var tripRepository: TripRepository,
) : BaseViewModel() {


    var isLoaded = ObservableBoolean()
    var showMessage = MutableLiveData<String>()
    var showProgress = MutableLiveData<Boolean>()
    var commands = MutableLiveData<MutableList<Commands>>()
    var toggle = MutableLiveData<Commands>()


    private var jsonObject: JsonObject = JsonObject()


    init {
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("deviceId", deviceId)
        isLoaded.set(false)
    }


    fun getDeviceCommands() =
     repository.getDeviceCommands(jsonObject).map{
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false

                }
            }

        }


    fun antiTheft(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("currentStatus", (commands.currentStatus as Boolean))
        obj.addProperty("userId", preference.getUserId())
        return repository.antiTheft(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    commands.currentStatus = !(commands.currentStatus as Boolean)
                    updateList(commands)
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false

                }
            }

        }

    }

    fun deviceAutoTroubleshoot(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("userId", preference.getUserId())
        return repository.deviceAutoTroubleshoot(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {

                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false

                }
            }

        }

    }


    fun parking(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("currentStatus", (commands.currentStatus as Boolean))
        obj.addProperty("userId", preference.getUserId())
        return repository.parking(obj).map{
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    commands.currentStatus = !(commands.currentStatus as Boolean)
                    updateList(commands)
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false

                }
            }

        }

    }


    fun refreshGps(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("userId", preference.getUserId())
        return repository.refreshGps(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false

                }
            }

        }

    }

    fun toggleSharedDevicesSharing(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("currentStatus", (commands.currentStatus as Boolean))
        obj.addProperty("userId", preference.getUserId())
        return repository.toggleSharedDevicesSharing(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    commands.currentStatus = !(commands.currentStatus as Boolean)
                    updateList(commands)
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false

                }
            }

        }

    }

    fun toggleGpsDeviceAccessory(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("accessoriesMacId", commands.accessoriesMacId)
        obj.addProperty("currentStatus", (commands.currentStatus as Boolean))
        obj.addProperty("userId", preference.getUserId())
        return repository.toggleGpsDeviceAccessory(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    commands.currentStatus = !(commands.currentStatus as Boolean)
                    updateList(commands)
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false

                }
            }

        }

    }


    fun untraceableMode(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("currentStatus", (commands.currentStatus as Boolean))
        obj.addProperty("userId", preference.getUserId())
        return repository.untraceableMode(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    commands.currentStatus = !(commands.currentStatus as Boolean)
                    updateList(commands)
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false

                }
            }

        }

    }

    fun followDevice(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("userId", preference.getUserId())
        return repository.untraceableMode(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true

                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                }
                else -> {
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false

                }
            }

        }

    }

    fun setSpeedLimit(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("DeviceId", deviceId)
        obj.addProperty("UserId", preference.getUserId())
        obj.addProperty("speedLimit", commands.currentStatus as String)

        return repository.setSpeedLimit(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    updateList(commands)
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false
                }
            }

        }

    }

    fun setOdometer(commands: Commands): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("userId", preference.getUserId())
        obj.addProperty("odometerValue", commands.currentStatus as String)

        return repository.setOdometer(obj).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    updateList(commands)
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false
                }
            }

        }

    }

    fun startTrip(
        name: String,
        lat: Double,
        lng: Double,
        address: String,
        isBusiness: Boolean,
        commands: Commands
    ): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("userId", preference.getUserId())
        if (!(commands.currentStatus as Boolean)) {
            obj.addProperty("startLat", lat)
            obj.addProperty("startLng", lng)
            obj.addProperty("startAddr", address)
            obj.addProperty("startAddr", address)
            obj.addProperty("isBusinessTrip", isBusiness)
            obj.addProperty("tripName", name)
        }

        return (
                if (!(commands.currentStatus as Boolean)) tripRepository.startTrip(
                    obj
                ) else tripRepository.endTrip(obj)
                ).map {
                when (it.status) {
                    ApiResult.Status.LOADING -> {
                        showProgress.value = true
                        null
                    }

                    ApiResult.Status.ERROR -> {
                        showMessage.value = it.message ?: ""
                        showProgress.value = false
                        null
                    }

                    else -> {
                        commands.currentStatus = !(commands.currentStatus as Boolean)
                        updateList(commands)
                        showMessage.value = it.data!!.result.msg
                        showProgress.value = false
                    }
                }

            }

    }


    private fun updateList(command: Commands) {
        val list = arrayListOf<Commands>()
        commands.value?.onEach {
            if (it.featureName == command.featureName) {
                list.add(command.copy())
            } else {
                list.add(it.copy())
            }
        }
        /*   //list.addAll(commands.value!!)
           list.forEachIndexed { index, it ->
               if (it.featureName == command.featureName) {
                   commands.value!![index] = command
                   return@forEachIndexed
               }
           }*/
        commands.value = list.toMutableList()

    }


    override fun onCleared() {
        super.onCleared()

    }

    fun endTrip(
        lat: Double,
        lng: Double,
        address: String,
        commands: Commands
    ): LiveData<Unit?> {
        val obj = JsonObject()
        obj.addProperty("deviceId", deviceId)
        obj.addProperty("userId", preference.getUserId())
        obj.addProperty("endLat", lat)
        obj.addProperty("endlng", lng)
        obj.addProperty("endAddr", address)
        return (
            if (!(commands.currentStatus as Boolean)) tripRepository.endTrip(
                obj
            ) else tripRepository.endTrip(obj)
        ) .map{
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    showProgress.value = true
                    null
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message ?: ""
                    showProgress.value = false
                    null
                }
                else -> {
                    commands.currentStatus = !(commands.currentStatus as Boolean)
                    updateList(commands)
                    showMessage.value = it.data!!.result.msg
                    showProgress.value = false
                }
            }

        }

    }


}