/*
 * *
 *  * Created by Surajit on 23/5/20 2:50 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 4:30 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.share

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemSelectedTimeBinding
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.model.SelectedTime
import java.util.*

class SelectedTimeAdapter : RecyclerView.Adapter<SelectedTimeAdapter.ViewHolder>() {

    var selectedTime = ArrayList<SelectedTime>()


    override fun getItemCount(): Int {
        return selectedTime.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemSelectedTimeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_selected_time,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.data = selectedTime[position]
        holder.binding.deleteLayout.setOnClickListener {
            selectedTime.removeAt(position)
            notifyItemRemoved(position)
        }

    }


    fun add(time: SelectedTime) {
        this.selectedTime.add(time)
        notifyItemInserted(selectedTime.size - 1)
    }


    inner class ViewHolder(
        val binding: ListItemSelectedTimeBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }


}

