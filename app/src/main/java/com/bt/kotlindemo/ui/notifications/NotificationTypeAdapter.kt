/*
 * *
 *  * Created by Surajit on 1/24/21 10:12 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/24/21 10:12 PM
 *  *
 */

package com.bt.kotlindemo.ui.notifications


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemNotificationTypeBinding


internal class NotificationTypeAdapter(val list: List<String>) :
    BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val holder: ViewHolder
        if (convertView == null) {
            val itemBinding: ListItemNotificationTypeBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent!!.context),
                R.layout.list_item_notification_type,
                parent,
                false
            )
            itemBinding.data = list[position]
            holder = ViewHolder(itemBinding)
            holder.binding = itemBinding
            holder.binding.root.tag = holder
        } else {
            holder = convertView.tag as ViewHolder
        }

        return holder.binding.root
    }

    override fun getItem(position: Int): String {
        return list[position]
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder(var binding: ListItemNotificationTypeBinding)


}
