/*
 * *
 *  * Created by Surajit on 15/5/20 3:56 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 15/5/20 3:35 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_detail.device_comands

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.CommandNavigation
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.custom.GridSpacingItemDecoration
import com.bt.kotlindemo.databinding.FragmentDeviceCommandBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.BottomSheetCloseCallBack
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.device_command.Commands
import com.bt.kotlindemo.ui.base.BaseFragment
import com.bt.kotlindemo.ui.device_detail.DeviceDetailActivity
import com.bt.kotlindemo.ui.device_options.device_management.add_device.AddDeviceActivity
import com.bt.kotlindemo.ui.device_options.shareable_link.add_new_link.AddNewLinkActivity
import com.bt.kotlindemo.ui.device_options.zone_management.add_zone.AddNewZoneActivity
import com.bt.kotlindemo.utils.AlertHelper
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


class DeviceCommandFragment : BaseFragment() {


    private lateinit var binding: FragmentDeviceCommandBinding
    var mCallback: BottomSheetCloseCallBack? = null
    private val viewModel: DeviceCommandViewModel by viewModel {
        parametersOf(
            requireArguments().getInt(
                IntentConstants.DEVICE_ID,
                0
            )
        )
    }

    private var isUserScrolling = false

    companion object {
        fun newInstance(
            id: Int,
            commands: ArrayList<Commands>
        ) =
            DeviceCommandFragment().apply {
                arguments = Bundle().apply {
                    putInt(IntentConstants.DEVICE_ID, id)
                    putSerializable(IntentConstants.DATA, commands)
                }
            }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_device_command, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = this@DeviceCommandFragment.viewModel
        binding.list.itemAnimator = null;
        binding.list.layoutManager = GridLayoutManager(activity, 3)
        binding.list.addItemDecoration(
            GridSpacingItemDecoration(
                3,
                activity.resources?.getDimension(com.intuit.sdp.R.dimen._10sdp)?.toInt()!!, true
            )
        )
        if (binding.adapter == null)
            binding.adapter = DeviceCommandsAdapter(object : RecyclerClickListener {
                override fun onClick(objects: Any) {
                    manageCommand(objects as Commands)
                }
            })
        return binding.root
    }


    @Suppress("UNCHECKED_CAST")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addObservers()
        viewModel.commands.value =
            requireArguments().getSerializable(IntentConstants.DATA) as ArrayList<Commands>
        setRecyclerviewHeight(viewModel.commands.value?.size ?: 0)

    }

    private fun addObservers() {

        viewModel.showProgress.observe(viewLifecycleOwner) {
            if (it) {
                showDialog()
            } else {
                dismissDialog()
            }
        }

        viewModel.showMessage.observe(viewLifecycleOwner) {
            AlertHelper.showOkAlert(requireContext(), it)
        }

        viewModel.commands.observe(viewLifecycleOwner) { commands ->
            commands?.apply {
                if (binding.adapter != null) {
                    binding.adapter?.updateList(this)
                }
            }
        }
    }


    private fun setRecyclerviewHeight(size: Int) {
        val linearLayout: ViewGroup.LayoutParams = binding.list.layoutParams
        if (size <= 12) {
            mCallback?.ignore()
            linearLayout.height = ViewGroup.LayoutParams.WRAP_CONTENT
        } else {
            addScrollListener()
            linearLayout.height = requireContext().resources.getDimension(com.intuit.sdp.R.dimen._370sdp).toInt()
            //(320 * Resources.getSystem().displayMetrics.density).toInt()
        }
        binding.list.layoutParams = linearLayout


    }

    private fun addScrollListener() {
        var isTop = true
        var didMove = false
        var ignore = false

        binding.list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    isUserScrolling = true
                } else if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (!didMove && isTop) {
                        if (ignore) {
                            ignore = false
                        } else {
                            mCallback?.close()
                        }
                    }
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (isUserScrolling) {
                    ignore = true
                    didMove = true
                    isTop = false
                    if (dy > 0) {
                        //Scroll list Down
                    } else {
                        didMove = false
                        isTop = true
                        if (!recyclerView.canScrollVertically(-1)) {

                        }
                    }
                }
            }
        })

    }


    fun setList(commands: List<Commands>?) {
        viewModel.commands.value = commands as ArrayList<Commands>
    }

    private fun handleCommand(commands: Commands) {
        when (commands.commandName) {
            CommandNavigation.CONTROL_ACCESSORY -> {
                viewModel.toggleGpsDeviceAccessory(commands)
                    .observe(viewLifecycleOwner) { }
            }
            CommandNavigation.ANTI_THEFT -> {
                viewModel.antiTheft(commands).observe(viewLifecycleOwner) { }
            }
            CommandNavigation.TROUBLESHOOT -> {
                viewModel.deviceAutoTroubleshoot(commands)
                    .observe(viewLifecycleOwner) { }
            }

            CommandNavigation.DISABLE_TRACKING -> {
                viewModel.toggleSharedDevicesSharing(commands)
                    .observe(viewLifecycleOwner) { }
            }
            CommandNavigation.PARKING -> {
                viewModel.parking(commands).observe(viewLifecycleOwner) { }
            }

            CommandNavigation.REFRESH_GPS -> {
                viewModel.refreshGps(commands).observe(viewLifecycleOwner) { }
            }
            CommandNavigation.UNTRACEABLE_MODE -> {
                viewModel.untraceableMode(commands)
                    .observe(viewLifecycleOwner) { }
            }
            CommandNavigation.FOLLOW_DEVICE -> {
                (activity as DeviceDetailActivity).followDevice(true)
            }
            CommandNavigation.TRIP -> {
                if (commands.currentStatus as Boolean) {
                    val latLng = (activity as DeviceDetailActivity).getLocation()
                    viewModel.endTrip(
                        latLng.latitude,
                        latLng.longitude,
                        (activity as DeviceDetailActivity).getAddress(),
                        commands
                    ).observe(viewLifecycleOwner) {}
                } else {
                    showAlertStartTrip(commands)
                }
            }
            CommandNavigation.EDIT_DEVICE -> {
                startActivityForResult(
                    Intent(requireActivity(), AddDeviceActivity::class.java).apply {
                        val bundle = Bundle().apply {
                            putBoolean(IntentConstants.EDIT, true)
                            putExtra(
                                IntentConstants.DEVICE,
                                (activity as DeviceDetailActivity).getDevice()
                            )

                        }
                        putExtras(bundle)
                    },
                    IntentRequestConstants.EDIT_DEVICE
                )
            }

            CommandNavigation.SHARABLE_LINK -> {
                startActivity(Intent(requireActivity(), AddNewLinkActivity::class.java))
            }

        }
    }

    private fun showAlertStartTrip(commands: Commands) {
        AlertHelper.showTripStartAlert(
            requireContext(),
            (activity as DeviceDetailActivity).getAddress(),
            object : AlertCallback {
                override fun action(isOk: Boolean, data: Any?) {
                    data?.apply {
                        val latLng = (activity as DeviceDetailActivity).getLocation()
                        viewModel.startTrip(
                            (data as ArrayList<Any>)[0] as String,
                            latLng.latitude,
                            latLng.longitude,
                            (activity as DeviceDetailActivity).getAddress(),
                            (data)[1] as Boolean,
                            commands
                        ).observe(viewLifecycleOwner) { }
                    }
                }
            })
    }

    private fun manageCommand(command: Commands) {
        when (command.commandName) {
            CommandNavigation.ANTI_THEFT -> {
                showAlert(command)
            }
            CommandNavigation.TROUBLESHOOT -> {
                showAlert(command)
            }
            CommandNavigation.PARKING -> {
                showAlert(command)
            }
            CommandNavigation.CONTROL_ACCESSORY -> {
                showAlert(command)
            }
            CommandNavigation.DISABLE_TRACKING -> {
                showAlert(command)
            }
            CommandNavigation.REFRESH_GPS -> {
                showAlert(command)
            }
            CommandNavigation.UNTRACEABLE_MODE -> {
                showAlert(command)
            }
            CommandNavigation.FOLLOW_DEVICE -> {
                showAlert(command)
            }
            CommandNavigation.SPEED_LIMIT -> {
                showAlertSpeed(command)
            }
            CommandNavigation.TRIP -> {
                showAlert(command)
            }
            CommandNavigation.EDIT_DEVICE -> {
                showAlert(command)
            }
            CommandNavigation.SHARABLE_LINK -> {
                showAlert(command)
            }
            CommandNavigation.SET_ODOMETER -> {
                showOdometerAlert(command)
            }
            CommandNavigation.MARK_AS_ZONE -> {
                startActivity(Intent(activity, AddNewZoneActivity::class.java).apply {
                    putExtra(
                        IntentConstants.LAT,
                        (activity as DeviceDetailActivity).getLocation().latitude
                    )
                    putExtra(
                        IntentConstants.LNG,
                        (activity as DeviceDetailActivity).getLocation().longitude
                    )
                })
            }
            CommandNavigation.NAVIGATE -> {
                val latLng = (activity as DeviceDetailActivity).getLocation()
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(
                        "http://maps.google.com/maps?daddr=${(latLng.latitude)}," +
                                "${(latLng.longitude)}"
                    )
                )
                startActivity(intent)
            }

        }
    }

    private fun showAlertSpeed(command: Commands) {
        AlertHelper.showSpeedAlert(
            requireContext(),
            command.currentStatus.toString(),
            object : AlertCallback {
                override fun action(isOk: Boolean, data: Any?) {
                    if (isOk) {
                        command.currentStatus = data!!
                        viewModel.setSpeedLimit(command)
                            .observe(viewLifecycleOwner) { }

                    }
                }
            })
    }

    private fun showOdometerAlert(command: Commands) {
        AlertHelper.showOdometerAlert(
            requireContext(),
            (activity as DeviceDetailActivity).getOdometerValue(),
            object : AlertCallback {
                override fun action(isOk: Boolean, data: Any?) {
                    if (isOk) {
                        command.currentStatus = data!!
                        viewModel.setOdometer(command)
                            .observe(viewLifecycleOwner) { }

                    }
                }
            })
    }


    private fun showAlert(commands: Commands) {
        AlertHelper.showOKCancelAlert(
            requireContext(),
            commands.commandDesc,
            object : AlertCallback {
                override fun action(isOk: Boolean, data: Any?) {
                    if (isOk) {
                        handleCommand(commands)
                    }
                }
            })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mCallback = context as BottomSheetCloseCallBack

    }


    override fun onDestroyView() {
        super.onDestroyView()


    }


}



