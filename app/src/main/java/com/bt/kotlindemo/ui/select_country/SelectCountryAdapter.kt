package com.bt.kotlindemo.ui.select_country

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.databinding.ListItemCountryBinding
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.utils.CountryHelper
import java.util.*


class SelectCountryAdapter(
    val countries: ArrayList<CountryList>,
    val mListener: RecyclerClickListener
) :
    RecyclerView.Adapter<ViewHolder>(), Filterable {

        private var mCountries: List<CountryList> = countries


    // Gets the number of countries in the list
    override fun getItemCount(): Int {
        return mCountries.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemCountryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_country,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    // Binds each country in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.country = mCountries[position]
        holder.binding.res =
            CountryHelper.getFlagDrawableResId(mCountries[position].isoAlpha.toLowerCase(Locale.getDefault()))
        holder.binding.layout.setOnClickListener {
            mListener.onClick(mCountries[position])
        }

    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                mCountries = filterResults.values as List<CountryList>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val queryString = charSequence.toString().toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString.isEmpty())
                    countries
                else
                    countries.filter {
                        it.country.toLowerCase(Locale.getDefault()).contains(queryString)
                    }
                return filterResults
            }
        }
    }
}

class ViewHolder(// Holds the binding that will add each country to
    val binding: ListItemCountryBinding
) : RecyclerView.ViewHolder(binding.root) {
}