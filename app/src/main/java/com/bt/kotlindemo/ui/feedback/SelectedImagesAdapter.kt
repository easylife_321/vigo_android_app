/*
 * *
 *  * Created by Surajit on 30/5/20 8:58 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/5/20 3:08 PM
 *  *
 */

package com.bt.kotlindemo.ui.feedback

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemFeedbackImagesBinding
import java.io.File
import java.util.*

class SelectedImagesAdapter(
    private val images: ArrayList<File>
) : RecyclerView.Adapter<SelectedImagesAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return images.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemFeedbackImagesBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_feedback_images,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.file = images[position]
        holder.binding.imgRemove.setOnClickListener {
            images.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, images.size);
        }


    }


    override fun onViewRecycled(holder: ViewHolder) {
        holder.itemView.setOnLongClickListener(null)
        super.onViewRecycled(holder)
    }


    inner class ViewHolder(
        val binding: ListItemFeedbackImagesBinding
    ) : RecyclerView.ViewHolder(binding.root)


}

