/*
 * *
 *  * Created by Surajit on 25/5/20 8:07 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/5/20 3:08 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.poi_management.poi_list

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.repository.PoiRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Poi
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject

class PoiListViewModel(
    var preference: AppPreferences,
    private var poiRepository: PoiRepository
) : BaseViewModel() {

    var poi = MutableLiveData<ArrayList<Poi>>()

    var isLoading = MutableLiveData<Boolean>()

    var showMessage = MutableLiveData<String>()
    var showMessageRetry = MutableLiveData<String>()
    var isEmpty = MutableLiveData<Boolean>()
    var showSelected = MutableLiveData<Boolean>()
    var selectAll = ObservableBoolean()
    var isAnySelected = ObservableBoolean()
    private var jsonObject: JsonObject = JsonObject()
    var onSearch = MutableLiveData<Boolean>()
    var addPoi = MutableLiveData<Boolean>()



    init {
        jsonObject.addProperty("userId", preference.getUserId())

    }


    fun getPoi() = poiRepository.getPoiList(jsonObject).map {
        if (it.status == ApiResult.Status.LOADING) {
            isLoading.value = true

        } else if (it.status == ApiResult.Status.ERROR || it.status == ApiResult.Status.CUSTOM) {
            if (it.status == ApiResult.Status.ERROR)
                showMessageRetry.value = it.message?:"" else showMessage.value = it.message?:""
            isLoading.value = false

        } else {
            isLoading.value = false
            poi.value = it.data!!.poi
        }


    }

    fun deletePoi(poiId: String): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("favPlaceId", poiId)
        return poiRepository.deletePoi(jsonObject)
    }


    fun search(){
        onSearch.value=true
    }


    fun addPoi(){
        addPoi.value=true
    }


}