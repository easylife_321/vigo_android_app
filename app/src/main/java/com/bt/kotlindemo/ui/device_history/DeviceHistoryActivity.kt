/*
 * *
 *  * Created by Surajit on 18/5/20 7:11 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 18/5/20 7:11 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_history

import android.app.DatePickerDialog
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import android.widget.PopupMenu
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.AppConstants
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.databinding.ActivityDeviceHistoryBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.model.device_history.History
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.AnimUtils
import com.bt.kotlindemo.utils.DateTimeUtil
import com.bt.kotlindemo.utils.ImageUtil
import com.bt.kotlindemo.utils.google_map.MapsUtil
import com.bt.kotlindemo.utils.material_rangebar.RangeBar
import com.bt.kotlindemo.utils.reObserver
import com.bt.kotlindemo.utils.toLocalTime
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.Calendar
import java.util.concurrent.TimeUnit

private const val INDEX_VALUE_24_HOURS = 144
private const val DEFAULT_PLAY_DURATION = 10000L

class DeviceHistoryActivity : BaseActivity() {

    private val viewModel: DeviceHistoryViewModel by viewModel {
        parametersOf(
            intent.getIntExtra(
                IntentConstants.DEVICE_ID, 0
            )
        )
    }
    private var mMap: GoogleMap? = null
    private lateinit var binding: ActivityDeviceHistoryBinding
    private var firstLoad = true
    private lateinit var stopMarkerIcon: Bitmap
    private lateinit var deviceMarkerIcon: BitmapDescriptor
    private var handler = Handler(Looper.getMainLooper())
    private var runnable: Runnable? = null
    private var current = 0
    private var deviceMarker: Marker? = null
    private var start: Long = 0
    private var elapsed: Long = 0
    private var t = 0f
    private var lng = 0.0
    private var lat = 0.0
    private var duration: Long = DEFAULT_PLAY_DURATION
    private var selectPos = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        makeStatusBarTransparent()
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_device_history)
        binding.apply {
            lifecycleOwner = this@DeviceHistoryActivity
            viewModel = this@DeviceHistoryActivity.viewModel
        }
        initIcons()
        initMap()
        setRangeBarListener()
    }

    private fun initIcons() {
        stopMarkerIcon = ImageUtil.bitmapFromVectorImage(this, R.drawable.user_marker)
        deviceMarkerIcon = ImageUtil.bitmapFromVector(this, R.drawable.car_icon)
    }

    private fun initMap() {
        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            mMap = it
            mMap!!.uiSettings.isRotateGesturesEnabled = false

        }
    }

    override fun initViews() {
        viewModel.onBackPressed.observe(this) {
            finish()
        }
        viewModel.selectDate.observe(this) {
            openDatePicker()
        }
        viewModel.timeList.observe(this) {
            setRangeBar()
            if (firstLoad) {
                firstLoad = false
                fetchData()
            }

        }
        viewModel.locationList.observe(this) {
            setData(it)
        }
        viewModel.changeView.observe(this) {
            if (binding.bottomLayout.visibility == View.VISIBLE) {
                AnimUtils.collapse(binding.bottomLayout)
            } else {
                AnimUtils.expand(binding.bottomLayout)
            }
            // setUpPopMenu(binding.mapViewLayout)
        }
        viewModel.speed.observe(this) {
            duration = DEFAULT_PLAY_DURATION / it
        }
        viewModel.fetch.observe(this) {
            fetchData()
        }
        viewModel.showMessage.observe(this) {
            AlertHelper.showOkAlert(this, it)
        }

        viewModel.playPause.observe(this) {
            if (viewModel.isPlaying.value!!) {
                viewModel.isPlaying.value = false
                handler.removeCallbacks(runnable!!)
            } else {
                viewModel.isPlaying.value = true
                handler.post(runnable!!)
            }
        }

        viewModel.route.observe(this) {
            AlertHelper.showOKCancelAlert(
                this,
                "The selected history will se saved as your route. Do you want to continue?",
                object : AlertCallback {
                    override fun action(isOk: Boolean, data: Any?) {
                        if (isOk) {
                            showRouteNameAlert()
                        }
                    }
                })


        }
        setInitialDate()
    }

    private fun showRouteNameAlert() {
        AlertHelper.showRouteAlert(this, object : AlertCallback {
            override fun action(isOk: Boolean, data: Any?) {
                if (isOk) {
                    viewModel.saveRoute(data as String).observe(this@DeviceHistoryActivity) {
                        if (it) showDialog() else dismissDialog()
                    }
                }
            }
        })
    }

    private fun setUpPopMenu(view: View) {
        val popup = PopupMenu(this@DeviceHistoryActivity, view, Gravity.END)
        popup.inflate(R.menu.map_view_menu)
        val item = popup.menu.getItem(selectPos)
        val s = SpannableString(item.title)
        s.setSpan(
            ForegroundColorSpan(
                ContextCompat.getColor(this, R.color.idle)
            ), 0, s.length, 0
        )
        item.title = s

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.default_ -> {
                    selectPos = 0
                    mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
                    false
                }

                R.id.terrain -> {
                    selectPos = 1
                    mMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
                    true
                }

                R.id.satellite -> {
                    selectPos = 2
                    mMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE
                    true
                }

                R.id.hybrid -> {
                    selectPos = 3
                    mMap?.mapType = GoogleMap.MAP_TYPE_HYBRID
                    true
                }

                else -> {
                    false
                }


            }


        }

        popup.show()
    }

    private fun setData(it: ArrayList<History>) {
        if (it.isNullOrEmpty()) {
            AlertHelper.showOkAlert(this, "No history found for the selected time range")
        } else {
            plotInMap(it)
        }
    }

    private fun plotInMap(data: ArrayList<History>) {
        mMap?.apply {
            clear()
            mMap?.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        viewModel.locationList.value!![0].lat,
                        viewModel.locationList.value!![0].lng
                    ), AppConstants.MAP_DEFAULT_ZOOM
                )
            )
            runnable?.apply {
                handler.removeCallbacks(this)
            }
            current = 0
            val latLng = arrayListOf<LatLng>()
            for (i in 0 until data.size) {
                latLng.add(LatLng(data[i].lat, data[i].lng))
                if (!data[i].isMoving) {
                    /*if (i == 0) {
                        addStopMarker(data[i], viewModel.timeList.value!![0], data[i].timestamp)
                    } else {
                        addStopMarker(data[i], data[i - 1].timestamp, data[i].timestamp)
                    }*/
                    addStopMarker(data[i], data[i].timestamp, data[i].stopTillTimestamp)
                }
            }
            addPolyLine(latLng)
            setSeekBar()
            addDevice()
            handler.postDelayed({ moveMarker() }, 1000)
            viewModel.showControls.value = true
            viewModel.isPlaying.value = true
        }

    }

    private fun setSeekBar() {
        binding.seekBar.max = viewModel.locationList.value!!.size - 1
        binding.seekBar.progress = current
        binding.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekbar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser) {
                    current = progress
                    handler.removeCallbacks(runnable!!)
                    moveMarker()
                }
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {

            }

            override fun onStopTrackingTouch(p0: SeekBar?) {

            }

        })

    }

    private fun addDevice() {
        deviceMarker = mMap?.addMarker(
            MarkerOptions().position(
                LatLng(
                    viewModel.locationList.value!![0].lat,
                    viewModel.locationList.value!![0].lng
                )
            ).icon(BitmapDescriptorFactory.fromResource(R.mipmap.car_moving)).anchor(.5f, .5f)
        )
    }

    private fun moveMarker() {
        try {
            if (current >= viewModel.locationList.value!!.size - 1) {
                if (viewModel.locationList.value.isNullOrEmpty()) return
                deviceMarker!!.position = LatLng(
                    viewModel.locationList.value!![viewModel.locationList.value!!.size - 1].lat,
                    viewModel.locationList.value!![viewModel.locationList.value!!.size - 1].lng
                )
                viewModel.currentLocation.value =
                    viewModel.locationList.value!![viewModel.locationList.value!!.size - 1]
                current = 0
                handler.removeCallbacks(runnable!!)
                mMap!!.uiSettings.isRotateGesturesEnabled = true
                mMap!!.uiSettings.isTiltGesturesEnabled = true
                return
            }
            mMap!!.uiSettings.isRotateGesturesEnabled = false

            val startPosition = LatLng(
                viewModel.locationList.value!![current].lat,
                viewModel.locationList.value!![current].lng
            )
            val finalPosition =
                LatLng(
                    viewModel.locationList.value!![current + 1].lat,
                    viewModel.locationList.value!![current + 1].lng
                )
            viewModel.currentLocation.value = viewModel.locationList.value!![current]

            if (MapsUtil.calculateDistance(startPosition, finalPosition) < 20) {
                current++
                moveMarker()
                return
            }

            start = SystemClock.uptimeMillis()
            // final boolean hideMarker = false;
            val interpolator: Interpolator = LinearInterpolator()
            val bearing: Float = MapsUtil.getBearing(
                startPosition,
                finalPosition
            )
            val mapZoom: Float =
                if (mMap!!.cameraPosition.zoom > 19) 19f else mMap!!
                    .cameraPosition.zoom
            val cameraPosition = CameraPosition.Builder()
                .target(startPosition).zoom(mapZoom)
                .build()
            mMap!!.animateCamera(
                CameraUpdateFactory
                    .newCameraPosition(cameraPosition)
            )
            //rotateMarker(myMarker,bearing,start_rotation);
            // txtTime.setText((history.get(index.get(current)).getTime()));
            runnable = object : Runnable {
                override fun run() {
                    elapsed = SystemClock.uptimeMillis() - start
                    t = interpolator.getInterpolation(
                        elapsed.toFloat() / duration
                    )
                    lng = t * finalPosition.longitude + (1 - t) * startPosition.longitude
                    lat = t * finalPosition.latitude + (1 - t) * startPosition.latitude
                    deviceMarker!!.rotation = bearing
                    deviceMarker!!.position = LatLng(lat, lng)
                    // Repeat till progress is complete.
                    if (t < 1) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16)
                    } else {
                        deviceMarker!!.isVisible = true
                        current++
                        if (current < viewModel.locationList.value!!.size - 1) {
                            binding.seekBar.progress = current
                            moveMarker()
                        } else {
                            binding.seekBar.progress = viewModel.locationList.value!!.size - 1
                            current = 0
                            handler.removeCallbacks(runnable!!)
                            mMap!!.uiSettings.isRotateGesturesEnabled = true
                            mMap!!.uiSettings.isTiltGesturesEnabled = true
                            // }
                        }
                    }
                }
            }
            handler.post(runnable!!)
        } catch (ignored: Exception) {
            ignored.printStackTrace()
            Log.d("error", ignored.message!!)
        }
    }


    private fun addPolyLine(latLng: ArrayList<LatLng>) {
        mMap?.apply {
            addPolyline(PolylineOptions().addAll(latLng).color(Color.RED).width(7f))
        }

    }

    private fun addStopMarker(loc: History, start: Long, end: Long) {
        mMap?.apply {
            addMarker(
                MarkerOptions().title(
                    "Stop time(${
                        DateTimeUtil.getTimeDifference(
                            TimeUnit.SECONDS.toMillis(
                                end - start
                            )
                        )
                    })"
                )
                    .snippet(
                        DateTimeUtil.formatSharableTime(start.toLocalTime()).plus("-")
                            .plus(DateTimeUtil.formatSharableTime(end.toLocalTime()))
                    )
                    .position(LatLng(loc.lat, loc.lng))
                    .icon(BitmapDescriptorFactory.fromBitmap(stopMarkerIcon))
            )
        }
    }

    private fun fetchData() {
        viewModel.fetchHistory().reObserver(this) {
            if (it) {
                showDialog()
            } else {
                dismissDialog()
            }
        }
    }

    private fun setInitialDate() {
        viewModel.date.value = DateTimeUtil.formatDateHistory(System.currentTimeMillis())
        viewModel.dateTimeStamp.value = System.currentTimeMillis()
        viewModel.arrangeDate()
    }

    private fun setRangeBar() {
        binding.rangeBar.tickStart = 0f
        binding.rangeBar.tickEnd = viewModel.timeList.value!!.size - 1.toFloat()
        when (viewModel.day.value) {
            DeviceHistoryViewModel.TODAY -> {
                binding.rangeBar.setRangePinsByIndices(
                    ((viewModel.timeList.value!!.size) - INDEX_VALUE_24_HOURS),
                    viewModel.timeList.value!!.size - 1
                )
            }

            DeviceHistoryViewModel.YESTERDAY -> {
                binding.rangeBar.setRangePinsByIndices(
                    INDEX_VALUE_24_HOURS,
                    INDEX_VALUE_24_HOURS * 2
                )
            }

            else -> {
                binding.rangeBar.setRangePinsByIndices(
                    INDEX_VALUE_24_HOURS,
                    INDEX_VALUE_24_HOURS * 2
                )
            }
        }
        viewModel.fromTime.value = viewModel.timeList.value!![binding.rangeBar.leftIndex]
        viewModel.toTime.value = viewModel.timeList.value!![binding.rangeBar.rightIndex]
    }

    private fun setRangeBarListener() {
        binding.rangeBar.setOnRangeBarChangeListener(object : RangeBar.OnRangeBarChangeListener {
            override fun onRangeChangeListener(
                rangeBar: RangeBar,
                leftPinIndex: Int,
                rightPinIndex: Int,
                leftPinValue: String,
                rightPinValue: String
            ) {

            }

            override fun onTouchEnded(rangeBar: RangeBar) {
                viewModel.fromTime.value = viewModel.timeList.value!![rangeBar.leftIndex]
                viewModel.toTime.value = viewModel.timeList.value!![rangeBar.rightIndex]
            }

            override fun onTouchStarted(rangeBar: RangeBar) {}
        })
    }


    private fun openDatePicker() {
        val c = Calendar.getInstance()
        val mYear = c.get(Calendar.YEAR)
        val mMonth = c.get(Calendar.MONTH)
        val mDay = c.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            this,
            { _, year, monthOfYear, dayOfMonth ->
                c.set(year, monthOfYear, dayOfMonth)
                viewModel.date.value = DateTimeUtil.formatDateHistory(c.timeInMillis)
                viewModel.dateTimeStamp.value = c.timeInMillis
                viewModel.arrangeDate()

            }, mYear, mMonth, mDay
        )
        datePickerDialog.datePicker.maxDate = c.timeInMillis
        datePickerDialog.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        runnable?.apply {
            handler.removeCallbacks(this)
        }
    }
}
