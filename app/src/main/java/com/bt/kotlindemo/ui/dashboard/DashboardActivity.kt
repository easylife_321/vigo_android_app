package com.bt.kotlindemo.ui.dashboard

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.MenuNavigation
import com.bt.kotlindemo.databinding.ActivityDashboardBinding
import com.bt.kotlindemo.global.App
import com.bt.kotlindemo.interfaces.DeviceListCallback
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.model.app_menu.SubMenu
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.dashboard.device_list_view.ListViewDevicesFragment
import com.bt.kotlindemo.ui.dashboard.map_view.MapViewFragment
import com.bt.kotlindemo.ui.device_options.device_management.add_device.AddDeviceActivity
import com.bt.kotlindemo.ui.device_options.device_management.device_list.DeviceManagementActivity
import com.bt.kotlindemo.ui.device_options.groups.GroupManagementActivity
import com.bt.kotlindemo.ui.device_options.poi_management.poi_list.PoiListActivity
import com.bt.kotlindemo.ui.device_options.shareable_link.link_list.ShareableLinkListActivity
import com.bt.kotlindemo.ui.device_options.trips_management.TripsListActivity
import com.bt.kotlindemo.ui.device_options.zone_management.zone_list.ZoneListActivity
import com.bt.kotlindemo.ui.notifications.NotificationActivity
import com.bt.kotlindemo.ui.profile.profile_detail.ProfileDetailActivity
import com.bt.kotlindemo.ui.webview_activity.WebViewActivity
import com.bt.kotlindemo.utils.ActivityUtils
import com.bt.kotlindemo.utils.AppPreferences
import com.squareup.picasso.Picasso
import org.koin.android.ext.android.inject
import java.lang.reflect.Method


class DashboardActivity : BaseActivity(), DeviceListCallback {


    private lateinit var binding: ActivityDashboardBinding
    private val preferences: AppPreferences by inject()


    override fun onCreate(savedInstanceState: Bundle?) {
        setNoLimits()
        super.onCreate(savedInstanceState)


    }


    override fun setBinding() {
        binding =
            DataBindingUtil.setContentView(this@DashboardActivity, R.layout.activity_dashboard)
        // addNavigationBarPadding(this, binding.content.root)
    }


    override fun initViews() {
        setDrawer()
        setLeftMenu()
        App.instance.getAppMenu().let {
            if (it.isNotEmpty())
                loadView(it[1], false)
        }

        setProfileClick()
        setUserDetail()
        checkNotificationPermission()
        setAppVersion()
    }

    private fun setAppVersion() {
        binding.txtVersion.text = getString(R.string.app_version, BuildConfig.VERSION_NAME)
    }

    private fun checkNotificationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.POST_NOTIFICATIONS
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(arrayOf(Manifest.permission.POST_NOTIFICATIONS), 1)

            }

        }
    }


    private fun setProfileClick() {
        val header: View = binding.navView.getHeaderView(0)
        (header.findViewById<ConstraintLayout>(R.id.layout))?.setOnClickListener {
            handleLeftDrawer()
            startActivity(Intent(this@DashboardActivity, ProfileDetailActivity::class.java))
        }
    }

    private fun setUserDetail() {
        val header: View = binding.navView.getHeaderView(0)
        (header.findViewById<TextView>(R.id.txtName))?.text = preferences.getUserName()
        val number = "+${preferences.getCountry().countryCode}${preferences.getMobile()}"
        (header.findViewById<TextView>(R.id.txtMobile))?.text = number
        // Glide.with(this).load("${preferences.getBaseImageUrl()}${preferences.getProfileImage()}").into((header.findViewById(R.id.imgProfile) as ImageView))
        if (!preferences.getProfileImage().isNullOrEmpty()) {
            Picasso.get()
                .load("${preferences.getBaseImageUrl()}${preferences.getProfileImage()}")
                .into((header.findViewById<ImageView>(R.id.imgProfile)))
        } else {
            (header.findViewById<ImageView>(R.id.imgProfile))?.setImageResource(R.drawable.user_icon)
        }

    }

    private fun setDrawer() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.title = ""
        val toggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        toggle.isDrawerIndicatorEnabled = false;
        binding.imgDrawer.setOnClickListener {
            handleLeftDrawer()
        }
        binding.content.imgOptionsFrame.setOnClickListener {
            handleRightDrawer()
        }


        //binding.drawerLayout.setScrimColor(Color.TRANSPARENT);
        binding.drawerLayout.addDrawerListener(toggle)
        binding.drawerLayout.setContrastThreshold(3f);
        toggle.syncState()


        binding.drawerLayout.setViewScale(Gravity.START, 0.9f)
        binding.drawerLayout.setRadius(Gravity.START, 35f)
        binding.drawerLayout.setViewElevation(Gravity.START, 20f)
        binding.drawerLayout.setViewScrimColor(GravityCompat.START, Color.TRANSPARENT);

        binding.drawerLayout.setViewScale(Gravity.END, 0.9f)
        binding.drawerLayout.setRadius(Gravity.END, 35f)
        binding.drawerLayout.setViewElevation(Gravity.END, 20f)
        binding.drawerLayout.useCustomBehavior(Gravity.END);
        binding.drawerLayout.setViewScrimColor(GravityCompat.END, Color.TRANSPARENT);
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)

        setLeftMenu()

    }


    private fun handleRightDrawer() {
        if (binding.drawerLayout.isDrawerVisible(GravityCompat.END)) {
            binding.drawerLayout.closeDrawer(GravityCompat.END)
        } else {
            binding.drawerLayout.openDrawer(GravityCompat.END)
        }
    }

    private fun handleLeftDrawer() {
        if (binding.drawerLayout.isDrawerVisible(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            setUserDetail()
            binding.drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            super.onBackPressed()
            binding.imgDrawer.visibility = View.VISIBLE
        } else {
            finish()
        }
    }


    private fun attachFragment(fragment: Fragment) {
        ActivityUtils.replaceFragmentWithAnim(
            supportFragmentManager,
            binding.content.container.id,
            fragment,
            MapViewFragment::class.java.simpleName
        )
    }

    private fun setLeftMenu() {
        App.instance.getAppMenu().let {
            binding.adapterLeft = LeftDrawerAdapter(it, object : RecyclerClickListener {
                override fun onClick(objects: Any) {
                    loadView(objects as com.bt.kotlindemo.model.app_menu.Menu)
                }


            }, preferences.getBaseImageUrl() ?: "")
        }

    }


    fun loadView(menu: com.bt.kotlindemo.model.app_menu.Menu, close: Boolean = true) {
        /*  if (menu.menuUrl != MenuNavigation.DEVICES && menu.menuUrl != MenuNavigation.DASHBOARD) {
              if (close)
                  handleLeftDrawer()
              return
          }*/

        when (menu.menuUrl) {
            MenuNavigation.DASHBOARD -> {
                //startActivity(Intent(this, ContactListActivity::class.java))
            }
            MenuNavigation.DEVICES -> {
                if (menu.submenuids.isNotEmpty()) {
                    binding.content.imgOptionsFrame.visibility = View.VISIBLE
                    setRightMenu(menu.submenuids)
                } else {
                    binding.adapter = null
                    binding.content.imgOptionsFrame.visibility = View.GONE
                }
                if (supportFragmentManager.fragments.isEmpty() || supportFragmentManager.fragments[0] !is MapViewFragment) {
                    attachFragment(MapViewFragment.newInstance())
                }
            }
            MenuNavigation.NOTIFICATIONS -> {
                startActivity(Intent(this, NotificationActivity::class.java))
            }
            MenuNavigation.URL -> {
                startActivity(Intent(this, WebViewActivity::class.java).apply {
                    putExtra(IntentConstants.TITLE, menu.menuName)
                    putExtra(IntentConstants.DATA, menu.redirectTo)
                })
            }

        }
        if (close)
            handleLeftDrawer()
    }

    fun showRightMenu() {
        if (binding.adapter != null) {
            binding.content.imgOptionsFrame.visibility = View.VISIBLE
        }
    }


    private fun setRightMenu(subMenu: List<SubMenu>) {
        binding.adapter = RightMenuAdapter(
            subMenu,
            object : RecyclerClickListener {
                override fun onClick(objects: Any) {
                    navigate(objects as SubMenu)
                }

            }, preferences.getBaseImageUrl() ?: ""
        )


    }

    private fun navigate(subMenu: SubMenu) {
        when (subMenu.subMenuURL) {
            MenuNavigation.ADD_DEVICE -> {
                startActivity(Intent(this@DashboardActivity, AddDeviceActivity::class.java))
                slideFromRight()
            }
            MenuNavigation.ZONE_MANAGEMENT -> {
                startActivity(Intent(this@DashboardActivity, ZoneListActivity::class.java))
                slideFromRight()
            }
            MenuNavigation.SHAREABLE_LINKS -> {
                startActivity(Intent(this@DashboardActivity, ShareableLinkListActivity::class.java))
                slideFromRight()
            }
            MenuNavigation.POI -> {
                startActivity(Intent(this@DashboardActivity, PoiListActivity::class.java))
                slideFromRight()
            }
            MenuNavigation.DEVICES -> {
                startActivity(Intent(this@DashboardActivity, PoiListActivity::class.java))
                slideFromRight()
            }
            MenuNavigation.GROUP_MANAGEMENT -> {
                startActivity(Intent(this@DashboardActivity, GroupManagementActivity::class.java))
                slideFromRight()
            }
            MenuNavigation.DEVICE_MANAGEMENT -> {
                startActivity(Intent(this@DashboardActivity, DeviceManagementActivity::class.java))
                slideFromRight()
            }
            MenuNavigation.TRIPS -> {
                startActivity(Intent(this@DashboardActivity, TripsListActivity::class.java))
                slideFromRight()
            }
            MenuNavigation.URL -> {
                startActivity(Intent(this, WebViewActivity::class.java).apply {
                    putExtra(IntentConstants.TITLE, subMenu.subMenuName)
                    putExtra(IntentConstants.DATA, subMenu.redirectTo)
                })
            }

        }
        handleRightDrawer()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //  menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun openList(device: ArrayList<Group>) {
        ActivityUtils.addFragment(
            supportFragmentManager,
            binding.content.container.id,
            ListViewDevicesFragment.newInstance(device),
            ListViewDevicesFragment::class.simpleName ?: ""
        )
        binding.imgDrawer.visibility = View.GONE
    }


    fun addNavigationBarPadding(context: Activity, v: View) {
        val resources = context.resources
        /* if (isSoftNavigationBarAvailable()) {
             val orientation = resources.configuration.orientation
             val size = getNavigationBarSize(resources)
             when (orientation) {
                 Configuration.ORIENTATION_LANDSCAPE -> {
                     if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N &&
                         context.isInMultiWindowMode
                     ) {
                         return
                     }
                     v.setPadding(
                         v.paddingLeft, v.paddingTop,
                         v.paddingRight + size, v.paddingBottom
                     )
                 }
                 Configuration.ORIENTATION_PORTRAIT -> v.setPadding(
                     v.paddingLeft, v.paddingTop,
                     v.paddingRight, v.paddingBottom + size
                 )
             }*/
        if (checkDeviceHasNavigationBar()) {
            val resourceId: Int =
                resources.getIdentifier("navigation_bar_height", "dimen", "android")
            val a = if (resourceId > 0) {
                resources.getDimensionPixelSize(resourceId)
            } else 0
            v.setPadding(
                v.paddingLeft, v.paddingTop,
                v.paddingRight, a
            )
        }

    }

    fun checkDeviceHasNavigationBar(): Boolean {
        var hasNavigationBar = false
        val id = resources.getIdentifier("config_showNavigationBar", "bool", "android")
        if (id > 0) {
            hasNavigationBar = resources.getBoolean(id)
            if (!hasNavigationBar) return false
        }
        try {
            val systemPropertiesClass = Class.forName("android.os.SystemProperties")
            val m: Method = systemPropertiesClass.getMethod("get", String::class.java)
            val navBarOverride = m.invoke(systemPropertiesClass, "qemu.hw.mainkeys") as String
            if ("1" == navBarOverride) {
                hasNavigationBar = false
            } else if ("0" == navBarOverride) {
                hasNavigationBar = true
            }
        } catch (e: Exception) {
        }
        return hasNavigationBar
    }

}

