/*
 * *
 *  * Created by Surajit on 15/5/20 6:08 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 15/5/20 1:48 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_detail.device_comands

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemDeviceCommandsBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.device_command.Commands


class DeviceCommandAdapter(
    private val commands: List<Commands>,
    private val recyclerClickListener: RecyclerClickListener
) :
    RecyclerView.Adapter<DeviceCommandAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_device_commands,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(commands[position])
        holder.binding.layout.setOnClickListener {
            recyclerClickListener.onClick(commands[position])
        }

    }

    override fun getItemCount(): Int {
        return commands.size
    }

    //the class is hodling the list view
    class ViewHolder(val binding: ListItemDeviceCommandsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindItems(command: Commands) {
            binding.command = command
        }
    }
}