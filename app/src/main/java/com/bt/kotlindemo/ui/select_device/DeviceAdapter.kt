/*
 * *
 *  * Created by Surajit on 30/5/20 8:58 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/5/20 3:08 PM
 *  *
 */

package com.bt.kotlindemo.ui.select_device

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemDeviceBinding
import com.bt.kotlindemo.databinding.ListItemDeviceManagermntBinding
import com.bt.kotlindemo.databinding.ListItemSelectDeviceBinding
import com.bt.kotlindemo.data.database.entity.Device
import java.util.*

class DeviceAdapter(
    private val devices: ArrayList<Device>
) : RecyclerView.Adapter<DeviceAdapter.ViewHolder>(), Filterable {

    var deviceFilterList = ArrayList<Device>()

    init {
        deviceFilterList = devices
    }

    override fun getItemCount(): Int {
        return deviceFilterList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemSelectDeviceBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_select_device,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.device = deviceFilterList[position]
        if (position == deviceFilterList.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }


        holder.binding.layout.setOnClickListener {
            deviceFilterList[position].isSelected = !deviceFilterList[position].isSelected
            notifyItemChanged(position)

        }


    }


    override fun onViewRecycled(holder: ViewHolder) {
        holder.itemView.setOnLongClickListener(null)
        super.onViewRecycled(holder)
    }


    inner class ViewHolder(
        val binding: ListItemSelectDeviceBinding
    ) : RecyclerView.ViewHolder(binding.root)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                deviceFilterList = if (charSearch.isEmpty()) {
                    devices
                } else {
                    val resultList = ArrayList<Device>()
                    for (row in devices) {
                        if (row.deviceName.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = deviceFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                deviceFilterList = results?.values as ArrayList<Device>
                notifyDataSetChanged()
            }

        }
    }

}

