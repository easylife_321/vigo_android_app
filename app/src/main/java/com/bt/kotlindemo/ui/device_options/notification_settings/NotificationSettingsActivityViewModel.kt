/*
 * *
 *  * Created by Surajit on 14/4/20 6:33 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 13/4/20 7:21 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.notification_settings

import androidx.lifecycle.LiveData
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.network.netwrok_responses.NotificationSettingsResponse
import com.bt.kotlindemo.data.repository.NotificationRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.NotificationSettings
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonArray
import com.google.gson.JsonObject

class NotificationSettingsActivityViewModel(
    var preference: AppPreferences,
    private var notificationSettingsRepository: NotificationRepository
) : BaseViewModel() {


    fun getNotificationSettings(id: Int): LiveData<ApiResult<NotificationSettingsResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("groupId", id)
        jsonObject.addProperty("userId", preference.getUserId())
        return notificationSettingsRepository.getNotificationSettings(jsonObject)
    }

    fun saveNotificationSettings(
        notificationList: ArrayList<NotificationSettings>,
        id: Int
    ): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("groupId", id)
        jsonObject.addProperty("userId", preference.getUserId())
        val jsonArray = JsonArray()
        for (i in notificationList.indices) {
            val item = JsonObject()
            item.addProperty("ntfTypeId", notificationList[i].ntfTypeId)
            item.addProperty("ntfValue", notificationList[i].ntfValue)
            jsonArray.add(item)
        }
        jsonObject.add("allNtfs", jsonArray)
        return notificationSettingsRepository.saveNotificationSettings(jsonObject)
    }

    fun selectAll(selectAll: Boolean, notifications: ArrayList<NotificationSettings>?) {
        if (selectAll) {
            notifications?.forEach {
                it.ntfValue = 1
            }
        } else {
            notifications?.forEach {
                it.ntfValue = 0
            }
        }


    }

    fun checkAll(notificationList: ArrayList<NotificationSettings>): Boolean {
        var selected: Int = 0
        var notSelcted: Int = 0
        notificationList.forEach {
            if (it.ntfValue == 1) {
                selected++
            } else {
                notSelcted++
            }
        }
        if (selected != 0 && notSelcted != 0)
            return false
        else return selected != 0 && notSelcted == 0
    }


}