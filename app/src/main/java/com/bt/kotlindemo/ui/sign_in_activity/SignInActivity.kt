package com.bt.kotlindemo.ui.sign_in_activity

import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ActivitySignInBinding
import com.bt.kotlindemo.di.selectCountryModule
import com.bt.kotlindemo.global.App
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.dashboard.DashboardActivity
import com.bt.kotlindemo.utils.ActivityUtils
import com.bt.kotlindemo.utils.reObserver
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules


class SignInActivity : BaseActivity() {

    private lateinit var binding: ActivitySignInBinding

    private val viewModel: RegistrationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        loadKoinModules(selectCountryModule)
        transparentStatusAndNavigation()
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in)
    }

    private fun getMenu() {
        val appMenu = viewModel.getAppMenu()
        appMenu.reObserver(this, Observer {
            when (it.status) {
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showToastRetry(
                        binding.root,
                        it.message!!,
                        retry = object : ToastRetry {
                            override fun retry() {
                                getMenu()
                            }
                        }
                    )
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    viewModel.preferences.setRegistered(true)
                    startActivity(Intent(this, DashboardActivity::class.java))
                    finish()
                }
                ApiResult.Status.LOADING -> {
                    showDialog()
                }

                else -> {}
            }
        })


    }

    override fun initViews() {
        attachFragment()
        viewModel.fetchMenu.observe(this, Observer { getMenu() })
    }


    fun changeFragment(fragment: Fragment) {
        ActivityUtils.replaceFragmentWithAnim(
            supportFragmentManager,
            binding.container.id,
            fragment,
            fragment::class.java.simpleName
        )
    }

    override fun onBackPressed() {
        val currentFragment: Fragment? =
            supportFragmentManager.findFragmentById(R.id.container)
        if (currentFragment is ChangePasswordFragment) return
        if (supportFragmentManager.backStackEntryCount > 1) {
            supportFragmentManager.popBackStack()
        } else {
            finish()
        }
    }

    private fun attachFragment() {
        ActivityUtils.addFragment(
            supportFragmentManager,
            binding.container.id,
            WelcomeFragment.newInstance(),
            WelcomeFragment::class.java.simpleName ?: ""
        )
    }


    override fun onDestroy() {
        super.onDestroy()
        unloadKoinModules(selectCountryModule)

    }
}
