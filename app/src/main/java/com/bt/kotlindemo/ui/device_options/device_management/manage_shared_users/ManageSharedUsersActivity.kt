/*
 * *
 *  * Created by Surajit on 9/4/20 11:57 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 11:56 AM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.manage_shared_users

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.databinding.ActivityManageSharedUsersBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.shared_users.UserDetail
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AlertHelper
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class ManageSharedUsersActivity : BaseActivity() {

    private val viewModel: ManagedSharedUsersViewModel by viewModel()
    private lateinit var binding: ActivityManageSharedUsersBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        setStatusBarColor(R.color.colorPrimaryDark)
        setStatusBarTint(false)
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_manage_shared_users)
        binding.apply {
            lifecycleOwner = this@ManageSharedUsersActivity
            viewModel = this@ManageSharedUsersActivity.viewModel

        }
        binding.adapter = SharedUserAdapter(object : RecyclerClickListener {
            override fun onClick(objects: Any) {

            }
        })
        setSupportActionBar(binding.toolbar)

    }


    override fun initViews() {

        viewModel.onBackPressed.observe(this) {
            finish()
        }

        viewModel.isFullTime.observe(this) {
            if (it) {
                setList(viewModel.listFull.value ?: emptyList())
            } else {
                setList(viewModel.listTimeSpecific.value ?: emptyList())
            }
        }

        viewModel.showMessage.observe(this) {
            AlertHelper.showOkAlert(this, it)
        }
        viewModel.showMessageSuccess.observe(this) {
            AlertHelper.showOkAlertFinish(this, it)
        }

        fetchData()
    }

    private fun fetchData() {
        viewModel.getDevicesSharedDetails(intent.getIntExtra(IntentConstants.DEVICE_ID, 0))
            .observe(this) {
                if (it)
                    showDialog() else dismissDialog()
            }
    }

    private fun setList(users: List<UserDetail>) {
        binding.adapter!!.setData(users)
    }


}
