/*
 * *
 *  * Created by Surajit on 10/4/20 3:08 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 5:21 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.manage_shared_users

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.bt.kotlindemo.data.repository.DeviceRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.shared_users.SharedDetails
import com.bt.kotlindemo.model.shared_users.UserDetail
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ManagedSharedUsersViewModel(
    var preference: AppPreferences,
    private var deviceRepository: DeviceRepository
) : BaseViewModel() {


    var showMessage = MutableLiveData<String>()
    var showMessageSuccess = MutableLiveData<String>()
    var listDevices = MutableLiveData<List<SharedDetails>>()
    var listFull = MutableLiveData<List<UserDetail>>()
    var listTimeSpecific = MutableLiveData<List<UserDetail>>()
    var isFullTime = MutableLiveData<Boolean>()


    fun getDevicesSharedDetails(
        deviceId: Int,
    ): LiveData<Boolean> {
        val jsonObject = JsonObject()

        jsonObject.addProperty("userId", preference.getUserId())

        val devices = JsonArray()
        val device = JsonObject()
        device.addProperty("deviceId", deviceId)
        devices.add(device)
        jsonObject.add("ShareDevicesList", devices)
        return deviceRepository.getDevicesSharedDetails(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> true
                ApiResult.Status.SUCCESS -> {
                    if (it.data == null) {
                        showMessage.value =
                            "(Error code:102)We encountered a small glitch. If the problem please contact support"
                    } else {
                        if (it?.data.details == null) {
                            showMessage.value =
                                "(Error code:101)We encountered a small glitch. If the problem please contact support"

                        } else {
                            listDevices.value = it.data.details?: emptyList()
                            filterData()
                        }
                    }
                    false
                }
                ApiResult.Status.ERROR -> {
                    showMessage.value = it.message?:""
                    false
                }
                ApiResult.Status.CUSTOM -> {
                    showMessage.value = it.message?:""
                    false
                }

            }
        }

    }

    private fun filterData() {
        viewModelScope.launch(Dispatchers.Default) {
            var isHeaderAdded: Boolean
            var isHeaderAddedTS: Boolean
            val listFull = arrayListOf<UserDetail>()
            val listTS = arrayListOf<UserDetail>()
            listDevices.value?.onEach {
                it.shareDate?.onEach { detail ->
                    isHeaderAdded = false
                    isHeaderAddedTS = false
                    detail.userdetails?.onEach { userDetail ->
                        if (userDetail.shareType == "F") {
                            if (!isHeaderAdded) {
                                listFull.add(
                                    UserDetail(isHeader = true, headerText = detail.date ?: "")

                                )
                                isHeaderAdded = true
                                listFull.add(userDetail)
                            }
                        } else {
                            if (!isHeaderAddedTS) {
                                listTS.add(
                                    UserDetail(isHeader = true, headerText = detail.date ?: "")

                                )
                                isHeaderAddedTS = true
                                listTS.add(userDetail)
                            }
                        }
                    }
                }
            }

            this@ManagedSharedUsersViewModel.listFull.postValue(listFull)
            this@ManagedSharedUsersViewModel.listTimeSpecific.postValue(listTS)
            isFullTime.postValue(true)
        }
    }


    fun getDevices() = deviceRepository.getDevicesAdmin()


    fun changeMode(isFull: Boolean) {
        isFullTime.value = isFull
    }


}