/*
 * *
 *  * Created by Surajit on 12/9/20 11:46 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 12/9/20 11:46 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.device_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.FragmentDeviceOptionsBinding
import com.bt.kotlindemo.databinding.FragmentGroupOptionsBinding
import com.bt.kotlindemo.ui.base.BaseFragment
import com.bt.kotlindemo.ui.device_options.groups.GroupManagementActivityViewModel
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class DeviceOptionsFragment : BaseFragment() {

    private val viewModel: DeviceManagementViewModel by sharedViewModel()
    private lateinit var binding: FragmentDeviceOptionsBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.fragment_device_options,
            container,
            false
        )
        binding.apply {
            lifecycleOwner = this@DeviceOptionsFragment
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addObservers()
    }

    private fun addObservers() {

    }


    companion object {
        @JvmStatic
        fun newInstance() = DeviceOptionsFragment()

    }
}