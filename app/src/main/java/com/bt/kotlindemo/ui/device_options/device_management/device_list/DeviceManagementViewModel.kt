/*
 * *
 *  * Created by Surajit on 15/4/20 7:47 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 11/4/20 12:40 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.device_list

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.repository.DeviceRepository
import com.bt.kotlindemo.data.repository.GroupRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.google.gson.JsonArray
import com.google.gson.JsonObject

class DeviceManagementViewModel(
    var preference: AppPreferences,
    private var groupRepository: GroupRepository,
    private var deviceRepository: DeviceRepository
) : BaseViewModel() {

    var isLoaded = MediatorLiveData<Boolean>()

    var allDevices = MutableLiveData<ArrayList<Device>>()
    var devicesToShow = MutableLiveData<List<Device>>()
    var isLoading = MutableLiveData<Boolean>()
    var showOptions = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var showMessageRetry = MutableLiveData<String>()
    var isEmpty = MutableLiveData<Boolean>()
    var showSelected = MutableLiveData<Boolean>()
    var selectAll = ObservableBoolean()
    var isAnySelected = ObservableBoolean()
    private var jsonObject: JsonObject = JsonObject()

    val edit = SingleLiveEvent<Boolean?>()
    val delete = SingleLiveEvent<Boolean?>()
    val share = SingleLiveEvent<Boolean?>()
    val move = SingleLiveEvent<Boolean?>()


    init {
        showOptions.value = true
        isAnySelected.set(false)
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("groupId", 0)
    }


    fun getIcons() = deviceRepository.getIcons()

    fun getGroup(): LiveData<List<Group>?> = groupRepository.getGroups().map {
        if (it.status == ApiResult.Status.LOADING) {
            isLoading.value = true
            isLoaded.value = false
            null
        } else if (it.status == ApiResult.Status.ERROR || it.status == ApiResult.Status.CUSTOM) {
            if (it.status == ApiResult.Status.ERROR)
                showMessageRetry.value = it.message?:"" else showMessage.value = it.message?:""
            isLoading.value = false
            isLoaded.value = false
            null
        } else {
            val grp = it?.data?.groups?.toMutableList()
            grp?.add(0, Group(0, "All Groups", 0))
            grp

        }


    }


    fun getAllDevices(groupId: Int = 0, isMyDeviceSelected: Boolean = true) {
        val devices = deviceRepository.getDeviceList(jsonObject)
        isLoaded.addSource(devices) {
            if (it.status == ApiResult.Status.SUCCESS) {
                allDevices.value = it.data?.devices as? ArrayList
                isLoaded.value = true
                isLoaded.removeSource(devices)
                getDevicesByGroup(groupId, isMyDeviceSelected)
            } else if (it.status == ApiResult.Status.ERROR) {
                isLoading.value = false
                showMessageRetry.value = it.message?:""
            } else if (it.status == ApiResult.Status.CUSTOM) {
                isLoading.value = false
                showMessage.value = it.message?:""
            }
        }


    }

    fun getDevicesByGroup(groupId: Int, isMyDeviceSelected: Boolean) {
        devicesToShow.value = allDevices.value?.filter {
            if (groupId == 0) {
                it.isAdmin == isMyDeviceSelected
            } else {
                it.groupid == groupId && it.isAdmin == isMyDeviceSelected
            }

        }
        isEmpty.value = devicesToShow.value.isNullOrEmpty()
        showOptions.value = devicesToShow.value.isNullOrEmpty()
        isLoading.value = false
    }


    fun getDevices(): LiveData<List<Device?>?> =
        deviceRepository.getDeviceList(jsonObject).map {
            if (it.status == ApiResult.Status.LOADING) {
                isLoading.value = true
                null
            } else if (it.status == ApiResult.Status.ERROR) {
                isLoading.value = false
                null
            } else {
                isLoading.value = false
                it?.data?.devices

            }


        }

    fun deleteDevice(deviceId: Int = 0): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()
        val arr = JsonArray()
        if (deviceId != 0) {
            val id = JsonObject()
            id.addProperty("deviceId", deviceId)
            arr.add(id)
        } else {
            arr.addAll(getSelectedDevices())
        }
        jsonObject.add("devicesList", arr)
        jsonObject.addProperty("userId", preference.getUserId())
        return deviceRepository.deleteDevice(jsonObject,deviceId)
    }

    fun moveDevice(deviceId: Int = 0, groupId: Int): LiveData<ApiResult<BaseResponse>> {
        val jsonObject = JsonObject()
        val arr = JsonArray()
        if (deviceId != 0) {
            val id = JsonObject()
            id.addProperty("deviceId", deviceId)
            arr.add(id)
        } else {
            arr.addAll(getSelectedDevices())
        }
        jsonObject.add("devicesList", arr)
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("togroupId", groupId)

        return deviceRepository.moveDevice(jsonObject)
    }


    private fun getSelectedDevices(): JsonArray {
        val arr = JsonArray()
        devicesToShow.value?.forEach {
            if (it.isSelected) {
                val id = JsonObject()
                id.addProperty("deviceId", it.deviceId)
                arr.add(id)
            }
        }
        return arr
    }


    fun selectAll(isSelected: Boolean) {
        devicesToShow.value?.forEach {
            it.isSelected = isSelected
        }
        if (!isSelected)
            selectAll.set(false)
        showSelected.value = isSelected
        isAnySelected.set(isSelected)
    }

    fun selectDevice(deviceId: Int) {
        devicesToShow.value?.forEach {
            if (it.deviceId == deviceId) {
                it.isSelected = true
                return@forEach
            }

        }
        showSelected.value = true
    }


    fun checkIsAllDevicesChecked() {
        var all = true
        var isAnySelected = false
        devicesToShow.value?.forEach {
            if (!it.isSelected) {
                all = false
                return@forEach
            } else {
                isAnySelected = true
            }
        }
        this@DeviceManagementViewModel.isAnySelected.set(isAnySelected)
        selectAll.set(all)
    }

    fun updateList(
        device: Device,
        groupId: Int,
        myDeviceSelected: Boolean
    ) {
        allDevices.value?.forEachIndexed { index, it ->
            if (device.deviceId == it.deviceId) {
                allDevices.value?.set(index, device)
                return@forEachIndexed
            }
        }
        getDevicesByGroup(groupId, myDeviceSelected)
    }

    fun edit() {
        edit.call()
    }

    fun delete() {
        delete.call()
    }

    fun share() {
        share.call()
    }

    fun move() {
        move.call()
    }


}