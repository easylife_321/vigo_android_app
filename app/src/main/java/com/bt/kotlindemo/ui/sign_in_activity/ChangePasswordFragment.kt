package com.bt.kotlindemo.ui.sign_in_activity


import android.os.Bundle
import android.text.TextUtils
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.FragmentChangePasswordBinding
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.ui.base.BaseFragment
import com.bt.kotlindemo.utils.onRightDrawableClicked
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class ChangePasswordFragment : BaseFragment() {


    private lateinit var binding: FragmentChangePasswordBinding
    private val viewModel: RegistrationViewModel by sharedViewModel()

    companion object {
        @JvmStatic
        fun newInstance() =
            ChangePasswordFragment()
    }

    private fun setViews() {
        binding.handler = ClickHandler()


        binding.etPassword.onRightDrawableClicked {
            if (it.tag == 1) {
                it.tag = 2
                it.transformationMethod = null
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_visible, 0);
            } else {
                it.tag = 1
                it.transformationMethod = PasswordTransformationMethod()
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_hidden, 0);
            }
        }
        binding.etConfirmPassword.onRightDrawableClicked {
            if (it.tag == 1) {
                it.tag = 2
                it.transformationMethod = null
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_visible, 0);
            } else {
                it.tag = 1
                it.transformationMethod = PasswordTransformationMethod()
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_hidden, 0);
            }
        }

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false)
        binding.handler = ClickHandler()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setViews()
    }


    private fun setPassword() {
        viewModel.setPassword().observe(viewLifecycleOwner, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    showSnackBar(binding.root, it.message!!)
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    viewModel.fetchMenu.value = true
                }

                else -> {}
            }
        })
    }


    inner class ClickHandler() {
        fun skip() {
            //redirectToHome()
            viewModel.fetchMenu.value = true
        }

        fun changePassword() {
            if (TextUtils.isEmpty(binding.etPassword.text.toString().trim())) {
                showSnackBar(
                    binding.root,
                    "Please enter your password"
                )
                binding.etPassword.error = "Required"
                return
            }

            if (TextUtils.isEmpty(binding.etConfirmPassword.text.toString().trim())) {
                showSnackBar(
                    binding.root,
                    "Please enter confirm password"
                )
                binding.etConfirmPassword.error = "Required"
                return
            }


            if (binding.etPassword.text.toString().trim() !=
                binding.etConfirmPassword.text.toString().trim()

            ) {

                showSnackBar(
                    binding.root,
                    "Password and confirm password does not match"
                )
                binding.etConfirmPassword.error = "Mismatch"
                return
            }
            viewModel.preferences.setPassword(binding.etPassword.text.toString().trim())
            setPassword()

        }


    }


}
