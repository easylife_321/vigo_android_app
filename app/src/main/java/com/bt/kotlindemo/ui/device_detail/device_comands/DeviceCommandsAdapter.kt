/*
 * *
 *  * Created by Surajit on 20/5/20 7:51 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/4/20 12:35 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_detail.device_comands

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemDeviceCommandsBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.device_command.Commands

class DeviceCommandsAdapter(
    private val recyclerClickListener: RecyclerClickListener
) :
    RecyclerView.Adapter<DeviceCommandsAdapter.ViewHolder>() {


    var commands = arrayListOf<Commands>()

    fun updateList(newList: List<Commands>) {
        //val diffResult = DiffUtil.calculateDiff(DiffCallback(this.commands, newList))
        commands.clear()
        commands.addAll(newList)
        notifyItemRangeChanged(0,commands.size)
       // diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int {
        return commands.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemDeviceCommandsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_device_commands,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.command = commands[position]
        holder.binding.layout.setOnClickListener {
            recyclerClickListener.onClick(commands[position])
        }

    }

    inner class ViewHolder(
        val binding: ListItemDeviceCommandsBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

    class DiffCallback(
        private val oldList: List<Commands>,
        private val newList: List<Commands>
    ) : DiffUtil.Callback() {

        override fun getOldListSize(): Int = oldList.size

        override fun getNewListSize(): Int = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList.get(newItemPosition)
        }

        override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
            val (_, value, _) = oldList[oldPosition]
            val (_, value1, _) = newList[newPosition]

            return value == value1
        }

        @Nullable
        override fun getChangePayload(oldPosition: Int, newPosition: Int): Any? {
            return super.getChangePayload(oldPosition, newPosition)
        }
    }
}

