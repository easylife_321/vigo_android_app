/*
 * *
 *  * Created by Surajit on 12/4/20 6:44 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 11/4/20 12:40 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.groups

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.repository.GroupRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.google.gson.JsonObject

class GroupManagementActivityViewModel(
    var preference: AppPreferences,
    private var groupRepository: GroupRepository,
    private val application: Application
) : BaseViewModel() {

    val edit = SingleLiveEvent<Boolean?>()
    val delete = SingleLiveEvent<Boolean?>()
    val share = SingleLiveEvent<Boolean?>()
    val notification = SingleLiveEvent<Boolean?>()
    val create = MutableLiveData<String>()
    val update = MutableLiveData<String>()
    val isLoading = MutableLiveData<Boolean>()
    val showMessage = MutableLiveData<String>()
    val closeFragment = SingleLiveEvent<Void?>()

    fun getGroups() = groupRepository.getGroups()


    fun createGroup(name: String): LiveData<String?> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("Name", name)
        jsonObject.addProperty("UserId", preference.getUserId())
        return groupRepository.createGroup(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                    null
                }

                ApiResult.Status.ERROR -> {
                    isLoading.value = false
                    showMessage.value = it.message?:""
                    null
                }
                ApiResult.Status.SUCCESS -> {
                    closeFragment.call()
                    it?.data?.result?.msg
                }
                ApiResult.Status.CUSTOM -> null

            }
        }
    }

    fun deleteGroup(id: Int): LiveData<String?> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("Id", id)
        return groupRepository.deleteGroup(jsonObject).map {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                    null
                }

                ApiResult.Status.ERROR -> {
                    isLoading.value = false
                    showMessage.value = it.message?:""
                    null
                }
                ApiResult.Status.SUCCESS -> {
                    closeFragment.call()
                    it?.data?.result?.msg
                }
                ApiResult.Status.CUSTOM -> null

            }
        }

    }

    fun updateGroup(name: String, id: Int): LiveData<String?> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("Name", name)
        jsonObject.addProperty("UserId", preference.getUserId())
        jsonObject.addProperty("Id", id)

        return groupRepository.updateGroup(jsonObject).map{
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    isLoading.value = true
                    null
                }

                ApiResult.Status.ERROR -> {
                    isLoading.value = false
                    showMessage.value = it.message?:""
                    null
                }
                ApiResult.Status.SUCCESS -> {
                    closeFragment.call()
                    it?.data?.result?.msg
                }
                ApiResult.Status.CUSTOM -> {
                    null
                }
            }
        }

    }

    fun edit() {
        edit.call()
    }

    fun delete() {
        delete.call()
    }

    fun share() {
        share.call()
    }

    fun notification() {
        notification.call()
    }

    fun create(name: String) {
        create.value = name
    }

    fun update(name: String) {
        update.value = name
    }


}