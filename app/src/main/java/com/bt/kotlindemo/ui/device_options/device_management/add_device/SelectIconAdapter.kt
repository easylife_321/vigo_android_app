/*
 * *
 *  * Created by Surajit on 10/4/20 9:32 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 10/4/20 9:32 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.add_device

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.databinding.ListItemIconBinding
import com.bt.kotlindemo.model.DeviceIcon

class SelectIconAdapter(
    val group: ArrayList<DeviceIcon>,
    val mListener: RecyclerClickListener
) :
    RecyclerView.Adapter<SelectIconAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return group.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemIconBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_icon,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.icon = group[position]
        if(position==group.size-1){
            holder.binding.view.visibility= View.INVISIBLE
        }else{
            holder.binding.view.visibility= View.VISIBLE
        }
        holder.binding.layout.setOnClickListener {
            mListener.onClick(group[position])
        }

    }

    class ViewHolder(
        val binding: ListItemIconBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

}

