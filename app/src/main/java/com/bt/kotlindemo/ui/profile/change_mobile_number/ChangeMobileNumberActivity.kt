/*
 * *
 *  * Created by Surajit on 7/7/20 4:07 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 7/7/20 4:07 PM
 *  *
 */

package com.bt.kotlindemo.ui.profile.change_mobile_number

import android.content.Intent
import android.view.KeyEvent
import android.view.View
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.databinding.ActivityChangeMobileNumberBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.select_country.SelectCountryActivity
import com.bt.kotlindemo.utils.AlertHelper
import com.bt.kotlindemo.utils.hideKeyboard
import com.bt.kotlindemo.utils.reObserver
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChangeMobileNumberActivity : BaseActivity() {

    private lateinit var binding: ActivityChangeMobileNumberBinding
    private val viewModel: ChangeMobileNumberViewModel by viewModel()
    private var whoHasFocus = 0
    private val phoneNumberUtil: PhoneNumberUtil by inject()

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_mobile_number)
        binding.apply {
            lifecycleOwner = this@ChangeMobileNumberActivity
            viewModel = this@ChangeMobileNumberActivity.viewModel

        }
    }

    override fun initViews() {
        binding.etCode1.setOnKeyListener(onKeyListener)
        binding.etCode2.setOnKeyListener(onKeyListener)
        binding.etCode3.setOnKeyListener(onKeyListener)
        binding.etCode4.setOnKeyListener(onKeyListener)

        binding.etCode1.onFocusChangeListener = onFocusChangeListener
        binding.etCode2.onFocusChangeListener = onFocusChangeListener
        binding.etCode3.onFocusChangeListener = onFocusChangeListener
        binding.etCode4.onFocusChangeListener = onFocusChangeListener

        attachObservers()

    }

    private var onKeyListener = View.OnKeyListener { v, keyCode, event ->
        if (event.action == KeyEvent.ACTION_DOWN) {
            if (keyCode == KeyEvent.KEYCODE_DEL) {
                when (v.id) {
                    R.id.etCode2 -> {
                        binding.etCode2.setText("")
                        binding.etCode1.requestFocus()
                    }
                    R.id.etCode3 -> {
                        binding.etCode3.setText("")
                        binding.etCode2.requestFocus()
                    }
                    R.id.etCode4 -> {
                        binding.etCode4.setText("")
                        binding.etCode3.requestFocus()
                    }

                }
            }
        }

        false
    }

    var onFocusChangeListener =
        View.OnFocusChangeListener { view, isFocused ->
            when (view.id) {
                R.id.etCode1 -> if (isFocused) whoHasFocus = 1
                R.id.etCode2 -> if (isFocused) whoHasFocus = 2
                R.id.etCode3 -> if (isFocused) whoHasFocus = 3
                R.id.etCode4 -> if (isFocused) whoHasFocus = 4
                else -> {
                }
            }
        }

    private fun textChange(text: CharSequence) {
        if (text.length == 1) {
            when (whoHasFocus) {
                1 -> {
                    viewModel.code[0] = text[0]
                    binding.etCode2.requestFocus()
                }
                2 -> {
                    viewModel.code[1] = text[0]
                    binding.etCode3.requestFocus()
                }
                3 -> {
                    viewModel.code[2] = text[0]
                    binding.etCode4.requestFocus()
                }
                4 -> {
                    viewModel.code[3] = text[0]
                    hideKeyboard()
                }

            }
        }
    }


    private fun attachObservers() {
        viewModel.onBackPressed.observe(this, {
            finish()
        })

        viewModel.inputCode.observe(this, { textChange(it) })

        viewModel.selectCountry.observe(this, {
            startActivityForResult(
                Intent(this, SelectCountryActivity::class.java),
                IntentRequestConstants.SELECT_COUNTRY
            )
        })

        viewModel.isLoading.observe(this, {
            if (it) showDialog() else dismissDialog()
        })

        viewModel.showMessage.observe(this, {
            AlertHelper.showOkAlert(this, it)
        })

        viewModel.showMessageFinish.observe(this, {
            setResult(RESULT_OK)
            AlertHelper.showOkAlertFinish(this, it)
        })


        viewModel.send.observe(this, {
            if (viewModel.isSent.value == false) {
                if (viewModel.number.value.isNullOrEmpty()) {
                    showSnackBar(binding.root, "Please enter your mobile number")
                    return@observe
                }
                AlertHelper.showPasswordAlert(
                    this@ChangeMobileNumberActivity,
                    viewModel.preference,
                    object : AlertCallback {
                        override fun action(isOk: Boolean, data: Any?) {
                            if (isOk) {
                                verify(data as String)
                            }
                        }
                    })

            } else {
                viewModel.changeAccountMobileNo().observe(this, {})
            }
        })

        viewModel.resend.observe(this, {
            verify(viewModel.password.value ?: "")
        })


    }

    fun verify(otp: String) {
        if (!phoneNumberUtil.isValidNumber(
                phoneNumberUtil.parse(
                    viewModel.number.value,
                    viewModel.country.value?.isoAlpha
                )
            )
        ) {
            showSnackBar(binding.root, "Enter a valid mobile number")
            return
        }
        binding.etMobile.isEnabled = false
        binding.etCountryCode.isEnabled = false
        viewModel.sendOtp(otp).reObserver(this, {})
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            viewModel.country.value =
                data!!.getSerializableExtra(IntentConstants.DATA) as CountryList
        }
    }


}
