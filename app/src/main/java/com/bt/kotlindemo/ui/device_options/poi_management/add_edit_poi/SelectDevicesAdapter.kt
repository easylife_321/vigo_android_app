/*
 * *
 *  * Created by Surajit on 23/5/20 5:43 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/5/20 3:08 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.poi_management.add_edit_poi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemDeviceBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.data.database.entity.Device
import java.util.*

class SelectDevicesAdapter(
    val devices: ArrayList<Device>,
    val mListener: RecyclerClickListener
) : RecyclerView.Adapter<SelectDevicesAdapter.ViewHolder>(), Filterable {


    private var mDevices: ArrayList<Device> = devices

    override fun getItemCount(): Int {
        return mDevices.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemDeviceBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_device,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.device = devices[position]
        if (position == mDevices.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }
        holder.binding.layout.setOnClickListener {
            mListener.onClick(mDevices[position])
        }

    }

    inner class ViewHolder(
        val binding: ListItemDeviceBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                mDevices = filterResults.values as ArrayList<Device>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val queryString = charSequence.toString().toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString.isEmpty())
                    devices
                else
                    devices.filter {
                        it.deviceName.toLowerCase(Locale.getDefault()).contains(queryString)
                    }
                return filterResults
            }
        }
    }
}




