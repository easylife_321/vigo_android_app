/*
 * *
 *  * Created by Surajit on 23/5/20 2:53 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/5/20 4:35 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.trips_management

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.bt.kotlindemo.data.repository.TripRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.Trips
import com.bt.kotlindemo.ui.base.BaseViewModel
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.SingleLiveEvent
import com.bt.kotlindemo.utils.toUtc
import com.google.gson.JsonObject
import java.time.Duration
import java.time.Instant
import java.util.Date

class TripsListViewModel(
    private val preference: AppPreferences,
    private var repository: TripRepository,
    private val context: Application
) : BaseViewModel() {

    var isLoading = MutableLiveData<Boolean>()
    var showMessage = MutableLiveData<String>()
    var addZone = MutableLiveData<Boolean>()
    var isEdit = ObservableField<Boolean>()
    var list = MutableLiveData<List<Trips>>()
    var search = MutableLiveData<Boolean>()
    var isEmpty = MutableLiveData<Boolean>()
    var to = MutableLiveData<Long>()
    var from = MutableLiveData<Long>()
    var fetch = SingleLiveEvent<Boolean?>()
    var setToDate = SingleLiveEvent<Void?>()
    var setFromDate = SingleLiveEvent<Boolean?>()

    init {
        isEmpty.value = false
        to.value = System.currentTimeMillis()
        from.value = Date.from(Instant.now().minus(Duration.ofDays(30))).time
    }

    fun search() {
        if (list.value != null)
            search.value = true
    }

    fun addZone() {
        addZone.value = true
    }


    fun getTripList(): LiveData<Unit> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("fromTimestamp", from.value?.toUtc())
        jsonObject.addProperty("toTimestamp", to.value?.toUtc())
        return repository.getTripList(jsonObject).map {
            if (it.status == ApiResult.Status.LOADING) {
                isLoading.value = true

            } else if (it.status == ApiResult.Status.ERROR || it.status == ApiResult.Status.CUSTOM) {
                if (it.status == ApiResult.Status.ERROR)
                    showMessage.value = it.message ?: ""
                isLoading.value = false

            } else {
                if (it.data == null) {
                    showMessage.value =
                        "(Error code:102)We encountered a small glitch. If the problem please contact support"
                } else {
                    isLoading.value = false
                    list.value = it.data.trips ?: emptyList()
                    isEmpty.value = it.data.trips?.isEmpty()
                }
            }


        }
    }

    fun endTrip(tripId: String): LiveData<Unit> {
        val jsonObject = JsonObject()
        jsonObject.addProperty("userId", preference.getUserId())
        jsonObject.addProperty("tripId", tripId)
        return repository.endTrip(jsonObject).map {
            if (it.status == ApiResult.Status.LOADING) {
                isLoading.value = true

            } else if (it.status == ApiResult.Status.ERROR || it.status == ApiResult.Status.CUSTOM) {
                if (it.status == ApiResult.Status.ERROR)
                    showMessage.value = it.message ?: ""
                isLoading.value = false

            } else {
                if (it.data == null) {
                    showMessage.value =
                        "(Error code:102)We encountered a small glitch. If the problem please contact support"
                } else {
                    fetch.call()
                    isLoading.value = false
                    showMessage.value = it.data.result.msg

                }
            }


        }
    }

    fun setToDate() {
        setToDate.call()
    }

    fun setFromDate() {
        setFromDate.call()
    }


}