package com.bt.kotlindemo.ui.sign_in_activity


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.databinding.FragmentSignInBinding
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.ui.base.BaseFragment
import com.bt.kotlindemo.ui.select_country.SelectCountryActivity
import com.bt.kotlindemo.utils.isValidMobile
import com.bt.kotlindemo.utils.onRightDrawableClicked
import com.bt.kotlindemo.utils.reObserver
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class SignInFragment : BaseFragment() {


    private lateinit var binding: FragmentSignInBinding
    val viewModel: RegistrationViewModel by sharedViewModel()
    private var countryList: CountryList? = null

    companion object {

        @JvmStatic
        fun newInstance() =
            SignInFragment()
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false)
        binding.apply {
            lifecycleOwner = this@SignInFragment
            handler = ClickHandler()
            country = viewModel.getCountry()
            maxLength = viewModel.preferences.getPhoneLength()
        }
        setViews()
        setObserver()
        return binding.root
    }

    private fun setObserver() {
        viewModel.token.reObserver(viewLifecycleOwner, Observer {
            if (viewLifecycleOwner.lifecycle.currentState == Lifecycle.State.RESUMED)
                login(
                    binding.etMobile.text.toString().trim(),
                    binding.etPassword.text.toString().trim(),
                    it
                )
        })
    }

    private fun setViews() {
        binding.etPassword.onRightDrawableClicked {
            if (it.tag == 1) {
                it.tag = 2
                it.transformationMethod = null
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_visible, 0);
            } else {
                it.tag = 1
                it.transformationMethod = PasswordTransformationMethod()
                it.setSelection(it.text.length)
                it.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_passowrd_hidden, 0);
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            countryList = data!!.getSerializableExtra(IntentConstants.DATA) as CountryList
            binding.country = countryList
            viewModel.preferences.setCountry(countryList!!)
            viewModel.preferences.setPhoneLength(
                viewModel.phoneNumberUtil.getExampleNumber(
                    countryList!!.isoAlpha
                ).nationalNumber.toString().length
            )
            binding.maxLength = viewModel.preferences.getPhoneLength()
            binding.etMobile.setText("")
        }
    }


    private fun login(mobile: String, password: String, token: String) {
        val login = viewModel.login(mobile, password, binding.country!!, token)
        login.reObserver(viewLifecycleOwner, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR,ApiResult.Status.CUSTOM -> {
                    dismissDialog()
                    showSnackBar(
                        binding.root,
                        it.message!!
                    )
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    // redirectToHome()
                    viewModel.fetchMenu.value = true
                }

                else -> {}
            }
        })
    }

    /*  private fun redirectToHome() {
          startActivity(Intent(activity, DashboardActivity::class.java))
          activity?.finish()
      }*/


    inner class ClickHandler {


        fun switchSignUp() {
            //viewModel.switchSignUp.value = true
            (context as SignInActivity).changeFragment(SignUpFragment.newInstance())
        }


        fun login() {
            if (TextUtils.isEmpty(binding.etMobile.text.toString().trim())) {
                showSnackBar(
                    binding.root,
                    "Please enter your mobile number"
                )
                binding.etMobile.error = "Required"
                return
            }

            if (!binding.etMobile.text.toString().trim().isValidMobile()) {
                showSnackBar(
                    binding.root,
                    "Please enter valid mobile number"
                )
                binding.etMobile.error = "Invalid"
                return
            }
            if (TextUtils.isEmpty(binding.etPassword.text.toString().trim())) {
                showSnackBar(
                    binding.root,
                    "Please enter your password"
                )
                binding.etPassword.error = "Required"
                return
            }
            viewModel.loginWithOtp = false
            viewModel.getToken()

        }


        fun loginWithOtp() {
            if (TextUtils.isEmpty(binding.etMobile.text.toString().trim())) {
                showSnackBar(
                    binding.root,
                    "Please enter your mobile number"
                )
                binding.etMobile.error = "Required"
                return
            }

            if (!binding.etMobile.text.toString().trim().isValidMobile()) {
                showSnackBar(
                    binding.root,
                    "Please enter valid mobile number"
                )
                binding.etMobile.error = "Invalid"
                return
            }

            viewModel.preferences.setCountry(binding.country!!)
            viewModel.preferences.setMobile(binding.etMobile.text.toString().trim())
            viewModel.preferences.setPassword(binding.etPassword.text.toString().trim())
            viewModel.loginWithOtp = true
            (context as SignInActivity).changeFragment(OtpFragment.newInstance())


        }

        fun selectCountry() {
            startActivityForResult(
                Intent(activity, SelectCountryActivity::class.java),
                IntentRequestConstants.SELECT_COUNTRY
            )
        }


    }


}
