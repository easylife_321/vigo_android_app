/*
 * *
 *  * Created by Surajit on 23/5/20 2:50 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 4:30 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.device_list

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemDeviceManagermntBinding
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.data.database.entity.Device
import java.util.*

class DeviceListAdapter(
    private val devices: ArrayList<Device>,
    private val mListener: GroupRecyclerClickListener? = null
) : RecyclerView.Adapter<DeviceListAdapter.ViewHolder>(), Filterable {

    private var showCheck = false;

    var deviceFilterList = ArrayList<Device>()

    init {
        deviceFilterList = devices
    }

    override fun getItemCount(): Int {
        return deviceFilterList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemDeviceManagermntBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_device_managermnt,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.device = deviceFilterList[position]
        holder.binding.showCheck = showCheck
        if (position == deviceFilterList.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }


        holder.binding.chkBox.setOnClickListener {
            devices[position].isSelected = !deviceFilterList[position].isSelected
            notifyItemChanged(position)
            checkIsAllChecked(it.context)

        }
        holder.binding.layout.setOnLongClickListener {
            showCheck = true
            deviceFilterList[position].isSelected = true
            notifyDataSetChanged()
            checkIsAllChecked(it.context)
            true
        }

        holder.binding.layout.setOnClickListener {
            // setUpPopMenu(holder.binding.imgOptions, devices[position])
            if (!showCheck)
                mListener?.onClick(devices[position], 0)
        }


    }

    private fun setUpPopMenu(imgOptions: ImageView, devices: Device) {
        val popup = PopupMenu(imgOptions.context, imgOptions, Gravity.END);
        popup.inflate(R.menu.device_options_menu);
        if (!devices.isAdmin) {
            popup.menu.findItem(R.id.share).isVisible = false
        }

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.delete -> {
                    mListener?.onClick(devices, GroupRecyclerClickListener.DELETE)
                }
                R.id.share -> {
                    mListener?.onClick(devices, GroupRecyclerClickListener.SHARE)
                }
                R.id.edit -> {
                    mListener?.onClick(devices, GroupRecyclerClickListener.EDIT)
                }
                R.id.move -> {
                    mListener?.onClick(devices, GroupRecyclerClickListener.MOVE)
                }
            }

            false
        }

        popup.show();
    }


    private fun checkIsAllChecked(context: Context) {
        (context as DeviceManagementActivity).checkAll()
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.itemView.setOnLongClickListener(null)
        super.onViewRecycled(holder)
    }

    fun updateData(devices: List<Device>) {
        this@DeviceListAdapter.devices.clear()
        this@DeviceListAdapter.devices.addAll(devices)
        this@DeviceListAdapter.deviceFilterList = this@DeviceListAdapter.devices
        notifyDataSetChanged()
    }

    fun showSelected(show: Boolean) {
        showCheck = show
        notifyDataSetChanged()
    }

    inner class ViewHolder(
        val binding: ListItemDeviceManagermntBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                deviceFilterList = if (charSearch.isEmpty()) {
                    devices
                } else {
                    val resultList = ArrayList<Device>()
                    for (row in devices) {
                        if (row.deviceName.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = deviceFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                deviceFilterList = results?.values as ArrayList<Device>
                notifyDataSetChanged()
            }

        }
    }

}

