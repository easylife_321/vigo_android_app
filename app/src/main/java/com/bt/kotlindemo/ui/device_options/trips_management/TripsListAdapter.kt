/*
 * *
 *  * Created by Surajit on 25/5/20 8:01 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:26 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.trips_management

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemTripsBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.interfaces.ZoneRecyclerClickListener
import com.bt.kotlindemo.model.Trips
import com.bt.kotlindemo.model.zone.Zone
import java.util.*

class TripsListAdapter(
    val trips: ArrayList<Trips>,
    val mListener: RecyclerClickListener
) : RecyclerView.Adapter<TripsListAdapter.ViewHolder>(), Filterable {


    private var mTrips: ArrayList<Trips> = trips

    override fun getItemCount(): Int {
        return mTrips.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemTripsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_trips,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.trips = mTrips[position]
        holder.binding.btnEnd.setOnClickListener {
            mListener.onClick(mTrips[position])
        }

    }

    inner class ViewHolder(
        val binding: ListItemTripsBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: FilterResults) {
                mTrips = filterResults.values as ArrayList<Trips>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val queryString = charSequence.toString().toLowerCase(Locale.getDefault())

                val filterResults = FilterResults()
                filterResults.values = if (queryString.isEmpty())
                    trips
                else
                    trips.filter {
                        it.tripName?.toLowerCase(Locale.getDefault())?.contains(queryString)
                            ?: false
                    }
                return filterResults
            }
        }
    }


}




