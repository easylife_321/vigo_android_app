/*
 * *
 *  * Created by Surajit on 14/4/20 7:39 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 13/4/20 6:24 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.notification_settings

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemNotificationSettingsBinding
import com.bt.kotlindemo.interfaces.GroupRecyclerClickListener
import com.bt.kotlindemo.model.NotificationSettings
import com.bt.kotlindemo.utils.toogle.ToggleButton


class NotificationSettingsAdapter(
    val notificationSettings: ArrayList<NotificationSettings>,
    private val mListener: GroupRecyclerClickListener? = null
) :
    RecyclerView.Adapter<NotificationSettingsAdapter.ViewHolder>() {

    private var context: Context? = null

    override fun getItemCount(): Int {
        return notificationSettings.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        if (context == null)
            context = parent.context
        val binding: ListItemNotificationSettingsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_notification_settings,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.group = notificationSettings[position]
        if (position == notificationSettings.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }

        val texts = arrayOf<String?>("OFF", "NORMAL", "SOS")
        holder.binding.mstbMultiId.setElements(texts)
        ContextCompat.getColor(context!!, R.color.white).let {
            holder.binding.mstbMultiId.colorPressedText = it
        }
        ContextCompat.getColor(context!!, R.color.colorPrimary).let {
            holder.binding.mstbMultiId.colorPressedBackground = it
        }

        ContextCompat.getColor(context!!, R.color.colorPrimary).let {
            holder.binding.mstbMultiId.colorNotPressedText = it
        }
        ContextCompat.getColor(context!!, R.color.white).let {
            holder.binding.mstbMultiId.colorNotPressedBackground = it
        }

        val pos =
            if (notificationSettings[position].ntfValue == 1) 1 else if (notificationSettings[position].ntfValue == 2) 2 else 0
        holder.binding.mstbMultiId.setValue(pos)

        holder.binding.switchNoti.setOnClickListener {
            notificationSettings[position].ntfValue =
                if (notificationSettings[position].ntfValue == 1) 0 else 1
            notifyItemChanged(position)
            (context as NotificationSettingsActivity).show()
        }

        holder.binding.mstbMultiId.setOnValueChangedListener(object :
            ToggleButton.OnValueChangedListener {
            override fun onValueChanged(value: Int) {
                notificationSettings[holder.bindingAdapterPosition].ntfValue = when (value) {
                    0 -> {
                        0
                    }
                    1 -> {
                        1
                    }
                    else -> {
                        2
                    }
                }
                (context as NotificationSettingsActivity).showSave()
            }
        })


    }

    inner class ViewHolder(
        val binding: ListItemNotificationSettingsBinding
    ) : RecyclerView.ViewHolder(binding.root)

}

