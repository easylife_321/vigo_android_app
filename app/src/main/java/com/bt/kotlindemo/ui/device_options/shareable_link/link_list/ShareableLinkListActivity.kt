/*
 * *
 *  * Created by Surajit on 25/5/20 7:43 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:43 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.shareable_link.link_list

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.databinding.ActivityShareableLinkListBinding
import com.bt.kotlindemo.databinding.FragmentLinkOptionsBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.LinkRecyclerClickListener
import com.bt.kotlindemo.interfaces.ToastRetry
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.shareable_link.ShareableLink
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.device_options.shareable_link.add_new_link.AddNewLinkActivity
import com.bt.kotlindemo.utils.*
import com.ferfalk.simplesearchview.SimpleSearchView
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class ShareableLinkListActivity : BaseActivity() {

    private lateinit var binding: ActivityShareableLinkListBinding
    private val viewModel: ShareableLInkListViewModel by viewModel()
    private var selectedLink: ShareableLink? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        setStatusBarColor(R.color.colorPrimary)
        super.onCreate(savedInstanceState)

    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shareable_link_list)
        binding.apply {
            viewModel = this@ShareableLinkListActivity.viewModel
            lifecycleOwner = this@ShareableLinkListActivity
        }
    }

    override fun initViews() {
        addObservers()
        fetchList()
        setSearchView()
    }

    private fun addObservers() {
        viewModel.isLoading.observe(this, Observer {
            if (it) {
                showDialog()
            } else {
                dismissDialog()
            }
        })

        viewModel.showMessageRetry.observe(this, Observer {
            showToastRetry(binding.root, it, object : ToastRetry {
                override fun retry() {
                    fetchList()
                }
            })

        })

        viewModel.link.observe(this, Observer {
            setList(it)
        })

        viewModel.onBackPressed.observe(this, Observer {
            finish()
        })

        viewModel.search.observe(this, Observer {
            binding.toolbar.visibility = View.INVISIBLE
            binding.searchView.visibility = View.VISIBLE
            binding.searchView.showSearch(true)
        })

        viewModel.add.observe(this, Observer {
            startActivityForResult(Intent(this, AddNewLinkActivity::class.java).apply {
                putExtras(Bundle().apply {
                    putBoolean(IntentConstants.EDIT, false)
                })
            }, IntentRequestConstants.ADD_LINK)
        })


        viewModel.delete.observe(this, Observer {
            AlertHelper.showOKCancelAlert(this@ShareableLinkListActivity,
                "Are you sure you want to delete this link? ",
                object : AlertCallback {
                    override fun action(isOk: Boolean, a: Any?) {
                        if (isOk) {
                            deleteAndObserve(selectedLink!!)
                        }
                    }
                })
        })

        viewModel.edit.observe(this, Observer {
            val intent =
                Intent(
                    this@ShareableLinkListActivity,
                    AddNewLinkActivity::class.java
                ).apply {
                    val bundle = Bundle().apply {
                        putSerializable(IntentConstants.DATA, selectedLink)
                        putBoolean(IntentConstants.EDIT, true)
                    }
                    putExtras(bundle)
                }
            startActivityForResult(intent, IntentRequestConstants.ADD_LINK)
        })

        viewModel.share.observe(this, {
            selectedLink?.let {
                if(it.isExpired){
                    showSnackBar(binding.root,"This link is expired.Please edit the link or add a new link to share.")
                }else if(!it.isActive){
                    showSnackBar(binding.root,"This link is not enabled. Please enable the link to share.")
                }
                else {
                    val msg = selectedLink?.trackingMessage?.replace(
                        "#fromDate#", DateTimeUtil.formatSharableTime(it.validfromTimestamp.toUtc())
                            .replace(
                                "#toDate#",
                                DateTimeUtil.formatSharableTime(it.validtillTimestamp.toUtc())
                            )
                    )
                    if (msg != null) {
                        ActivityUtils.share(this@ShareableLinkListActivity, msg)
                    }
                }
            }

        })


    }

    private fun setSearchView() {
        binding.searchView.setOnSearchViewListener(object : SimpleSearchView.SearchViewListener {
            override fun onSearchViewShownAnimation() {
            }

            override fun onSearchViewClosed() {
                if (binding.adapter != null)
                    binding.adapter!!.filter.filter("")
                binding.toolbar.visibility = View.VISIBLE
                binding.searchView.visibility = View.INVISIBLE
            }

            override fun onSearchViewClosedAnimation() {

            }

            override fun onSearchViewShown() {

            }

        })

        binding.searchView.setOnQueryTextListener(object : SimpleSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (binding.adapter != null) {
                    binding.adapter!!.filter.filter(newText)
                }
                return false
            }

            override fun onQueryTextCleared(): Boolean {
                return false
            }
        })

    }

    override fun onBackPressed() {
        if (binding.searchView.isSearchOpen) {
            binding.searchView.closeSearch(true)
        } else {
            super.onBackPressed()
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            fetchList()
        }
    }

    override fun onPause() {
        super.onPause()
        if (isFinishing) {
            slideToRight()
        }
    }

    private fun setList(it: ArrayList<ShareableLink>) {
        binding.adapter = ShareableLinkListAdapter(it, object : LinkRecyclerClickListener {
            override fun onClick(data: Any, action: Int) {
                selectedLink = data as ShareableLink
                when (action) {
                    LinkRecyclerClickListener.ENABLE_DISABLE -> {
                        AlertHelper.showOKCancelAlert(this@ShareableLinkListActivity,
                            "Are you sure you want to ${if (selectedLink?.isActive == true) "disable" else "enable"} this link",
                            object : AlertCallback {
                                override fun action(isOk: Boolean, data: Any?) {
                                    if (isOk) {
                                        toggleAndObserve(selectedLink!!)
                                    }
                                }
                            })
                    }
                    else -> {
                        /*  ActivityUtils.removeFragmentWithAnimToBottom(
                              supportFragmentManager, supportFragmentManager.findFragmentByTag(
                                  LinkOptionsFragment::class.java.simpleName
                              )
                          )
                          ActivityUtils.replaceFragmentWithAnimFromBottom(
                              supportFragmentManager,
                              binding.container.id,
                              LinkOptionsFragment.newInstance(),
                              AddGroupFragment::class.java.simpleName
                          )*/
                        showOptions()
                    }
                }
            }
        })
    }

    private fun toggleAndObserve(link: ShareableLink) {
        viewModel.toggle(link).observe(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    binding.adapter!!.updateLink(link, false)
                    dismissDialog()
                    AlertHelper.showOkAlert(this@ShareableLinkListActivity, it.message!!, null)
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(
                        this@ShareableLinkListActivity,
                        it.data!!.result.msg,
                        object : AlertCallback {
                            override fun action(isOk: Boolean, data: Any?) {
                                binding.adapter!!.updateLink(link)
                            }
                        })
                }
                else -> return@Observer
            }
        })
    }


    private fun deleteAndObserve(link: ShareableLink) {
        viewModel.deleteLink(link).observe(this, Observer {
            when (it.status) {
                ApiResult.Status.LOADING -> showDialog()
                ApiResult.Status.ERROR -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(this@ShareableLinkListActivity, it.message!!, null)
                }
                ApiResult.Status.SUCCESS -> {
                    dismissDialog()
                    AlertHelper.showOkAlert(
                        this@ShareableLinkListActivity,
                        it.data!!.result.msg,
                        object : AlertCallback {
                            override fun action(isOk: Boolean, data: Any?) {
                                binding.adapter!!.removeDevice(link)
                            }
                        })
                }
                else -> return@Observer
            }
        })
    }

    private fun fetchList() {
        viewModel.getLink().observe(this, Observer { })
    }

    private fun showOptions() {
        try {
            val binding: FragmentLinkOptionsBinding =DataBindingUtil.inflate(layoutInflater,R.layout.fragment_link_options,null,false)
            val mBottomDialogNotificationAction =
                BottomSheetDialog(this, R.style.bottomSheetDialogTheme)
            mBottomDialogNotificationAction.setContentView(binding.root)
            mBottomDialogNotificationAction.show()
            binding.imgShare.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.share.call()
            }
            binding.imgEdit.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.edit.call()
            }

            binding.imgDelete.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
                viewModel.delete.call()
            }

            binding.layout.setOnClickListener {
                mBottomDialogNotificationAction.dismiss()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}
