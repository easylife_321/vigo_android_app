/*
 * *
 *  * Created by Surajit on 1/3/21 1:30 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/3/21 1:30 AM
 *  *
 */

package com.bt.kotlindemo.ui.profile.update_profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemStateBinding
import com.bt.kotlindemo.model.Country
import com.bt.kotlindemo.model.State

class CountrySpinnerAdapter(private val items: List<Country>) : BaseAdapter() {
    override fun getItem(p0: Int): Any {
        return items.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val viewHolder: ViewHolder
        val retView: View
        if (convertView == null) {
            val binding: ListItemStateBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_state, parent, false
            )
            retView = binding.root
            viewHolder = ViewHolder(binding)
            retView.tag = viewHolder
        } else {
            viewHolder = convertView.tag as ViewHolder
            retView = convertView
        }
        viewHolder.binding.text = items[position].countryName
        return retView
    }

    private class ViewHolder(var binding: ListItemStateBinding)
}