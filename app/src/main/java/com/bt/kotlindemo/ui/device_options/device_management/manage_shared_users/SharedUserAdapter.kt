/*
 * *
 *  * Created by Surajit on 10/4/20 9:32 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 10/4/20 9:32 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.manage_shared_users

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemHeaderBinding
import com.bt.kotlindemo.databinding.ListItemSharedUserBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.shared_users.UserDetail
import com.chauthai.swipereveallayout.ViewBinderHelper

class SharedUserAdapter(
    val mListener: RecyclerClickListener
) :
    RecyclerView.Adapter<SharedUserAdapter.BaseViewHolder>() {

    private var users = ArrayList<UserDetail>()
    private val viewBinderHelper = ViewBinderHelper()

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        if (viewType == 0) {
            return HeaderViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.list_item_header,
                    parent,
                    false
                )
            )
        } else {
            return ViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.list_item_shared_user,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        if (holder is HeaderViewHolder) {
            holder.binding.data = users[position].headerText
        } else if (holder is ViewHolder) {
            viewBinderHelper.bind(holder.binding.reveallayout, users[position].userId.toString())
            holder.binding.data = users[position]
        }

    }

    fun setData(users: List<UserDetail>) {
        this.users.clear()
        this.users.addAll(users)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return if (users[position].isHeader) 0 else 1
    }

    class HeaderViewHolder(
        val binding: ListItemHeaderBinding
    ) : BaseViewHolder(binding.root)

    class ViewHolder(
        val binding: ListItemSharedUserBinding
    ) : BaseViewHolder(binding.root)

    abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}

