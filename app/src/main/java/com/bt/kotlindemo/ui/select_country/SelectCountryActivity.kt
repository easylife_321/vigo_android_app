package com.bt.kotlindemo.ui.select_country


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.databinding.ActivitySelectCountryBinding
import com.bt.kotlindemo.di.selectCountryModule
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.utils.AppPreferences
import com.ferfalk.simplesearchview.SimpleSearchView
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import org.koin.android.ext.android.inject
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules


class SelectCountryActivity : BaseActivity() {

    private lateinit var binding: ActivitySelectCountryBinding
    private val countries: ArrayList<CountryList> by inject()
    val phoneNumberUtil: PhoneNumberUtil by inject()
    val preferences: AppPreferences by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        loadKoinModules(selectCountryModule)
        setStatusBarColor(R.color.colorPrimaryDark)
        super.onCreate(savedInstanceState)


    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_country)
        setSearchView()
        setSupportActionBar(binding.toolbar)

    }

    override fun initViews() {
        binding.adapter = SelectCountryAdapter(countries, object : RecyclerClickListener {
            override fun onClick(objects: Any) {
                val value = objects as CountryList
                val length =
                    phoneNumberUtil.getExampleNumber(value.isoAlpha).nationalNumber.toString().length
                preferences.setPhoneLength(length)
                val intent = Intent()
                intent.putExtra(IntentConstants.DATA, value)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        })

        binding.imgBack.setOnClickListener {
            finish()
        }

        binding.searchLayout.setOnClickListener {
            openSearch()
        }
    }

    fun openSearch() {
        binding.toolbar.visibility = View.INVISIBLE
        binding.searchView.visibility = View.VISIBLE
        binding.searchView.showSearch(true)
    }

    private fun setSearchView() {
        binding.searchView.setOnSearchViewListener(object : SimpleSearchView.SearchViewListener {
            override fun onSearchViewShownAnimation() {
            }

            override fun onSearchViewClosed() {
                if (binding.adapter != null) {
                    binding.adapter!!.filter.filter("")
                }
                binding.toolbar.visibility = View.VISIBLE
                binding.searchView.visibility = View.INVISIBLE
            }

            override fun onSearchViewClosedAnimation() {

            }

            override fun onSearchViewShown() {

            }

        })

        binding.searchView.setOnQueryTextListener(object : SimpleSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {

                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (binding.adapter != null) {
                    binding.adapter!!.filter.filter(newText)
                }
                return false
            }

            override fun onQueryTextCleared(): Boolean {
                return false
            }
        })

    }


    override fun onDestroy() {
        super.onDestroy()
        unloadKoinModules(selectCountryModule)
    }
}
