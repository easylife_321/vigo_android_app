/*
 * *
 *  * Created by Surajit on 9/4/20 11:57 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 11:56 AM
 *  *
 */

package com.bt.kotlindemo.ui.device_options.device_management.share

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.constants.IntentRequestConstants
import com.bt.kotlindemo.data.database.entity.Contacts
import com.bt.kotlindemo.databinding.ActivityTimeSpecificDevciceSharingBinding
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.model.Days
import com.bt.kotlindemo.model.SelectedTime
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.contacts.ContactListActivity
import com.bt.kotlindemo.ui.device_options.device_management.manage_shared_users.ManageSharedUsersActivity
import com.bt.kotlindemo.ui.select_country.SelectCountryActivity
import com.bt.kotlindemo.utils.AlertHelper
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class TimeSpecificDeviceSharingActivity : BaseActivity() {

    private val viewModel: TimeSpecificDeviceSharingViewModel by viewModel()
    private lateinit var binding: ActivityTimeSpecificDevciceSharingBinding
    private val phoneNumberUtil: PhoneNumberUtil by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        setStatusBarColor(R.color.colorPrimaryDark)
        setStatusBarTint(false)
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_time_specific_devcice_sharing)
        binding.apply {
            lifecycleOwner = this@TimeSpecificDeviceSharingActivity
            viewModel = this@TimeSpecificDeviceSharingActivity.viewModel

        }
        setSupportActionBar(binding.toolbar)

    }


    override fun initViews() {
        binding.daysAdapter = SelectDayAdapter()
        binding.adapter = SelectedTimeAdapter()
        binding.contactsAdapter = SelectContactsAdapter()


        viewModel.onBackPressed.observe(this, {
            finish()
        })


        viewModel.addTime.observe(this, {
            add()
        })

        viewModel.showPickerTo.observe(this, {
            openPicker(2)
        })
        viewModel.showPickerFrom.observe(this, {
            openPicker(1)
        })
        viewModel.selectCountry.observe(this, {
            startActivityForResult(
                Intent(this@TimeSpecificDeviceSharingActivity, SelectCountryActivity::class.java),
                IntentRequestConstants.SELECT_COUNTRY
            )
        })
        viewModel.addNumber.observe(this, {
            addContacts()
        })

        viewModel.verify.observe(this, {
            verify()
        })

        viewModel.openContacts.observe(this, {
            startActivityForResult(
                Intent(
                    this@TimeSpecificDeviceSharingActivity,
                    ContactListActivity::class.java
                ).apply {
                    putExtra(IntentConstants.IS_SELECTABLE, true)
                },
                IntentRequestConstants.SELECT_CONTACTS
            )
        })

        viewModel.showMessage.observe(this, {
            AlertHelper.showOkAlert(this, it)
        })
        viewModel.showMessageSuccess.observe(this, {
            AlertHelper.showOkAlertFinish(this, it)
        })

        viewModel.showShared.observe(this, {
            startActivity(
                Intent(
                    this@TimeSpecificDeviceSharingActivity,
                    ManageSharedUsersActivity::class.java
                ).apply {
                    putExtra(IntentConstants.DEVICE_ID, intent.getIntExtra(IntentConstants.DEVICE_ID, 0))
                }
            )
        })
    }


    private fun verify() {
        if (binding.contactsAdapter!!.numbers.isEmpty()) {
            showSnackBar(binding.root, getString(R.string.please_select_contacts_to_share_device))
            return
        }
        if (viewModel.isFullTime.value == false) {
            if (!binding.daysAdapter!!.isAnySelected()) {
                showSnackBar(binding.root, getString(R.string.select_days))
                return
            }

            if (binding.adapter!!.selectedTime.isEmpty()) {
                showSnackBar(binding.root, getString(R.string.select_time_interval))
                return
            }

        }

        share(
            binding.contactsAdapter!!.numbers,
            binding.daysAdapter!!.days,
            binding.adapter!!.selectedTime
        )
    }


    private fun share(
        contacts: ArrayList<Contacts>,
        days: ArrayList<Days>? = null,
        time: ArrayList<SelectedTime>? = null
    ) {
        viewModel.shareDevice(
            contacts,
            intent.getIntExtra(IntentConstants.DEVICE_ID, 0),
            days,
            time
        ).observe(this,
            {
                if (it) {
                    showDialog()
                } else {
                    dismissDialog()
                }
            })
    }


    private fun openPicker(forTime: Int) {
        val mCurrentTime = Calendar.getInstance()
        val hour: Int = mCurrentTime.get(Calendar.HOUR_OF_DAY)
        val minute: Int = mCurrentTime.get(Calendar.MINUTE)
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(
            this@TimeSpecificDeviceSharingActivity,
            { _, selectedHour, selectedMinute ->
                mCurrentTime.set(Calendar.HOUR_OF_DAY, selectedHour)
                mCurrentTime.set(Calendar.MINUTE, selectedMinute)
                if (forTime == 1) {
                    viewModel.from.set(mCurrentTime.timeInMillis)
                } else {
                    viewModel.to.set(mCurrentTime.timeInMillis)
                }
            }, hour, minute, false
        )
        mTimePicker.setTitle("Select ${if (forTime == 1) "From" else "To"} Time")
        mTimePicker.show()

    }

    private fun addContacts() {
        if (viewModel.mobile.get().isNullOrEmpty()) {
            showSnackBar(binding.root, "Enter a number to add")
            return
        }
        if (!phoneNumberUtil.isValidNumber(
                phoneNumberUtil.parse(
                    viewModel.mobile.get(),
                    viewModel.country.get()!!.isoAlpha
                )
            )
        ) {
            showSnackBar(binding.root, "Enter a valid mobile number")
            return
        }
        binding.contactsAdapter!!.add(viewModel.country.get()?.countryCode + viewModel.mobile.get()!!)
    }


    fun add() {
        if (viewModel.from.get() == 0L) {
            showSnackBar(binding.root, "Enter from time")
            return
        }
        if (viewModel.to.get() == 0L) {
            showSnackBar(binding.root, "Enter to time")
            return
        }
        if (viewModel.from.get()!! > viewModel.to.get()!!) {
            showSnackBar(binding.root, "From time should be before to time")
            return
        }
        binding.adapter!!.add(SelectedTime(viewModel.from.get()!!, viewModel.to.get()!!))
        viewModel.from.set(0)
        viewModel.to.set(0)
        binding.etStart.text = ""
        binding.etEnd.text = ""
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == IntentRequestConstants.SELECT_COUNTRY) {
                viewModel.country.set(data!!.getSerializableExtra(IntentConstants.DATA) as CountryList)
            } else {
                val contacts =
                    data?.getSerializableExtra(IntentConstants.DATA) as? ArrayList<Contacts>
                contacts?.apply {
                    this.onEach {
                        binding.contactsAdapter!!.add(it)
                    }
                }

            }
        }
    }


}
