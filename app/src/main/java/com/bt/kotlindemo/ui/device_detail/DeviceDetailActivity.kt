/*
 * *
 *  * Created by Surajit on 23/4/20 6:39 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/4/20 6:22 PM
 *  *
 */

package com.bt.kotlindemo.ui.device_detail

import android.animation.ValueAnimator
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.Gravity
import android.view.View
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import android.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.AppConstants
import com.bt.kotlindemo.constants.DeviceIconType
import com.bt.kotlindemo.constants.DeviceStatus
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.custom.CustomBottomSheetBehavior
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.ActivityDeviceDetailBinding
import com.bt.kotlindemo.interfaces.BottomSheetCloseCallBack
import com.bt.kotlindemo.model.device_command.Commands
import com.bt.kotlindemo.ui.base.BaseActivity
import com.bt.kotlindemo.ui.device_detail.device_comands.DeviceCommandFragment
import com.bt.kotlindemo.ui.device_history.DeviceHistoryActivity
import com.bt.kotlindemo.utils.ImageUtil
import com.bt.kotlindemo.utils.google_map.IconUtil
import com.bt.kotlindemo.utils.google_map.MapsUtil
import com.bt.kotlindemo.utils.reObserver
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.Projection
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.bt.kotlindemo.utils.location.AirLocation
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import kotlin.math.roundToInt


class DeviceDetailActivity : BaseActivity(), BottomSheetCloseCallBack {


    private var circle: Circle? = null
    private var polyline: Polyline? = null
    private val viewModel: DeviceDetailViewModel by viewModel {
        parametersOf(
            intent!!.getIntExtra(
                IntentConstants.DEVICE_ID,
                0
            )
        )
    }

    private var airLocation: AirLocation? = null
    private lateinit var behavior: CustomBottomSheetBehavior<View>
    private var isNotAdded = true
    private var ignoreScroll: Boolean = false
    val animator: ValueAnimator = ValueAnimator.ofFloat(0f, 100f)
    var userMarker: Marker? = null
    private lateinit var binding: ActivityDeviceDetailBinding
    private var once = true
    private var adjust = true
    private var mMap: GoogleMap? = null
    private lateinit var marker: Marker
    private var previousLatLng: LatLng? = null
    private val handler = Handler()
    private var runnable: Runnable? = null
    private var selectPos = 0
    private var isFollowing = false


    override fun onCreate(savedInstanceState: Bundle?) {
        makeStatusBarTransparent()
        super.onCreate(savedInstanceState)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_device_detail)
        binding.apply {
            viewModel = this@DeviceDetailActivity.viewModel
            lifecycleOwner = this@DeviceDetailActivity
        }
        binding.bottomSheetViewpager.disableScroll(true)

    }

    override fun initViews() {
        setMap()
        setObservers()


    }

    private fun setMap() {
        val mapFragment =
            supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync {
            mMap = it
            mMap!!.uiSettings.isRotateGesturesEnabled = false
            viewModel.startTimer()
        }
    }

    private fun setObservers() {
        /*  viewModel.getDeviceData().observe(this, Observer {
              return@Observer
          })*/

        viewModel.fetchData.observe(this, Observer {
            viewModel.getDevices().reObserver(this, Observer {
                return@Observer
            })
        })

        viewModel.commands.observe(this) {
            if (binding.adapter != null)
                (binding.adapter!!.getItem(1) as DeviceCommandFragment).setList(viewModel.commands.value)
        }
        viewModel.deviceDetail.observe(this) {
            if (isNotAdded) {
                isNotAdded = false
                setViewPager()
                setBottomSheet()
            }
            if (previousLatLng == null) {
                addMarker(LatLng(it.lat, it.lang))
            } else {
                moveMarker(LatLng(it.lat, it.lang))
            }
        }

        viewModel.onBackPressed.observe(this) {
            finish()
        }
        viewModel.fetchCurrentLocation.observe(this) {
            fetchLocation()
        }
        viewModel.changeView.observe(this) {
            setUpPopMenu(binding.mapViewLayout)
        }
        viewModel.history.observe(this) {
            startActivity(Intent(this@DeviceDetailActivity, DeviceHistoryActivity::class.java)
                .apply {
                    putExtra(IntentConstants.DEVICE_ID, viewModel.deviceDetail.value!!.deviceId)
                })
        }
        viewModel.showTraffic.observe(this) {
            mMap?.isTrafficEnabled = it
        }
    }

    private fun moveMarker(latLng: LatLng) {
        val mapZoom =
            (if (mMap?.cameraPosition?.zoom!! > AppConstants.MAP_DEFAULT_ZOOM) AppConstants.MAP_DEFAULT_ZOOM else mMap?.cameraPosition?.zoom!!).toFloat()
        val bounds: LatLngBounds = mMap?.projection?.visibleRegion?.latLngBounds!!
        if (!isFollowing) {
            val bound = previousLatLng?.let {
                LatLngBounds.Builder()
                    .include(it)
                    .include(latLng)
            }
            if (bound != null) {
                mMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bound.build(), 50))
            }
        }
        /* if (!bounds.contains(latLng)) {
             val cameraPosition = CameraPosition.Builder()
                 .target(latLng).zoom(mapZoom).build()
             mMap?.animateCamera(
                 CameraUpdateFactory
                     .newCameraPosition(cameraPosition)
             )
         }*/
        if (MapsUtil.calculateDistance(previousLatLng!!, latLng) > 20) {
            moveMarker2(latLng)
            viewModel.speed.value = viewModel.deviceDetail.value!!.speed.toDouble()
        } else {
            viewModel.speed.value = 0.0

        }
        //VigoMarker.move(marker, latLng, animator)
        previousLatLng = latLng
    }

    private fun addMarker(latLng: LatLng) {
        marker = mMap?.addMarker(
            MarkerOptions()
                .icon(
                    ImageUtil.bitmapFromVector(
                        this,
                        IconUtil.getIcon(
                            viewModel.deviceDetail.value?.deviceIcon ?: DeviceIconType.CAR,
                            viewModel.deviceDetail.value?.movingStatusShort ?: DeviceStatus.MOVING
                        )
                    )
                ).position(latLng)
                .anchor(.5f, .5f)
        )!!
        mMap?.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                LatLng(
                    latLng.latitude,
                    latLng.longitude
                ), AppConstants.MAP_DEFAULT_ZOOM
            )
        )
        previousLatLng = latLng
    }

    private fun setAccessory() {

    }

    private fun setViewPager() {
        val list = arrayListOf<Fragment>()
        list.add(DeviceInfoFragment.newInstance())
        list.add(
            DeviceCommandFragment.newInstance(
                intent!!.getIntExtra(
                    IntentConstants.DEVICE_ID,
                    0
                ),
                viewModel.commands.value as ArrayList<Commands>
            )
        )

        binding.adapter = PagerAdapter(supportFragmentManager, list)
        // (binding.adapter!!.getItem(1) as DeviceCommandFragment).setList(viewModel.commands.value)
        binding.bottomSheetViewpager.addOnPageChangeListener(object :
            ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                val fragment = binding.adapter!!.getItem(position)
                binding.bottomSheetViewpager.measureCurrentView(fragment.view)
                if (binding.bottomSheetViewpager.currentItem != 0 && !ignoreScroll) {
                    behavior.setAllowDragging(false)
                } else {
                    behavior.setAllowDragging(true)
                }

            }
        })
    }

    private fun setBottomSheet() {

        behavior =
            BottomSheetBehavior.from<View>(binding.bottomSheet) as CustomBottomSheetBehavior
        behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                if (binding.bottomSheetViewpager.currentItem != 0 && !ignoreScroll)
                    behavior.setAllowDragging(false)
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (binding.bottomSheetViewpager.currentItem != 0 && !ignoreScroll)
                    if (newState == BottomSheetBehavior.STATE_EXPANDED || newState == BottomSheetBehavior.STATE_DRAGGING) {
                        behavior.setAllowDragging(false)

                    } else {
                        behavior.setAllowDragging(true)
                    }
                when (newState) {
                    BottomSheetBehavior.STATE_DRAGGING -> {
                        if (once) {
                            once = false
                            val fragment = binding.adapter!!.getItem(0)
                            binding.bottomSheetViewpager.measureCurrentView(fragment.view)
                        }
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        binding.bottomSheetViewpager.currentItem = 0
                        binding.bottomSheetViewpager.disableScroll(true)
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                        binding.bottomSheetViewpager.disableScroll(false)
                    }
                    BottomSheetBehavior.STATE_HALF_EXPANDED -> {

                    }
                    BottomSheetBehavior.STATE_HIDDEN -> {

                    }
                    BottomSheetBehavior.STATE_SETTLING -> {

                    }
                }
            }
        })
    }


    override fun onDestroy() {
        super.onDestroy()
        if (animator.isRunning) {
            animator.cancel()

        }
        runnable?.apply {
            handler.removeCallbacks(this)
        }

    }


    private fun moveMarker2(finalPosition: LatLng) {
        try {
            val start = SystemClock.uptimeMillis()
            val proj: Projection = mMap!!.projection
            val startPoint = proj.toScreenLocation(marker.position)
            val startLatLng = proj.fromScreenLocation(startPoint)
            val duration: Long = 20000
            val interpolator: Interpolator = LinearInterpolator()
            val mapZoom = if (mMap!!.cameraPosition.zoom > 16f) 16f else mMap!!.cameraPosition.zoom

            val bounds: LatLngBounds = mMap?.projection?.visibleRegion?.latLngBounds!!
            if (!bounds.contains(finalPosition)) {
                val cameraPosition = CameraPosition.Builder()
                    .target(marker.position).zoom(mapZoom).build()
                mMap!!.animateCamera(
                    CameraUpdateFactory
                        .newCameraPosition(cameraPosition)
                )
            }
            runnable?.apply {
                handler.removeCallbacks(this)
            }
            runnable = object : Runnable {
                override fun run() {
                    val elapsed = SystemClock.uptimeMillis() - start
                    val t = interpolator.getInterpolation(
                        elapsed.toFloat()
                                / duration
                    )
                    val lng = t * finalPosition.longitude + (1 - t) * startLatLng.longitude
                    val lat = t * finalPosition.latitude + (1 - t) * startLatLng.latitude

                    marker.rotation = MapsUtil.getAngle(marker.position, finalPosition).toFloat()
                    marker.position = LatLng(lat, lng)
                    // Repeat till progress is complete.
                    if (t < 1) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16)
                    } else {
                        // marker.setVisible(true);
                        marker.hideInfoWindow()
                    }
                }
            }
            handler.post(runnable!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        airLocation?.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        airLocation?.onActivityResult(requestCode, resultCode, data)
    }

    private fun fetchLocation(single: Boolean = true) {
        airLocation = AirLocation(this, object : AirLocation.Callback {

            override fun onSuccess(locations: ArrayList<Location>) {
                addCurrentMarker(
                    LatLng(locations[0].latitude, locations[0].longitude),
                    locations[0].accuracy.roundToInt()
                )
            }

            override fun onFailure(locationFailedEnum: AirLocation.LocationFailedEnum) {
                showSnackBar(
                    binding.root,
                    "Could not fetch your current location"
                )
            }

        }, single)
        airLocation?.start()


    }

    private fun addCurrentMarker(
        latLng: LatLng,
        accuracy: Int
    ) {

        if (userMarker == null) {
            val builder: LatLngBounds.Builder = LatLngBounds.Builder()
            builder.include(
                LatLng(
                    viewModel.deviceDetail.value!!.lat,
                    viewModel.deviceDetail.value!!.lang
                )
            )
            builder.include(latLng)
            val bounds = builder.build()
            mMap?.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50))
        }
        userMarker?.apply {
            remove()
        }
        circle?.apply {
            remove()
        }
        polyline?.apply {
            remove()
        }
        mMap?.apply {
            userMarker = addMarker(
                MarkerOptions().position(latLng)
                    .title("You are here")
                    .icon(
                        ImageUtil.bitmapFromVector(
                            this@DeviceDetailActivity,
                            R.drawable.user_marker
                        )
                    )
            )
            val co = CircleOptions()
            co.center(latLng)
            co.radius(accuracy.toDouble())
            co.fillColor(Color.argb(50, 255, 0, 0))
            co.strokeColor(Color.RED)
            co.strokeWidth(2.0f)
            circle = mMap?.addCircle(co)
            polyline = MapsUtil.showCurvedPolyline(
                LatLng(
                    viewModel.deviceDetail.value!!.lat,
                    viewModel.deviceDetail.value!!.lang
                ), userMarker?.position!!, 0.5, mMap!!
            )
        }
    }


    override fun close() {
        behavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    override fun ignore() {
        ignoreScroll = true
    }


    private fun setUpPopMenu(view: View) {
        val popup = PopupMenu(this@DeviceDetailActivity, view, Gravity.END);
        popup.inflate(R.menu.map_view_menu);
        val item = popup.menu.getItem(selectPos)
        val s = SpannableString(item.title);
        s.setSpan(
            ForegroundColorSpan(
                resources.getColor(R.color.idle)
            ), 0, s.length, 0
        )
        item.title = s

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.default_ -> {
                    selectPos = 0
                    mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
                    false
                }
                R.id.terrain -> {
                    selectPos = 1
                    mMap?.mapType = GoogleMap.MAP_TYPE_TERRAIN
                    true
                }
                R.id.satellite -> {
                    selectPos = 2
                    mMap?.mapType = GoogleMap.MAP_TYPE_SATELLITE
                    true
                }
                R.id.hybrid -> {
                    selectPos = 3
                    mMap?.mapType = GoogleMap.MAP_TYPE_HYBRID
                    true
                }
                else -> {
                    false
                }


            }


        }

        popup.show();
    }

    fun getAddress(): String {
        return viewModel.address.get()?:""
    }

    fun getLocation(): LatLng {
        return LatLng(marker.position.latitude, marker.position.longitude)
    }

    fun getOdometerValue(): String {
        return (viewModel.deviceDetail.value?.odometer?: 0L).toString()
    }

    fun getDevice(): Device {
        return viewModel.deviceDetail.value!!
    }

    fun followDevice(follow: Boolean) {
        isFollowing = follow
        fetchLocation(false)
    }


}
