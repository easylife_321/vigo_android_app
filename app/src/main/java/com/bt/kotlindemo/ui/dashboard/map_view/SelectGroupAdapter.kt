/*
 * *
 *  * Created by Surajit on 28/5/20 9:06 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/5/20 3:08 PM
 *  *
 */

package com.bt.kotlindemo.ui.dashboard.map_view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ListItemGroupBinding
import com.bt.kotlindemo.model.Group
import java.util.*

class SelectGroupAdapter(
    val group: ArrayList<Group>
) : BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val holder: ViewHolder
        val retView: View
        if (convertView == null) {
            val binding: ListItemGroupBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent!!.context),
                R.layout.list_item_group,
                parent,
                false
            )
            holder = ViewHolder(binding)
            retView = binding.root
            retView.tag = holder

        } else {
            holder = convertView.tag as ViewHolder
            retView = convertView
        }
        if (position == group.size - 1) {
            holder.binding.view.visibility = View.INVISIBLE
        } else {
            holder.binding.view.visibility = View.VISIBLE
        }
        holder.binding.group = getItem(position)
        return retView

    }

    override fun getItem(position: Int): Group {
        return group[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return group.size
    }

    inner class ViewHolder(
        val binding: ListItemGroupBinding
    )

}

