/*
 * *
 *  * Created by Surajit on 25/5/20 8:01 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:26 PM
 *  *
 */

package com.bt.kotlindemo.ui.profile.profile_detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.ProfileOption
import com.bt.kotlindemo.databinding.ListItemProfileBinding
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.ProfileOptions

class ProfileOptionsListAdapter(
    private val options: List<ProfileOptions>,
    val mListener: RecyclerClickListener
) : RecyclerView.Adapter<ProfileOptionsListAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return options.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ListItemProfileBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_profile,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.option = options[position]
        holder.binding.layout.setOnClickListener {
            mListener.onClick(options[position])
        }


    }

    fun setDnd(dnd: Boolean) {
        options.onEachIndexed { index, item ->
            if (item.id == ProfileOption.DO_NOT_DISTURB) {
                item.isOn = dnd
                notifyItemChanged(index)
                return@onEachIndexed
            }
        }
        notifyItemRangeChanged(0, options.size - 1)

    }

    inner class ViewHolder(
        val binding: ListItemProfileBinding
    ) : RecyclerView.ViewHolder(binding.root) {
    }


}




