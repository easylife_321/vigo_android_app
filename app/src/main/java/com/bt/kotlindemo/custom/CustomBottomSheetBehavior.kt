/*
 * *
 *  * Created by Surajit on 22/5/20 2:17 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/5/20 1:05 PM
 *  *
 */
package com.bt.kotlindemo.custom

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior

class CustomBottomSheetBehavior<V : View> : BottomSheetBehavior<V> {
    private var allowDragging = true

    constructor() {}
    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!,
        attrs
    ) {
    }

    fun setAllowDragging(allowDragging: Boolean) {
        this.allowDragging = allowDragging
    }

    override fun onInterceptTouchEvent(
        parent: CoordinatorLayout,
        child: V,
        event: MotionEvent
    ): Boolean {
        return if (!allowDragging) {
            false
        } else super.onInterceptTouchEvent(parent, child, event)
    }

    companion object {
        private const val TAG = "CustomBottomSheetBehavi"
    }
}