/*
 * *
 *  * Created by Surajit on 23/4/20 9:23 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/4/20 9:23 PM
 *  *
 */

package com.bt.kotlindemo.custom

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.viewpager.widget.ViewPager


class SDViewPager : ViewPager {
    private var mCurrentView: View? = null
    private var disable = false

    constructor(context: Context) : super(context) {}
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {}

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasure: Int) {
        var heightMeasureSpec = heightMeasure
        if (mCurrentView == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
            return
        }
        var height = 0
        mCurrentView!!.measure(
            widthMeasureSpec,
            MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED)
        )
        val h: Int = mCurrentView!!.getMeasuredHeight()
        if (h > height) height = h
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onInterceptTouchEvent(event: MotionEvent?): Boolean {
        return !disable && super.onInterceptTouchEvent(event)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return !disable && super.onTouchEvent(event)
    }

    fun disableScroll(disable: Boolean?) {
        this@SDViewPager.disable = disable!!
    }


    fun measureCurrentView(currentView: View?) {
        mCurrentView = currentView
        requestLayout()
    }

    fun measureFragment(view: View?): Int {
        if (view == null) return 0
        view.measure(0, 0)
        return view.measuredHeight
    }
}