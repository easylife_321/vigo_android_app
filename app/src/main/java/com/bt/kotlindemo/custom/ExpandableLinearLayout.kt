/*
 * *
 *  * Created by Surajit on 3/5/20 10:43 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/5/20 10:43 PM
 *  *
 */

package com.bt.kotlindemo.custom


import android.content.Context
import android.content.res.TypedArray
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.LinearLayout
import androidx.annotation.Nullable
import androidx.annotation.RequiresApi
import com.bt.kotlindemo.R


class ExpandableLinearLayout : LinearLayout {
    private var expanded = false
    private var duration = 0

    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, @Nullable attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?, @Nullable attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs)
    }

    private fun init(attributeSet: AttributeSet?) {
        val customValues: TypedArray =
            context.obtainStyledAttributes(attributeSet, R.styleable.ExpandableLinearLayout)
        duration = customValues.getInt(R.styleable.ExpandableLinearLayout_expandDuration, -1)
        customValues.recycle()
    }

    fun isExpanded(): Boolean {
        return expanded
    }

    fun setExpanded(expanded: Boolean) {
        Log.e("layout", expanded.toString() + "")
        this.expanded = expanded
    }

    fun toggle() {
        if (expanded) expandView(this) else hideView(this)
    }

    private fun expandView(view: View) {
        view.measure(
            WindowManager.LayoutParams.MATCH_PARENT
            , View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
        )
        val targetHeight: Int = view.measuredHeight
        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        view.layoutParams.height = 1
        view.visibility = View.VISIBLE
        val a: Animation = object : Animation() {
            override fun applyTransformation(
                interpolatedTime: Float,
                t: Transformation?
            ) {
                view.layoutParams.height =
                    if (interpolatedTime == 1f) targetHeight else (targetHeight * interpolatedTime).toInt()
                view.requestLayout()
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        if (duration == -1) a.setDuration(
            (targetHeight / view.context.resources
                .displayMetrics.density * 1.5).toLong()
        ) else a.duration = duration.toLong()
        view.startAnimation(a)
    }

    private fun hideView(view: View) {
        val initialHeight: Int = view.measuredHeight
        val a: Animation = object : Animation() {
            override fun applyTransformation(
                interpolatedTime: Float,
                t: Transformation?
            ) {
                if (interpolatedTime == 1f) {
                    view.visibility = View.GONE
                } else {
                    view.layoutParams.height =
                        initialHeight - (initialHeight * interpolatedTime).toInt()
                    view.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }
        if (duration == -1) a.duration = ((initialHeight / view.context.resources
            .displayMetrics.density * 1.5).toLong()) else a.duration = duration.toLong()
        view.startAnimation(a)
    }
}