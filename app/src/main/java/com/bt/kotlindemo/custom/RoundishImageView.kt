/*
 * *
 *  * Created by Surajit on 5/4/20 10:16 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 5/4/20 10:13 AM
 *  *
 */


package com.bt.kotlindemo.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.bt.kotlindemo.R


class RoundishImageView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    AppCompatImageView(context, attrs, defStyleAttr) {
    private val path = Path()
    private var cornerRadius: Int
    private var roundedCorners: Int
    fun setCornerRadius(radius: Int) {
        if (cornerRadius != radius) {
            cornerRadius = radius
            setPath()
            invalidate()
        }
    }

    fun getCornerRadius(): Int {
        return cornerRadius
    }

    fun setRoundedCorners(corners: Int) {
        if (roundedCorners != corners) {
            roundedCorners = corners
            setPath()
            invalidate()
        }
    }

    fun isCornerRounded(corner: Int): Boolean {
        return roundedCorners and corner == corner
    }

    override fun onDraw(canvas: Canvas) {
        if (!path.isEmpty) {
            canvas.clipPath(path)
        }
        super.onDraw(canvas)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        setPath()
    }

    private fun setPath() {
        path.rewind()
        if (cornerRadius >= 1f && roundedCorners != CORNER_NONE) {
            val radii = FloatArray(8)
            for (i in 0..3) {
                if (isCornerRounded(CORNERS[i])) {
                    radii[2 * i] = cornerRadius.toFloat()
                    radii[2 * i + 1] = cornerRadius.toFloat()
                }
            }
            path.addRoundRect(
                RectF(0f, 0f, width.toFloat(), height.toFloat()),
                radii, Path.Direction.CW
            )
        }
    }

    companion object {
        const val CORNER_NONE = 0
        const val CORNER_TOP_LEFT = 1
        const val CORNER_TOP_RIGHT = 2
        const val CORNER_BOTTOM_RIGHT = 4
        const val CORNER_BOTTOM_LEFT = 8
        const val CORNER_ALL = 15
        private val CORNERS = intArrayOf(
            CORNER_TOP_LEFT,
            CORNER_TOP_RIGHT,
            CORNER_BOTTOM_RIGHT,
            CORNER_BOTTOM_LEFT
        )
    }

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.RoundishImageView)
        cornerRadius = a.getDimensionPixelSize(R.styleable.RoundishImageView_cornerRadius, 0)
        roundedCorners = a.getInt(
            R.styleable.RoundishImageView_roundedCorners,
            CORNER_NONE
        )
        a.recycle()
    }
}