/*
 * *
 *  * Created by Surajit on 3/7/20 2:05 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/7/20 2:05 AM
 *  *
 */

package com.bt.kotlindemo.custom

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.appcompat.widget.AppCompatSeekBar


class VerticalSeekBar : AppCompatSeekBar {
    private var myListener: OnSeekBarChangeListener? = null
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
    }



    constructor(context: Context, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
    }

    constructor(context: Context) : super(context) {
    }

    override fun setOnSeekBarChangeListener(mListener: OnSeekBarChangeListener?) {
        myListener = mListener
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(h, w, oldh, oldw)
    }

    @Synchronized
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec)
        setMeasuredDimension(measuredHeight, measuredWidth)
    }

    override fun onDraw(c: Canvas) {
        //将SeekBar转转90度
        c.rotate(-90f)
        //将旋转后的视图移动回来
        c.translate(-height.toFloat(), 0f)
        super.onDraw(c)
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) {
            return false
        }

        when (event.action) {
            MotionEvent.ACTION_DOWN -> if (myListener != null) myListener!!.onStartTrackingTouch(
                this
            )
            MotionEvent.ACTION_MOVE -> {
                progress = max - (max * event.y / height).toInt()
                onSizeChanged(width, height, 0, 0)
                myListener!!.onProgressChanged(this, max - (max * event.y / height).toInt(), true)
            }
            MotionEvent.ACTION_UP -> myListener!!.onStopTrackingTouch(this)
            MotionEvent.ACTION_CANCEL -> {
            }
        }
        return true
    }

    @Synchronized
    override fun setProgress(progress: Int) {
        super.setProgress(progress)
        onSizeChanged(width, height, 0, 0)
    }
}