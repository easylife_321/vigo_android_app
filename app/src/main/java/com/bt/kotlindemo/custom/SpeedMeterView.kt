/*
 * *
 *  * Created by Surajit on 3/5/20 12:26 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/5/20 12:26 PM
 *  *
 */

package com.bt.kotlindemo.custom

import android.animation.TypeEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.bt.kotlindemo.R
import com.bt.kotlindemo.utils.getColorFromAttr
import kotlin.math.cos
import kotlin.math.sin

class SpeedMeterView : View {


    private var centerX: Float = 0f
    private var centerY: Float = 0f
    private var radius: Float = 0f
    private var majorLinePaint: Paint = Paint()
    private var minorLinePaint: Paint = Paint()
    private var needlePath: Path = Path()
    private val needleScrewPaint: Paint = Paint()
    private val needlePaint: Paint = Paint()
    private var textPaint: Paint = Paint()
    private var needleWidth = 0f
    private val tickValue = 20
    private val maxValue = 240
    private val startAngle = 180f
    private var currentSpeed = 120.0

    /* private lateinit var textPaintMajor: Paint
     private lateinit var textPaintMinor: Paint
     private lateinit var needlePaint: Paint
     private lateinit var needleCircleOuter: Paint
     private lateinit var needleCircleInner: Paint*/


    constructor(context: Context) : super(context) {

    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    init {
        majorLinePaint.apply {
            color = context.getColorFromAttr(R.attr.tickColor)
            isAntiAlias = true
            style = Paint.Style.STROKE
            strokeWidth = 6f
        }
        minorLinePaint.apply {
            color = context.getColorFromAttr(R.attr.tickColorSmall)
            isAntiAlias = true
            style = Paint.Style.STROKE
            strokeWidth = 4f
        }


        needlePaint.apply {
            color = context.resources.getColor(R.color.colorPrimary)
            style = Paint.Style.FILL_AND_STROKE
            isAntiAlias = true
        }

        needleScrewPaint.apply {
            color = context.resources.getColor(R.color.black)
            style = Paint.Style.FILL_AND_STROKE
            isAntiAlias = true
        }

        textPaint.apply {
            color = context.getColorFromAttr(R.attr.tickColor)
            style = Paint.Style.FILL_AND_STROKE
            strokeWidth = 0.005f
            // textAlign = Paint.Align.CENTER
            textSize = context.resources.getDimension(com.intuit.sdp.R.dimen._10sdp)
            typeface = Typeface.SANS_SERIF
            setShadowLayer(0.01f, 0.002f, 0.002f, Color.argb(100, 0, 0, 0))
        }

    }

    companion object {
        private const val defaultDimension = 300
    }


    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        drawTicks(canvas)
        drawNeedle(canvas)
    }

    private fun drawTicks(canvas: Canvas) {
        var currentAngle = startAngle
        val majorTicks = maxValue / tickValue
        var textValue = 0
        var showText = true;
        for (i in 0..majorTicks) {

            //draw major line
            val majorLineAngle = currentAngle * (Math.PI / 180)
            canvas.drawLine(
                (centerX + cos(majorLineAngle) * radius).toFloat(),
                (centerY - sin(majorLineAngle) * radius).toFloat(),
                (centerX + cos(majorLineAngle) * (radius - (radius * .15f))).toFloat(),
                (centerY - sin(majorLineAngle) * (radius - (radius * .15f))).toFloat(),
                majorLinePaint
            )
            if (showText) {
                drawText(canvas, textValue, currentAngle)
                showText = false
            } else {
                showText = true
            }


            if (i == majorTicks)
                return

            //draw minor line
            var currentAngleMinor = currentAngle - 5
            for (j in 0..1) {
                val minorLineAngle = currentAngleMinor * (Math.PI / 180)
                canvas.drawLine(
                    (centerX + cos(minorLineAngle) * radius).toFloat(),
                    (centerY - sin(minorLineAngle) * radius).toFloat(),
                    (centerX + cos(minorLineAngle) * (radius - (radius * .09f))).toFloat(),
                    (centerY - sin(minorLineAngle) * (radius - (radius * .09f))).toFloat(),
                    minorLinePaint
                )
                currentAngleMinor -= 10
            }

            //draw mid line
            val midLineAngle = (currentAngle - tickValue / 2) * (Math.PI / 180)
            canvas.drawLine(
                (centerX + cos(midLineAngle) * radius).toFloat(),
                (centerY - sin(midLineAngle) * radius).toFloat(),
                (centerX + cos(midLineAngle) * (radius - (radius * .11f))).toFloat(),
                (centerY - sin(midLineAngle) * (radius - (radius * .11f))).toFloat(),
                minorLinePaint
            )

            textValue += tickValue
            currentAngle -= tickValue
        }

    }

    private fun drawText(canvas: Canvas, textValue: Int, currentAngle: Float) {
        val bounds = Rect()
        textPaint.getTextBounds(textValue.toString(), 0, textValue.toString().length, bounds)
        val majorLineAngle = currentAngle * (Math.PI / 180)
        var x =
            (centerX + cos(majorLineAngle) * (radius - (radius * if (textValue == -0) .25f else .32f)
                    )).toFloat() - (bounds.width() / 2)
        var y =
            (centerY - sin(majorLineAngle) * (radius - (radius * if (textValue == -0) .25f else .32f)
                    )).toFloat() + (bounds.height() / 2)

        /* if (textValue >= 160) {
            // x -= (bounds.width() / 2)
            //  y += (bounds.height() / 2)
            textPaint.textAlign = Paint.Align.LEFT
        } else if (textValue in 80..159)

        else {
            textPaint.textAlign = Paint.Align.RIGHT
        }*/
        // canvas.save()
        // canvas.rotate((90 - currentAngle), x, y)
        canvas.drawText(textValue.toString(), x, y, textPaint)
        // canvas.restore()
    }

    private fun drawNeedle(canvas: Canvas) {
        canvas.save()
        canvas.rotate(
            (90 - (startAngle - tickValue * currentSpeed)).toFloat(),
            centerX,
            centerY
        )
        needlePath.moveTo(centerX, centerY)
        needlePath.lineTo(centerX - needleWidth, centerY)
        needlePath.lineTo(centerX, centerY - width * 0.40f)
        needlePath.lineTo(centerX, centerY)
        needlePath.lineTo(centerX - needleWidth, centerY)
        needlePath.moveTo(centerX, centerY)
        needlePath.lineTo(centerX + needleWidth, centerY)
        needlePath.lineTo(centerX, centerY - width * 0.40f)
        needlePath.lineTo(centerX, centerY)
        needlePath.lineTo(centerX + needleWidth, centerY)
        needlePath.addCircle(
            centerX,
            centerY,
            width / 49f,
            Path.Direction.CW
        )
        needlePath.close()
        canvas.drawPath(needlePath, needlePaint)
       /* needleScrewPaint.apply {
            shader = RadialGradient(
                centerX, centerY, 5F,
                Color.DKGRAY, Color.BLACK, Shader.TileMode.REPEAT
            )
        }*/
        canvas.drawCircle(centerX, centerY, radius / 10f, needleScrewPaint)
        canvas.restore()
    }


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        centerX = h / 2f;
        centerY = h / 2f;
        radius = h / 2f - (h / 2f * .1f)
        needleWidth = h / 40f


    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val widthMode: Int = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode: Int = MeasureSpec.getMode(heightMeasureSpec)
        val widthSize: Int = MeasureSpec.getSize(widthMeasureSpec)
        val heightSize: Int = MeasureSpec.getSize(heightMeasureSpec)
        val chosenWidth = chooseDimension(widthMode, widthSize)
        val chosenHeight = chooseDimension(heightMode, heightSize)
        setMeasuredDimension(chosenWidth, chosenHeight)
    }

    private fun chooseDimension(mode: Int, size: Int): Int {
        return when (mode) {
            MeasureSpec.AT_MOST, MeasureSpec.EXACTLY -> size
            MeasureSpec.UNSPECIFIED -> defaultDimension
            else -> defaultDimension
        }


    }

    fun setSpeed(
        progress: Double,
        duration: Long = 2000,
        startDelay: Long = 0
    ): ValueAnimator? {
        if (currentSpeed == progress/2)
            return null
        val va = ValueAnimator.ofObject(
            TypeEvaluator { fraction: Float, startValue: Double, endValue: Double -> startValue + fraction * (endValue - startValue) } as TypeEvaluator<Double>,
            currentSpeed,
            progress /2
        )
        va.duration = duration
        va.startDelay = startDelay
        va.addUpdateListener { animation: ValueAnimator ->
            val value = animation.animatedValue as Double
            setValue(value)
        }
        va.start()
        return va
    }

    fun setValue(value: Double) {
        var value = value
        if (value < 0) value = 0.0
        if (currentSpeed > maxValue) currentSpeed = maxValue.toDouble()
        this.currentSpeed = value
        invalidate()
    }
}