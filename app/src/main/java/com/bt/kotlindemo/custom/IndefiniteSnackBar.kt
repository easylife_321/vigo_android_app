/*
 * *
 *  * Created by Surajit on 5/4/20 10:16 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/3/20 3:26 PM
 *  *
 */


package com.bt.kotlindemo.custom

import android.view.View
import com.bt.kotlindemo.R

import com.google.android.material.snackbar.Snackbar

object IndefiniteSnackbar {

    private var snackbar: Snackbar? = null

    fun show(view: View, text: String, action: () -> Unit) {
        snackbar = Snackbar.make(view, text, Snackbar.LENGTH_INDEFINITE).apply {
            setAction(view.context.getString(R.string.retry)) { action() }
            show()
        }
    }

    fun hide() {
        snackbar?.dismiss()
    }

}