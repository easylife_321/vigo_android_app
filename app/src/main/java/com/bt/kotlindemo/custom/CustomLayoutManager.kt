/*
 * *
 *  * Created by Surajit on 22/5/20 2:18 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 22/5/20 1:05 PM
 *  *
 */
package com.bt.kotlindemo.custom

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Recycler

class CustomLayoutManager(
    context: Context?,
    orientation: Int,
    reverseLayout: Boolean
) : LinearLayoutManager(context, orientation, reverseLayout) {
    override fun canScrollVertically(): Boolean {
        return true
    }

    override fun scrollVerticallyBy(
        dy: Int,
        recycler: Recycler,
        state: RecyclerView.State
    ): Int {
        return super.scrollVerticallyBy(dy, recycler, state)
    }
}