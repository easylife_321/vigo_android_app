/*
 * *
 *  * Created by Surajit on 3/6/20 7:46 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/6/20 7:46 PM
 *  *
 */
package com.bt.kotlindemo.custom

import android.content.Context
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import androidx.recyclerview.widget.RecyclerView


class SDRecyclerView : RecyclerView {
    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!,
        attrs
    ) {
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context!!, attrs, defStyle) {
    }


    private val gestureDetector = GestureDetector(this.context,GestureListener());


    override fun onInterceptTouchEvent(e: MotionEvent): Boolean {
        gestureDetector.onTouchEvent(e)
        return false
    }

    override fun onScrolled(dx: Int, dy: Int) {
        super.onScrolled(dx, dy)
        parent.requestDisallowInterceptTouchEvent(false)
    }

    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)



    }
    inner class GestureListener : SimpleOnGestureListener() {
        private val Y_BUFFER = 10
        override fun onDown(e: MotionEvent): Boolean {
            // Prevent ViewPager from intercepting touch events as soon as a DOWN is detected.
            // If we don't do this the next MOVE event may trigger the ViewPager to switch
            // tabs before this view can intercept the event.
            parent.requestDisallowInterceptTouchEvent(true)
            return super.onDown(e)
        }

        override fun onScroll(
            e1: MotionEvent?,
            e2: MotionEvent,
            distanceX: Float,
            distanceY: Float
        ): Boolean {
            /*if (Math.abs(distanceX) > Math.abs(distanceY)) {
                parent.requestDisallowInterceptTouchEvent(false)
            } else if (abs(distanceY) > Y_BUFFER) {
                parent.requestDisallowInterceptTouchEvent(true)
            }*/
            parent.requestDisallowInterceptTouchEvent(false)
            return super.onScroll(e1, e2, distanceX, distanceY)
        }
    }
}