/*
 * *
 *  * Created by Surajit on 3/7/20 2:08 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 3/7/20 2:08 AM
 *  *
 */

package com.bt.kotlindemo.custom

import android.content.Context
import android.os.Handler
import android.os.Message
import android.util.AttributeSet
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.RelativeLayout
import java.util.*



class VerticalSeekBarContainer : RelativeLayout {
    private val TAG = this.javaClass.simpleName
    private var mGestureDetector: GestureDetector? = null
    private var context_: Context? = null
    private var seekbar: VerticalSeekBar? = null
    var seekBarContainer: RelativeLayout? = null
    private val mHandler =
        DisappearHandler()

    private inner class DisappearHandler : Handler() {
        override fun handleMessage(msg: Message) {
            setVisibilityOfLayout(View.GONE)
        }
    }

    private var disappearTimer: Timer? = null

    private inner class DisappearTimerTask : TimerTask() {
        override fun run() {
            mHandler.sendEmptyMessage(0)
        }
    }

    /**
     * 用来感知手势的内部类接口
     */
    private inner class TGestureListener : GestureDetector.OnGestureListener {
        override fun onDown(e: MotionEvent): Boolean {
            //Log.d(TAG,">>>>>onDown");
            return true
        }

        override fun onShowPress(e: MotionEvent) {
            //Log.d(TAG,">>>>>onShowPress");
        }

        override fun onSingleTapUp(e: MotionEvent): Boolean {
            //Log.d(TAG,">>>>>onSingleTapUp");
            return true
        }

        override fun onScroll(
            e1: MotionEvent?, e2: MotionEvent,
            distanceX: Float, distanceY: Float
        ): Boolean {
            //Log.d(TAG,">>>>>onScroll");
            setVisibilityOfLayout(View.VISIBLE)
            traceSeekBar(distanceY)
            return true
        }

        override fun onLongPress(e: MotionEvent) {
            //Log.d(TAG,">>>>>onLongPress");
        }

        override fun onFling(
            e1: MotionEvent?, e2: MotionEvent, velocityX: Float,
            velocityY: Float
        ): Boolean {
            //Log.d(TAG,">>>>>onFling");
            return true
        }
    }

    private fun traceSeekBar(distanceY: Float) {
        if (seekbar == null) {
            return
        }
        val percentOfScreen = distanceY / 100f / 8f
        val nowProgress = seekbar!!.progress.toFloat()
        var newProgress = (nowProgress + 100f * percentOfScreen).toInt()
        if (newProgress > 100) {
            newProgress = 100
        } else if (newProgress < 0) {
            newProgress = 0
        }
        seekbar!!.progress = newProgress
    }

    constructor(
        context: Context?, attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
        initRelativeModel(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        initRelativeModel(context)
    }

    constructor(context: Context?) : super(context) {
        initRelativeModel(context)
    }

    fun initRelativeModel(context: Context?) {
        this.context_ = context
        mGestureDetector =
            GestureDetector(this.context, TGestureListener())
        disappearTimer = Timer()
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
            }
            MotionEvent.ACTION_UP -> {
                disappearTimer?.cancel()
                disappearTimer = Timer()
                disappearTimer?.schedule(DisappearTimerTask(), 2000L)
            }
            MotionEvent.ACTION_MOVE -> {
            }
            MotionEvent.ACTION_CANCEL -> {
            }
            else -> {
            }
        }
        return mGestureDetector!!.onTouchEvent(event)
    }

    fun setSeekbar(seekbar: VerticalSeekBar?) {
        this.seekbar = seekbar
    }

    private fun setVisibilityOfLayout(visibility: Int) {
        if (seekBarContainer != null) {
            seekBarContainer!!.visibility = visibility
        }
    }

}