/*
 * *
 *  * Created by Surajit on 2/21/21 3:11 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 2/21/21 3:11 PM
 *  *
 */
package com.bt.kotlindemo.custom

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class TopCropImageView : AppCompatImageView {
    constructor(context: Context?) : super(context!!) {
        scaleType = ScaleType.MATRIX
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context!!, attrs
    ) {
        scaleType = ScaleType.MATRIX
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context!!, attrs, defStyle
    ) {
        scaleType = ScaleType.MATRIX
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        computeMatrix()
    }

    override fun setFrame(l: Int, t: Int, r: Int, b: Int): Boolean {
        computeMatrix()
        return super.setFrame(l, t, r, b)
    }

    private fun computeMatrix() {
        if (drawable == null) return
        val matrix = imageMatrix
        val scaleFactor = width / drawable.intrinsicWidth.toFloat()
        matrix.setScale(scaleFactor, scaleFactor, 0f, 0f)
        imageMatrix = matrix
    }
}