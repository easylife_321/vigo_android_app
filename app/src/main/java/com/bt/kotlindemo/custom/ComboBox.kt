package com.bt.kotlindemo.custom


import android.content.Context
import android.database.DataSetObserver
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.Filter
import androidx.annotation.ArrayRes
import androidx.appcompat.widget.AppCompatAutoCompleteTextView
import androidx.core.content.ContextCompat
import com.bt.kotlindemo.R


class ComboBox : AppCompatAutoCompleteTextView, OnItemClickListener {
    private var isPopup = false
    var position = -1
        private set

    constructor(context: Context) : super(context) {
        setAdapter(
            ArrayAdapter<Any?>(
                context,
                android.R.layout.simple_expandable_list_item_1,
                arrayOfNulls<String>(0)
            )
        )
        onItemClickListener = this
        keyListener = null
    }

    constructor(context: Context, attributes: AttributeSet) : super(context, attributes) {
        setAdapter(
            ComboBoxAdapter(
                context,
                attributes.getAttributeListValue(
                    "http://schemas.android.com/apk/res/android",
                    "entries",
                    arrayOfNulls<String>(0),
                    R.array.default_empty_list
                )
            )
        )
        onItemClickListener = this
        keyListener = null
    }

    constructor(context: Context, attributes: AttributeSet, arg2: Int) : super(
        context,
        attributes,
        arg2
    ) {
        setAdapter(
            ComboBoxAdapter(
                context,
                attributes.getAttributeListValue(
                    "http://schemas.android.com/apk/res/android",
                    "entries",
                    arrayOfNulls<String>(0),
                    R.array.default_empty_list
                )
            )
        )
        onItemClickListener = this
        keyListener = null
    }

    class ComboBoxAdapter(context: Context, @ArrayRes array: Int) :
        ArrayAdapter<String?>(
            context,
            android.R.layout.simple_expandable_list_item_1,
            context.resources.getStringArray(array)
        ) {
        private val list: Array<String> = context.resources.getStringArray(array)
        override fun getFilter(): Filter {
            return object : Filter() {
                protected override fun performFiltering(constraint: CharSequence?): FilterResults {
                    val out = FilterResults()
                    out.values = list
                    out.count = list.size
                    return out
                }

                protected override fun publishResults(
                    constraint: CharSequence?,
                    results: FilterResults?
                ) {
                    notifyDataSetChanged()
                }
            }
        }

    }

    override fun enoughToFilter(): Boolean {
        return true
    }

    protected override fun onFocusChanged(
        focused: Boolean,
        direction: Int,
        previouslyFocusedRect: Rect?
    ) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focused) {
            val imm: InputMethodManager =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(windowToken, 0)
            showDropDown()
        }
    }

    override fun performClick(): Boolean {
        if (isPopup) {
            dismissDropDown()
        } else {
            showDropDown()
        }
        return super.performClick()
    }

    override fun showDropDown() {
        super.showDropDown()
        isPopup = true
    }

    override fun dismissDropDown() {
        super.dismissDropDown()
        isPopup = false
    }

    override fun setCompoundDrawablesWithIntrinsicBounds(
        left: Drawable?,
        top: Drawable?,
        right: Drawable?,
        bottom: Drawable?
    ) {
        var end = right
        val dropdownIcon =
            ContextCompat.getDrawable(context, R.drawable.ic_arrow_drop_down)
        if (dropdownIcon != null) {
            end = dropdownIcon
            end.mutate().alpha = 66
        }
        super.setCompoundDrawablesRelativeWithIntrinsicBounds(left, top, end, bottom)
    }

    val currentText: String
        get() = if (position == -1) {
            ""
        } else {
            text.toString()
        }

    fun registerDataSetObserver(observer: DataSetObserver?) {
        adapter.registerDataSetObserver(observer)
    }

    fun unregisterDataSetObserver(observer: DataSetObserver?) {
        adapter.unregisterDataSetObserver(observer)
    }

    override fun onItemClick(
        parent: AdapterView<*>,
        view: View?,
        position: Int,
        id: Long
    ) {
        setText(parent.getItemAtPosition(position).toString())
        this.position = position
    }
}
