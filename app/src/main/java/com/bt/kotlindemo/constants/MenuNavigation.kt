/*
 * *
 *  * Created by Surajit on 15/5/20 1:17 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 15/5/20 1:17 PM
 *  *
 */

package com.bt.kotlindemo.constants

class MenuNavigation {

    companion object {
        const val DASHBOARD = "m_dashboard"
        const val DEVICES = "m_devices"
        const val PLAN_RENEWAL = "m_planRenewal"
        const val DOCUMENT_WALLET = "m_documentWallet"
        const val NEAR_BY_HELP = "m_nearbyHelp"
        const val VIDEOS = "m_videos"
        const val SUPPORT = "m_url"
        const val FAQ = "m_url"
        const val T_AND_C = "m_url"
        const val PRIVACY_POLICY = "m_url"
        const val ABOUT_US = "m_url"
        const val NOTIFICATIONS = "m_notification"
        const val URL = "m_url"

        //sub menu
        const val ADD_DEVICE = "s_addDevice"
        const val ZONE_MANAGEMENT = "s_zoneManagement"
        const val SHAREABLE_LINKS = "s_sharableLinks"
        const val POI = "s_favoritePlace"
        const val TRIPS = "s_trips"
        const val DEVICE_MANAGEMENT = "s_deviceManagement"
        const val GROUP_MANAGEMENT = "s_groupManagement"

    }

}