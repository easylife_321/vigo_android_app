/*
 * *
 *  * Created by Surajit on 27/12/20 4:49 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/12/20 4:49 PM
 *  *
 */

package com.bt.kotlindemo.constants

import com.bt.kotlindemo.R
import com.bt.kotlindemo.model.ProfileOptions

object ProfileOption {

    const val EDIT_PROFILE = 1
    const val DO_NOT_DISTURB = 2
    const val NOTIFICATION = 3
    const val CHANGE_MOBILE_NO = 4
    const val SHARE_APP = 5
    const val MY_CONTACTS = 6
    const val FEEDBACK = 7
    const val LOGOUT = 8
    const val CHANGE_PASSWORD = 9

    fun getOption(isDndOn: Boolean = false): List<ProfileOptions> {
        val list = mutableListOf<ProfileOptions>()
        list.add(
            ProfileOptions(
                "Edit Profile", R.drawable.ic_user,
                EDIT_PROFILE
            )
        )
        list.add(
            ProfileOptions(
                "Do Not Disturb", R.drawable.ic_dnd,
                DO_NOT_DISTURB, isDndOn
            )
        )
       /* list.add(
            ProfileOptions(
                "Notifications", R.drawable.ic_notification,
                NOTIFICATION
            )
        )*/
        list.add(
            ProfileOptions(
                "Change Mobile No", R.drawable.ic_change_mobile,
                CHANGE_MOBILE_NO
            )
        )
      /*  list.add(
            ProfileOptions(
                "Share App", R.drawable.ic_share,
                SHARE_APP
            )
        )*/
        list.add(
            ProfileOptions(
                "My Contacts", R.drawable.ic_contacts,
                MY_CONTACTS
            )
        )
      /*  list.add(
            ProfileOptions(
                "Feedback", R.drawable.ic_feedback,
                FEEDBACK
            )
        )*/
        list.add(
            ProfileOptions(
                "Change Password", R.drawable.ic_change_password,
                CHANGE_PASSWORD
            )
        )

        list.add(
            ProfileOptions(
                "Logout", R.drawable.ic_feedback,
                LOGOUT
            )
        )


        return list
    }

}