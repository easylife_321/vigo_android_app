/*
 * *
 *  * Created by Surajit on 27/12/20 4:49 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/12/20 4:49 PM
 *  *
 */

package com.bt.kotlindemo.constants

import com.bt.kotlindemo.model.NotificationTypeModel

object NotificationType {

    const val ACC_CHANGED = "accChanged"
    const val PARKING = "parking"
    const val ANTI_THEFT = "antitheft"
    const val ZONE = "zone"
    const val SHAREABLE_LINK = "shareableLink"
    const val AC = "ac"
    const val DOOR = "door"
    const val ENGINE_CUT = "engineCut"
    const val SUN_ROOF = "sunroof"
    const val SEAT_BELT = "seatBelt"
    const val INTER_LOCK = "interlock"
    const val FUEL_METER = "fuelMeter"
    const val TEMPERATURE = "temperature"
    const val HEAD_LIGHTS = "headLights"
    const val MUSIC_SYSTEM = "musicSystem"
    const val TRUNK_DOOR = "trunkDoor"
    const val OTHERS = "others"
    const val ALL = "All"


    fun getNtfType(): List<NotificationTypeModel> {
        val list = mutableListOf<NotificationTypeModel>()
        list.add(NotificationTypeModel("Parking", PARKING))
        list.add(NotificationTypeModel("Ignition", ACC_CHANGED))
        list.add(NotificationTypeModel("Anti Theft", ANTI_THEFT))
        list.add(NotificationTypeModel("Zone", ZONE))
        list.add(NotificationTypeModel("Sharable Link", SHAREABLE_LINK))
        list.add(NotificationTypeModel("AC", AC))
        list.add(NotificationTypeModel("Door", DOOR))
        list.add(NotificationTypeModel("Engine Cut", ENGINE_CUT))
        list.add(NotificationTypeModel("Sun Roof", SUN_ROOF))
        list.add(NotificationTypeModel("Seat Belt", SEAT_BELT))
        list.add(NotificationTypeModel("Inter Lock", INTER_LOCK))
        list.add(NotificationTypeModel("Fuel Meter", FUEL_METER))
        list.add(NotificationTypeModel("Temperature", TEMPERATURE))
        list.add(NotificationTypeModel("Head Lights", HEAD_LIGHTS))
        list.add(NotificationTypeModel("Music System", MUSIC_SYSTEM))
        list.add(NotificationTypeModel("Trunk Door", TRUNK_DOOR))
        list.add(NotificationTypeModel("Others", OTHERS))
        list.sortBy {
            it.title
        }
        list.add(0, NotificationTypeModel("All", ALL))
        return list
    }

}