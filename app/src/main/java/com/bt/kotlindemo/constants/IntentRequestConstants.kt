package com.bt.kotlindemo.constants

class IntentRequestConstants {


    companion object {
        const val SELECT_COUNTRY = 1
        const val ADD_DEVICE = 2
        const val EDIT_DEVICE = 3
        const val EDIT_POI = 4
        const val SELECT_DEVICES = 5
        const val ADD_LINK = 6
        const val ADD_ZONE = 7
        const val ADD_POI = 8
        const val CONTACT_PERMISSION = 9
        const val SHARE_DEVICE = 10
        const val SELECT_CONTACTS = 11
        const val STORAGE_PERMISSION = 12
        const val PICK_IMAGE = 13
        const val CAMERA_PERMISSION = 14
        const val CHANGE_MOBILE_NUMBER = 15
        const val UPDATE_PROFILE = 16
    }
}