/*
 * *
 *  * Created by Surajit on 10/4/20 7:28 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 10/4/20 7:28 PM
 *  *
 */

package com.bt.kotlindemo.constants

class DeviceIconType {

    companion object {
        val AUTO = 1
        val BIKE = 2
        val BUS = 3
        val CAR = 4
        val HEAVY_MACHINERY = 5
        val PERSONAL = 6
        val SCOOTER = 7
        val SCOOTY = 8
        val TRUCK = 9
        val TANKER_TRUCK = 10
        val TRACKTOR = 11
        val TRAIN = 12

    }
}