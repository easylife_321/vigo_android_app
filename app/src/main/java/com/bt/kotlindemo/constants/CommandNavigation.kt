/*
 * *
 *  * Created by Surajit on 18/5/20 6:14 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 18/5/20 6:14 PM
 *  *
 */

package com.bt.kotlindemo.constants

class CommandNavigation {

    companion object {

        const val ASSIGN_ZONE = ""
        const val CHANGE_ICON = ""
        const val MARK_AS_ZONE = "c_markAsZone"
        const val MOVE_TO_GROUP = ""
        const val REFRESH_GPS = "c_refreshGPS"
        const val SHARABLE_LINK = "c_shareablelink"
        const val TRIP = "c_startTrip"
        const val PARKING = "c_parking"
        const val ANTI_THEFT = "c_antitheft"
        const val TROUBLESHOOT = "c_troubleshoot"
        const val DISABLE_TRACKING = "c_disableSharedTracking"
        const val CONTROL_ACCESSORY = "controlGpsDeviceAccessory"
        const val UNTRACEABLE_MODE = "c_traceableMode"
        const val FOLLOW_DEVICE = "c_followThis"
        const val SPEED_LIMIT = "c_setSpeedLimit"
        const val EDIT_DEVICE = "c_editDevice"
        const val SUBSCRIPTIONS = "c_subscriptions"
        const val SET_ODOMETER = "c_setOdometer"
        const val NAVIGATE = "c_navigate"


    }
}