package com.bt.kotlindemo.constants

class IntentConstants {


    companion object {
       const val IS_SELECTABLE="is_selectable"
        const val DATA = "data"
        const val DEVICE_ID = "device_id"
        const val DEVICE = "device"
        const val GROUP = "group"
        const val EDIT = "edit"
        const val POI_ID = "poi_id"
        const val ZONE = "zone"
        const val TITLE = "title"
        const val LAT = "lat"
        const val LNG = "lng"
        const val ADDRESS = "address"
        const val SATE = "state"
        const val POSTAL_CODE = "postal_code"
        const val CITY = "city"

    }
}