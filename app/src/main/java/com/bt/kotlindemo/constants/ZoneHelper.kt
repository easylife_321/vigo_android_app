/*
 * *
 *  * Created by Surajit on 16/6/20 7:36 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 16/6/20 7:36 PM
 *  *
 */

package com.bt.kotlindemo.constants

import java.util.*

object ZoneHelper {

    fun getNotifyOn(value: String): Int {
        return when (value) {
            "Notification" -> 1
            "Notification & Email" -> 1
            "Email" -> 3
            "Do not notify" -> 0
            else -> 0
        }
    }

    fun getNotifyOnText(value: Int): String {
        return when (value) {
            1 -> "Notification"
            2 -> "Notification & Email"
            3 -> "Email"
            0 -> "Do not notify"
            else -> ""
        }
    }

    fun getNotifyWhen(value: String): Int {
        return when (value.lowercase(Locale.ROOT)) {
            "zone in" -> 1
            "zone out" -> 3
            "zone in & out" -> 2
            "never" -> 0
            else -> 0
        }
    }

    fun getNotifyWhenText(value: Int): String {
        return when (value) {
            1 -> "Zone In"
            3 -> "Zone Out"
            2 -> "Zone In & Out"
            0 -> "Never"
            else -> ""
        }
    }
}