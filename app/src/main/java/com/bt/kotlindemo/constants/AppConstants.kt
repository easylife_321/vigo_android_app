/*
 * *
 *  * Created by Surajit on 27/4/20 4:25 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/4/20 4:25 PM
 *  *
 */

package com.bt.kotlindemo.constants

class AppConstants {

    companion object {
        const val MAP_DEFAULT_ZOOM = 16f
        const val EMPTY = ""
        const val MIME_TYPE_IMAGE = "image/*"
    }
}