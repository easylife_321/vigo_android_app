/*
 * *
 *  * Created by Surajit on 2/21/21 11:59 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 2/21/21 11:59 PM
 *  *
 */

package com.bt.kotlindemo.constants

class DeviceStatus {
    companion object {
        const val MOVING = "MV"
        const val STANDING = "ST"
        const val IDLE = "ID"
        const val OFFLINE = "OF"

    }
}