package com.bt.kotlindemo.constants

class Preferences {

    companion object {

        const val PREFERENCE_NAME="preference_name"

        const val BASE_URL = "base_url"
        const val BASE_URL_IMG = "base_url_img"

        const val IS_REGISTERED = "is_registered"
        const val USER_ID = "user_id"
        const val USER_NAME = "user_name"
        const val PASSWORD = "password"
        const val TOKEN = "token"
        const val TOKEN_EXPIRY = "token_expiry"
        const val ACCOUNT_ID = "account_id"
        const val EMAIL_ID = "email_id"
        const val MOBILE = "mobile"
        const val COUNTRY="country"
        const val FIRE_BASE_TOKEN="fire_base_token"
        const val UPDATE_AVL="update_avl"
        const val PHONE_LENGTH="phone_length"
        const val PROFILE_IMAGE="profile_image"
        const val ADDRESS_LINE1="address_line1"
        const val ADDRESS_LINE2="address_line2"
        const val PIN="pin"
        const val STATE="state"
        const val STATE_ID="state_id"
        const val COUNTRY_ID="country_id"
        const val CITY="city"
        const val DND="dnd"
        const val MENU="menu"
    }
}