/*
 * *
 *  * Created by Surajit on 28/7/20 4:27 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 28/7/20 4:27 PM
 *  *
 */

package com.bt.kotlindemo.constants

object OnBoardingConstants {
    const val IS_CONTACT_SYNCED="is_contact_synced"
}