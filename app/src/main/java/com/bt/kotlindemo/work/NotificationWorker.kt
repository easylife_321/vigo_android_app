/*
 * *
 *  * Created by Surajit on 1/24/21 2:04 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/24/21 2:04 AM
 *  *
 */
package com.bt.kotlindemo.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.data.repository.NotificationRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.notifications.NotificationResponse
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import org.koin.core.KoinComponent
import org.koin.core.inject

class NotificationWorker(appContext: Context, workerParams: WorkerParameters) :
    CoroutineWorker(appContext, workerParams), KoinComponent {

    private val notificationRepository: NotificationRepository by inject()
    val preferences: AppPreferences by inject()

    override suspend fun doWork(): Result {
        try {
            val response = fetch()
            if (response.status == ApiResult.Status.SUCCESS) {
                if (response.data?.result?.code == ApiCode.SUCCESS) {
                    notificationRepository.addNotifications(
                        response.data.notifications ?: emptyList()
                    )
                }
            } else {
                return Result.failure()
            }

            return Result.success()

        } catch (e: Exception) {
            return Result.failure()
        }
    }

    private suspend fun fetch(): ApiResult<NotificationResponse> {

        val ts = notificationRepository.getLastTs()
        val obj = JsonObject()
        obj.addProperty("userId", preferences.getUserId())
        obj.addProperty("lastSentTime", ts)
        return notificationRepository.getNotifications(obj)
    }

}