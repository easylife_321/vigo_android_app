/*
 * *
 *  * Created by Surajit on 1/24/21 2:04 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/24/21 2:04 AM
 *  *
 */
package com.bt.kotlindemo.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.WorkerParameters
import com.bt.kotlindemo.BuildConfig
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.data.network.netwrok_responses.AddAddressResponse
import com.bt.kotlindemo.data.repository.AddressRepository
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import org.koin.core.KoinComponent
import org.koin.core.inject

class AddressWorker(appContext: Context, private val workerParams: WorkerParameters) :
    CoroutineWorker(appContext, workerParams), KoinComponent {

    private val notificationRepository: AddressRepository by inject()
    val preferences: AppPreferences by inject()

    override suspend fun doWork(): Result {
        try {
            val response = save(workerParams.inputData)
            if (response.status == ApiResult.Status.SUCCESS) {
            } else {
                return Result.failure()
            }

            return Result.success()

        } catch (e: Exception) {
            return Result.failure()
        }
    }

    private suspend fun save(inputData: Data): ApiResult<AddAddressResponse> {
        val obj = JsonObject()
        obj.addProperty("lat", inputData.getDouble(IntentConstants.LAT, 0.0))
        obj.addProperty("lng", inputData.getDouble(IntentConstants.LNG, 0.0))
        obj.addProperty("address", inputData.getString(IntentConstants.ADDRESS))
        obj.addProperty("stateName", inputData.getString(IntentConstants.SATE))
        obj.addProperty("postalCode", inputData.getString(IntentConstants.POSTAL_CODE))
        obj.addProperty("cityName", inputData.getString(IntentConstants.CITY))
        obj.addProperty("accountId", BuildConfig.ACCOUNT_ID)
        return notificationRepository.saveAddress(obj)
    }

}