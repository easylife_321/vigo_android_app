/*
 * *
 *  * Created by Surajit on 9/8/20 5:28 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/8/20 5:28 PM
 *  *
 */

package com.bt.kotlindemo.services

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.data.repository.NotificationRepository
import com.bt.kotlindemo.model.notifications.Notification
import com.bt.kotlindemo.ui.device_detail.DeviceDetailActivity
import com.bt.kotlindemo.ui.splash_activity.SplashActivity
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.NotificationHelper
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import timber.log.Timber

class FCMService : FirebaseMessagingService() {


    private val repository: NotificationRepository by inject()
    private val preference: AppPreferences by inject()

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Timber.tag("Surajit").d("onMessageReceived: ${message.data}")
        if (preference.isRegistered()) {
            try {
                if (message.data["body"] != null) {
                    insert(message.data["body"].toString())
                } else {
                    val notificationHelper = NotificationHelper(this@FCMService)
                    notificationHelper.createNotification(
                        message.notification?.title ?: resources.getString(R.string.app_name),
                        message.notification?.body ?: "",
                        R.mipmap.ic_launcher, null,
                        Intent(this@FCMService, SplashActivity::class.java)
                    )
                }
            } catch (ex: Exception) {
                FirebaseAnalytics.getInstance(this).logEvent("fcm_exception", Bundle().apply {
                    putString("body", message.data["body"])
                    putString("exception", ex.message)
                })
            }
        }
    }

    fun insert(data: String) {
        GlobalScope.launch(Dispatchers.IO) {
            val notification = Gson().fromJson(data, Notification::class.java)
            val notificationHelper = NotificationHelper(this@FCMService)
            notificationHelper.createNotificationDevice(
                title = notification?.title ?: resources.getString(R.string.app_name),
                message = notification?.msg ?: "",
                isSos = notification.soundType == 2,
                resultIntent = Intent(this@FCMService, DeviceDetailActivity::class.java).apply {
                    putExtra(
                        IntentConstants.DEVICE_ID,
                        notification.deviceId
                    )
                }
            )
            repository.addNotification(notification)
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
    }
}