package com.bt.kotlindemo.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.databinding.ConnectContactLayoutBinding

class CustomContactSyncPopup(val context1: Context, val listener: ContactSyncPopupListener) :
    Dialog(context1) {
    private lateinit var binding: ConnectContactLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context1),
            R.layout.connect_contact_layout,
            null,
            false
        )
        setContentView(binding.root)
        window?.setLayout(
            ConstraintLayout.LayoutParams.WRAP_CONTENT,
            ConstraintLayout.LayoutParams.WRAP_CONTENT
        )
        window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        setCanceledOnTouchOutside(true)
        setCancelable(true)

        contactMediaSync()
    }

    private fun contactMediaSync() {
        binding.btnContinue.setOnClickListener {
            listener.onContinueBtnPressed()
            dismiss()
        }
        binding.btnNotNow.setOnClickListener {
            listener.notNowBtnPressed()
            dismiss()
        }
        binding.tvPolicy.setOnClickListener {
            listener.onPolicyBtnPressed()
        }
    }
}

interface ContactSyncPopupListener {

    fun onContinueBtnPressed()

    fun notNowBtnPressed()

    fun onPolicyBtnPressed()

}
