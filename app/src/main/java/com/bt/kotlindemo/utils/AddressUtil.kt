/*
 * *
 *  * Created by Surajit on 25/4/20 8:54 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/4/20 8:54 PM
 *  *
 */

package com.bt.kotlindemo.utils

import android.content.Context
import android.location.Address
import android.location.Geocoder
import java.util.*
import com.bt.kotlindemo.model.address.Address as AddressSever

object AddressUtil {

    fun getAddress(
        lat: Double,
        lng: Double,
        context: Context,
        returnNullable: Boolean = false
    ): String? {
        val fullAddress = "Unknown Location"
        var toUpdate: AddressSever? = null
        val address: String? = try {

            val addresses: List<Address>? =
                Geocoder(context, Locale.getDefault()).getFromLocation(lat, lng, 1)
            if (addresses.isNullOrEmpty()) {
                if (returnNullable) null else "Location not found"
            } else {
                toUpdate = AddressSever(
                    address = addresses[0].getAddressLine(0),
                    stateName = addresses[0].adminArea,
                    cityName = addresses[0].locality,
                    postalCode = addresses[0].postalCode,
                    lat = lat,
                    lng = lng
                )
                addresses[0].getAddressLine(0) // If any additional address line present than
                // only, check with max available address lines
                // by getMaxAddressLineIndex
            }
        } catch (ex: Exception) {
            return if (returnNullable) null else fullAddress
        }
        toUpdate?.let {
            WorkHelper.startAddressWork(it)
        }

        return address
    }


}