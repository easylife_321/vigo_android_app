/*
 * *
 *  * Created by Surajit on 27/4/20 3:41 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/4/20 3:41 PM
 *  *
 */

package com.bt.kotlindemo.utils

import android.animation.ValueAnimator
import com.bt.kotlindemo.utils.google_map.MapsUtil

import com.google.android.gms.maps.model.LatLng

import com.google.android.gms.maps.model.Marker


object VigoMarker {

    fun move(marker: Marker, newLatLng: LatLng, animator: ValueAnimator,rotateMarker: Boolean = true){
        MapsUtil.moveMarkerSmoothly(marker, newLatLng,rotateMarker,animator)
        animator.start()
        return
    }

}