/*
 * *
 *  * Created by Surajit on 9/8/20 5:41 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/8/20 5:41 PM
 *  *
 */
package com.bt.kotlindemo.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.os.StrictMode
import android.provider.Settings
import android.text.Html
import android.text.TextUtils
import android.util.Patterns
import androidx.core.app.NotificationCompat
import com.bt.kotlindemo.R
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class NotificationHelper(private val mContext: Context) {
    private var mNotificationManager: NotificationManager? = null
    private var mBuilder: NotificationCompat.Builder? = null

    /**
     * Create and push the notification
     */
    fun createNotificationDevice(
        title: String?,
        message: String?,
        isSos: Boolean,
        resultIntent: Intent
    ) {
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val resultPendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PendingIntent.getActivity(
                mContext,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
            )
        } else {
            PendingIntent.getActivity(
                mContext,
                0 /* Request code */, resultIntent,
                 PendingIntent.FLAG_UPDATE_CURRENT
            )
        }
        var sound = Settings.System.DEFAULT_NOTIFICATION_URI
        if(isSos){
            sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.packageName + "/" + R.raw.sos)
        }
        var channelId =  mContext.getString(R.string.notification_device_channel_id)
        var channelName = mContext.getString(R.string.notification_device_channel_name)
        if(isSos){
            channelId =  mContext.getString(R.string.notification_sos_channel_id)
            channelName = mContext.getString(R.string.notification_sos_channel_name)
        }
        mBuilder = NotificationCompat.Builder(mContext,channelId)
        mBuilder?.apply {
            setSmallIcon(R.drawable.ic_notifications)
            setContentTitle(title)
            setContentText(message)
            setAutoCancel(false)
            setSound(sound)
            setContentIntent(resultPendingIntent)
        }

        mNotificationManager =
            mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val audioAttributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(
                channelId,
                channelName,
                importance
            )
            notificationChannel.setSound(sound, audioAttributes)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(
                100,
                200,
                300,
                400,
                500,
                400,
                300,
                200,
                400
            )
            mNotificationManager?.createNotificationChannel(notificationChannel)
        }
        mNotificationManager?.notify(0, mBuilder?.build())
    }


    fun createNotification(
        title: String,
        message: String,
        icon: Int,
        imageUrl: String?,
        intent: Intent
    ) {
        // notification icon
        //final int icon = R.drawable.ic_luncher;
        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        mBuilder = NotificationCompat.Builder(mContext)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val resultPendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PendingIntent.getActivity(
                mContext,
                0 /* Request code */, intent,
                PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
            )
        } else {
            PendingIntent.getActivity(
                mContext,
                0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        }
        if (!TextUtils.isEmpty(imageUrl)) {
            if (imageUrl != null && imageUrl.length > 4 && Patterns.WEB_URL.matcher(imageUrl)
                    .matches()
            ) {
                val bitmap = getBitmapFromURL(imageUrl)
                if (bitmap != null) {
                    showBigNotification(
                        bitmap,
                        mBuilder!!,
                        icon,
                        title,
                        message,
                        resultPendingIntent
                    )
                } else {
                    showSmallNotification(mBuilder!!, icon, title, message, resultPendingIntent)
                }
            }
        } else {
            showSmallNotification(mBuilder!!, icon, title, message, resultPendingIntent)
        }
    }

    private fun showSmallNotification(
        mBuilder: NotificationCompat.Builder,
        icon: Int,
        title: String,
        message: String,
        resultPendingIntent: PendingIntent
    ) {
        mBuilder.setSmallIcon(icon)
        mBuilder.setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(false)
            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
            .setContentIntent(resultPendingIntent)
        mNotificationManager =
            mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(
                mContext.getString(R.string.notification_other_channel_id),
                "NOTIFICATION_CHANNEL_NAME",
                importance
            )
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(
                100,
                200,
                300,
                400,
                500,
                400,
                300,
                200,
                400
            )
            mBuilder.setChannelId(mContext.getString(R.string.notification_other_channel_id))
            mNotificationManager!!.createNotificationChannel(notificationChannel)
        }
        mNotificationManager!!.notify(0 /* Request Code */, mBuilder.build())
    }

    private fun showBigNotification(
        bitmap: Bitmap,
        mBuilder: NotificationCompat.Builder,
        icon: Int,
        title: String,
        message: String,
        resultPendingIntent: PendingIntent
    ) {
        val bigPictureStyle =
            NotificationCompat.BigPictureStyle()
        bigPictureStyle.setBigContentTitle(title)
        bigPictureStyle.setSummaryText(Html.fromHtml(message).toString())
        bigPictureStyle.bigPicture(bitmap)
        mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
            .setAutoCancel(true)
            .setContentTitle(title)
            .setContentIntent(resultPendingIntent)
            .setStyle(bigPictureStyle)
            .setSmallIcon(icon)
            .setLargeIcon(BitmapFactory.decodeResource(mContext.resources, icon))
            .setContentText(message)
            .build()
        mNotificationManager =
            mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(
                mContext.getString(R.string.notification_other_channel_id),
                mContext.getString(R.string.notification_other_channel_name),
                importance
            )
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(
                100,
                200,
                300,
                400,
                500,
                400,
                300,
                200,
                400
            )
            mBuilder.setChannelId(mContext.getString(R.string.notification_other_channel_id))
            mNotificationManager!!.createNotificationChannel(notificationChannel)
        }
        mNotificationManager!!.notify(0 /* Request Code */, mBuilder.build())
    }

    /**
     * Downloading push notification image before displaying it in
     * the notification tray
     */
    private fun getBitmapFromURL(strURL: String?): Bitmap? {
        return try {
            val url = URL(strURL)
            val connection =
                url.openConnection() as HttpURLConnection
            connection.doInput = true
            connection.connect()
            val input = connection.inputStream
            BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            e.printStackTrace()
            null
        }
    }


}