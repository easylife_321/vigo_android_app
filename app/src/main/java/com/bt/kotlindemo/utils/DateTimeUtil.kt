/*
 * *
 *  * Created by Surajit on 31/5/20 9:54 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 31/5/20 9:54 PM
 *  *
 */

package com.bt.kotlindemo.utils

import java.text.SimpleDateFormat
import java.time.OffsetDateTime
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*

object DateTimeUtil {

    fun formatDateHistory(timeStamp: Long): String {
        val pattern = "dd MMM yyyy"
        return SimpleDateFormat(pattern, Locale.getDefault()).format(Date(timeStamp))
    }


    fun formatSharableTime(timeStamp: Long): String {
        val pattern = "dd MMM yyyy hh:mm a"
        return SimpleDateFormat(pattern, Locale.getDefault()).format(Date(timeStamp))
    }

    fun formatDeviceShare(timeStamp: Long): String {
        val pattern = "hh:mm a"
        return SimpleDateFormat(pattern, Locale.getDefault()).format(Date(timeStamp))
    }

    fun formatDeviceShare24Hour(timeStamp: Long): String {
        val pattern = "HH:mm"
        return SimpleDateFormat(pattern, Locale.getDefault()).format(Date(timeStamp))
    }




    fun formatSharableTime(timeStamp: ZonedDateTime): String {
        val pattern = "dd MMM yyyy hh:mm a"
        return timeStamp.format(DateTimeFormatter.ofPattern(pattern))
        //return SimpleDateFormat(pattern, Locale.getDefault()).format(Date(timeStamp))
    }
    fun formatHistoryTime(timeStamp: ZonedDateTime): String {
        val pattern = "dd MMM hh:mm a"
        return timeStamp.format(DateTimeFormatter.ofPattern(pattern))
        //return SimpleDateFormat(pattern, Locale.getDefault()).format(Date(timeStamp))
    }

    fun formatSharableTimeToTimeStamp(time: String): Long? {
        val pattern = "dd MMM yyyy hh:mm a"
        return SimpleDateFormat(pattern, Locale.getDefault()).parse(time)?.time
    }

    fun isValidShareableDate(from: String, to: String): Boolean {
        val pattern = "dd MMM yyyy hh:mm a"
        val dateFormat = SimpleDateFormat(
            pattern, Locale.getDefault()
        )
        val toDate: Date?
        val fromDate: Date?
        try {
            fromDate = dateFormat.parse(from)
            toDate = dateFormat.parse(to)
            return toDate.after(fromDate)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    fun getTimeDifference(start: Long, end: Long): String {
        try {

            val difference = (end - start)
            val days = (difference / (1000 * 60 * 60 * 24)).toInt()
            val hours = (difference / (1000 * 60 * 60)).toInt()
            val min =
                ((difference / (1000 * 60)) % 60).toInt()
            return if (hours == 0) min.toString() + "min" else hours.toString() + "hr" + min + "min"
        } catch (ignored: Exception) {
        }
        return ""
    }

    fun getTimeDifference(difference: Long): String {
        try {
            val days = (difference / (1000 * 60 * 60 * 24)).toInt()
            val hours = (difference / (1000 * 60 * 60)).toInt()
            val min =
                ((difference / (1000 * 60)) % 60).toInt()
            return if (hours == 0) min.toString() + "min" else hours.toString() + "hr" + min + "min"
        } catch (ignored: Exception) {
        }
        return ""
    }


    fun isValidHistoryRange(from: Long, to: Long): Boolean {
        return ((to - from) / (1000 * 60 * 60)).toInt() <= 24
    }

    fun formatSelectedTime(timeStamp: Long): String {
        val pattern = "hh:mm a"
        return SimpleDateFormat(pattern, Locale.getDefault()).format(Date(timeStamp))
    }

    fun getDate(timeStamp: ZonedDateTime): String {
        val pattern = "dd MMM yyyy"
        return timeStamp.format(DateTimeFormatter.ofPattern(pattern))
    }

}