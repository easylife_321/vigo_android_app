package com.bt.kotlindemo.utils


import android.content.res.ColorStateList
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.DeviceIconType
import com.bt.kotlindemo.constants.DeviceStatus
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import org.koin.core.KoinComponent
import org.koin.core.get
import java.io.File


object CustomBindingAdapter : KoinComponent {

    var picasso: Picasso = get()
    var pref: AppPreferences = get()


    @BindingAdapter("imgRes")
    @JvmStatic
    fun bindCurrency(view: ImageView, resource: Int) {
        view.setImageResource(resource)
    }

    @BindingAdapter("backgroundImage")
    @JvmStatic
    fun bindBackgroundImage(view: ImageView, resource: Int) {
       picasso.load(resource).error(R.mipmap.light_alu).into(view)
    }

    @BindingAdapter("setBackGround")
    @JvmStatic
    fun setBackGround(btn: ImageView, isSelected: Boolean) {
        if (isSelected) {
            btn.setBackgroundResource(R.drawable.tab_selected)
        } else {
            btn.background = null
        }
    }


    @BindingAdapter("setImgFromUrl")
    @JvmStatic
    fun setImgFromUrl(img: ImageView, url: String?) {
        url?.apply {
            picasso.load(pref.getBaseImageUrl() + url).error(android.R.drawable.stat_notify_error)
                .into(img)
        }
    }

    @BindingAdapter("setImgFromFile")
    @JvmStatic
    fun setImgFromFile(img: ImageView, file: File?) {
        file?.apply {
            picasso.load(file).error(android.R.drawable.stat_notify_error).into(img)
        }
    }

    @BindingAdapter("setBackground")
    @JvmStatic
    fun setBackground(img: ImageView,res:Int) {
      picasso.load(res).into(img)
    }

    @BindingAdapter("setImgFromSrc")
    @JvmStatic
    fun setImgFromSrc(img: ImageView, obj: Any?) {
        obj?.apply {
            when (obj) {
                is File -> {
                    picasso.load(obj).error(R.drawable.user_icon).into(img)
                }
                is String -> {
                    if (obj.isEmpty()) {
                        img.setImageResource(R.drawable.user_icon)
                    } else {
                        picasso.load(pref.getBaseImageUrl() + obj).error(R.drawable.user_icon)
                            .into(img)
                    }
                }
                is Int -> {
                    img.setImageResource(obj)
                }
                is Uri -> {
                    picasso.load(obj).error(R.drawable.user_icon).into(img)
                }
            }
        }
    }

    @BindingAdapter("setCommandStatus")
    @JvmStatic
    fun setCommandStatus(txt: TextView, status: Any) {
        if (status is Boolean) {
            if (status) {
                txt.text = "ON "
                txt.setTextColor(
                    ContextCompat.getColor(
                        txt.context,
                        R.color.colorBlueNoChange
                    )
                )
            } else {
                txt.text = "OFF"
                txt.setTextColor(
                    ContextCompat.getColor(
                        txt.context,
                        R.color.colorRedNoChange
                    )
                )

            }
        }
    }

    @BindingAdapter("setCommandBg")
    @JvmStatic
    fun setCommandBg(view: RelativeLayout, status: Any) {
        if (status is Boolean) {
            if (status) {
                view.background.setTint(
                    ContextCompat.getColor(
                        view.context,
                        R.color.colorBlueNoChange
                    )
                )
            } else {
                view.background.setTint(
                    ContextCompat.getColor(
                        view.context,
                        R.color.colorRedNoChange
                    )
                )
            }
        }
    }

    @BindingAdapter("url", "status")
    @JvmStatic
    fun setAccessoryImage(img: ImageView,   url: String?, status: String) {
      url?.apply {
          var tempUrl = url
          tempUrl = if (status == "true") {
              tempUrl.replace("#", "1")
          } else {
              tempUrl.replace("#", "0")
          }
          picasso.load(pref.getBaseImageUrl() + tempUrl)
              .error(android.R.drawable.stat_notify_error)
              .into(img)
      }



    }


    @BindingAdapter("url", "status", "checkStatus")
    @JvmStatic
    fun loadFromNetwork(img: ImageView, url: String?, status: Any, checkStatus: Boolean) {
        if (checkStatus) {
            url?.apply {
                var tempUrl = url
                tempUrl = if ((status as Boolean)) {
                    tempUrl.replace("#", "1")
                } else {
                    tempUrl.replace("#", "0")
                }

                picasso.load(pref.getBaseImageUrl() + tempUrl)
                    .error(android.R.drawable.stat_notify_error)
                    .into(img)
            }
        } else {
           // img.imageTintList =
            //    ColorStateList.valueOf(img.context.getColorFromAttr(R.attr.bgColorDark))
            url?.apply {
                picasso.load(pref.getBaseImageUrl() + url)
                    .error(android.R.drawable.stat_notify_error)
                    .into(img)
            }
        }
    }



    @BindingAdapter("loadGif")
    @JvmStatic
    fun loadGif(img: ImageView, res: Int) {
        Glide.with(img.context).asGif().load(res).into(img)
    }

    @BindingAdapter("formatTime")
    @JvmStatic
    fun formatTime(txt: TextView, time: Long) {
        txt.text = DateTimeUtil.formatSharableTime(time)
    }

    @BindingAdapter("toTimeSelected","fromTimeSelected")
    @JvmStatic
    fun historyTime(txt: TextView, toTime: Long,fromTime: Long) {
        val text = DateTimeUtil.formatSharableTime(fromTime) +" - "+
                DateTimeUtil.formatSharableTime(toTime)+
                " (${DateTimeUtil.getTimeDifference(fromTime, toTime)})"
        txt.text = text
    }

    @BindingAdapter("formatTimeShare")
    @JvmStatic
    fun formatTimeShare(txt: TextView, time: Long) {
        if (time == 0L) return
        txt.text = DateTimeUtil.formatDeviceShare(time)
    }
    @BindingAdapter("formatTimeHistory")
    @JvmStatic
    fun formatTimeHistory(txt: TextView, time: Long) {
        if (time == 0L) return
        txt.text = DateTimeUtil.formatDeviceShare(time*1000L)
    }

    @BindingAdapter("from", "to")
    @JvmStatic
    fun timeDifference(txt: TextView, from: Long, to: Long) {
        txt.text = "(".plus(DateTimeUtil.getTimeDifference(from, to)).plus(")")
    }

    @BindingAdapter("formatTimeLocal")
    @JvmStatic
    fun formatTimeLocal(txt: TextView, time: Long) {
        txt.text = DateTimeUtil.formatSharableTime(time.toLocalTime())
    }


    @BindingAdapter("fromTime", "toTime")
    @JvmStatic
    fun timeSelected(txt: TextView, from: Long, to: Long) {
        txt.text = DateTimeUtil.formatSelectedTime(from).plus("-")
            .plus(DateTimeUtil.formatSelectedTime(to))
    }

    @BindingAdapter("formatToDate")
    @JvmStatic
    fun formatToDate(txt: TextView, time: Long) {
        txt.text = DateTimeUtil.formatDateHistory(time)
    }


    @BindingAdapter("isConnected")
    @JvmStatic
    fun setTint(img: ImageView, isConnected: Boolean) {
        if (isConnected) {
            img.imageTintList =
                ColorStateList.valueOf(img.context.resources.getColor(R.color.colorPrimary))
        } else {
            img.imageTintList =
                ColorStateList.valueOf(img.context.resources.getColor(R.color.disabled_color))
        }
    }


    @BindingAdapter("formatTimeToLocal")
    @JvmStatic
    fun formatTimeToLocal(txt: TextView, time: Long) {
        if (time == 0L) return
        txt.text = DateTimeUtil.formatDeviceShare(time.toLocalTime().toInstant().toEpochMilli())
    }


    @BindingAdapter("markerTint")
    @JvmStatic
    fun setMarkerTint(view: View, status: String) {
        when (status) {
            DeviceStatus.MOVING -> {
                when (view) {
                    is ImageView -> {
                        view.imageTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(view.context,R.color.moving))
                    }
                    is TextView -> {
                        view.setTextColor(ContextCompat.getColor(view.context,R.color.moving))
                    }
                    else -> {
                        view.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(view.context,R.color.moving))
                    }
                }
            }
            DeviceStatus.STANDING -> {
                when (view) {
                    is ImageView -> {
                        view.imageTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(view.context,R.color.standing))
                    }
                    is TextView -> {
                        view.setTextColor(ContextCompat.getColor(view.context,R.color.standing))
                    }
                    else -> {
                        view.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(view.context,R.color.standing))
                    }
                }
            }
            DeviceStatus.IDLE -> {
                when (view) {
                    is ImageView -> {
                        view.imageTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(view.context,R.color.idle))
                    }
                    is TextView -> {
                        view.setTextColor(ContextCompat.getColor(view.context,R.color.idle))
                    }
                    else -> {
                        view.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(view.context,R.color.idle))
                    }
                }
            }
            DeviceStatus.OFFLINE -> {
                when (view) {
                    is ImageView -> {
                        view.imageTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(view.context,R.color.offline))
                    }
                    is TextView -> {
                        view.setTextColor(ContextCompat.getColor(view.context,R.color.offline))
                    }
                    else -> {
                        view.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(view.context,R.color.offline))
                    }
                }
            }
        }
    }

    @BindingAdapter("setDeviceIcon")
    @JvmStatic
    fun setDeviceIcon(img: ImageView, type: Int) {
        when (type) {
            DeviceIconType.AUTO -> {
                img.setImageResource(R.drawable.ic_auto)
            }
            DeviceIconType.CAR -> {
                img.setImageResource(R.drawable.ic_car)
            }
            DeviceIconType.BUS -> {
                img.setImageResource(R.drawable.ic_bus)
            }
            DeviceIconType.TRUCK -> {
                img.setImageResource(R.drawable.ic_truck)
            }
            DeviceIconType.BIKE -> {
                img.setImageResource(R.drawable.ic_bike)
            }
            DeviceIconType.SCOOTER -> {
                img.setImageResource(R.drawable.ic_scooter)
            }
            else -> {
                img.setImageResource(R.drawable.ic_car)
            }
        }
    }


}


