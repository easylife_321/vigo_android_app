/*
 * *
 *  * Created by Surajit on 2/26/21 1:19 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 2/26/21 1:19 PM
 *  *
 */

package com.bt.kotlindemo.utils

import android.app.DatePickerDialog
import android.content.Context
import com.bt.kotlindemo.interfaces.DateSelectionCallback
import java.util.*

object DatePickerHelper {

    fun showDatePicker(context: Context, callback: DateSelectionCallback) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val dpd = DatePickerDialog(context, { view, year, monthOfYear, dayOfMonth ->
            c.set(Calendar.YEAR, year)
            c.set(Calendar.MONTH, monthOfYear)
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            callback.dateSelected(c.timeInMillis)
        }, year, month, day)
        dpd.datePicker.maxDate = c.timeInMillis
        dpd.show()
    }

}
