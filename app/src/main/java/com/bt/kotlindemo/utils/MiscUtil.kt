package com.bt.kotlindemo.utils

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import android.os.Environment
import android.telephony.TelephonyManager
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*


object MiscUtil {
    @Throws(IOException::class)
    fun getJsonAsString(filename: String, context: Context): String? {

        return context.assets.open(filename).bufferedReader().use {
            it.readText()
        }
    }


    fun getDeviceCountryCode(context: Context): String? {
        var countryCode: String?
        // try to get country code from TelephonyManager service
        val tm =
            context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        // query first getSimCountryIso()
        countryCode = tm.simCountryIso
        if (countryCode != null && countryCode.length == 2) return countryCode.toLowerCase(
            Locale.getDefault()
        )
        countryCode = tm.networkCountryIso
        //  }
        if (countryCode != null && countryCode.length == 2) return countryCode.toLowerCase(
            Locale.getDefault()
        )
        // if network country not available (tablets maybe), get country code from Locale class
        countryCode = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales[0].country
        } else {
            context.resources.configuration.locale.country
        }
        return if (countryCode != null && countryCode.length == 2) countryCode.toLowerCase(Locale.getDefault()) else "us"
        // general fallback to "us"
    }


    fun prepareFilePart(
        partName: String,
        file: File
    ): MultipartBody.Part {
        // create RequestBody instance from file
        val requestFile: RequestBody = file.asRequestBody("image/jpeg".toMediaTypeOrNull())
        /* MultipartBody.Part is used to send also the actual file name */
        return MultipartBody.Part.createFormData(partName, file.name, requestFile)
    }

    fun prepareBodyPart(data: String) = data.toRequestBody("text/plain".toMediaTypeOrNull())

    fun bitmapToFile(bitmap: Bitmap, fileNameToSave: String): File? { // File name like "image.png"
        //create a file to write bitmap data
        var file: File? = null
        return try {
            file = File(Environment.getDataDirectory().toString() + File.separator + fileNameToSave)
            file.createNewFile()

            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, bos) // YOU can also save it in JPEG
            val bitmapData = bos.toByteArray()

            //write the bytes in file
            val fos = FileOutputStream(file)
            fos.write(bitmapData)
            fos.flush()
            fos.close()
            file
        } catch (e: Exception) {
            e.printStackTrace()
            file // it will return null
        }
    }


}