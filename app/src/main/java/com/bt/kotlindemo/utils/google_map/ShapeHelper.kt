/*
 * *
 *  * Created by Surajit on 8/6/20 10:51 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 8/6/20 10:51 PM
 *  *
 */

package com.bt.kotlindemo.utils.google_map

import android.content.Context
import android.graphics.Color
import com.bt.kotlindemo.utils.squareMeterToFeet
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.maps.android.SphericalUtil
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.sqrt


class ShapeHelper(
    val mMap: GoogleMap,
    val context: Context,
    val listener: ShapeListener
) : GoogleMap.OnMapClickListener,
    GoogleMap.OnMarkerClickListener,
    GoogleMap.OnMarkerDragListener {


    private var currentMarkerIndex: Int = 0
    private var selectedShaped: Shape = Shape.CIRCLE


    private val latLngListCurrent = ArrayList<LatLng>()
    private val currentMarkers = ArrayList<Marker>()
    private var currentPolygon: Polygon? = null
    private var currentCircle: Circle? = null
    private var currentPolyline: Polyline? = null
    private val latLngListTemp = ArrayList<LatLng>()
    var isDrawing = false
        set(value) {
            field = value
        }
    private var id = UUID.randomUUID().toString()
    private var isCompleted = false
    private var radius = 100f
    var shape: Shape
        get() = selectedShaped
        set(value) {
            selectedShaped = value
            clearAll()
        }

    fun clearAll() {
        latLngListCurrent.clear()
        currentMarkers.clear()
        currentPolyline = null
        latLngListTemp.clear()
        isCompleted = false
        currentCircle = null
        currentPolygon = null
        mMap.clear()
        isCompleted = false
    }

    init {
        mMap.setOnMarkerClickListener(this)
        mMap.setOnMarkerDragListener(this)
        mMap.setOnMapClickListener(this)
    }


    override fun onMapClick(latLng: LatLng) {
        if (isCompleted)
            return
        isDrawing = true
        when (selectedShaped) {
            Shape.POLYGON -> {
                addMarkerOnMap(latLng)
                latLngListCurrent.add(latLng)
                if (latLngListCurrent.size <= 1) {
                    // Initiate a polyline
                    val polylineOptions = PolylineOptions()
                    polylineOptions.addAll(latLngListCurrent)
                    currentPolyline = mMap.addPolyline(polylineOptions)
                    currentPolyline?.width = 7f
                    currentPolyline?.color = Color.RED
                } else {
                    updatePolyline(
                        // latLngListCurrent[latLngListCurrent.size.minus(3)],
                        latLngListCurrent[latLngListCurrent.size.minus(2)]
                    )

                    // for B-C
                    updatePolyline(
                        // latLngListCurrent[latLngListCurrent.size.minus(2)],
                        latLngListCurrent.last()
                    )
                }

            }
            Shape.CIRCLE -> {
                drawCircle(latLng)
            }
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        when (selectedShaped) {
            Shape.POLYGON -> {
                if (!latLngListCurrent.isNullOrEmpty()) {
                    if (latLngListCurrent.first() == marker.position) {
                        isCompleted = drawPolygon(id, latLngListCurrent)
                        if (isCompleted)
                            currentPolyline?.remove()


                    }
                }
            }
            Shape.CIRCLE -> {

            }
        }

        return false
    }

    override fun onMarkerDragEnd(marker: Marker) {
        when (selectedShaped) {
            Shape.POLYGON -> {
                currentPolygon?.let {
                    // Edit completed polygon
                    latLngListTemp[currentMarkerIndex] = marker.position
                    // Update marker position in marker list
                    val updatedM = currentMarkers[currentMarkerIndex]
                    updatedM.position = marker.position
                    currentMarkers[currentMarkerIndex] = updatedM
                    // Final attempt of draw polygon and update in polygonlist
                    val shapeid = it.tag.toString()
                    it.remove()
                    drawPolygon(shapeid, latLngListTemp)

                }
                    ?: run {
                        // Edit uncompleted polygon (that is actually a polyline)
                        latLngListCurrent.set(currentMarkerIndex, marker.position)
                    }
            }
            Shape.CIRCLE -> {
                latLngListTemp[currentMarkerIndex] = marker.position

            }
        }
    }

    override fun onMarkerDragStart(marker: Marker) {
        val markerTagParts = marker.tag.toString().split("_")
        currentMarkerIndex = markerTagParts[0].toInt()
        when (selectedShaped) {
            Shape.POLYGON -> {
                if (isCompleted) {
                    latLngListTemp.clear()
                    latLngListTemp.addAll(currentPolygon!!.points.dropLast(1))
                } else {
                    currentPolygon = null
                    latLngListCurrent.clear()
                    latLngListCurrent.addAll(currentPolyline!!.points)
                }

            }
            Shape.CIRCLE -> {

                latLngListTemp.clear()
                latLngListTemp.add(currentCircle?.center!!)
                latLngListTemp.add(
                    MapsUtil.getMarkerForCircle(
                        currentCircle?.center!!, radius.toDouble()
                    )
                )
            }
        }


    }

    override fun onMarkerDrag(marker: Marker) {
        when (selectedShaped) {
            Shape.POLYGON -> {
                currentPolygon?.let {
                    // Edit completed polygon
                    latLngListTemp[currentMarkerIndex] = marker.position
                    // Update marker position in marker list
                    val updatedM = currentMarkers[currentMarkerIndex]
                    updatedM.position = marker.position
                    currentMarkers.set(currentMarkerIndex, updatedM)
                    // Final attempt of draw polygon and update in polygonlist
                    val shapeid = it.tag.toString()
                    it.remove()
                    drawPolygon(shapeid, latLngListTemp)

                }
                    ?: run {
                        // Edit uncompleted polygon (that is actually a polyline)
                        latLngListCurrent[currentMarkerIndex] = marker.position
                        val points = currentPolyline?.points
                        points?.set(currentMarkerIndex, marker.position)
                        if (points != null) {
                            currentPolyline?.points = points
                        }

                    }
            }

            Shape.CIRCLE -> {
                latLngListTemp[currentMarkerIndex] = marker.position
                updateCircle(marker)
            }
        }
    }

    private fun addMarkerOnMap(latLng: LatLng): Marker? {
        val markerOptions = MarkerOptions().position(latLng)
        markerOptions.icon(MapsUtil.getRoundMarker(context))
        markerOptions.anchor(.5f, .5f)
        val marker = mMap.addMarker(markerOptions)
        marker?.isDraggable = true
        // Id formatted as point index in list # shape id # shape type
        marker?.tag =
            latLngListCurrent.size.toString() + "_" + System.currentTimeMillis() + "_" + selectedShaped
        if (marker != null) {
            currentMarkers.add(marker)
        }
        return marker
    }

    private fun updatePolyline(endLatLng: LatLng) {
        val points = currentPolyline?.points
        points?.add(endLatLng)
        if (points != null) {
            currentPolyline?.points = points
        }

    }

    fun drawPolygon(
        polygonID: String,
        latLngList: List<LatLng>,
        latLng: List<LatLng>? = null
    ): Boolean {
        if (latLng != null)
            latLngListCurrent.addAll(latLng)
        if (latLngListCurrent.size >= 3) {
            val polygonOptions = PolygonOptions()
            polygonOptions.addAll(latLngList)
            polygonOptions.strokeColor(Color.RED)
            polygonOptions.strokeWidth(7f)
            polygonOptions.fillColor(Color.parseColor("#50E7330D"))
            currentPolygon = mMap.addPolygon(polygonOptions)
            currentPolygon?.tag = polygonID
            currentPolygon?.let {

                // Calculate polygon area size
                val areaInSqFt = SphericalUtil.computeArea(latLngList).squareMeterToFeet()

                // Get polygon center
                val builder = LatLngBounds.Builder()
                latLngList.forEach { builder.include(it) }
                val bounds = builder.build()
                mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50))


            }


            listener.completed()
            isCompleted = true

            return true
        }
        return false
    }

    private fun drawCircle(latLngCenter: LatLng) {
        // Center point
        addMarkerOnMap(latLngCenter)
        latLngListCurrent.add(latLngCenter)

        // Perimeter points
        val markerLocation = MapsUtil.getMarkerForCircle(latLngCenter, radius.toDouble())
        addMarkerOnMap(markerLocation)
        latLngListCurrent.add(markerLocation)

        val circleOptions = CircleOptions()
        circleOptions.center(latLngCenter)
        circleOptions.strokeColor(Color.RED)
        circleOptions.fillColor(Color.parseColor("#50E7330D"))
        circleOptions.strokeWidth(7f)
        circleOptions.radius(radius.toDouble())

        // Get a point between two points for number badge
        val builder = LatLngBounds.Builder()
        builder.include(latLngCenter)
        builder.include(markerLocation)
        val bounds = builder.build()

        // Add Num Badge
        val areaInSqFt = (Math.PI * sqrt(circleOptions.radius)).squareMeterToFeet()

        currentCircle = mMap.addCircle(circleOptions)
        currentCircle?.tag = System.currentTimeMillis()

        isCompleted = true
        listener.completed()
    }


    fun drawCircle(latLngCenter: LatLng, radius: Int) {
        // Center point
        addMarkerOnMap(latLngCenter)
        latLngListCurrent.add(latLngCenter)

        // Perimeter points
        val markerLocation = MapsUtil.getMarkerForCircle(latLngCenter, radius.toDouble())
        addMarkerOnMap(markerLocation)
        latLngListCurrent.add(markerLocation)

        val circleOptions = CircleOptions()
        circleOptions.center(latLngCenter)
        circleOptions.strokeColor(Color.RED)
        circleOptions.fillColor(Color.parseColor("#50E7330D"))
        circleOptions.strokeWidth(7f)
        circleOptions.radius(radius.toDouble())


        // Add Num Badge
        val areaInSqFt = (Math.PI * sqrt(circleOptions.radius)).squareMeterToFeet()

        currentCircle = mMap.addCircle(circleOptions)
        currentCircle?.tag = System.currentTimeMillis()

        // Get a point between two points for number badge
        val builder = LatLngBounds.Builder()
        builder.include(latLngCenter)
        builder.include(markerLocation)
        val bounds = builder.build()
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngCenter, getZoomLevel()))

        isCompleted = true
        listener.completed()
    }

    fun getZoomLevel(): Float {
        var zoomLevel = 11
        if (currentCircle != null) {
            val radius = currentCircle!!.radius + currentCircle!!.radius / 2
            val scale = radius / 500
            zoomLevel = (16 - Math.log(scale) / Math.log(2.0)).toInt()
        }
        return zoomLevel + .4f
    }


    private fun updateCircle(marker: Marker) {

        var areaInSqFt = 0.0
        if (currentMarkerIndex == 0) {
            // Drag Circle
            currentCircle?.center = marker.position

            currentCircle?.let {
                // Update Perimeter
                val perimeter = MapsUtil.getMarkerForCircle(marker.position, it.radius)
                // Update Perimeter point position
                currentMarkers.last().position = perimeter

                // Update num badge
                val builder = LatLngBounds.Builder()
                builder.include(marker.position)
                builder.include(perimeter)

            }
        } else {
            currentCircle?.let {
                // Resize Circle
                it.radius = SphericalUtil.computeDistanceBetween(it.center, marker.position)

                currentCircle?.let {
                    // Update Perimeter
                    val perimeter = MapsUtil.getMarkerForCircle(it.center, it.radius)
                    // Update Perimeter point position
                    marker.position = perimeter

                    // Update num badge
                    val builder = LatLngBounds.Builder()
                    builder.include(it.center)
                    builder.include(perimeter)


                    // Recalculate area
                    areaInSqFt = (Math.PI * sqrt(it.radius)).squareMeterToFeet()

                }
            }
        }

        listener.updated(areaInSqFt)


    }

    fun getCircle() = currentCircle

    fun getPolygon() = currentPolygon

    fun getShapeId() = id


}