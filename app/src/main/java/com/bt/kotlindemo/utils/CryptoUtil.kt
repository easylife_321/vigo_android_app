/*
 * *
 *  * Created by Surajit on 5/4/20 2:09 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 5/4/20 2:09 PM
 *  *
 */

package com.bt.kotlindemo.utils

import android.util.Base64
import com.bt.kotlindemo.BuildConfig
import timber.log.Timber
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

class CryptoUtil {


    fun decrypt(text: String): String? {
        try {
            val cipher =
                Cipher.getInstance("AES/CBC/PKCS5Padding") //this parameters should not be changed
            val keyBytes = ByteArray(16)
            val b = BuildConfig.KEY.toByteArray(charset("UTF-8"))
            var len = b.size
            if (len > keyBytes.size) len = keyBytes.size
            System.arraycopy(b, 0, keyBytes, 0, len)
            val keySpec =
                SecretKeySpec(keyBytes, "AES")
            val ivSpec =
                IvParameterSpec(keyBytes)
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec)
            val results: ByteArray?

            results = cipher.doFinal(Base64.decode(text, Base64.DEFAULT))

            return String(results!!, Charsets.UTF_8) // it returns the result as a String
        } catch (e: Exception) {
            Timber.i(e.toString())
        }
        return null
    }

    fun encrypt(text: String): String? {
        try {
            val cipher =
                Cipher.getInstance("AES/CBC/PKCS5Padding")
            val keyBytes = ByteArray(16)
            val b = BuildConfig.KEY.toByteArray(charset("UTF-8"))
            var len = b.size
            if (len > keyBytes.size) len = keyBytes.size
            System.arraycopy(b, 0, keyBytes, 0, len)
            val keySpec =
                SecretKeySpec(keyBytes, "AES")
            val ivSpec =
                IvParameterSpec(keyBytes)
            cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec)
            val results = cipher.doFinal(text.toByteArray(charset("UTF-8")))
            return Base64.encodeToString(results, Base64.DEFAULT)
        } catch (e: Exception) {

        }
        return null
    }


}