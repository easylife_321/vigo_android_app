package com.bt.kotlindemo.utils

import android.content.Context
import android.content.Intent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.bt.kotlindemo.R


class ActivityUtils{

    companion object {

        @JvmStatic
        fun addFragment(fragmentManager: FragmentManager, layout: Int, fragment: Fragment,tag:String) {
            fragmentManager.beginTransaction().add(layout, fragment).addToBackStack(tag).commit()
        }

        @JvmStatic
        fun replaceFragment(
            fragmentManager: FragmentManager,
            layout: Int,
            fragment: Fragment,
            tag: String
        ) {
            fragmentManager.beginTransaction().add(layout, fragment, tag).addToBackStack(tag)
                .commit()
        }

        @JvmStatic
        fun replaceFragmentWithAnim(
            fragmentManager: FragmentManager,
            layout: Int,
            fragment: Fragment,
            tag: String
        ) {
            fragmentManager.beginTransaction()
                  .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_up)
                .replace(layout, fragment, tag).addToBackStack(tag)
                .commit()
        }

        @JvmStatic
        fun replaceFragmentWithAnimFromBottom(
            fragmentManager: FragmentManager,
            layout: Int,
            fragment: Fragment,
            tag: String
        ) {
            fragmentManager.beginTransaction()
                .setCustomAnimations(R.anim.slide_in_up,R.anim.slide_in_down,R.anim.slide_in_up,R.anim.slide_in_down)
                .add(layout, fragment, tag).addToBackStack(tag)
                .commit()
        }

        @JvmStatic
        fun removeFragmentWithAnimToBottom(
            fragmentManager: FragmentManager?,
            fragment: Fragment?
        ) {
            fragmentManager?.apply {
                fragment?.apply {
                    beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_up,R.anim.slide_in_down,R.anim.slide_in_up,R.anim.slide_in_down)
                        .remove(this)
                        .commit()
                }

            }
        }

/*
        R.anim.fragment_close_enter,
        R.anim.fragment_close_exit,
        R.anim.fragment_open_enter,
        R.anim.fragment_close_exit*/

        @JvmStatic
        fun addFragmentOnActivity(
            fragmentManager: FragmentManager,
            fragment: Fragment, frameId: Int,
            enterAnim: Int, exitAnim: Int,
            popEnterAnim: Int, popExitAnim: Int
        ) {
            fragmentManager.beginTransaction()
                .setCustomAnimations(enterAnim, exitAnim, popEnterAnim, popExitAnim)
                .add(frameId, fragment).commit()
        }

        @JvmStatic
        fun share(context: Context, msg: String) {
            val sharingIntent = Intent(Intent.ACTION_SEND)
            sharingIntent.type = "text/plain"
            sharingIntent.putExtra(
                Intent.EXTRA_SUBJECT,
                context.resources.getString(R.string.app_name)
            )
            sharingIntent.putExtra(
                Intent.EXTRA_TEXT,
                msg
            )
            context.startActivity(Intent.createChooser(sharingIntent, "Share app via"))
        }

        @JvmStatic
        fun addIntentsToList(
            context: Context,
            list: MutableList<Intent>,
            intent: Intent
        ): MutableList<Intent> {
            val resInfo = context.packageManager.queryIntentActivities(intent, 0)
            for (resolveInfo in resInfo) {
                val packageName = resolveInfo.activityInfo.packageName
                val targetedIntent = Intent(intent)
                targetedIntent.setPackage(packageName)
                list.add(targetedIntent)
            }
            return list
        }
    }



}