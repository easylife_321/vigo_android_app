package com.bt.kotlindemo.utils


import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.bt.kotlindemo.R
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.databinding.AlertViewChangePasswordBinding
import com.bt.kotlindemo.databinding.AlertViewLayoutSelectDeviceBinding
import com.bt.kotlindemo.databinding.AlertViewOdometerBinding
import com.bt.kotlindemo.databinding.AlertViewPasswordBinding
import com.bt.kotlindemo.databinding.AlertViewRouteNameBinding
import com.bt.kotlindemo.databinding.AlertViewSpeedLimitBinding
import com.bt.kotlindemo.databinding.AlertViewStartTripBinding
import com.bt.kotlindemo.databinding.LayoutAlertViewCreateGroupBinding
import com.bt.kotlindemo.databinding.LayoutAlertViewOkBinding
import com.bt.kotlindemo.databinding.LayoutAlertViewOkCancelBinding
import com.bt.kotlindemo.databinding.LayoutAlertViewSelectGroupBinding
import com.bt.kotlindemo.databinding.LayoutAlertViewSelectIconBinding
import com.bt.kotlindemo.interfaces.AlertCallback
import com.bt.kotlindemo.interfaces.RecyclerClickListener
import com.bt.kotlindemo.model.DeviceIcon
import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.ui.device_options.device_management.add_device.SelectGroupAdapter
import com.bt.kotlindemo.ui.device_options.device_management.add_device.SelectIconAdapter
import com.bt.kotlindemo.ui.device_options.poi_management.add_edit_poi.SelectDevicesAdapter
import com.google.android.material.snackbar.Snackbar
import kotlin.math.roundToInt


object AlertHelper {

    fun showOkAlert(context: Context, msg: String, callBack: AlertCallback? = null) {
        val binding: LayoutAlertViewOkBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.layout_alert_view_ok, null, false
        )
        binding.msg = msg
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp

        binding.btn.setOnClickListener {
            callBack?.action(true)
            dialog.dismiss()


        }
    }

    fun showOkAlertFinish(context: Context, msg: String) {
        val binding: LayoutAlertViewOkBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.layout_alert_view_ok, null, false
        )
        binding.msg = msg
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp

        binding.btn.setOnClickListener {
            (context as Activity).finish()
            dialog.dismiss()


        }
    }


    fun showOKCancelAlert(context: Context, msg: String, callBack: AlertCallback? = null) {
        val binding: LayoutAlertViewOkCancelBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.layout_alert_view_ok_cancel, null, false
        )
        binding.msg = msg
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        binding.btnOk.setOnClickListener {
            callBack?.action(true)
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener {
            callBack?.action(false)
            dialog.dismiss()
        }
    }

    fun showOKCancelAlert(
        context: Context, msg: String,
        okTitle: String, cancelTitle: String, callBack: AlertCallback? = null
    ) {
        val binding: LayoutAlertViewOkCancelBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.layout_alert_view_ok_cancel, null, false
        )
        binding.msg = msg
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        binding.btnOk.text = okTitle
        binding.btnCancel.text = cancelTitle
        dialog.show()
        dialog.window?.attributes = lp
        binding.btnOk.setOnClickListener {
            callBack?.action(true)
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener {
            callBack?.action(false)
            dialog.dismiss()
        }
    }


    fun permissionDeniedMessage(context: Context, msg: String, callBack: AlertCallback? = null) {
        val binding: LayoutAlertViewOkCancelBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.layout_alert_view_ok_cancel, null, false
        )
        binding.msg = msg
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        binding.btnOk.text = context.resources.getString(R.string.action_settings)
        binding.btnOk.setOnClickListener {
            callBack?.action(true)
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener {
            callBack?.action(false)
            dialog.dismiss()
        }
    }


    fun showSpeedAlert(context: Context, speed: String, callBack: AlertCallback? = null) {
        val binding: AlertViewSpeedLimitBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.alert_view_speed_limit, null, false
        )
        binding.speed = speed
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        binding.btnOk.setOnClickListener {
            if (binding.etSpeed.text.toString().trim().isEmpty()) {
                binding.etSpeed.error = context.getString(R.string.enter_limit)
                return@setOnClickListener
            }
            callBack?.action(true, binding.etSpeed.text.toString().trim())
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener {
            callBack?.action(false)
            dialog.dismiss()
        }
    }

    fun showChangePasswordAlert(context: Context, callBack: AlertCallback? = null) {
        val binding: AlertViewChangePasswordBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.alert_view_change_password, null, false
        )
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        binding.btnOk.setOnClickListener {
            if (binding.etPassword.text.toString().trim().isEmpty()) {
                binding.etPassword.error = "Enter password"
                return@setOnClickListener
            }
            if (binding.etPassword.text.toString().trim().isEmpty()) {
                binding.etPassword.error = "Enter confirm password"
                return@setOnClickListener
            }
            if (binding.etPassword.text.toString()
                    .trim() != binding.etConfirmPassword.text.toString().trim()
            ) {
                binding.etConfirmPassword.error = "Password does not match"
                return@setOnClickListener
            }
            callBack?.action(true, binding.etPassword.text.toString().trim())
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener {
            callBack?.action(false)
            dialog.dismiss()
        }
    }


    fun showOdometerAlert(context: Context, odometer: String, callBack: AlertCallback? = null) {
        val binding: AlertViewOdometerBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.alert_view_odometer, null, false
        )
        binding.odometer = odometer
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        binding.btnOk.setOnClickListener {
            if (binding.etSpeed.text.toString().trim().isEmpty()) {
                binding.etSpeed.error = context.getString(R.string.enter_odometer_value)
                return@setOnClickListener
            }
            callBack?.action(true, binding.etSpeed.text.toString().trim())
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener {
            callBack?.action(false)
            dialog.dismiss()
        }
    }


    fun showPasswordAlert(
        context: Context,
        preferences: AppPreferences,
        callBack: AlertCallback
    ) {
        val binding: AlertViewPasswordBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.alert_view_password, null, false
        )
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        binding.btnOk.setOnClickListener {
            if (binding.etPassword.text.toString().trim().isEmpty()) {
                binding.etPassword.error = context.getString(R.string.enter_your_password)
                return@setOnClickListener
            }
            callBack.action(true, binding.etPassword.text.toString().trim())
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener {
            callBack.action(false)
            dialog.dismiss()
        }
    }


    fun showCreateGroupAlert(context: Context, group: Group? = null, callBack: AlertCallback) {
        val binding: LayoutAlertViewCreateGroupBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.layout_alert_view_create_group, null, false
        )
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        group?.let {
            binding.txtHeader.text = context.resources.getString(R.string.remane_group)
            binding.btn.text = context.resources.getString(R.string.rename)
            binding.etName.setText(it.name)
        }
        binding.btn.setOnClickListener {
            if (binding.etName.text.toString().trim().isEmpty()) {
                binding.nameFiled.error = "Enter group name"
                return@setOnClickListener
            }
            callBack.action(true, binding.etName.text.toString().trim())
            dialog.dismiss()

        }
        binding.btnCancel.setOnClickListener {
            callBack.action(false)
            dialog.dismiss()
        }
    }


    fun showGroupListAlert(context: Context, groups: ArrayList<Group>, callBack: AlertCallback) {
        val binding: LayoutAlertViewSelectGroupBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.layout_alert_view_select_group, null, false
        )
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        val adapter = SelectGroupAdapter(groups, object : RecyclerClickListener {
            override fun onClick(objects: Any) {
                callBack.action(true, objects as Group)
                dialog.dismiss()
            }
        })
        binding.adapter = adapter
    }


    fun showIconListAlert(
        context: Context,
        groups: ArrayList<DeviceIcon>,
        callBack: AlertCallback
    ) {
        val binding: LayoutAlertViewSelectIconBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.layout_alert_view_select_icon, null, false
        )
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        val adapter = SelectIconAdapter(groups, object : RecyclerClickListener {
            override fun onClick(objects: Any) {
                callBack.action(true, objects as DeviceIcon)
                dialog.dismiss()
            }
        })
        binding.adapter = adapter
    }


    fun showDeviceList(
        context: Context,
        devices: ArrayList<Device>,
        callBack: AlertCallback
    ) {
        val binding: AlertViewLayoutSelectDeviceBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.alert_view_layout_select_device, null, false
        )
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        val adapter = SelectDevicesAdapter(devices, object : RecyclerClickListener {
            override fun onClick(objects: Any) {
                callBack.action(true, objects as DeviceIcon)
                dialog.dismiss()
            }
        })
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                binding.adapter!!.filter.filter(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })
        binding.adapter = adapter
    }


    fun showTripStartAlert(context: Context, address: String, callBack: AlertCallback? = null) {
        val binding: AlertViewStartTripBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.alert_view_start_trip, null, false
        )
        binding.address = address
        binding.timeStamp = DateTimeUtil.formatSharableTime(System.currentTimeMillis())
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        binding.btnOk.setOnClickListener {
            if (!binding.rdBusiness.isChecked && !binding.rdPersonal.isChecked) {
                Snackbar.make(binding.root, "Select Trip Type", Snackbar.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (binding.etName.text.toString().trim().isEmpty()) {
                binding.etName.error = context.getString(R.string.enter_trip_name)
                return@setOnClickListener
            }
            val list = arrayListOf<Any>()
            list.add(binding.etName.text.toString().trim())
            list.add(binding.rdBusiness.isChecked)
            callBack?.action(true, list)
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener {
            callBack?.action(false)
            dialog.dismiss()
        }
    }


    fun showRouteAlert(context: Context, callBack: AlertCallback) {
        val binding: AlertViewRouteNameBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.alert_view_route_name, null, false
        )
        val dialog = Dialog(context)
        dialog.setContentView(binding.root)
        dialog.setCancelable(false)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = (context.resources.displayMetrics.widthPixels * 0.90).roundToInt()
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        dialog.show()
        dialog.window?.attributes = lp
        binding.btnOk.setOnClickListener {
            if (binding.etName.text.toString().trim().isEmpty()) {
                Snackbar.make(binding.root, "Enter route name", Snackbar.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            callBack.action(true, binding.etName.text.toString().trim())
            dialog.dismiss()
        }
        binding.btnCancel.setOnClickListener {
            callBack.action(false)
            dialog.dismiss()
        }
    }


}