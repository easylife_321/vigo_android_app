package com.bt.kotlindemo.utils.google_map


 interface ShapeListener {

    fun completed()

    fun updated(areaInSqFt: Double)

}