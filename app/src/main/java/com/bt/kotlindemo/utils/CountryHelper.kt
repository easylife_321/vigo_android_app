package com.bt.kotlindemo.utils

import com.bt.kotlindemo.R


object CountryHelper {


    fun getFlagDrawableResId(iso: String): Int {
        return when (iso) {
            "af" -> R.drawable.flag_afghanistan
            "al" -> R.drawable.flag_albania
            "dz" -> R.drawable.flag_algeria
            "ad" -> R.drawable.flag_andorra
            "ao" -> R.drawable.flag_angola
            "aq" -> R.drawable.flag_antarctica
            "ar" -> R.drawable.flag_argentina
            "am" -> R.drawable.flag_armenia
            "aw" -> R.drawable.flag_aruba
            "au" -> R.drawable.flag_australia
            "at" -> R.drawable.flag_austria
            "az" -> R.drawable.flag_azerbaijan
            "bh" -> R.drawable.flag_bahrain
            "bd" -> R.drawable.flag_bangladesh
            "by" -> R.drawable.flag_belarus
            "be" -> R.drawable.flag_belgium
            "bz" -> R.drawable.flag_belize
            "bj" -> R.drawable.flag_benin
            "bt" -> R.drawable.flag_bhutan
            "bo" -> R.drawable.flag_bolivia
            "ba" -> R.drawable.flag_bosnia
            "bw" -> R.drawable.flag_botswana
            "br" -> R.drawable.flag_brazil
            "bn" -> R.drawable.flag_brunei
            "bg" -> R.drawable.flag_bulgaria
            "bf" -> R.drawable.flag_burkina_faso
            "mm" -> R.drawable.flag_myanmar
            "bi" -> R.drawable.flag_burundi
            "kh" -> R.drawable.flag_cambodia
            "cm" -> R.drawable.flag_cameroon
            "ca" -> R.drawable.flag_canada
            "cv" -> R.drawable.flag_cape_verde
            "cf" -> R.drawable.flag_central_african_republic
            "td" -> R.drawable.flag_chad
            "cl" -> R.drawable.flag_chile
            "cn" -> R.drawable.flag_china
            "cx" -> R.drawable.flag_christmas_island
            "cc" -> R.drawable.flag_cocos // custom
            "co" -> R.drawable.flag_colombia
            "km" -> R.drawable.flag_comoros
            "cg" -> R.drawable.flag_republic_of_the_congo
            "cd" -> R.drawable.flag_democratic_republic_of_the_congo
            "ck" -> R.drawable.flag_cook_islands
            "cr" -> R.drawable.flag_costa_rica
            "hr" -> R.drawable.flag_croatia
            "cu" -> R.drawable.flag_cuba
            "cy" -> R.drawable.flag_cyprus
            "cz" -> R.drawable.flag_czech_republic
            "dk" -> R.drawable.flag_denmark
            "dj" -> R.drawable.flag_djibouti
            "tl" -> R.drawable.flag_timor_leste
            "ec" -> R.drawable.flag_ecuador
            "eg" -> R.drawable.flag_egypt
            "sv" -> R.drawable.flag_el_salvador
            "gq" -> R.drawable.flag_equatorial_guinea
            "er" -> R.drawable.flag_eritrea
            "ee" -> R.drawable.flag_estonia
            "et" -> R.drawable.flag_ethiopia
            "fk" -> R.drawable.flag_falkland_islands
            "fo" -> R.drawable.flag_faroe_islands
            "fj" -> R.drawable.flag_fiji
            "fi" -> R.drawable.flag_finland
            "fr" -> R.drawable.flag_france
            "pf" -> R.drawable.flag_french_polynesia
            "ga" -> R.drawable.flag_gabon
            "gm" -> R.drawable.flag_gambia
            "ge" -> R.drawable.flag_georgia
            "de" -> R.drawable.flag_germany
            "gh" -> R.drawable.flag_ghana
            "gi" -> R.drawable.flag_gibraltar
            "gr" -> R.drawable.flag_greece
            "gl" -> R.drawable.flag_greenland
            "gt" -> R.drawable.flag_guatemala
            "gn" -> R.drawable.flag_guinea
            "gw" -> R.drawable.flag_guinea_bissau
            "gy" -> R.drawable.flag_guyana
            "gf" -> R.drawable.flag_guyane
            "ht" -> R.drawable.flag_haiti
            "hn" -> R.drawable.flag_honduras
            "hk" -> R.drawable.flag_hong_kong
            "hu" -> R.drawable.flag_hungary
            "in" -> R.drawable.flag_india
            "id" -> R.drawable.flag_indonesia
            "ir" -> R.drawable.flag_iran
            "iq" -> R.drawable.flag_iraq
            "ie" -> R.drawable.flag_ireland
            "im" -> R.drawable.flag_isleof_man // custom
            "il" -> R.drawable.flag_israel
            "it" -> R.drawable.flag_italy
            "ci" -> R.drawable.flag_cote_divoire
            "jp" -> R.drawable.flag_japan
            "jo" -> R.drawable.flag_jordan
            "kz" -> R.drawable.flag_kazakhstan
            "ke" -> R.drawable.flag_kenya
            "ki" -> R.drawable.flag_kiribati
            "kw" -> R.drawable.flag_kuwait
            "kg" -> R.drawable.flag_kyrgyzstan
            "ky" -> R.drawable.flag_cayman_islands
            "la" -> R.drawable.flag_laos
            "lv" -> R.drawable.flag_latvia
            "lb" -> R.drawable.flag_lebanon
            "ls" -> R.drawable.flag_lesotho
            "lr" -> R.drawable.flag_liberia
            "ly" -> R.drawable.flag_libya
            "li" -> R.drawable.flag_liechtenstein
            "lt" -> R.drawable.flag_lithuania
            "lu" -> R.drawable.flag_luxembourg
            "mo" -> R.drawable.flag_macao
            "mk" -> R.drawable.flag_macedonia
            "mg" -> R.drawable.flag_madagascar
            "mw" -> R.drawable.flag_malawi
            "my" -> R.drawable.flag_malaysia
            "mv" -> R.drawable.flag_maldives
            "ml" -> R.drawable.flag_mali
            "mt" -> R.drawable.flag_malta
            "mh" -> R.drawable.flag_marshall_islands
            "mr" -> R.drawable.flag_mauritania
            "mu" -> R.drawable.flag_mauritius
            "yt" -> R.drawable.flag_martinique // no exact flag found
            "re" -> R.drawable.flag_martinique // no exact flag found
            "mq" -> R.drawable.flag_martinique
            "mx" -> R.drawable.flag_mexico
            "fm" -> R.drawable.flag_micronesia
            "md" -> R.drawable.flag_moldova
            "mc" -> R.drawable.flag_monaco
            "mn" -> R.drawable.flag_mongolia
            "me" -> R.drawable.flag_of_montenegro // custom
            "ma" -> R.drawable.flag_morocco
            "mz" -> R.drawable.flag_mozambique
            "na" -> R.drawable.flag_namibia
            "nr" -> R.drawable.flag_nauru
            "np" -> R.drawable.flag_nepal
            "nl" -> R.drawable.flag_netherlands
            "an" -> R.drawable.flag_netherlands_antilles
            "nc" -> R.drawable.flag_new_caledonia // custom
            "nz" -> R.drawable.flag_new_zealand
            "ni" -> R.drawable.flag_nicaragua
            "ne" -> R.drawable.flag_niger
            "ng" -> R.drawable.flag_nigeria
            "nu" -> R.drawable.flag_niue
            "kp" -> R.drawable.flag_north_korea
            "no" -> R.drawable.flag_norway
            "om" -> R.drawable.flag_oman
            "pk" -> R.drawable.flag_pakistan
            "pw" -> R.drawable.flag_palau
            "pa" -> R.drawable.flag_panama
            "pg" -> R.drawable.flag_papua_new_guinea
            "py" -> R.drawable.flag_paraguay
            "pe" -> R.drawable.flag_peru
            "ph" -> R.drawable.flag_philippines
            "pn" -> R.drawable.flag_pitcairn_islands
            "pl" -> R.drawable.flag_poland
            "pt" -> R.drawable.flag_portugal
            "pr" -> R.drawable.flag_puerto_rico
            "qa" -> R.drawable.flag_qatar
            "ro" -> R.drawable.flag_romania
            "ru" -> R.drawable.flag_russian_federation
            "rw" -> R.drawable.flag_rwanda
            "bl" -> R.drawable.flag_saint_barthelemy // custom
            "ws" -> R.drawable.flag_samoa
            "sm" -> R.drawable.flag_san_marino
            "st" -> R.drawable.flag_sao_tome_and_principe
            "sa" -> R.drawable.flag_saudi_arabia
            "sn" -> R.drawable.flag_senegal
            "rs" -> R.drawable.flag_serbia // custom
            "sc" -> R.drawable.flag_seychelles
            "sl" -> R.drawable.flag_sierra_leone
            "sg" -> R.drawable.flag_singapore
            "sx" ->  //TODO: Add Flag.
                0
            "sk" -> R.drawable.flag_slovakia
            "si" -> R.drawable.flag_slovenia
            "sb" -> R.drawable.flag_soloman_islands
            "so" -> R.drawable.flag_somalia
            "za" -> R.drawable.flag_south_africa
            "kr" -> R.drawable.flag_south_korea
            "es" -> R.drawable.flag_spain
            "lk" -> R.drawable.flag_sri_lanka
            "sh" -> R.drawable.flag_saint_helena // custom
            "pm" -> R.drawable.flag_saint_pierre
            "sd" -> R.drawable.flag_sudan
            "sr" -> R.drawable.flag_suriname
            "sz" -> R.drawable.flag_swaziland
            "se" -> R.drawable.flag_sweden
            "ch" -> R.drawable.flag_switzerland
            "sy" -> R.drawable.flag_syria
            "tw" -> R.drawable.flag_taiwan
            "tj" -> R.drawable.flag_tajikistan
            "tz" -> R.drawable.flag_tanzania
            "th" -> R.drawable.flag_thailand
            "tg" -> R.drawable.flag_togo
            "tk" -> R.drawable.flag_tokelau // custom
            "to" -> R.drawable.flag_tonga
            "tn" -> R.drawable.flag_tunisia
            "tr" -> R.drawable.flag_turkey
            "tm" -> R.drawable.flag_turkmenistan
            "tv" -> R.drawable.flag_tuvalu
            "ae" -> R.drawable.flag_uae
            "ug" -> R.drawable.flag_uganda
            "gb" -> R.drawable.flag_united_kingdom
            "ua" -> R.drawable.flag_ukraine
            "uy" -> R.drawable.flag_uruguay
            "us" -> R.drawable.flag_united_states_of_america
            "uz" -> R.drawable.flag_uzbekistan
            "vu" -> R.drawable.flag_vanuatu
            "va" -> R.drawable.flag_vatican_city
            "ve" -> R.drawable.flag_venezuela
            "vn" -> R.drawable.flag_vietnam
            "wf" -> R.drawable.flag_wallis_and_futuna
            "ye" -> R.drawable.flag_yemen
            "zm" -> R.drawable.flag_zambia
            "zw" -> R.drawable.flag_zimbabwe
            "ai" -> R.drawable.flag_anguilla
            "ag" -> R.drawable.flag_antigua_and_barbuda
            "bs" -> R.drawable.flag_bahamas
            "bb" -> R.drawable.flag_barbados
            "bm" -> R.drawable.flag_bermuda
            "vg" -> R.drawable.flag_british_virgin_islands
            "dm" -> R.drawable.flag_dominica
            "do" -> R.drawable.flag_dominican_republic
            "gd" -> R.drawable.flag_grenada
            "jm" -> R.drawable.flag_jamaica
            "ms" -> R.drawable.flag_montserrat
            "kn" -> R.drawable.flag_saint_kitts_and_nevis
            "lc" -> R.drawable.flag_saint_lucia
            "vc" -> R.drawable.flag_saint_vicent_and_the_grenadines
            "tt" -> R.drawable.flag_trinidad_and_tobago
            "tc" -> R.drawable.flag_turks_and_caicos_islands
            "vi" -> R.drawable.flag_us_virgin_islands
            "ss" -> R.drawable.flag_south_sudan
            "xk" -> R.drawable.flag_kosovo
            "is" -> R.drawable.flag_iceland
            "ax" -> R.drawable.flag_aland_islands
            "as" -> R.drawable.flag_american_samoa
            "io" -> R.drawable.flag_british_indian_ocean_territory
            "gp" -> R.drawable.flag_guadeloupe
            "gu" -> R.drawable.flag_guam
            "gg" -> R.drawable.flag_guernsey
            "je" -> R.drawable.flag_jersey
            "nf" -> R.drawable.flag_norfolk_island
            "mp" -> R.drawable.flag_northern_mariana_islands
            "ps" -> R.drawable.flag_palestian_territory
            "mf" -> R.drawable.flag_saint_martin
            "gs" -> R.drawable.flag_south_georgia
            else -> R.drawable.flag_transparent
        }
    }


}