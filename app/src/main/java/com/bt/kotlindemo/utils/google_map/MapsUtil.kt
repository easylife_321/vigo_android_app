/*
 * *
 *  * Created by Surajit on 8/6/20 10:27 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/5/20 3:42 PM
 *  *
 */

package com.bt.kotlindemo.utils.google_map

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.location.Location
import android.os.Handler
import android.os.SystemClock
import android.view.animation.LinearInterpolator
import com.bt.kotlindemo.R
import com.bt.kotlindemo.utils.getColorFromAttr
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import java.util.*
import kotlin.math.*


const val MARKER_MOVE_TIME = 20000L

object MapsUtil {

    fun moveMarkerSmoothly(
        marker: Marker,
        newLatLng: LatLng,
        rotateMarker: Boolean,
        animator: ValueAnimator
    ): ValueAnimator {


        val deltaLatitude = doubleArrayOf(newLatLng.latitude - marker.position.latitude)
        val deltaLongitude = newLatLng.longitude - marker.position.longitude
        val prevStep = floatArrayOf(0f)
        animator.duration = MARKER_MOVE_TIME

        animator.addUpdateListener { animation ->
            val deltaStep = (animation.animatedValue as Float - prevStep[0]).toDouble()
            prevStep[0] = animation.animatedValue as Float
            val latLng = LatLng(
                marker.position.latitude + deltaLatitude[0] * deltaStep * 1.0 / 100,
                marker.position.longitude + deltaStep * deltaLongitude * 1.0 / 100
            )
            marker.position = latLng
            if (rotateMarker) {
                val f = getAngle(
                    LatLng(
                        marker.position.latitude,
                        marker.position.longitude
                    ), newLatLng
                )
                rotateMarker(
                    marker,
                    f.toFloat()
                )
            }

        }

        return animator
    }


    fun rotateMarker(marker: Marker, toRotation: Float) {
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val startRotation = marker.rotation
        val duration: Long = 300

        handler.post(object : Runnable {
            override fun run() {
                val elapsed = SystemClock.uptimeMillis() - start
                val t = LinearInterpolator().getInterpolation(elapsed.toFloat() / duration)

                val rot = t * toRotation + (1 - t) * startRotation

                marker.rotation = if (-rot > 180) rot / 2 else rot
                if (t < 1.0) {
                    handler.postDelayed(this, 16)
                }
            }
        })

    }


    fun getAngle(fromSmartLatLon: LatLng, toSmartLatLon: LatLng): Double {
        var heading = 0.0
        if (fromSmartLatLon !== toSmartLatLon) {
            heading = computeHeading(
                fromSmartLatLon,
                toSmartLatLon
            )
        }
        return heading
    }


    fun computeHeading(from: LatLng, to: LatLng): Double {
        val fromLat = Math.toRadians(from.latitude)
        val fromLng = Math.toRadians(from.longitude)
        val toLat = Math.toRadians(to.latitude)
        val toLng = Math.toRadians(to.longitude)
        val dLng = toLng - fromLng
        val heading = Math.atan2(
            Math.sin(dLng) * Math.cos(toLat),
            Math.cos(fromLat) * Math.sin(toLat) - Math.sin(fromLat) * Math.cos(
                toLat
            ) * Math.cos(dLng)
        )
        return wrap(
            Math.toDegrees(
                heading
            )
        )
    }

    private fun wrap(n: Double): Double {
        val min = -180.0
        val max = 180.0
        return if (n >= min && n < max) n else mod(
            n - -180.0,
            max - min
        ) + min
    }

    private fun mod(x: Double, m: Double): Double {
        return (x % m + m) % m
    }

    fun calculateDistance(startLatLng: LatLng, endLatLng: LatLng): Double {
        val startPoint = Location("locationA")
        startPoint.latitude = startLatLng.latitude
        startPoint.longitude = startLatLng.longitude
        val endPoint = Location("locationA")
        endPoint.latitude = endLatLng.latitude
        endPoint.longitude = endLatLng.longitude
        return startPoint.distanceTo(endPoint).toDouble()
    }

    fun isValidLatLang(latitude: Double?, longitude: Double?): Boolean {
        return latitude?.toInt() in -90 until 90 && longitude?.toInt() in -180 until 180
    }


    fun getRoundMarker(context: Context): BitmapDescriptor {
        val width = context.resources.getDimension(com.intuit.sdp.R.dimen._15sdp).toInt()

        val paint = Paint()
        paint.color = context.getColorFromAttr(R.attr.colorRedNoChange)
        paint.style = Paint.Style.FILL
        val image = Bitmap.createBitmap(width, width, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(image)
        canvas.translate(0f, width.toFloat())
        canvas.drawCircle(
            (width / 2).toFloat(),
            (-width / 2).toFloat(),
            (width / 2).toFloat(),
            paint
        )
        return BitmapDescriptorFactory.fromBitmap(image)
    }

    fun getMarkerForCircle(startLoc: LatLng, distance: Double): LatLng {
        val bearing = 1.6 // create a right side point
        val newLocation = Location("temp")

        val radius = 6371000.0 // earth's mean radius in m
        val lat1 = Math.toRadians(startLoc.latitude)
        val lng1 = Math.toRadians(startLoc.longitude)
        val lat2 = asin(
            sin(lat1) * cos(distance / radius) + Math.cos(lat1)
                    * sin(distance / radius) * Math.cos(bearing)
        )
        var lng2 = lng1 + atan2(
            Math.sin(bearing) * sin(distance / radius) * Math.cos(lat1),
            Math.cos(distance / radius) - sin(lat1) * sin(lat2)
        )
        lng2 = (lng2 + Math.PI) % (2 * Math.PI) - Math.PI

        // normalize to -180...+180
        if (lat2 == 0.0 || lng2 == 0.0) {
            newLocation.latitude = 0.0
            newLocation.longitude = 0.0
        } else {
            newLocation.latitude = Math.toDegrees(lat2)
            newLocation.longitude = Math.toDegrees(lng2)
        }
        return LatLng(newLocation.latitude, newLocation.longitude)
    }

    fun generateSmallIcon(context: Context): Bitmap {
        val dimes = context.resources.getDimension(com.intuit.sdp.R.dimen._50sdp).toInt()
        //val width = 100
        val bitmap = BitmapFactory.decodeResource(context.resources, R.mipmap.user_marker)
        return Bitmap.createScaledBitmap(bitmap, dimes, dimes, false)
    }

    fun showCurvedPolyline(
        p1: LatLng,
        p2: LatLng,
        k: Double,
        mMap: GoogleMap
    ): Polyline {
        /* //Calculate distance and heading between two points
         val d = SphericalUtil.computeDistanceBetween(p1, p2)
         val h = SphericalUtil.computeHeading(p1, p2)

         //Midpoint position
         val p = SphericalUtil.computeOffset(p1, d * 0.5, h)

         //Apply some mathematics to calculate position of the circle center
         val x = (1 - k * k) * d * 0.5 / (2 * k);
         val r = (1 + k * k) * d * 0.5 / (2 * k);

         val c = SphericalUtil.computeOffset(p, x, h + 90.0)

         //Polyline options
         val options = PolylineOptions()
         val pattern = Arrays.asList(Dash(30f), Gap(20f))

         //Calculate heading between circle center and two points
         val h1 = SphericalUtil.computeHeading(c, p1)
         val h2 = SphericalUtil.computeHeading(c, p2)

         //Calculate positions of points on circle border and add them to polyline options
         val numpoints = 100;
         val step = (h2 - h1) / numpoints;

         for (i in 0 until numpoints) {
             val pi = SphericalUtil.computeOffset(c, r, h1 + i * step);
             options.add(pi);
         }*/

        var cLat: Double = (p1.latitude + p2.latitude) / 2
        var cLon: Double = (p1.longitude + p2.longitude) / 2

        //add skew and arcHeight to move the midPoint

        //add skew and arcHeight to move the midPoint
        if (abs(p1.longitude - p1.longitude) < 0.0011) {
            cLon -= 0.0195
        } else {
            cLat += 0.0195
        }



        val options = PolylineOptions()
        val pattern = listOf(Dash(30f), Gap(20f))
        //Draw polyline
        return mMap.addPolyline(
            options.width(10f).color(Color.RED).add(p1).add(p2).geodesic(true).pattern(pattern)
        )

    }
    fun getBearing(begin: LatLng, end: LatLng): Float {
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)
        if (begin.latitude < end.latitude && begin.longitude < end.longitude) {
            return Math.toDegrees(Math.atan(lng / lat)).toFloat()
        } else if (begin.latitude >= end.latitude && begin.longitude < end.longitude) {
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
        } else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude) {
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
        } else if (begin.latitude < end.latitude && begin.longitude >= end.longitude) {
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()
        }
        return (-1).toFloat()
    }
    const val STRAIGHT_ANGLE = 180
    const val FULL_ROTATION = 360
    fun calcMinAngle(
        markerCurrentRotation: Float,
        _markerNextRotation: Float
    ): Float {
        var markerNextRotation = _markerNextRotation
        val angleDifference =
            Math.abs(markerNextRotation - markerCurrentRotation)
        if (angleDifference > STRAIGHT_ANGLE) {
            markerNextRotation = if (markerCurrentRotation < 0) {
                -FULL_ROTATION + angleDifference + markerCurrentRotation
            } else {
                FULL_ROTATION - angleDifference + markerCurrentRotation
            }
        }
        return if (markerNextRotation > FULL_ROTATION) markerNextRotation - FULL_ROTATION else markerNextRotation
    }


}