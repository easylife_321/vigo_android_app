/*
 * *
 *  * Created by Surajit on 27/08/24, 4:22 pm
 *  * Copyright (c) 2024 . All rights reserved.
 *  * Last modified 27/08/24, 4:22 pm
 *  *
 */

package com.bt.kotlindemo.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Parcelable
import android.os.ext.SdkExtensions.getExtensionVersion
import android.provider.MediaStore
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.AppConstants.Companion.EMPTY
import com.bt.kotlindemo.constants.AppConstants.Companion.MIME_TYPE_IMAGE
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import java.io.File

class CameraHelper(val context: Context) {
    private var imgPath: String = EMPTY
    private var imageUri: Uri? = null
    private var queryImageUrl: String = EMPTY
    private val ANDROID_R_REQUIRED_EXTENSION_VERSION = 2

    fun getPickImageIntent(isCamera: Boolean): Intent? {
        var chooserIntent: Intent? = null

        var intentList: MutableList<Intent> = ArrayList()

        val pickIntent = if (isPhotoPickerAvailable()) {
            Intent(MediaStore.ACTION_PICK_IMAGES)
        } else {
            Intent(Intent.ACTION_OPEN_DOCUMENT).apply { type = MIME_TYPE_IMAGE }
        }

        val takePhotoIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val uri = setImageUri(context)
        imageUri = uri.second
        imgPath = uri.first
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri)

        intentList = ActivityUtils.addIntentsToList(context, intentList, takePhotoIntent)
        if (!isCamera) {
            intentList = ActivityUtils.addIntentsToList(context, intentList, pickIntent)
        }


        if (intentList.size > 0) {
            chooserIntent = Intent.createChooser(
                intentList.removeAt(intentList.size - 1), context.getString(R.string.app_name)
            )
            chooserIntent?.putExtra(
                Intent.EXTRA_INITIAL_INTENTS, intentList.toTypedArray<Parcelable>()
            )
        }

        return chooserIntent
    }

     suspend fun handleImageRequest(
         data: Uri?, lifecycleScope: LifecycleOwner,
         exceptionHandler: CoroutineExceptionHandler,
         context: Context
     ): File? {
         val url = lifecycleScope.lifecycleScope.async(Dispatchers.IO + exceptionHandler) {
             if (data != null) {     //Photo from gallery
                 imageUri = data
                 queryImageUrl = imageUri?.path!!
                 queryImageUrl = context.compressImageFile(queryImageUrl, false, imageUri!!)
                 File(queryImageUrl)
             } else {
                 queryImageUrl = imgPath ?: ""
                 context.compressImageFile(queryImageUrl, uri = imageUri!!)
                 File(queryImageUrl)
             }
         }
         return url.await()

     }

    private fun isPhotoPickerAvailable(): Boolean {
        return when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU -> true
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.R -> {
                getExtensionVersion(Build.VERSION_CODES.R) >= ANDROID_R_REQUIRED_EXTENSION_VERSION
            }

            else -> false
        }
    }

}