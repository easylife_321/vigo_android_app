package com.bt.kotlindemo.utils

import android.content.Context
import io.github.rupinderjeet.kprogresshud.KProgressHUD


object ProgressHelper {


    fun showProgress(context: Context): KProgressHUD = KProgressHUD.create(context)
        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
        .setLabel("Please wait")
        .setCancellable(true)
        .setAnimationSpeed(2)
        .setDimAmount(0.5f)
        .show()

    fun dismissProgress(progress: KProgressHUD?) {
        if (progress != null && progress.isShowing)
            progress.dismiss()
    }


}