/*
 * *
 *  * Created by Surajit on 7/7/20 6:16 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 7/7/20 6:16 PM
 *  *
 */
package com.bt.kotlindemo.utils

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import com.bt.kotlindemo.constants.IntentRequestConstants
import java.lang.ref.WeakReference
import java.util.*

class PermissionHelper {
    private var TYPE = 0
    private val ACTIVITY = 1
    private val FRAGMENT = 2

    interface PermissionsListener {
        fun onPermissionGranted(requestCode: Int)
        fun onPermissionRejectedManyTimes(
            rejectedPerms: List<String>,
            request_code: Int
        )

        fun onPermissionDenied(
            request_code: Int
        )
        fun showRationale()
    }

    private var _activity: WeakReference<Activity?>? = null
    private var fragment: WeakReference<androidx.fragment.app.Fragment?>? = null
    private var listener: PermissionsListener? = null
    private val deniedpermissions: MutableList<String> =
        ArrayList()
    private val granted: MutableList<String> =
        ArrayList()

    constructor(view: Activity?) {
        _activity = WeakReference(view)
        TYPE = ACTIVITY
    }

    constructor(view: androidx.fragment.app.Fragment?) {
        fragment = WeakReference(view)
        TYPE = FRAGMENT
    }


    /**
     * Request permissions.
     *
     * @param permissions  -String Array of permissions to request, for eg: new String[]{PermissionManager.CAMERA} or multiple new String[]{PermissionManger.CAMERE,PermissionManager.CONTACTS}
     * @param request_code - Request code to check on callback.
     */
    fun checkPermission(
        permissions: Array<String>,
        requestCode: Int
    ) {
        deniedpermissions.clear()
        if (!isViewAttached) {
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var allPermissionGranted = true
            for (permission in permissions) {
                try {
                    if (activity!!.checkSelfPermission(permission) == PackageManager.PERMISSION_DENIED) {
                        allPermissionGranted = false
                        deniedpermissions.add(permission)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    return
                }
            }
            if (!allPermissionGranted) {
                listener!!.onPermissionDenied(requestCode)

            } else {
                listener!!.onPermissionGranted(requestCode)
            }
        } else {
            listener!!.onPermissionGranted(requestCode)
        }
    }


    /**
     * Request permissions.
     *
     * @param permissions  -String Array of permissions to request, for eg: new String[]{PermissionManager.CAMERA} or multiple new String[]{PermissionManger.CAMERE,PermissionManager.CONTACTS}
     * @param request_code - Request code to check on callback.
     */
    fun requestPermission(
        permissions: Array<String>,
        request_code: Int
    ) {
        deniedpermissions.clear()
        if (!isViewAttached) {
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var allPermissionGranted = true
            for (permission in permissions) {
                try {
                    if (activity!!.checkSelfPermission(permission) == PackageManager.PERMISSION_DENIED) {
                        allPermissionGranted = false
                        deniedpermissions.add(permission)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    return
                }
            }
            if (!allPermissionGranted) {
                when (TYPE) {
                    ACTIVITY -> activityView!!.requestPermissions(
                        deniedpermissions.toTypedArray(),
                        request_code
                    )
                    FRAGMENT -> fragmentView!!.requestPermissions(
                        deniedpermissions.toTypedArray(),
                        request_code
                    )

                }
            } else {
                listener!!.onPermissionGranted(request_code)
            }
        } else {
            listener!!.onPermissionGranted(request_code)
        }
    }


    /***
     * After the permissions are requested, pass the results from Activity/fragments onRequestPermissionsResult to this function for processing
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @RequiresApi(Build.VERSION_CODES.M)
    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (isViewAttached) {
            val permission_name = StringBuilder()
            var never_ask_again = false
            granted.clear()
            for (permission in deniedpermissions) {
                if (activity!!.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
                    granted.add(permission)
                } else {
                    if (!activity!!.shouldShowRequestPermissionRationale(permission)) {
                        never_ask_again = true
                    }
                    permission_name.append(",")
                    permission_name.append(
                        getNameFromPermission(
                            permission
                        )
                    )
                }
            }
            var res = permission_name.toString()
            deniedpermissions.removeAll(granted)
            if (deniedpermissions.size > 0) {
                res = res.substring(1)
                if (!never_ask_again) {
                    getRequestAgainAlertDialog(activity, res, requestCode)
                } else {
                    goToSettingsAlertDialog(activity, res, requestCode)
                }
            } else {
                listener!!.onPermissionGranted(requestCode)
            }
        }
    }

    private fun goToSettingsAlertDialog(
        view: Activity?,
        permission_name: String,
        request_code: Int
    ): AlertDialog {
        return AlertDialog.Builder(view!!).setTitle("Permission Required")
            .setMessage("We need you allow $permission_name permissions to use this features")
            .setPositiveButton(
                "GO TO SETTINGS"
            ) { _: DialogInterface?, _: Int ->
                val intent = Intent()
                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                intent.addCategory(Intent.CATEGORY_DEFAULT)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.flags = Intent.FLAG_ACTIVITY_NO_HISTORY
                intent.data = Uri.parse("package:" + view.packageName)
                view.startActivityForResult(intent,IntentRequestConstants.CONTACT_PERMISSION)
            }
            .setNegativeButton(
                "NO"
            ) { dialogInterface: DialogInterface?, i: Int ->
                listener!!.onPermissionRejectedManyTimes(
                    deniedpermissions,
                    request_code
                )
            }.show()
    }

    private fun getRequestAgainAlertDialog(
        view: Activity?,
        permission_name: String,
        request_code: Int
    ): AlertDialog {
        return AlertDialog.Builder(view!!).setTitle("Permission Required")
            .setMessage("We need $permission_name permissions to use this feature of the app")
            .setPositiveButton(
                "OK"
            ) { dialogInterface: DialogInterface?, i: Int ->
                requestPermission(
                    deniedpermissions.toTypedArray(),
                    request_code
                )
            }
            .setNegativeButton(
                "NO"
            ) { dialogInterface: DialogInterface?, i: Int ->
                listener!!.onPermissionRejectedManyTimes(
                    deniedpermissions,
                    request_code
                )
            }.show()
    }

    private val isViewAttached: Boolean
        get() {
            when (TYPE) {
                ACTIVITY -> return activityView != null
                FRAGMENT -> return fragmentView != null
            }
            return false
        }

    private val activityView: Activity?
        get() = _activity!!.get()


    private val fragmentView: androidx.fragment.app.Fragment?
        get() = fragment!!.get()


    private val activity: Activity?
        get() {
            when (TYPE) {
                ACTIVITY -> return activityView
                FRAGMENT -> return fragmentView!!.activity

            }
            return null
        }

    fun setListener(pListener: PermissionsListener?): PermissionHelper {
        listener = pListener
        return this
    }

    fun onDestroy() {
        listener = null
        _activity = null
        fragment = null
    }

    companion object {
        private const val TAG = "Permission Helper"
        private var labelsMap: MutableMap<String, String>? = null
        fun getNameFromPermission(permission: String): String {
            if (labelsMap == null) {
                labelsMap = HashMap()
                labelsMap!![Manifest.permission.READ_EXTERNAL_STORAGE] = "Read Storage"
                labelsMap!![Manifest.permission.WRITE_EXTERNAL_STORAGE] = "Write Storage"
                labelsMap!![Manifest.permission.CAMERA] = "Camera"
                labelsMap!![Manifest.permission.CALL_PHONE] = "Call"
                labelsMap!![Manifest.permission.READ_SMS] = "SMS"
                labelsMap!![Manifest.permission.RECEIVE_SMS] = "Receive SMS"
                labelsMap!![Manifest.permission.ACCESS_FINE_LOCATION] = "Exact Location"
                labelsMap!![Manifest.permission.ACCESS_COARSE_LOCATION] = "Close Location"
                labelsMap!![Manifest.permission.READ_CONTACTS] = "Contacts"
            }
            val value = labelsMap!![permission]
            return if (value == null) {
                val split =
                    permission.split("\\.".toRegex()).toTypedArray()
                split[split.size - 1]
            } else {
                value
            }
        }
    }
}