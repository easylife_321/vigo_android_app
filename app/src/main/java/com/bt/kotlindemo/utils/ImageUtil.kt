/*
 * *
 *  * Created by Surajit on 27/4/20 3:37 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/4/20 3:37 PM
 *  *
 */

package com.bt.kotlindemo.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory

object ImageUtil {
    fun bitmapFromVector(context: Context, @DrawableRes icon: Int): BitmapDescriptor {

        val background = ContextCompat.getDrawable(context, icon)
        background!!.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)

        val bitmap = Bitmap.createBitmap(
            background.intrinsicWidth,
            background.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)

        background.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun bitmapFromVectorImage(context: Context, @DrawableRes icon: Int): Bitmap {

        val background = ContextCompat.getDrawable(context, icon)
        background!!.setBounds(0, 0, background.intrinsicWidth, background.intrinsicHeight)

        var bitmap = Bitmap.createBitmap(
            background.intrinsicWidth,
            background.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)

        background.draw(canvas)
        return bitmap

    }


}