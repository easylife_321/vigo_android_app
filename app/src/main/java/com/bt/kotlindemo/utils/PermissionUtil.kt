/*
 * *
 *  * Created by Surajit on 27/08/24, 4:17 pm
 *  * Copyright (c) 2024 . All rights reserved.
 *  * Last modified 27/08/24, 4:17 pm
 *  *
 */

package com.bt.kotlindemo.utils

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.bt.kotlindemo.interfaces.IPermissionGrantedDeniedCallback
import com.permissionx.guolindev.PermissionX

object PermissionUtil {


    fun askPermissionFor(
        permission: String, callback: IPermissionGrantedDeniedCallback,
        fragment: Fragment, context: Context?
    ) {
        context?.let {
            if (ActivityCompat.checkSelfPermission(
                    it,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                PermissionX.init(fragment)
                    .permissions(
                        permission,
                    )
                    .onForwardToSettings { scope, deniedList ->
                        callback.afterCompletePermissionDenied()
                    }
                    .request { allGranted, _, deniedList ->
                        if (allGranted) {
                            //Read contacts
                            callback.onPermissionGrantedOrDenied(permission, isGranted = true)

                        } else {
                            // handle denied permission list
                            callback.onPermissionGrantedOrDenied(permission, isGranted = false)

                        }
                    }

            } else {
                callback.onPermissionGrantedOrDenied(permission, isGranted = true)
            }
        }
    }

    fun askPermissionFor(
        permission: String, callback: IPermissionGrantedDeniedCallback,
        fragment: FragmentActivity, context: Context?
    ) {
        context?.let {
            if (ActivityCompat.checkSelfPermission(
                    it,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                PermissionX.init(fragment)
                    .permissions(
                        permission,
                    )
                    .onForwardToSettings { scope, deniedList ->
                        callback.afterCompletePermissionDenied()
                    }
                    .request { allGranted, _, deniedList ->
                        if (allGranted) {
                            //Read contacts
                            callback.onPermissionGrantedOrDenied(permission, isGranted = true)

                        } else {
                            // handle denied permission list
                            callback.onPermissionGrantedOrDenied(permission, isGranted = false)

                        }
                    }

            } else {
                callback.onPermissionGrantedOrDenied(permission, isGranted = true)
            }
        }
    }

    fun askMultiplePermissions(
        act: FragmentActivity,
        permissions: List<String>,
        callback: IPermissionGrantedDeniedCallback,
    ) {
        val builder =
            PermissionX.init(act)
                .permissions(
                    permissions,
                )
        builder.onForwardToSettings { scope, deniedList ->
            scope.showForwardToSettingsDialog(
                deniedList,
                "Permissions to camera and external storage is required to access media",
                "Allow",
                "Do it later"
            )
        }
        builder.request { allGranted, grantedList, deniedList ->
            if (allGranted) {
                callback.onPermissionGrantedOrDenied(grantedList.first(), isGranted = true)
            } else {
                if (grantedList.isNotEmpty())
                    callback.onPermissionGrantedOrDenied(deniedList.first(), isGranted = false)
                else
                    callback.afterCompletePermissionDenied()
            }
        }
    }


}