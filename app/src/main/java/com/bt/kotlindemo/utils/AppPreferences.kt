package com.bt.kotlindemo.utils

import android.content.Context
import android.content.SharedPreferences
import com.bt.kotlindemo.constants.Preferences
import com.bt.kotlindemo.model.CountryList
import com.bt.kotlindemo.model.app_menu.Menu
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class AppPreferences(var context: Context) {

    var sharedPreferences: SharedPreferences =
        context.getSharedPreferences(Preferences.PREFERENCE_NAME, Context.MODE_PRIVATE);


    fun isRegistered(): Boolean {
        return sharedPreferences.getBoolean(Preferences.IS_REGISTERED, false)
    }

    fun setRegistered(isRegistered: Boolean) {
        sharedPreferences.edit().putBoolean(Preferences.IS_REGISTERED, isRegistered).apply()
    }


    fun getBaseUrl(): String? {
        return sharedPreferences.getString(Preferences.BASE_URL, "http://www.default.com/")
    }

    fun setBaseUrl(url: String) {
        sharedPreferences.edit().putString(Preferences.BASE_URL, url).apply()
    }


    fun getBaseImageUrl(): String? {
        return sharedPreferences.getString(Preferences.BASE_URL_IMG, "")
    }

    fun setBaseImageUrl(url: String?) {
        sharedPreferences.edit().putString(Preferences.BASE_URL_IMG, url).apply()
    }


    fun getUserName(): String? {
        return sharedPreferences.getString(Preferences.USER_NAME, "")
    }

    fun setUserName(url: String) {
        sharedPreferences.edit().putString(Preferences.USER_NAME, url).apply()
    }

    fun getUserId(): Int {
        return sharedPreferences.getInt(Preferences.USER_ID, 0)
    }

    fun setUserId(url: Int) {
        sharedPreferences.edit().putInt(Preferences.USER_ID, url).apply()
    }

    fun getPhoneLength(): Int {
        return sharedPreferences.getInt(Preferences.PHONE_LENGTH, 0)
    }

    fun setPhoneLength(url: Int) {
        sharedPreferences.edit().putInt(Preferences.PHONE_LENGTH, url).apply()
    }

    fun getPassword(): String? {
        return sharedPreferences.getString(Preferences.PASSWORD, "")
    }

    fun setPassword(url: String) {
        sharedPreferences.edit().putString(Preferences.PASSWORD, url).apply()
    }

    fun getFirebaseToken(): String? {
        return sharedPreferences.getString(Preferences.FIRE_BASE_TOKEN, "")
    }

    fun setFireBaseToken(url: String) {
        sharedPreferences.edit().putString(Preferences.FIRE_BASE_TOKEN, url).apply()
    }

    fun getEmail(): String? {
        return sharedPreferences.getString(Preferences.EMAIL_ID, "")
    }

    fun setEmail(url: String) {
        sharedPreferences.edit().putString(Preferences.EMAIL_ID, url).apply()
    }

    fun getUpdateAvl(): Boolean {
        return sharedPreferences.getBoolean(Preferences.UPDATE_AVL, false)
    }

    fun setUpdateAvl(avl: Boolean) {
        sharedPreferences.edit().putBoolean(Preferences.UPDATE_AVL, avl).apply()
    }


    fun getMobile(): String? {
        return sharedPreferences.getString(Preferences.MOBILE, "")
    }

    fun setMobile(url: String) {
        sharedPreferences.edit().putString(Preferences.MOBILE, url).apply()
    }


    fun getProfileImage(): String? {
        return sharedPreferences.getString(Preferences.PROFILE_IMAGE, "")
    }

    fun setProfileImage(url: String) {
        sharedPreferences.edit().putString(Preferences.PROFILE_IMAGE, url).apply()
    }

    fun getStateId(): Int {
        return sharedPreferences.getInt(Preferences.STATE_ID, 0)
    }

    fun setStateId(url: Int) {
        sharedPreferences.edit().putInt(Preferences.STATE_ID, url).apply()
    }

    fun getCountryId(): Int {
        return sharedPreferences.getInt(Preferences.COUNTRY_ID, 0)
    }

    fun setCountryId(url: Int) {
        sharedPreferences.edit().putInt(Preferences.COUNTRY_ID, url).apply()
    }

    fun getStateName(): String? {
        return sharedPreferences.getString(Preferences.STATE, "")
    }

    fun setState(url: String) {
        sharedPreferences.edit().putString(Preferences.STATE, url).apply()
    }

    fun getCity(): String? {
        return sharedPreferences.getString(Preferences.CITY, "")
    }

    fun setCity(url: String) {
        sharedPreferences.edit().putString(Preferences.CITY, url).apply()
    }


    fun getAddress1(): String? {
        return sharedPreferences.getString(Preferences.ADDRESS_LINE1, "")
    }

    fun setAddress1(url: String) {
        sharedPreferences.edit().putString(Preferences.ADDRESS_LINE1, url).apply()
    }

    fun getAddress2(): String? {
        return sharedPreferences.getString(Preferences.ADDRESS_LINE2, "")
    }

    fun setAddress2(url: String) {
        sharedPreferences.edit().putString(Preferences.ADDRESS_LINE2, url).apply()
    }

    fun getPin(): String? {
        return sharedPreferences.getString(Preferences.PIN, "")
    }

    fun setPin(url: String) {
        sharedPreferences.edit().putString(Preferences.PIN, url).apply()
    }


    fun setCountry(country: CountryList) {
        val gson = Gson()
        val json = gson.toJson(country)
        sharedPreferences.edit().putString(Preferences.COUNTRY, json).apply()

    }

    fun getCountry(): CountryList {
        val gson = Gson()
        val json: String? = sharedPreferences.getString(Preferences.COUNTRY, "")
        return gson.fromJson(json, CountryList::class.java)
    }

    fun getDnd(): Boolean {
        return sharedPreferences.getBoolean(Preferences.DND, false)
    }

    fun setDnd(dnd: Boolean) {
        sharedPreferences.edit().putBoolean(Preferences.DND, dnd).apply()
    }

    fun getMenu(): ArrayList<Menu> {
        val menu = sharedPreferences.getString(Preferences.MENU, "")
        val type = object : TypeToken<ArrayList<Menu?>?>() {}.type
        if(menu.isNullOrEmpty()) return ArrayList()
        return Gson().fromJson(menu, type)
    }

    fun setMenu(menu: ArrayList<Menu>) {
        sharedPreferences.edit().putString(Preferences.MENU, Gson().toJson(menu)).apply()
    }

    fun clear() {
        sharedPreferences.edit().clear().apply()
    }

}