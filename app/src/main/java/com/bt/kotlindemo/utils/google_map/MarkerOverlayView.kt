/*
 * *
 *  * Created by Surajit on 2/7/20 2:07 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 2/7/20 2:05 AM
 *  *
 */

package com.bt.kotlindemo.utils.google_map

import android.content.Context
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Point
import android.util.AttributeSet
import android.view.View
import androidx.annotation.Nullable
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.Projection
import java.util.*


class MarkerOverlayView : View, OverlayMarker.MarkerRemoveListner {
    private val mSvgLock = Any()

    /**
     * TO be converted to a HashMap.
     */
    private var overlayMarkers: MutableList<OverlayMarker>? = null

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(
        context: Context?,
        @Nullable attrs: AttributeSet?
    ) : super(context, attrs) {
        init()
    }

    private fun init() {
        setLayerType(LAYER_TYPE_SOFTWARE, null)
        overlayMarkers = ArrayList()
    }

    fun addMarker(
        overlayMarker: OverlayMarker,
        projection: Projection
    ) {
        overlayMarker.screenPoint = (overlayMarker.latLng?.let { projection.toScreenLocation(it) })
        overlayMarker.markerRemoveListner = this
        overlayMarkers!!.add(overlayMarker)
        invalidate()
    }

    fun addMarkers(
        overlayMarkers: List<OverlayMarker>,
        projection: Projection
    ) {
        for (overlayMarker in overlayMarkers) {
            overlayMarker.screenPoint = overlayMarker.latLng?.let { projection.toScreenLocation(it) }
            overlayMarker.markerRemoveListner = this
            this.overlayMarkers!!.add(overlayMarker)
        }
        invalidate()
    }

    fun updateMarker(
        overlayMarker: OverlayMarker,
        projection: Projection
    ) {
        val currentMarker: OverlayMarker? = findMarkerById(overlayMarker.markerId)
        currentMarker?.latLng = (overlayMarker.latLng)
        currentMarker?.screenPoint = overlayMarker.latLng?.let { projection.toScreenLocation(it) }
        currentMarker?.markerRemoveListner = this
        invalidate()
    }

    fun updateMarkerAngle(overlayMarker: OverlayMarker) {
        val currentMarker: OverlayMarker? = findMarkerById(overlayMarker.markerId)
        currentMarker?.latLng = overlayMarker.latLng
        currentMarker?.markerRemoveListner = this
        invalidate()
    }

    val markerCount: Int
        get() = overlayMarkers!!.size

    fun findMarkerById(markerId: Int): OverlayMarker? {
        for (marker in overlayMarkers!!) {
            if (marker.markerId == markerId) {
                return marker
            }
        }
        return null
    }

    override fun onRemove(overlayMarker: OverlayMarker?) {
        overlayMarkers!!.remove(overlayMarker)
        invalidate()
    }

    fun removeAllMarker() {
        overlayMarkers!!.clear()
        invalidate()
    }

    fun onCameraMove(googleMap: GoogleMap) {
        for (overlayMarker in overlayMarkers!!) {
            overlayMarker.screenPoint =
                overlayMarker.latLng?.let { googleMap.projection.toScreenLocation(it) }

        }
        invalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        synchronized(mSvgLock) { drawMarkers(canvas) }
    }

    private fun drawMarkers(canvas: Canvas) {
        if (overlayMarkers!!.size > 0) {
            for (overlayMarker in overlayMarkers!!) {
                val point = Point()
                point.x = overlayMarker.screenPoint!!.x - overlayMarker.icon!!.width / 2
                point.y = overlayMarker.screenPoint!!.y - overlayMarker.icon!!.height / 2
                val rotateMatrix = Matrix()
                val xRotatePoint: Int = overlayMarker.icon!!.width / 2
                val yRotatePoint: Int = overlayMarker.icon!!.height / 2
                rotateMatrix.postRotate(
                    overlayMarker.bearing,
                    xRotatePoint.toFloat(),
                    yRotatePoint.toFloat()
                )
                rotateMatrix.postTranslate(point.x.toFloat(), point.y.toFloat())
                canvas.drawBitmap(overlayMarker.icon!!, rotateMatrix, null)
            }
        }
    } //  private void drawMarker(Canvas canvas, Bitmap bitmap, Point point, @Nullable int gravity) {
    //    if (gravity == MarkerGravity.CENTER) {
    //      point.x = point.x - bitmap.getWidth() / 2;
    //      point.y = point.y - bitmap.getHeight() / 2;
    //    } else { // bottom, for now
    //      point.x = point.x - bitmap.getWidth() / 2;
    //      point.y = point.y - bitmap.getHeight();
    //    }
    //    canvas.drawBitmap(bitmap, point.x, point.y, null);
    //  }
}

//OverlayMarker click functionality
//  private OnOverlayMarkerClickListner onOverlayMarkerClickListner;

//  public void setOnMarkerClickListener(OnOverlayMarkerClickListner onOverlayMarkerClickListner) {
//    this.onOverlayMarkerClickListner = onOverlayMarkerClickListner;
//  }

//  public interface OnOverlayMarkerClickListner {
//    void onOverlayMarkerClick(OverlayMarker marker);
//  }
