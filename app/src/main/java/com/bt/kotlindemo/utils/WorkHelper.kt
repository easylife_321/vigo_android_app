/*
 * *
 *  * Created by Surajit on 1/24/21 2:00 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/24/21 2:00 AM
 *  *
 */

package com.bt.kotlindemo.utils

import androidx.work.*
import com.bt.kotlindemo.constants.IntentConstants
import com.bt.kotlindemo.model.address.Address
import com.bt.kotlindemo.work.AddressWorker
import com.bt.kotlindemo.work.NotificationWorker
import java.util.concurrent.TimeUnit

object WorkHelper {


    fun startWork() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val notificationRequest = OneTimeWorkRequestBuilder<NotificationWorker>()
            .setConstraints(constraints)
            .setBackoffCriteria(
                BackoffPolicy.LINEAR,
                WorkRequest.MIN_BACKOFF_MILLIS,
                TimeUnit.MILLISECONDS
            )
            .build()
        WorkManager.getInstance().enqueue(notificationRequest)
    }

    fun startAddressWork(address:Address) {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val request = OneTimeWorkRequestBuilder<AddressWorker>()
            .setConstraints(constraints)
            .setInputData(
                Data.Builder().putDouble(IntentConstants.LAT, address.lat?:0.0)
                    .putDouble(IntentConstants.LNG, address.lng?:0.0)
                    .putString(IntentConstants.ADDRESS, address.address?:"")
                    .putString(IntentConstants.POSTAL_CODE, address.postalCode?:"")
                    .putString(IntentConstants.SATE, address.stateName?:"")
                    .putString(IntentConstants.CITY, address.cityName?:"")
                    .build()
            )
            .setBackoffCriteria(
                BackoffPolicy.LINEAR,
                WorkRequest.MIN_BACKOFF_MILLIS,
                TimeUnit.MILLISECONDS
            )
            .build()
        WorkManager.getInstance().enqueue(request)
    }
}