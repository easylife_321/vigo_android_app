package com.bt.kotlindemo.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.WindowInsets
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.bt.kotlindemo.R
import com.bt.kotlindemo.model.zone.Loc
import com.google.android.gms.maps.model.LatLng
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneId
import java.time.ZoneOffset
import java.util.*
import java.util.regex.Pattern


fun String?.isValidEmail() =
    !this.isNullOrEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()

fun String.isValidMobile() = !Pattern.matches("[a-zA-Z]+", this) && this.length in 7..13

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun <T> LiveData<T>.observeOnce(lifecycleOwner: LifecycleOwner, observer: Observer<T>) {

    observe(lifecycleOwner, object : Observer<T> {
        override fun onChanged(value: T) {
            observer.onChanged(value)
            removeObserver(this)
        }
    })

}

fun <T> LiveData<T>.reObserver(lifecycleOwner: LifecycleOwner, observer: Observer<T>) {
    removeObserver(observer)
    observe(lifecycleOwner, observer)
}

@SuppressLint("ClickableViewAccessibility")
fun EditText.onRightDrawableClicked(onClicked: (view: EditText) -> Unit) {
    this.setOnTouchListener { v, event ->
        var hasConsumed = false
        if (v is EditText) {
            if (event.x >= v.width - v.totalPaddingRight) {
                if (event.action == MotionEvent.ACTION_UP) {
                    onClicked(this)
                }
                hasConsumed = true
            }
        }
        hasConsumed
    }
}

fun String?.capitalize(): String {
    if (this.isNullOrEmpty()) {
        return ""
    }
    return if (Character.isUpperCase(this[0])) {
        this
    } else {
        Character.toUpperCase(this[0]).toString() + this.substring(1)
    }
}

@ColorInt
fun Context.getColorFromAttr(
    @AttrRes attrColor: Int,
    typedValue: TypedValue = TypedValue(),
    resolveRefs: Boolean = true
): Int {
    theme.resolveAttribute(attrColor, typedValue, resolveRefs)
    return typedValue.data
}

internal fun Context.showPermanentlyDeniedDialog() {

    AlertDialog.Builder(this)
        .setTitle(R.string.permissions_required)
        .setMessage(R.string.permissions_required_msg)
        .setPositiveButton(R.string.action_settings) { _, _ ->
            // Open the app's settings.
            val intent = createAppSettingsIntent()
            startActivity(intent)
        }
        .setNegativeButton(android.R.string.cancel, null)
        .show()
}

private fun Context.createAppSettingsIntent() = Intent().apply {
    action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    data = Uri.fromParts("package", packageName, null)
}




fun String.isValidLat() =
    this.isNotEmpty() && (this.toDouble() >= -90 && this.toDouble() <= 90)

fun String.isValidLng() =
    this.isNotEmpty() && (this.toDouble() >= -180 && this.toDouble() <= 180)

fun Long.toLocalTime() =
    Instant.ofEpochSecond(this).atZone(ZoneOffset.UTC).withZoneSameInstant(ZoneId.systemDefault())!!

fun Long.toUtc() = Instant.ofEpochMilli(this).epochSecond


fun Long.localToGMT(): Long {
    val date = Date(this)
    val sdf = SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ROOT)
    sdf.timeZone = TimeZone.getTimeZone("UTC")
    return Date(sdf.format(date)).time
}
fun Double.squareMeterToFeet(): Double = this * 10.764

fun Double.roundTo(n: Int): Double {
    return "%.${n}f".format(this).toDouble()
}

fun List<Loc>.toListLatLng(): List<LatLng> {
    val list = mutableListOf<LatLng>()
    this.forEach {
        list.add(LatLng(it.lat, it.lng))
    }
    return list
}

fun View.visible(){
    this.visibility=View.VISIBLE
}

fun View.gone(){
    this.visibility=View.GONE
}

val Context.navigationBarHeight: Int
    @RequiresApi(Build.VERSION_CODES.R)
    get() {
        val windowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        return if (Build.VERSION.SDK_INT >= 30) {
            windowManager
                .currentWindowMetrics
                .windowInsets
                .getInsets(WindowInsets.Type.navigationBars())
                .bottom

        } else {
            val currentDisplay = try {
                display
            } catch (e: NoSuchMethodError) {
                windowManager.defaultDisplay
            }

            val appUsableSize = Point()
            val realScreenSize = Point()
            currentDisplay?.apply {
                getSize(appUsableSize)
                getRealSize(realScreenSize)
            }

            // navigation bar on the side
            if (appUsableSize.x < realScreenSize.x) {
                return realScreenSize.x - appUsableSize.x
            }

            // navigation bar at the bottom
            return if (appUsableSize.y < realScreenSize.y) {
                realScreenSize.y - appUsableSize.y
            } else 0
        }
    }

fun String?.nullToEmpty() = this.orEmpty()




