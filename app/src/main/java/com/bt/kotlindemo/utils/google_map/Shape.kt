/*
 * *
 *  * Created by Surajit on 8/6/20 10:26 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 8/6/20 10:26 PM
 *  *
 */

package com.bt.kotlindemo.utils.google_map

enum class Shape {
    POLYGON, CIRCLE
}