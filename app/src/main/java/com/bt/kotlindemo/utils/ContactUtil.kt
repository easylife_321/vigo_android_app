/*
 * *
 *  * Created by Surajit on 23/7/20 12:42 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/7/20 12:42 AM
 *  *
 */

package com.bt.kotlindemo.utils

import android.app.Application
import android.database.Cursor
import android.provider.ContactsContract
import com.bt.kotlindemo.model.Contact
import timber.log.Timber

object ContactUtil {
    fun getAllContacts(
        ownNumber:String,
        activity: Application,
        countryCode: String,
        length: Int
    ): ArrayList<Contact> {
        val startTime = System.currentTimeMillis()
        val projection = arrayOf(
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER
        )

        val contactList = arrayListOf<Contact>()
        val phoneList = arrayListOf<String>()
        val cr = activity.contentResolver

            cr?.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection,
                null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
            )?.use {

                val idIndex = it.getColumnIndex(ContactsContract.Data.CONTACT_ID)
                val nameIndex = it.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
                val numberIndex =
                    it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)

                var id: String
                var name: String
                var number: String
                while (it.moveToNext() && it.getString(it.getColumnIndex(ContactsContract.CommonDataKinds.Phone.HAS_PHONE_NUMBER))
                        .toInt() > 0
                ) {
                    number = it.getString(numberIndex).replace(Regex("[()*#tel:\\-\\s]"), "")
                        .replace(Regex("^0"), "").trim()
                    if (!phoneList.contains(number) && number.length > 6 && !number.contains(ownNumber)) {
                        if (!number.startsWith("+")) {
                            if (number.length == length) {
                                number = countryCode.plus(number)
                            }

                        } else {
                           number= number.replace("+", "")
                        }
                        phoneList.add(number)
                        val contacts = Contact()
                        id = it.getLong(idIndex).toString()
                        name = it.getString(nameIndex)
                        contacts.contactId = id
                        contacts.contactName = name
                        contacts.contactNumber = number
                        contactList.add(contacts)
                    }
                }

                it.close()
                val fetchingTime = System.currentTimeMillis() - startTime
                Timber.d("Fetching Completed in $fetchingTime ms")


            }

        return contactList

    }

    private suspend fun getPhoneContacts(mApplication: Application): ArrayList<Contact> {
        val contactsList = ArrayList<Contact>()
        val contactsCursor = mApplication.contentResolver?.query(
            ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
        )
        if (contactsCursor != null && contactsCursor.count > 0) {
            val idIndex = contactsCursor.getColumnIndex(ContactsContract.Contacts._ID)
            val nameIndex =
                contactsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
            while (contactsCursor.moveToNext()) {
                val id = contactsCursor.getString(idIndex)
                val name = contactsCursor.getString(nameIndex)
                if (name != null) {
                    contactsList.add(Contact(id, name))
                }
            }
            contactsCursor.close()
        }
        return contactsList
    }

    private suspend fun getContactNumbers(mApplication: Application): HashMap<String, ArrayList<String>> {
        val contactsNumberMap = HashMap<String, ArrayList<String>>()
        val phoneCursor: Cursor? = mApplication.contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            null,
            null,
            null
        )
        if (phoneCursor != null && phoneCursor.count > 0) {
            val contactIdIndex =
                phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID)
            val numberIndex =
                phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            while (phoneCursor.moveToNext()) {
                val contactId = phoneCursor.getString(contactIdIndex)
                val number: String = phoneCursor.getString(numberIndex)
                //check if the map contains key or not, if not then create a new array list with number
                if (contactsNumberMap.containsKey(contactId)) {
                    contactsNumberMap[contactId]?.add(number)
                } else {
                    contactsNumberMap[contactId] = arrayListOf(number)
                }
            }
            //contact contains all the number of a particular contact
            phoneCursor.close()
        }
        return contactsNumberMap
    }

}


