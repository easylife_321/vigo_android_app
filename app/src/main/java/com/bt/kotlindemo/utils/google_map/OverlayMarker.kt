/*
 * *
 *  * Created by Surajit on 2/7/20 2:06 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 2/7/20 2:06 AM
 *  *
 */

package com.bt.kotlindemo.utils.google_map

import android.animation.ValueAnimator
import android.graphics.Bitmap
import android.graphics.Point
import com.google.android.gms.maps.model.LatLng

class OverlayMarker {
    var markerId = -1
    var latLng: LatLng? = null
        set(latLng) {
            if (onMarkerUpdate != null) onMarkerUpdate!!.onMarkerUpdate()
            field = latLng
        }
    var bearing = 0f
    var icon: Bitmap? = null
    var screenPoint: Point? = null
    var markerRemoveListner: MarkerRemoveListner? = null
    var onMarkerUpdate: OnMarkerUpdate? = null
    var translateValueAnimator: ValueAnimator? = null

    //  private Bitmap rotateBitmap(Bitmap bitmap, float bearing) {
    var rotateValueAnimator: ValueAnimator? = null

    fun rotateIcon(degrees: Float) {
//    int width = icon.getWidth();
//    int height = icon.getHeight();
//
//    Matrix matrix = new Matrix();
//    matrix.postRotate(degrees);
//
//    Bitmap scaledBitmap = Bitmap.createScaledBitmap(icon, width, height, true);
//    icon = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
    }

    fun remove() {
        if (markerRemoveListner != null) {
            markerRemoveListner!!.onRemove(this)
        }
    }

    override fun equals(obj: Any?): Boolean {
        if (obj == null) {
            return false
        }
        if (!OverlayMarker::class.java.isAssignableFrom(obj.javaClass)) {
            return false
        }
        val objectToBeCompared = obj as OverlayMarker
        return markerId == objectToBeCompared.markerId
    }

    interface MarkerRemoveListner {
        fun onRemove(overlayMarker: OverlayMarker?)
    }

    interface OnMarkerUpdate {
        fun onMarkerUpdate()
    }

    //
    //    Matrix matrix = new Matrix();
    //    matrix.setRotate(bearing);
    //    bitmap.getWidth();
    //    bitmap.getHeight();
    //    return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    //  }
    companion object {
        var count = 0
    }
}

