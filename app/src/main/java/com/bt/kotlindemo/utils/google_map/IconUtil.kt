/*
 * *
 *  * Created by Surajit on 4/7/21 12:52 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 4/7/21 12:52 AM
 *  *
 */

package com.bt.kotlindemo.utils.google_map

import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.DeviceIconType
import com.bt.kotlindemo.constants.DeviceStatus

object IconUtil {
    fun getIcon(marker: Int, status: String): Int {
        when (marker) {
            DeviceIconType.CAR -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.car_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.car_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.car_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.car_offline
                    }
                    else -> {
                        return R.mipmap.car_moving
                    }
                }


            }
            DeviceIconType.BIKE -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.bike_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.bike_moving
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.bike_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.bike_offline
                    }
                    else -> {
                        return R.mipmap.bike_moving
                    }
                }


            }
            DeviceIconType.TRACKTOR -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.tractor_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.tractor_moving
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.tractor_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.tractor_offline
                    }
                    else -> {
                        return R.mipmap.tractor_moving
                    }
                }


            }
            DeviceIconType.SCOOTER -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.scooter_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.scooter_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.scooter_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.scooter_offline
                    }
                    else -> {
                        return R.mipmap.scooter_moving
                    }
                }


            }
            DeviceIconType.SCOOTY -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.scooty_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.scooter_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.scooty_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.scooty_offline
                    }
                    else -> {
                        return R.mipmap.scooty_moving
                    }
                }


            }

            DeviceIconType.TANKER_TRUCK -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.tanker_truck_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.tanker_truck_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.tanker_truck_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.tanker_truck_offline
                    }
                    else -> {
                        return R.mipmap.tanker_truck_moving
                    }
                }


            }
            DeviceIconType.TRUCK -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.truck_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.truck_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.truck_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.truck_offline
                    }
                    else -> {
                        return R.mipmap.truck_moving
                    }
                }


            }
            DeviceIconType.AUTO -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.rickshaw_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.rickshaw_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.rickshaw_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.rickshaw_offline
                    }
                    else -> {
                        return R.mipmap.rickshaw_moving
                    }
                }


            }
            DeviceIconType.BUS -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.bus_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.bus_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.bus_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.bus_offline
                    }
                    else -> {
                        return R.mipmap.bus_moving
                    }
                }


            }
            DeviceIconType.HEAVY_MACHINERY -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.heavy_machinery_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.heavy_machinery_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.heavy_machinery_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.heavy_machinery_offline
                    }
                    else -> {
                        return R.mipmap.heavy_machinery_moving
                    }
                }


            }
            DeviceIconType.PERSONAL -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.personal_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.personal_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.personal_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.personal_offline
                    }
                    else -> {
                        return R.mipmap.personal_moving
                    }
                }


            }

            DeviceIconType.TRAIN -> {
                when (status) {
                    DeviceStatus.MOVING -> {
                        return R.mipmap.train_moving
                    }
                    DeviceStatus.IDLE -> {
                        return R.mipmap.tractor_idle
                    }
                    DeviceStatus.STANDING -> {
                        return R.mipmap.tractor_standing
                    }
                    DeviceStatus.OFFLINE -> {
                        return R.mipmap.tractor_offline
                    }
                    else -> {
                        return R.mipmap.train_moving
                    }
                }


            }

            else -> {
                return R.mipmap.car_moving
            }

        }
    }
}