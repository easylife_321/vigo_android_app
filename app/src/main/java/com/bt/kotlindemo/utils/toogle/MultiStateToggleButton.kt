package com.bt.kotlindemo.utils.toogle

import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatButton
import com.bt.kotlindemo.R
import kotlin.math.max

class MultiStateToggleButton : ToggleButton {
    /**
     * A list of rendered buttons. Used to get state, among others
     */
    var buttons: MutableList<View>? = null

    /**
     * @return An array of the buttons' text
     */
    /**
     * The specified texts
     */
    var texts: Array<String?>? = null

    /**
     * If true, multiple buttons can be pressed at the same time
     */
    var mMultipleChoice: Boolean = false

    /**
     * The layout containing all buttons
     */
    private var mainLayout: LinearLayout? = null

    constructor(context: Context) : super(context, null)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.MultiStateToggleButton, 0, 0)
        try {
            val texts = a.getTextArray(R.styleable.MultiStateToggleButton_values)
            colorPressed = a.getColor(R.styleable.MultiStateToggleButton_mstbPrimaryColor, 0)
            colorNotPressed = a.getColor(R.styleable.MultiStateToggleButton_mstbSecondaryColor, 0)
            colorPressedText =
                a.getColor(R.styleable.MultiStateToggleButton_mstbColorPressedText, 0)
            colorPressedBackground =
                a.getColor(R.styleable.MultiStateToggleButton_mstbColorPressedBackground, 0)
            pressedBackgroundResource = a.getResourceId(
                R.styleable.MultiStateToggleButton_mstbColorPressedBackgroundResource,
                0
            )
            colorNotPressedText =
                a.getColor(R.styleable.MultiStateToggleButton_mstbColorNotPressedText, 0)
            colorNotPressedBackground =
                a.getColor(R.styleable.MultiStateToggleButton_mstbColorNotPressedBackground, 0)
            notPressedBackgroundResource = a.getResourceId(
                R.styleable.MultiStateToggleButton_mstbColorNotPressedBackgroundResource,
                0
            )

            var length = 0
            if (texts != null) {
                length = texts.size
            }
            val text = arrayOf<String?>("OFF", "NORMAL","SOS")
            setElements(text, null, BooleanArray(length))
        } finally {
            a.recycle()
        }
    }

    /**
     * If multiple choice is enabled, the user can select multiple
     * values simultaneously.
     *
     * @param enable
     */
    fun enableMultipleChoice(enable: Boolean) {
        this.mMultipleChoice = enable
    }

    public override fun onSaveInstanceState(): Parcelable? {
        val bundle = Bundle()
        bundle.putParcelable(KEY_INSTANCE_STATE, super.onSaveInstanceState())
        bundle.putBooleanArray(KEY_BUTTON_STATES, states)
        return bundle
    }

    public override fun onRestoreInstanceState(state: Parcelable) {
        var state: Parcelable? = state
        if (state is Bundle) {
            val bundle = state
            states = bundle.getBooleanArray(KEY_BUTTON_STATES)
            state = bundle.getParcelable(KEY_INSTANCE_STATE)
        }
        super.onRestoreInstanceState(state)
    }

    /**
     * Set the enabled state of this MultiStateToggleButton, including all of its child buttons.
     *
     * @param enabled True if this view is enabled, false otherwise.
     */
    override fun setEnabled(enabled: Boolean) {
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            child.isEnabled = enabled
        }
    }

    /**
     * Set multiple buttons with the specified texts and default
     * initial values. Initial states are allowed, but both
     * arrays must be of the same size.
     *
     * @param texts            An array of CharSequences for the buttons
     * @param imageResourceIds an optional icon to show, either text, icon or both needs to be set.
     * @param selected         The default value for the buttons
     */
    fun setElements(
        texts: Array<String?>,
        imageResourceIds: IntArray?,
        selected: BooleanArray?
    ) {
        this.texts = texts
        val textCount = texts?.size ?: 0
        val iconCount = imageResourceIds?.size ?: 0
        val elementCount = max(textCount.toDouble(), iconCount.toDouble()).toInt()
        if (elementCount == 0) {
            return
        }

        var enableDefaultSelection = true
        if (selected == null || elementCount != selected.size) {
            enableDefaultSelection = false
        }

        orientation = HORIZONTAL
        gravity = Gravity.CENTER_VERTICAL

        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (mainLayout == null) {
            mainLayout = inflater.inflate(
                R.layout.view_multi_state_toggle_button,
                this,
                true
            ) as LinearLayout
        }
        mainLayout!!.removeAllViews()

        this.buttons = ArrayList(elementCount)
        for (i in 0 until elementCount) {
            var b = if (i == 0) {
                // Add a special view when there's only one element
                if (elementCount == 1) {
                    inflater.inflate(
                        R.layout.view_single_toggle_button,
                        mainLayout,
                        false
                    ) as Button
                } else {
                    inflater.inflate(
                        R.layout.view_left_toggle_button,
                        mainLayout,
                        false
                    ) as Button
                }
            } else if (i == elementCount - 1) {
                inflater.inflate(
                    R.layout.view_right_toggle_button,
                    mainLayout,
                    false
                ) as Button
            } else {
                inflater.inflate(
                    R.layout.view_center_toggle_button,
                    mainLayout,
                    false
                ) as Button
            }
            b.text = if (texts != null) texts[i] else ""
            if (imageResourceIds != null && imageResourceIds[i] != 0) {
                b.setCompoundDrawablesWithIntrinsicBounds(imageResourceIds[i], 0, 0, 0)
            }
            val position = i
            b.setOnClickListener { setValue(position) }
            mainLayout!!.addView(b)
            if (enableDefaultSelection) {
                setButtonState(b, selected!![i])
            }
            buttons?.add(b)
        }
        mainLayout!!.setBackgroundResource(R.drawable.button_section_shape)
    }

    /**
     * Set multiple buttons with the specified texts and default
     * initial values. Initial states are allowed, but both
     * arrays must be of the same size.
     *
     * @param buttons  the array of button views to use
     * @param selected The default value for the buttons
     */
    fun setButtons(buttons: Array<View>, selected: BooleanArray?) {
        val elementCount = buttons.size
        if (elementCount == 0) {
            return
        }

        var enableDefaultSelection = true
        if (selected == null || elementCount != selected.size) {
            Log.d(TAG, "Invalid selection array")
            enableDefaultSelection = false
        }

        orientation = HORIZONTAL
        gravity = Gravity.CENTER_VERTICAL

        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (mainLayout == null) {
            mainLayout = inflater.inflate(
                R.layout.view_multi_state_toggle_button,
                this,
                true
            ) as LinearLayout
        }
        mainLayout!!.removeAllViews()

        this.buttons = ArrayList()
        for (i in 0 until elementCount) {
            val b = buttons[i]
            val position = i
            b.setOnClickListener { setValue(position) }
            mainLayout!!.addView(b)
            if (enableDefaultSelection) {
                setButtonState(b, selected!![i])
            }
            this.buttons?.add(b)
        }
        mainLayout!!.setBackgroundResource(R.drawable.button_section_shape)
    }

    fun setElements(elements: Array<String?>) {
        val size = elements.size ?: 0
        setElements(elements, null, BooleanArray(size))
    }

    fun setElements(elements: List<*>?) {
        val size = elements?.size ?: 0
        setElements(elements, BooleanArray(size))
    }

    fun setElements(elements: List<*>?, selected: Any?) {
        var size = 0
        var index = -1
        if (elements != null) {
            size = elements.size
            index = elements.indexOf(selected)
        }
        val selectedArray = BooleanArray(size)
        if (index != -1 && index < size) {
            selectedArray[index] = true
        }
        setElements(elements, selectedArray)
    }

    fun setElements(texts: List<String>?, selected: BooleanArray?) {
        var texts = texts
        if (texts == null) {
            texts = ArrayList(0)
        }
        val size = texts.size
        setElements(texts.toTypedArray<String?>(), null, selected)
    }

    fun setElements(arrayResourceId: Int, selectedPosition: Int) {
        // Get resources
        val elements = this.resources.getStringArray(arrayResourceId)

        // Set selected boolean array
        val size = elements?.size ?: 0
        val selected = BooleanArray(size)
        if (selectedPosition >= 0 && selectedPosition < size) {
            selected[selectedPosition] = true
        }

        // Super
        setElements(elements, null, selected)
    }

    fun setElements(arrayResourceId: Int, selected: BooleanArray?) {
        setElements(this.resources.getStringArray(arrayResourceId), null, selected)
    }

    fun setButtonState(button: View?, selected: Boolean) {
        if (button == null) {
            return
        }
        button.isSelected = selected
        button.setBackgroundResource(if (selected) R.drawable.button_pressed else R.drawable.button_not_pressed)
        if (colorPressed != 0 || colorNotPressed != 0) {
            button.setBackgroundColor(if (selected) colorPressed else colorNotPressed)
        } else if (colorPressedBackground != 0 || colorNotPressedBackground != 0) {
            button.setBackgroundColor(if (selected) colorPressedBackground else colorNotPressedBackground)
        }
        if (button is Button) {
            val style = if (selected) R.style.WhiteBoldText else R.style.PrimaryNormalText
            (button as AppCompatButton).setTextAppearance(this.getContext(), style)
            if (colorPressed != 0 || colorNotPressed != 0) {
                button.setTextColor(if (!selected) colorPressed else colorNotPressed)
            }
            if (colorPressedText != 0 || colorNotPressedText != 0) {
                button.setTextColor(if (selected) colorPressedText else colorNotPressedText)
            }
            if (pressedBackgroundResource != 0 || notPressedBackgroundResource != 0) {
                button.setBackgroundResource(if (selected) pressedBackgroundResource else notPressedBackgroundResource)
            }
        }
    }

    fun getValue(): Int {
        for (i in buttons!!.indices) {
            if (buttons!![i].isSelected) {
                return i
            }
        }
        return -1
    }

    override fun setValue(position: Int) {
        for (i in buttons!!.indices) {
            if (mMultipleChoice) {
                if (i == position) {
                    val b = buttons!![i]
                    if (b != null) {
                        setButtonState(b, !b.isSelected)
                    }
                }
            } else {
                if (i == position) {
                    setButtonState(buttons!![i], true)
                } else if (!mMultipleChoice) {
                    setButtonState(buttons!![i], false)
                }
            }
        }
        super.setValue(position)
    }

    var states: BooleanArray?
        get() {
            val size =
                if (this.buttons == null) 0 else buttons!!.size
            val result = BooleanArray(size)
            for (i in 0 until size) {
                result[i] = buttons!![i].isSelected
            }
            return result
        }
        set(selected) {
            if (this.buttons == null || selected == null || buttons!!.size != selected.size) {
                return
            }
            var count = 0
            for (b in buttons!!) {
                setButtonState(b, selected[count])
                count++
            }
        }

    /**
     * {@inheritDoc}
     */
    override fun setColors(colorPressed: Int, colorNotPressed: Int) {
        super.setColors(colorPressed, colorNotPressed)
        refresh()
    }

    private fun refresh() {
        val states = states!!
        for (i in states.indices) {
            setButtonState(buttons!![i], states[i])
        }
    }

    companion object {
        private val TAG: String = MultiStateToggleButton::class.java.simpleName

        private const val KEY_BUTTON_STATES = "button_states"
        private const val KEY_INSTANCE_STATE = "instance_state"
    }
}
