/*
 * *
 *  * Created by Surajit on 9/4/20 4:43 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 8/4/20 8:07 PM
 *  *
 */

package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.R
import com.bt.kotlindemo.constants.DeviceIconType
import com.bt.kotlindemo.data.database.dao.DeviceDao
import com.bt.kotlindemo.data.network.ApiService
import com.bt.kotlindemo.model.DeviceIcon
import com.bt.kotlindemo.data.database.entity.Device
import com.google.gson.JsonObject

class DeviceDataSource(private val apiService: ApiService, private val deviceDao: DeviceDao) :
    BaseDataSource() {

    suspend fun getUserDevices(obj: JsonObject) = getResult { apiService.getDevices(obj) }


    suspend fun getCurrentTracking(obj: JsonObject) =
        getResult { apiService.getDeviceCurrentData(obj) }


    suspend fun getDeviceHistory(obj: JsonObject) =
        getResult { apiService.getDeviceHistory(obj) }

    suspend fun addDevice(obj: JsonObject) =
        getResult { apiService.addDevice(obj) }

    suspend fun updateDevice(obj: JsonObject) =
        getResult { apiService.updateDevice(obj) }

    suspend fun deleteDevice(obj: JsonObject) =
        getResult { apiService.deleteDevice(obj) }

    fun getDeviceIcons(): ArrayList<DeviceIcon> {
        val icons = arrayListOf<DeviceIcon>()
        icons.add(DeviceIcon("Car", R.drawable.ic_car, DeviceIconType.CAR))
        icons.add(DeviceIcon("Bike", R.drawable.ic_bike, DeviceIconType.BIKE))
        icons.add(DeviceIcon("Scooter", R.drawable.ic_scooter, DeviceIconType.SCOOTER))
        icons.add(DeviceIcon("Truck", R.drawable.ic_truck, DeviceIconType.TRUCK))
        icons.add(DeviceIcon("Bus", R.drawable.ic_bus, DeviceIconType.BUS))
        icons.add(DeviceIcon("Tractor", R.drawable.ic_tractor, DeviceIconType.TRACKTOR))
        icons.add(DeviceIcon("Auto", R.drawable.ic_auto, DeviceIconType.AUTO))
        icons.add(DeviceIcon("Tanker Truck", R.drawable.ic_tanker, DeviceIconType.TANKER_TRUCK))
        /* icons.add(DeviceIcon("Car", DeviceIconType.CAR))
         icons.add(DeviceIcon("Car", DeviceIconType.CAR))
         icons.add(DeviceIcon("Car", DeviceIconType.CAR))
         icons.add(DeviceIcon("Car", DeviceIconType.CAR))
         icons.add(DeviceIcon("Car", DeviceIconType.CAR))*/

        return icons


    }

    suspend fun getDeviceList(json: JsonObject) =
        getResult { apiService.getDevices(json) }


    suspend fun shareDevice(json: JsonObject) =
        getResult { apiService.shareDevice(json) }

    suspend fun shareDeviceTimeSpecific(json: JsonObject) =
        getResult { apiService.shareDeviceTimeSpecific(json) }


    suspend fun moveDevice(json: JsonObject) =
        getResult { apiService.moveDevice(json) }

    suspend fun getDevicesSharedDetails(json: JsonObject) =
        getResult { apiService.getDevicesSharedDetails(json) }


    suspend fun saveRoute(json: JsonObject) =
        getResult { apiService.saveRoute(json) }


    fun getDevicesAdmin() = deviceDao.getDevicesAdmin()

    fun getDevices() = deviceDao.getDevices()


    fun addDevices(devices: List<Device>) = deviceDao.insertAll(devices)

    fun deleteDevice(deviceId: Int) = deviceDao.deleteDevice(deviceId)


}