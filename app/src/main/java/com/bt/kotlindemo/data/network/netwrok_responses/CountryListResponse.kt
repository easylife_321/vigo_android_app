/*
 * *
 *  * Created by Surajit on 1/3/21 7:57 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/3/21 7:56 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.bt.kotlindemo.model.Country
import com.bt.kotlindemo.model.Result
import com.bt.kotlindemo.model.State
import com.google.gson.annotations.SerializedName

data class CountryListResponse(
    @SerializedName("result")
    val result: Result,
    @SerializedName("countries")
    val state: List<Country>?
)