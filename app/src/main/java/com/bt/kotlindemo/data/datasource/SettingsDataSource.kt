package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody

class SettingsDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun feedback(
        userId: RequestBody,
        typeId: RequestBody,
        details: RequestBody,
        itemId: RequestBody,
        file: List<MultipartBody.Part>
    ) = getResult { apiService.feedback(userId, typeId, details, itemId, file) }


    suspend fun updateProfileImage(
        userId: RequestBody,
        file: MultipartBody.Part
    ) = getResult { apiService.updateProfileImage(userId, file) }


    suspend fun getFeedBackItems() = getResult { apiService.getFeedBackItems() }


    suspend fun getStateList(jsonObject: JsonObject) = getResult { apiService.getStateList(jsonObject) }


    suspend fun getCountryList() = getResult { apiService.getCountryList() }


    suspend fun updateProfile(jsonObject: JsonObject) =
        getResult { apiService.updateProfile(jsonObject) }



    suspend fun sendOtpNumberChange(jsonObject: JsonObject) =
        getResult { apiService.sendOtpNumberChange(jsonObject) }


    suspend fun changeAccountMobileNo(jsonObject: JsonObject) =
        getResult { apiService.changeAccountMobileNo(jsonObject) }

    suspend fun logout(jsonObject: JsonObject) =
        getResult { apiService.logout(jsonObject) }

    suspend fun doNotDisturb(jsonObject: JsonObject) =
        getResult { apiService.doNotDisturb(jsonObject) }




}