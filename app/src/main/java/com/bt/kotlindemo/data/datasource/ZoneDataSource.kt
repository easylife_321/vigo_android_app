/*
 * *
 *  * Created by Surajit on 23/5/20 2:54 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 3:40 PM
 *  *
 */

package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject

class ZoneDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun addZone(obj: JsonObject) = getResult { apiService.addZone(obj) }

    suspend fun updateZone(obj: JsonObject) = getResult { apiService.updateZone(obj) }

    suspend fun deleteZone(obj: JsonObject) = getResult { apiService.deleteZone(obj) }


    suspend fun getZoneList(userId: String) = getResult { apiService.getZoneList(userId) }


}