package com.bt.kotlindemo.data.network

import com.bt.kotlindemo.utils.ConnectivityUtil
import com.bt.kotlindemo.utils.ErrorHandler
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException


class NetworkConnectionInterceptor(val connectivityManager: ConnectivityUtil) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!connectivityManager.isNetworkAvailable()) {
            throw ErrorHandler.NoInternetException()
        }
        val builder: Request.Builder = chain.request().newBuilder()
        return chain.proceed(builder.build())
    }

}