/*
 * *
 *  * Created by Surajit on 10/4/20 3:13 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 10/4/20 3:13 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses

import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.model.Result
import com.google.gson.annotations.SerializedName

data class GetGroupResponse(
    @SerializedName("result")
    val result: Result,
    @SerializedName("value")
    val groups: List<Group>
)