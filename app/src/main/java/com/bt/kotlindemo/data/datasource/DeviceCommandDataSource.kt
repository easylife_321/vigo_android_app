/*
 * *
 *  * Created by Surajit on 15/5/20 4:25 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 3:40 PM
 *  *
 */

package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject

class DeviceCommandDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun getDeviceCommands(obj: JsonObject) = getResult { apiService.getDeviceCommands(obj) }

    suspend fun setSpeedLimit(obj: JsonObject) = getResult { apiService.setSpeedLimit(obj) }

    suspend fun setOdometer(obj: JsonObject) = getResult { apiService.setOdometer(obj) }

    suspend fun engineCut(obj: JsonObject) = getResult { apiService.deviceEngineCut(obj) }

    suspend fun antiTheft(obj: JsonObject) = getResult { apiService.setAntiTheft(obj) }

    suspend fun untraceableMode(obj: JsonObject) = getResult { apiService.markAsUntraceable(obj) }


    suspend fun shareableLink(obj: JsonObject) = getResult { apiService.shareableLink(obj) }

    suspend fun deviceAutoTroubleshoot(obj: JsonObject) =
        getResult { apiService.deviceAutoTroubleshoot(obj) }

    suspend fun parking(obj: JsonObject) = getResult { apiService.markAsParked(obj) }


    suspend fun refreshGps(obj: JsonObject) = getResult { apiService.refreshGps(obj) }

    suspend fun toggleSharedDevicesSharing(obj: JsonObject) =
        getResult { apiService.toggleSharedDevicesSharing(obj) }


    suspend fun toggleGpsDeviceAccessory(obj: JsonObject) =
        getResult { apiService.toggleGpsDeviceAccessory(obj) }

    suspend fun followDevice(obj: JsonObject) = getResult { apiService.followDevice(obj) }


}

