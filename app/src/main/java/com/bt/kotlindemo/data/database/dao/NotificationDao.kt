/*
 * *
 *  * Created by Surajit on 30/5/20 2:33 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/3/20 3:26 PM
 *  *
 */

package com.bt.kotlindemo.data.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bt.kotlindemo.data.database.entity.Notification

@Dao
interface NotificationDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(device: Notification)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(device: List<Notification>)

    @Query("delete from notifications")
    fun deleteAll()

    @Query("select * from notifications where device_id=:deviceId order by timestamp desc")
    fun getAllNotificationForDevice(deviceId: Int): LiveData<List<Notification>>

    @Query("select * from notifications order by timestamp desc")
    fun getAllNotification(): LiveData<List<Notification>>

    @Query("update notifications set read=1")
    fun updateAllNotification(): Int

    @Query("update notifications set read=1 where device_id=:deviceId")
    fun updateANotificationForDevice(deviceId: Int): Int


    @Query("select  MAX(timestamp) from notifications")
    fun getLastTimeStamp(): Long


    @Query("delete from notifications where type=:type and device_id=:id")
    fun deleteSelectedType(type: String, id: Int)

    @Query("delete from notifications where device_id=:id")
    fun deleteAllDevice(id: Int)

    @Query("delete from notifications where type=:type")
    fun deleteAllType(type: String)


}