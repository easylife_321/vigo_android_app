package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject

class AddressDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun saveAndGetAddress(obj: JsonObject) = getResult { apiService.saveAndGetAddress(obj) }


}