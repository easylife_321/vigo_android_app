/*
 * *
 *  * Created by Surajit on 9/4/20 5:21 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/3/20 3:26 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses

import com.bt.kotlindemo.model.Result
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BaseResponse(
    @SerializedName("result")
    @Expose
    val result: Result
)