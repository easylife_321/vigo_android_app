/*
 * *
 *  * Created by Surajit on 9/4/20 5:17 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 5:17 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.model.Result
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.model.device_command.Commands
import com.google.gson.annotations.SerializedName

data class DeviceListResponse(
    @SerializedName("devices")
    val devices: List<Device>,
    @SerializedName("commands")
    var commands: List<Commands>,
    @SerializedName("Groups")
    var groups: List<Group>,
    @SerializedName("result")
    val result: Result
)