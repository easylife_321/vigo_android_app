/*
 * *
 *  * Created by Surajit on 9/4/20 4:17 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 8/4/20 8:07 PM
 *  *
 */

package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject

class GroupDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun createGroup(obj: JsonObject) = getResult { apiService.createGroup(obj) }

    suspend fun updateGroup(obj: JsonObject) = getResult { apiService.updateGroup(obj) }

    suspend fun deleteGroup(obj: JsonObject) = getResult { apiService.deleteGroup(obj) }

    suspend fun getGroup(userId: Int) = getResult { apiService.getGroup(userId) }


}