package com.bt.kotlindemo.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.Serializable


@Entity(tableName = "contacts")
data class Contacts(
    @ColumnInfo(name = "user_id")
    var userId: Int = 0,
    @ColumnInfo(name = "phone_name")
    var phoneName: String = "",
    @ColumnInfo(name = "registered_name")
    var registeredName: String = "",
    @ColumnInfo(name = "mobile_no")
    @PrimaryKey(autoGenerate = false)
    var mobileNo: String = "",
    @ColumnInfo(name = "profile_pic")
    var profilePic: String = "",
    @ColumnInfo(name = "profile_thumb")
    var profileThumb: String = "",
    @Ignore
    var isSelected:Boolean=false
):Serializable