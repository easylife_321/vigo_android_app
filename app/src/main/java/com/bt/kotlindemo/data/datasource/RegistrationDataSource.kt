package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject

class RegistrationDataSource(val apiService: ApiService) : BaseDataSource() {

    suspend fun registerUser(obj: JsonObject) = getResult { apiService.registerUser(obj) }

    suspend fun getOtp(obj: JsonObject) = getResult { apiService.getOtp(obj) }

    suspend fun login(obj: JsonObject) = getResult { apiService.login(obj) }

    suspend fun loginWithOtp(obj: JsonObject) = getResult { apiService.loginWithOtp(obj) }

    suspend fun setPassword(obj: JsonObject) = getResult { apiService.setPassword(obj) }

    suspend fun changePassword(obj: JsonObject) = getResult { apiService.changePassword(obj) }


}