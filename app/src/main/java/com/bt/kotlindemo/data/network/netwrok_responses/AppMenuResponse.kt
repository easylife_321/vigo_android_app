/*
 * *
 *  * Created by Surajit on 11/5/20 8:13 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 11/5/20 8:13 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.bt.kotlindemo.model.app_menu.Menu
import com.google.gson.annotations.SerializedName
import com.bt.kotlindemo.model.Result

data class AppMenuResponse(
    @SerializedName("menuSubmenu")
    val menu: ArrayList<Menu>,
    @SerializedName("result")
    val result: Result,
    @SerializedName("isDndOn")
    val isDndOn: Boolean,
    @SerializedName("shareAppText")
    val shareText: String
)