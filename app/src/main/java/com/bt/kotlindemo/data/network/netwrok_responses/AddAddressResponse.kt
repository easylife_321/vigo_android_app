/*
 * *
 *  * Created by Surajit on 3/28/22, 6:33 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 3/28/22, 6:32 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.bt.kotlindemo.model.Result
import com.bt.kotlindemo.model.address.Address
import com.google.gson.annotations.SerializedName

data class AddAddressResponse(
    @SerializedName("address")
    val address: Address?,
    @SerializedName("result")
    val result: Result
)