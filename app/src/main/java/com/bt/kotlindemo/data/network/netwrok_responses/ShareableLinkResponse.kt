/*
 * *
 *  * Created by Surajit on 2/6/20 11:31 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 2/6/20 11:29 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.google.gson.annotations.SerializedName
import com.bt.kotlindemo.model.Result
import com.bt.kotlindemo.model.shareable_link.ShareableLink

data class ShareableLinkResponse(
    @SerializedName("links")
    val details: List<ShareableLink>?,
    @SerializedName("result")
    val result: Result
)