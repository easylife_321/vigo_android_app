/*
 * *
 *  * Created by Surajit on 14/4/20 6:27 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 13/4/20 6:44 PM
 *  *
 */

package com.bt.kotlindemo.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.bt.kotlindemo.data.datasource.NotificationDataSource
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.network.netwrok_responses.NotificationSettingsResponse
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.model.notifications.Notification
import com.bt.kotlindemo.model.notifications.NotificationResponse
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers


class NotificationRepository(
    val dataSource: NotificationDataSource,
    var pref: AppPreferences
) {


    fun getNotificationSettings(json: JsonObject): LiveData<ApiResult<NotificationSettingsResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.getNotificationSettings(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error("", null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun saveNotificationSettings(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.saveGroupAllNotifications(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error("", null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun getNotificationList(json: JsonObject): LiveData<ApiResult<NotificationResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.getNotificationList(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                        dataSource.addNotifications(
                            responseStatus.data.notifications ?: emptyList()
                        )
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error("", null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    suspend fun getNotifications(json: JsonObject) = dataSource.getNotificationList(json)

    fun addNotifications(notifications: List<Notification>) =
        dataSource.addNotifications(notifications)

    fun addNotification(notifications: Notification) =
        dataSource.addNotifications(notifications)


    fun getNotificationsFromDb(id: Int) = dataSource.getNotificationListFromDb(id)

    fun getNotificationsFromDb() = dataSource.getNotificationListFromDb()


    fun getLastTs() = dataSource.getLastTs()


    fun getNotificationType() = dataSource.getNotificationType()

    fun deleteSelectedTypeNotification(type: String, id: Int) =
        dataSource.deleteSelectedTypeNotification(type, id)

    fun deleteAllDeviceNotification( id: Int) =
        dataSource.deleteAllDeviceNotification(id)

    fun deleteAllTypeNotification(type: String) =
        dataSource.deleteAllTypeNotification(type)

    fun deleteAllNotification() =
        dataSource.deleteAllNotification()



}