/*
 * *
 *  * Created by Surajit on 14/4/20 6:02 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 13/4/20 6:44 PM
 *  *
 */

package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.constants.NotificationType
import com.bt.kotlindemo.data.database.dao.NotificationDao
import com.bt.kotlindemo.data.network.ApiService
import com.bt.kotlindemo.model.notifications.Notification
import com.google.gson.JsonObject

class NotificationDataSource(
    private val apiService: ApiService,
    private val notificationDao: NotificationDao
) : BaseDataSource() {

    suspend fun getNotificationSettings(obj: JsonObject) =
        getResult { apiService.getNotificationSettings(obj) }

    suspend fun saveGroupAllNotifications(obj: JsonObject) =
        getResult { apiService.saveGroupAllNotifications(obj) }

    suspend fun getNotificationList(obj: JsonObject) =
        getResult { apiService.getAllNotifications(obj) }

    fun addNotifications(notification: List<Notification>) {
        val notifications: List<com.bt.kotlindemo.data.database.entity.Notification> =
            notification.map {
                com.bt.kotlindemo.data.database.entity.Notification(
                    it.ntfId ?: "",
                    it.title ?: "",
                    it.msg ?: "",
                    it.ntfTime ?: 0,
                    "",
                    false,
                    it.ntfType ?: "",
                    it.userId ?: 0,
                    it.deviceId ?: 0,
                    it.lat ?: 0.0,
                    it.lng ?: 0.0,
                    it.iconType ?: 0,
                    it.speed ?: 0,
                    it.imgUrl ?: ""

                )
            }
        notificationDao.insertAll(notifications)
    }

    fun addNotifications(notification: Notification) {
        notification.apply {
            val data = com.bt.kotlindemo.data.database.entity.Notification(
                ntfId ?: "",
                title ?: "",
                msg ?: "",
                ntfTime ?: 0,
                "",
                false,
                ntfType ?: "",
                userId ?: 0,
                deviceId ?: 0,
                lat ?: 0.0,
                lng ?: 0.0,
                iconType ?: 0,
                speed ?: 0,
                imgUrl ?: ""

            )
            notificationDao.insert(data)
        }


    }


    fun getLastTs() = notificationDao.getLastTimeStamp()

    fun getNotificationListFromDb(id: Int) = notificationDao.getAllNotificationForDevice(id)

    fun getNotificationListFromDb() = notificationDao.getAllNotification()

    fun getNotificationType() = NotificationType.getNtfType()


    fun deleteSelectedTypeNotification(type: String, id: Int) =
        notificationDao.deleteSelectedType(type, id)

    fun deleteAllDeviceNotification(id: Int) =
        notificationDao.deleteAllDevice(id)

    fun deleteAllTypeNotification(type: String) =
        notificationDao.deleteAllType(type)

    fun deleteAllNotification() =
        notificationDao.deleteAll()


}