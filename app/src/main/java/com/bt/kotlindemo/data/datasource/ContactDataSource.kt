package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.database.dao.ContactsDao
import com.bt.kotlindemo.data.database.entity.Contacts
import com.bt.kotlindemo.data.network.ApiService
import com.bt.kotlindemo.model.Contact
import com.google.gson.JsonObject

class ContactDataSource(private val apiService: ApiService, private val contactsDao: ContactsDao) :
    BaseDataSource() {

    suspend fun syncContacts(obj: JsonObject) = getResult { apiService.syncContact(obj) }


    fun addContacts(friends: List<Contact>) {
        var contacts: List<Contacts> = friends.map {
            Contacts(
                it.userId,
                it.contactName?:"",
                it.registeredName?:"",
                it.contactNumber?:"",
                it.profilePic?:"",
                it.profilePicThumb?:""
            )
        }
        contactsDao.deleteAll()
        contactsDao.insertAll(contacts)
    }

    fun getFriends() = contactsDao.getAllRegisteredContacts()


}