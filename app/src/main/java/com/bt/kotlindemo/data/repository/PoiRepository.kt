/*
 * *
 *  * Created by Surajit on 23/5/20 2:54 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 3:40 PM
 *  *
 */

package com.bt.kotlindemo.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.data.datasource.PoiDataSource
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers


class PoiRepository(
    private val dataSource: PoiDataSource,
    private var pref: AppPreferences
) {


    fun addPoi(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.addPoi(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun updatePoi(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.updatePoi(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun deletePoi(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.deletePoi(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun getPoiList(json: JsonObject) =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.getPoiList(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }


}