/*
 * *
 *  * Created by Surajit on 8/4/20 8:04 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 5/4/20 6:36 PM
 *  *
 */

package com.bt.kotlindemo.data.repository.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.data.datasource.RegistrationDataSource
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.data.network.netwrok_responses.RegistrationResponse
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers


class DashboardRepository(var dataSource: RegistrationDataSource, var pref: AppPreferences) {


    fun getUserDevices(
        json: JsonObject
    ): LiveData<ApiResult<RegistrationResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.registerUser(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        pref.setMobile(responseStatus.data.detail[0].mobileNo)
                        pref.setUserName(responseStatus.data.detail[0].name)
                        pref.setUserId(responseStatus.data.detail[0].userId)
                        pref.setFireBaseToken(responseStatus.data.detail[0].firebaseToken)
                        pref.setRegistered(true)
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error("", null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }






}