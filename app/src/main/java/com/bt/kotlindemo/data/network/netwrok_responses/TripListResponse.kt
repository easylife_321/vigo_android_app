/*
 * *
 *  * Created by Surajit on 1/5/21 10:29 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/5/21 10:29 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.bt.kotlindemo.model.Result
import com.bt.kotlindemo.model.Trips
import com.google.gson.annotations.SerializedName

data class TripListResponse(
    @SerializedName("result")
    val result: Result,
    @SerializedName("tris")
    val trips: List<Trips>?
)