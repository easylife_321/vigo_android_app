package com.bt.kotlindemo.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.bt.kotlindemo.data.datasource.RegistrationDataSource
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.network.netwrok_responses.RegistrationResponse
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.utils.AppPreferences
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine


class RegistrationRepository(var dataSource: RegistrationDataSource, var pref: AppPreferences) {


    fun registerUser(
        json: JsonObject
    ): LiveData<ApiResult<RegistrationResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.registerUser(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        pref.setMobile(responseStatus.data.detail[0].mobileNo)
                        pref.setUserName(responseStatus.data.detail[0].name)
                        pref.setUserId(responseStatus.data.detail[0].userId)
                        pref.setFireBaseToken(responseStatus.data.detail[0].firebaseToken)
                        pref.setProfileImage(responseStatus.data.detail[0].profilePic ?: "")
                        pref.setAddress1(responseStatus.data.detail[0].addressLine1 ?: "")
                        pref.setAddress2(responseStatus.data.detail[0].addressLine2 ?: "")
                        pref.setPin(responseStatus.data.detail[0].pin ?: "")
                        pref.setStateId(responseStatus.data.detail[0].stateId ?: 0)
                        pref.setState(responseStatus.data.detail[0].stateName ?: "")
                        pref.setEmail(responseStatus.data.detail[0].emailId ?: "")
                        pref.setRegistered(true)
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun login(
        json: JsonObject
    ): LiveData<ApiResult<RegistrationResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.login(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        pref.setMobile(responseStatus.data.detail[0].mobileNo)
                        pref.setUserName(responseStatus.data.detail[0].name)
                        pref.setUserId(responseStatus.data.detail[0].userId)
                        pref.setFireBaseToken(responseStatus.data.detail[0].firebaseToken)
                        // pref.setPassword(responseStatus.data.detail[0].password)
                        pref.setProfileImage(responseStatus.data.detail[0].profilePic ?: "")
                        pref.setAddress1(responseStatus.data.detail[0].addressLine1 ?: "")
                        pref.setAddress2(responseStatus.data.detail[0].addressLine2 ?: "")
                        pref.setPin(responseStatus.data.detail[0].pin ?: "")
                        pref.setStateId(responseStatus.data.detail[0].stateId ?: 0)
                        pref.setState(responseStatus.data.detail[0].stateName ?: "")
                        pref.setEmail(responseStatus.data.detail[0].emailId ?: "")
                        pref.setRegistered(true)
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.errorCustom(responseStatus.data.result.msg))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun sendOtp(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.getOtp(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> emit(ApiResult.success(responseStatus.data))
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun loginWithOtp(json: JsonObject): LiveData<ApiResult<RegistrationResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.loginWithOtp(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        pref.setMobile(responseStatus.data.detail[0].mobileNo)
                        // pref.setPassword(responseStatus.data.detail[0].password)
                        pref.setUserName(responseStatus.data.detail[0].name)
                        pref.setUserId(responseStatus.data.detail[0].userId)
                        pref.setFireBaseToken(responseStatus.data.detail[0].firebaseToken)
                        pref.setProfileImage(responseStatus.data.detail[0].profilePic ?: "")
                        pref.setAddress1(responseStatus.data.detail[0].addressLine1 ?: "")
                        pref.setAddress2(responseStatus.data.detail[0].addressLine2 ?: "")
                        pref.setPin(responseStatus.data.detail[0].pin ?: "")
                        pref.setStateId(responseStatus.data.detail[0].stateId ?: 0)
                        pref.setState(responseStatus.data.detail[0].stateName ?: "")
                        pref.setEmail(responseStatus.data.detail[0].emailId ?: "")
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }


    fun setPassword(
        json: JsonObject
    ): LiveData<ApiResult<RegistrationResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.setPassword(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun changePassword(
        json: JsonObject
    ): LiveData<ApiResult<RegistrationResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.changePassword(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }





}