/*
 * *
 *  * Created by Surajit on 30/5/20 2:33 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/3/20 3:26 PM
 *  *
 */

package com.bt.kotlindemo.data.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bt.kotlindemo.data.database.entity.Contacts

@Dao
interface ContactsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(device: Contacts)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(device: List<Contacts>)

    @Query("delete from contacts")
    fun deleteAll()

    @Query("select * from contacts where user_id!=0")
    fun getAllRegisteredContacts(): LiveData<List<Contacts>>
}