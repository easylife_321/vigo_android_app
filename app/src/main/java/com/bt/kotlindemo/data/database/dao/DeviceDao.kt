/*
 * *
 *  * Created by Surajit on 30/5/20 2:33 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/3/20 3:26 PM
 *  *
 */

package com.bt.kotlindemo.data.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bt.kotlindemo.data.database.entity.Device

@Dao
interface DeviceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(device: Device)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(device: List<Device>)

    @Query("select * from devices where isAdmin=1")
    fun getDevicesAdmin(): LiveData<List<Device>>

    @Query("select * from devices")
    fun getDevices(): List<Device>

    @Query("delete  from devices where deviceId=:deviceId")
    fun deleteDevice(deviceId: Int)
}