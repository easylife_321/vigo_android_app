/*
 * *
 *  * Created by Surajit on 27/5/20 11:13 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 7:23 PM
 *  *
 */

package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject

class ShareableLinkDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun addLink(obj: JsonObject) = getResult { apiService.addLink(obj) }

    suspend fun deleteLink(obj: JsonObject) = getResult { apiService.deleteLink(obj) }

    suspend fun toggle(obj: JsonObject) = getResult { apiService.toggle(obj) }

    suspend fun getLinkList(obj: JsonObject) = getResult { apiService.getLinkList(obj) }



}