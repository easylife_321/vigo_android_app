/*
 * *
 *  * Created by Surajit on 27/5/20 11:13 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/5/20 8:15 PM
 *  *
 */

package com.bt.kotlindemo.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.data.datasource.ShareableLinkDataSource
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers


class ShareableLinkRepository(
    private val dataSource: ShareableLinkDataSource,
    private var pref: AppPreferences
) {


    fun addLink(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.addLink(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }


    fun deleteLink(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.deleteLink(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun toggle(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.toggle(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun getLinkList(json: JsonObject) =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.getLinkList(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }


}