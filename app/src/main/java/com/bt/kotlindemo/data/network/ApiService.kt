package com.bt.kotlindemo.data.network

import com.bt.kotlindemo.data.network.netwrok_responses.*
import com.bt.kotlindemo.model.notifications.NotificationResponse
import com.bt.kotlindemo.model.shared_users.SharedUserResponse
import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*


interface ApiService {


    //app data
    @POST("/api/getbaseurl")
    suspend fun getBaseUrl(@Body jsonObject: JsonObject): Response<BaseUrlResponse>

    @Headers("content-type: application/json;charset=UTF-8")
    @FormUrlEncoded
    @POST("requestToken")
    suspend fun getToken(
        @Field("grant_type") type: String?,
        @Field("username") userName: String?,
        @Field("password") password: String?
    ): Response<BaseUrlResponse>

    @POST("/api/appData")
    suspend fun getMenu(@Body jsonObject: JsonObject): Response<AppMenuResponse>


    //user
    @POST("/api/register")
    suspend fun registerUser(@Body jsonObject: JsonObject): Response<RegistrationResponse>

    @POST("/api/sendOTPForRegistration")
    suspend fun getOtp(@Body jsonObject: JsonObject): Response<BaseResponse>

    @POST("/api/login")
    suspend fun login(@Body obj: JsonObject): Response<RegistrationResponse>

    @POST("/api/loginWithOTP")
    suspend fun loginWithOtp(@Body obj: JsonObject): Response<RegistrationResponse>

    @POST("/api/setPassword")
    suspend fun setPassword(@Body obj: JsonObject): Response<RegistrationResponse>

    @POST("/api/changePassword")
    suspend fun changePassword(@Body obj: JsonObject): Response<RegistrationResponse>


    //group
    @POST("/api/createGroup")
    suspend fun createGroup(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/removeGroup")
    suspend fun deleteGroup(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/GetUserGroups")
    suspend fun getGroup(@Query("UserId") id: Int): Response<GetGroupResponse>

    @POST("/api/updateGroup")
    suspend fun updateGroup(@Body obj: JsonObject): Response<BaseResponse>


    //device
    @POST("/api/AddUserDevice")
    suspend fun addDevice(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/UpdateUserDevice")
    suspend fun updateDevice(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/DeleteUserDevice")
    suspend fun deleteDevice(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/getUserDevices")
    suspend fun getDevices(@Body obj: JsonObject): Response<DeviceListResponse>


    @POST("/api/DeviceHistory_mongo")
    suspend fun getDeviceHistory(@Body obj: JsonObject): Response<DeviceHistoryResponse>

    @POST("/api/DeviceCurrentTracking")
    suspend fun getDeviceCurrentData(@Body obj: JsonObject): Response<RegistrationResponse>

    @POST("/api/ShareUserDevice")
    suspend fun shareDevice(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/MoveDevicesToOtherGroup")
    suspend fun moveDevice(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/ShareUserDeviceTimespecific")
    suspend fun shareDeviceTimeSpecific(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/getDevicesSharedwithInfo")
    suspend fun getDevicesSharedDetails(@Body obj: JsonObject): Response<SharedUserResponse>

    @POST("/api/createRoute")
    suspend fun saveRoute(@Body obj: JsonObject): Response<BaseResponse>


    //notification
    @POST("/api/getGroupAllNotificationsSettings")
    suspend fun getNotificationSettings(@Body obj: JsonObject): Response<NotificationSettingsResponse>

    @POST("/api/saveGroupAllNotifications")
    suspend fun saveGroupAllNotifications(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/getAllNotifications")
    suspend fun getAllNotifications(@Body obj: JsonObject): Response<NotificationResponse>


    //device command
    @POST("/api/GetAllAvlCommands")
    suspend fun getDeviceCommands(@Body obj: JsonObject): Response<AllDeviceCommandResponse>

    @POST("/api/SetSpeedLimit")
    suspend fun setSpeedLimit(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/setOdometerValue")
    suspend fun setOdometer(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/toggleAntiTheft")
    suspend fun setAntiTheft(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/toggleParkingLocation")
    suspend fun markAsParked(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/toggleUntraceable")
    suspend fun markAsUntraceable(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/DeviceEngineCut")
    suspend fun deviceEngineCut(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/shareableLink")
    suspend fun shareableLink(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/deviceAutoTroubleshoot")
    suspend fun deviceAutoTroubleshoot(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/refreshGPS")
    suspend fun refreshGps(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/toggleSharedDevicesSharing")
    suspend fun toggleSharedDevicesSharing(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/toggleGpsDeviceAccessory")
    suspend fun toggleGpsDeviceAccessory(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/followDevice")
    suspend fun followDevice(@Body obj: JsonObject): Response<BaseResponse>


    //poi
    @POST("/api/CreateFavPlace")
    suspend fun addPoi(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/UpdateFavPlace")
    suspend fun updatePoi(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/DeleteFavPlace")
    suspend fun deletePoi(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/GetFavPlaceList")
    suspend fun getPoiList(@Body obj: JsonObject): Response<PoiListResponse>


    //shareable Link
    @POST("/api/shareableLink")
    suspend fun addLink(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/shareableLinkGetAll")
    suspend fun getLinkList(@Body obj: JsonObject): Response<ShareableLinkResponse>

    @POST("/api/deleteShareableLink")
    suspend fun deleteLink(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/shareableLinkToggleStatus")
    suspend fun toggle(@Body obj: JsonObject): Response<BaseResponse>


    //zone
    @POST("/api/CreateZone")
    suspend fun addZone(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/UpdateZone")
    suspend fun updateZone(@Body obj: JsonObject): Response<BaseResponse>

    @GET("/api/GetZoneList")
    suspend fun getZoneList(@Query("userId") userId: String): Response<ZoneListResponse>

    @POST("/api/DeleteZone")
    suspend fun deleteZone(@Body obj: JsonObject): Response<BaseResponse>


    //trips
    @POST("/api/StartTrip")
    suspend fun startTrip(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/EndTrip")
    suspend fun endTrip(@Body obj: JsonObject): Response<BaseResponse>

    @POST("/api/GetUserTrips")
    suspend fun tripList(@Body obj: JsonObject): Response<TripListResponse>

    //contacts
    @POST("/api/syncContact")
    suspend fun syncContact(@Body obj: JsonObject): Response<ContactSyncResponse>


    //settings
    @Multipart
    @POST("api/Feedback")
    suspend fun feedback(
        @Part("userId") userId: RequestBody,
        @Part("typeId") typeId: RequestBody,
        @Part("details") details: RequestBody,
        @Part("itemId") itemId: RequestBody,
        @Part files: List<MultipartBody.Part?>
    ): Response<BaseResponse>

    @Multipart
    @POST("api/updateProfileImage")
    suspend fun updateProfileImage(
        @Part("userId") userId: RequestBody,
        @Part file: MultipartBody.Part?
    ): Response<UpdateProfileImageResponse>


    @POST("/api/GetFeedbackItem")
    suspend fun getFeedBackItems(): Response<FeedbackItemListResponse>

    @POST("/api/UpdateUserProfile")
    suspend fun updateProfile(@Body obj: JsonObject): Response<BaseResponse>


    @POST("/api/GetStateList")
    suspend fun getStateList(@Body jsonObject: JsonObject): Response<StateListResponse>


    @POST("/api/getCountryList")
    suspend fun getCountryList(): Response<CountryListResponse>

    @POST("/api/SendOTPForChangeMobileNo")
    suspend fun sendOtpNumberChange(@Body jsonObject: JsonObject): Response<BaseResponse>

    @POST("/api/changeAccountMobileNo")
    suspend fun changeAccountMobileNo(@Body jsonObject: JsonObject): Response<BaseResponse>

    @POST("/api/UserLogout")
    suspend fun logout(@Body jsonObject: JsonObject): Response<BaseResponse>


    @POST("/api/doNotDisturb")
    suspend fun doNotDisturb(@Body jsonObject: JsonObject): Response<BaseResponse>

    @POST("/api/updateGeoAddress")
    suspend fun saveAndGetAddress(@Body jsonObject: JsonObject): Response<AddAddressResponse>
}