/*
 * *
 *  * Created by Surajit on 9/4/20 5:20 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 4/4/20 7:13 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses

import com.bt.kotlindemo.model.Result
import com.bt.kotlindemo.model.register.UserDetail
import com.google.gson.annotations.SerializedName

data class RegistrationResponse(
    @SerializedName("result")
    val result: Result,
    @SerializedName("userdetails")
    val detail: List<UserDetail>
)