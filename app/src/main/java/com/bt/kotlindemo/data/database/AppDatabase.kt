package com.bt.kotlindemo.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bt.kotlindemo.data.database.dao.ContactsDao
import com.bt.kotlindemo.data.database.dao.DeviceDao
import com.bt.kotlindemo.data.database.dao.NotificationDao
import com.bt.kotlindemo.data.database.entity.Contacts
import com.bt.kotlindemo.data.database.entity.Device
import com.bt.kotlindemo.data.database.entity.Notification

@Database(
    entities = [Device::class, Contacts::class, Notification::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getDeviceDao(): DeviceDao

    abstract fun getContactsDao(): ContactsDao

    abstract fun getNotificationDao(): NotificationDao


}