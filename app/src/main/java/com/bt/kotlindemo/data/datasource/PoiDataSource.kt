/*
 * *
 *  * Created by Surajit on 23/5/20 2:54 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 3:40 PM
 *  *
 */

package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject

class PoiDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun addPoi(obj: JsonObject) = getResult { apiService.addPoi(obj) }

    suspend fun updatePoi(obj: JsonObject) = getResult { apiService.updatePoi(obj) }

    suspend fun deletePoi(obj: JsonObject) = getResult { apiService.deletePoi(obj) }


    suspend fun getPoiList(obj: JsonObject) = getResult { apiService.getPoiList(obj) }


}