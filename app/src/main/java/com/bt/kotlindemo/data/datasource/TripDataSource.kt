/*
 * *
 *  * Created by Surajit on 23/5/20 2:54 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 3:40 PM
 *  *
 */

package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject

class TripDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun startTrip(obj: JsonObject) = getResult { apiService.startTrip(obj) }

    suspend fun endTrip(obj: JsonObject) = getResult { apiService.endTrip(obj) }

    suspend fun deleteZone(obj: JsonObject) = getResult { apiService.deleteZone(obj) }

    suspend fun getTripList(obj: JsonObject) = getResult { apiService.tripList(obj) }


}