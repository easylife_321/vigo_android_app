/*
 * *
 *  * Created by Surajit on 9/4/20 5:20 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 2/4/20 8:50 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses

import com.bt.kotlindemo.model.Result
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class BaseUrlResponse(
    @SerializedName("android_ver")
    @Expose
    val androidVer: Int,
    @SerializedName("base_url")
    @Expose
    val baseUrl: String,
    @SerializedName("image_base_url")
    @Expose
    val imageBaseUrl: String,
    @SerializedName("is_mandatory_update")
    @Expose
    val isMandatoryUpdate: Boolean,
    @SerializedName("mandatoryUpdate_msg")
    @Expose
    val mandatoryUpdateMsg: String,
    @SerializedName("is_under_maintenance")
    @Expose
    val isUnderMaintenance: Boolean,
    /* @SerializedName("undermaintenance_msg")
     @Expose
     val underMaintenanceMsh: String,*/
    @SerializedName("result")
    @Expose
    val result: Result
)


