/*
 * *
 *  * Created by Surajit on 10/4/20 2:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 10/4/20 2:47 PM
 *  *
 */

package com.bt.kotlindemo.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.data.datasource.GroupDataSource
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.network.netwrok_responses.GetGroupResponse
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers


class GroupRepository(
    private val dataSource: GroupDataSource,
    private var pref: AppPreferences
) {


    fun createGroup(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.createGroup(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error("", null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }


    fun getGroups(): LiveData<ApiResult<GetGroupResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.getGroup(pref.getUserId())
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error("", null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun deleteGroup(obj: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.deleteGroup(obj)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error("", null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun updateGroup(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.updateGroup(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error("", null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }


}