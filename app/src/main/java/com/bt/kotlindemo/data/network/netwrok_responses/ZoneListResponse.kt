/*
 * *
 *  * Created by Surajit on 25/5/20 8:12 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 10/4/20 3:31 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses

import com.bt.kotlindemo.model.Group
import com.bt.kotlindemo.model.Poi
import com.bt.kotlindemo.model.Result
import com.bt.kotlindemo.model.zone.Zone
import com.google.gson.annotations.SerializedName

data class ZoneListResponse(
    @SerializedName("result")
    val result: Result,
    @SerializedName("zoneList")
    val zones: ArrayList<Zone>
)