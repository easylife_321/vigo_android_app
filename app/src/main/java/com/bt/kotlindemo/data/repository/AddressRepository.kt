package com.bt.kotlindemo.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.bt.kotlindemo.data.datasource.AddressDataSource
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.data.network.netwrok_responses.AddAddressResponse
import com.bt.kotlindemo.model.ApiResult
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers


class AddressRepository(
    val dataSource: AddressDataSource
) {


    suspend fun saveAddress(json: JsonObject): ApiResult<AddAddressResponse> =
        dataSource.saveAndGetAddress(json)


    suspend fun getAddress(json: JsonObject): LiveData<ApiResult<AddAddressResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.saveAndGetAddress(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> emit(ApiResult.success(responseStatus.data))
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error("", null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }


}