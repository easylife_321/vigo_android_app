package com.bt.kotlindemo.data.datasource

import com.bt.kotlindemo.data.network.ApiService
import com.google.gson.JsonObject

class AppDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun getBaseUrl(obj: JsonObject) = getResult { apiService.getBaseUrl(obj) }

    suspend fun getToken(obj: JsonObject) = getResult { apiService.getBaseUrl(obj) }

    suspend fun getMenu(obj: JsonObject) = getResult { apiService.getMenu(obj) }


}