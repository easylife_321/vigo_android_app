/*
 * *
 *  * Created by Surajit on 15/5/20 4:55 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 15/5/20 4:41 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.google.gson.annotations.SerializedName
import com.bt.kotlindemo.model.Result
import com.bt.kotlindemo.model.device_command.Commands

data class AllDeviceCommandResponse(
    @SerializedName("result")
    val result: Result,
    @SerializedName("values")
    val commands: List<Commands>
)