/*
 * *
 *  * Created by Surajit on 28/7/20 12:56 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 28/7/20 12:56 AM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.bt.kotlindemo.model.Contact
import com.bt.kotlindemo.model.Result
import com.google.gson.annotations.SerializedName

data class ContactSyncResponse(
    @SerializedName("friends")
    val friends: List<Contact>?,
    @SerializedName("result")
    val result: Result
)