/*
 * *
 *  * Created by Surajit on 28/7/20 6:20 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 28/7/20 6:20 PM
 *  *
 */
package com.bt.kotlindemo.data.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.bt.kotlindemo.utils.DateTimeUtil
import com.bt.kotlindemo.utils.toUtc

@Entity(tableName = "notifications")
data class Notification(
    @PrimaryKey
    var id: String = "",
    var title: String = "",
    var message: String? = "",
    @ColumnInfo(name = "timestamp")
    var timeStamp: Long = 0,
    var name: String? = "",
    var read: Boolean = false,
    var type: String = "",
    @ColumnInfo(name = "user_id")
    var userId: Int = 0,
    @ColumnInfo(name = "device_id")
    var deviceId: Int = 0,
    var lat: Double = 0.0,
    var lng: Double = 0.0,
    var iconType: Int = 0,
    var speed: Int = 0,
    @ColumnInfo(name = "img_url")
    var imgUrl: String = "",
    @Ignore
    var isHeader: Boolean = false,
    @Ignore
    var headerText: String = ""

)