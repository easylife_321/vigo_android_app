package com.bt.kotlindemo.data.repository

import androidx.lifecycle.liveData
import com.bt.kotlindemo.data.datasource.ContactDataSource
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers


class ContactRepository(
    val dataSource: ContactDataSource,
    var pref: AppPreferences
) {

    fun syncContact(json: JsonObject) =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.syncContacts(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        if (responseStatus.data.friends != null) {
                            dataSource.addContacts(responseStatus.data.friends)
                        }
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun getFriends() = dataSource.getFriends()


}