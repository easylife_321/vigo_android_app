/*
 * *
 *  * Created by Surajit on 29/6/20 11:56 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/6/20 11:56 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses



import com.google.gson.annotations.SerializedName
import com.bt.kotlindemo.model.Result
import com.bt.kotlindemo.model.device_history.History

data class DeviceHistoryResponse(
    @SerializedName("result")
    val result: Result,
    @SerializedName("values")
    val history: List<History>
)