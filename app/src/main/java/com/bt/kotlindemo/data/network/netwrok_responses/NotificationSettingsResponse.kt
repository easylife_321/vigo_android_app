/*
 * *
 *  * Created by Surajit on 14/4/20 5:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 14/4/20 5:59 PM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.bt.kotlindemo.model.NotificationSettings
import com.google.gson.annotations.SerializedName
import com.bt.kotlindemo.model.Result

data class NotificationSettingsResponse(
    @SerializedName("ntf")
    val ntf: List<NotificationSettings>,
    @SerializedName("result")
    val result: Result
)