/*
 * *
 *  * Created by Surajit on 10/4/20 2:34 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 5:21 PM
 *  *
 */

package com.bt.kotlindemo.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.bt.kotlindemo.data.datasource.DeviceDataSource
import com.bt.kotlindemo.data.network.ApiCode
import com.bt.kotlindemo.data.network.netwrok_responses.BaseResponse
import com.bt.kotlindemo.data.network.netwrok_responses.DeviceListResponse
import com.bt.kotlindemo.model.ApiResult
import com.bt.kotlindemo.utils.AppPreferences
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers


class DeviceRepository(
    private val dataSource: DeviceDataSource,
    private var pref: AppPreferences
) {


    fun addDevice(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.addDevice(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun updateDevice(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.updateDevice(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                    ApiCode.ERROR -> emit(ApiResult.error(responseStatus.data.result.msg, null))
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }

    fun getDeviceList(json: JsonObject): LiveData<ApiResult<DeviceListResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val response = dataSource.getDeviceList(json)
            if (response.status == ApiResult.Status.SUCCESS) {
                when (response.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        dataSource.addDevices(response.data.devices)
                        emit(ApiResult.success(response.data))
                    }
                    else -> {
                        emit(ApiResult.errorCustom(response.data.result.msg, null))
                    }
                }
            } else if (response.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(response.message!!))
            }
        }


    fun shareDevice(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.shareDevice(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(
                        ApiResult.error(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                    ApiCode.ERROR -> emit(
                        ApiResult.error(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }


    fun shareDeviceTimeSpecific(json: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.shareDeviceTimeSpecific(json)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(
                        ApiResult.error(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                    ApiCode.ERROR -> emit(
                        ApiResult.error(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }
        }


    fun deleteDevice(jsonObject: JsonObject, deviceId: Int): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.deleteDevice(jsonObject)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        deleteDevice(deviceId)
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(
                        ApiResult.errorCustom(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                    ApiCode.ERROR -> emit(
                        ApiResult.error(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }


        }


    fun moveDevice(jsonObject: JsonObject): LiveData<ApiResult<BaseResponse>> =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.moveDevice(jsonObject)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(
                        ApiResult.errorCustom(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                    ApiCode.ERROR -> emit(
                        ApiResult.error(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }


        }

    fun getHistory(jsonObject: JsonObject) =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.getDeviceHistory(jsonObject)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(
                        ApiResult.errorCustom(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                    ApiCode.ERROR -> emit(
                        ApiResult.error(
                            responseStatus.data.result.msg,
                            null
                        )
                    )
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }


        }

    fun getDevicesSharedDetails(jsonObject: JsonObject) =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.getDevicesSharedDetails(jsonObject)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result?.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(
                        ApiResult.errorCustom(
                            responseStatus.data.result?.msg,
                            null
                        )
                    )
                    ApiCode.ERROR -> emit(
                        ApiResult.error(
                            responseStatus.data.result?.msg ?: "",
                            null
                        )
                    )
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }


        }

    fun saveRoute(jsonObject: JsonObject) =
        liveData(Dispatchers.IO) {
            emit(ApiResult.loading())
            val responseStatus = dataSource.saveRoute(jsonObject)
            if (responseStatus.status == ApiResult.Status.SUCCESS) {
                when (responseStatus.data!!.result?.code) {
                    ApiCode.SUCCESS -> {
                        emit(ApiResult.success(responseStatus.data))
                    }
                    ApiCode.CUSTOM -> emit(
                        ApiResult.errorCustom(
                            responseStatus.data.result?.msg,
                            null
                        )
                    )
                    ApiCode.ERROR -> emit(
                        ApiResult.error(
                            responseStatus.data.result?.msg ?: "",
                            null
                        )
                    )
                }
            } else if (responseStatus.status == ApiResult.Status.ERROR) {
                emit(ApiResult.error(responseStatus.message!!))

            }


        }


    fun getDevicesAdmin() = dataSource.getDevicesAdmin()

    fun getDevices() = dataSource.getDevices()

    fun getIcons() = dataSource.getDeviceIcons()

    fun deleteDevice(deviceId: Int) = dataSource.deleteDevice(deviceId)


}