/*
 * *
 *  * Created by Surajit on 1/3/21 1:12 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/3/21 1:11 AM
 *  *
 */

package com.bt.kotlindemo.data.network.netwrok_responses


import com.bt.kotlindemo.model.Item
import com.bt.kotlindemo.model.Result
import com.google.gson.annotations.SerializedName

data class FeedbackItemListResponse(
    @SerializedName("item")
    val item: List<Item>?,
    @SerializedName("result")
    val result: Result
)