/*
 * *
 *  * Created by Surajit on 28/7/20 6:27 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 2/7/20 5:00 PM
 *  *
 */

package com.bt.kotlindemo.data.database.entity


import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.bt.kotlindemo.model.device.Accessory
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "devices")
data class Device(
    @SerializedName("batteryPer")
    var batteryPer: Int = 0,
    @PrimaryKey
    @SerializedName("deviceId")
    var deviceId: Int = 0,
    @SerializedName("deviceName")
    var deviceName: String = "",
    @SerializedName("driverName")
    var driverName: String = "",
    @SerializedName("driverNumber")
    var driverNumber: String = "",
    @SerializedName("gpsTime")
    var gpsTime: String = "",
    @SerializedName("gpsTimestamp")
    var gpsTimeStamp: Long = 0,
    @SerializedName("groupId")
    var groupid: Int = 0,
    @SerializedName("imei")
    var imei: String = "",
    @SerializedName("inActiveReason")
    var inActiveReason: String = "",
    @SerializedName("isActive")
    var isActive: Boolean = false,
    @SerializedName("isParked")
    var isParked: Boolean = false,
    @SerializedName("lng")
    var lang: Double = 0.0,
    @SerializedName("lat")
    var lat: Double = 0.0,
    @SerializedName("prevLng")
    var prvLang: Double = 0.0,
    @SerializedName("prevLat")
    var prvLat: Double = 0.0,
    @SerializedName("movingStatusLong")
    var movingStatusLong: String = "",
    @SerializedName("movingStatusShort")
    var movingStatusShort: String = "",
    @SerializedName("parkedLang")
    var parkedLang: Double = 0.0,
    @SerializedName("parkedLat")
    var parkedLat: Double = 0.0,
    @SerializedName("speed")
    var speed: Int = 0,
    @SerializedName("validUptoDate")
    var validUptoDate: String = "",
    @SerializedName("validUptoTimestamp")
    var validUptoTimeStamp: Long = 0,
    @SerializedName("vehicleRegNo")
    var vehicleRegNo: String = "",
    @SerializedName("isAdmin")
    var isAdmin: Boolean = false,
    @SerializedName("sharedByName")
    var sharedByName: String = "",
    @Ignore
    @SerializedName("accessories")
    var accessories: List<Accessory>? = null,
    @SerializedName("deviceIconType")
    var deviceIcon: Int = 0,
    @SerializedName("deviceNickname")
    var deviceNickName: String = "",
    @Ignore
    var isSelected: Boolean = false,
    @Ignore
    var address: String = "",
    @SerializedName("isGprsConnected")
    var isGprsConnected: Boolean = false,
    @SerializedName("isGpsConnected")
    var isGpsConnected: Boolean = false,
    @SerializedName("isGpsWeak")
    var isGpsWeak: Boolean = false,
    @SerializedName("satellite")
    var satellite: Int = 0,
    @SerializedName("mileage")
    var odometer: Long = 0
) : Serializable
