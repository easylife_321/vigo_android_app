package com.bt.kotlindemo.data.network

class ApiCode {
    companion object {
        const val SUCCESS = 1
        const val CUSTOM = 2
        const val ERROR = 0
    }
}