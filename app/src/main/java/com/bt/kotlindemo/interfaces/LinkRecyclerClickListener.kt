/*
 * *
 *  * Created by Surajit on 25/5/20 8:29 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/4/20 4:30 PM
 *  *
 */

package com.bt.kotlindemo.interfaces

interface LinkRecyclerClickListener {
    fun onClick(data: Any, action: Int)

    companion object {
        const val OPEN_SHEE = 1
        const val ENABLE_DISABLE = 2
    }
}