/*
 * *
 *  * Created by Surajit on 10/4/20 3:50 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/3/20 3:26 PM
 *  *
 */

package com.bt.kotlindemo.interfaces

interface DateSelectionCallback {
    fun dateSelected(milis:Long)
}