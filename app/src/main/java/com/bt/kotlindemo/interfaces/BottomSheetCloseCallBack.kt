/*
 * *
 *  * Created by Surajit on 20/5/20 6:58 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/4/20 12:05 PM
 *  *
 */

package com.bt.kotlindemo.interfaces

interface BottomSheetCloseCallBack {
    fun close()
    fun ignore()
}