package com.bt.kotlindemo.interfaces


interface IPermissionGrantedDeniedCallback {

    fun onPermissionGrantedOrDenied(permission:String, isGranted: Boolean)

    fun afterCompletePermissionDenied()
}