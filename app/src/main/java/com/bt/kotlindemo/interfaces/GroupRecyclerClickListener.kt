/*
 * *
 *  * Created by Surajit on 13/4/20 6:07 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/3/20 3:26 PM
 *  *
 */

package com.bt.kotlindemo.interfaces

interface GroupRecyclerClickListener {
    fun onClick(data: Any, action: Int)

    companion object {
        const val DELETE = 1
        const val SHARE = 2
        const val EDIT = 3
        const val NOTIFICATION = 4
        const val MOVE = 5
    }
}