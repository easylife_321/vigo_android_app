/*
 * *
 *  * Created by Surajit on 26/12/20 4:33 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 26/12/20 4:33 PM
 *  *
 */

package com.bt.kotlindemo.interfaces

import com.bt.kotlindemo.model.Group

interface DeviceListCallback {

    fun openList(devices: ArrayList<Group>)
}