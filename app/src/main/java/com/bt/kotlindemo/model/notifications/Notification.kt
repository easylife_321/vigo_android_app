/*
 * *
 *  * Created by Surajit on 1/24/21 1:21 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/24/21 1:21 AM
 *  *
 */

package com.bt.kotlindemo.model.notifications


import com.google.gson.annotations.SerializedName

data class Notification(
    @SerializedName("deviceId")
    val deviceId: Int?,
    @SerializedName("icontType")
    val iconType: Int?,
    @SerializedName("lat")
    val lat: Double?,
    @SerializedName("lng")
    val lng: Double?,
    @SerializedName("msg")
    val msg: String?,
    @SerializedName("ntfId")
    val ntfId: String?,
    @SerializedName("ntfTime")
    val ntfTime: Long?,
    @SerializedName("ntfType")
    val ntfType: String?,
    @SerializedName("speed")
    val speed: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("userId")
    val userId: Int?,
    @SerializedName("imgUrl")
    val imgUrl: String?,
    @SerializedName("soundType")
    val soundType: Int?
)