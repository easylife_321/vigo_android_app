/*
 * *
 *  * Created by Surajit on 7/4/20 7:43 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 7/4/20 7:43 PM
 *  *
 */

package com.bt.kotlindemo.model

data class MapMenuOption(
    var title: String,
    var resource: Int,
    var id: Int
)