/*
 * *
 *  * Created by Surajit on 27/12/20 4:42 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/12/20 4:42 PM
 *  *
 */

package com.bt.kotlindemo.model

data class NotificationTypeModel(
    val title: String,
    val id: String
)