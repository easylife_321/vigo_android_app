/*
 * *
 *  * Created by Surajit on 1/16/21 10:27 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/16/21 10:27 PM
 *  *
 */

package com.bt.kotlindemo.model.shared_users


import com.google.gson.annotations.SerializedName

data class Shift(
    @SerializedName("from")
    val from: String?,
    @SerializedName("to")
    val to: String?
)