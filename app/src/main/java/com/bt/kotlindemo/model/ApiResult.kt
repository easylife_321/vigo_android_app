package com.bt.kotlindemo.model

data class ApiResult<out T>(val status: Status, val data: T?, val message: String?) {

    enum class Status {
        SUCCESS,
        ERROR,
        CUSTOM,
        LOADING
    }

    companion object {
        fun <T> success(data: T): ApiResult<T> {
            return ApiResult(Status.SUCCESS, data, null)
        }

        fun <T> error(message: String, data: T? = null): ApiResult<T> {
            return ApiResult(Status.ERROR, data, message)
        }

        fun <T> errorCustom(message: String?, data: T? = null): ApiResult<T> {
            return ApiResult(Status.CUSTOM, null, message)
        }

        fun <T> loading(): ApiResult<T> {
            return ApiResult(Status.LOADING, null, null)
        }
    }
}