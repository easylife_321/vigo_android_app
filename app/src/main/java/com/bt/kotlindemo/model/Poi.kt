/*
 * *
 *  * Created by Surajit on 25/5/20 8:03 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 15/4/20 5:20 PM
 *  *
 */

package com.bt.kotlindemo.model


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Poi(
    @SerializedName("favPlaceId")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("lat")
    val lat: Double,
    @SerializedName("lng")
    val lng: Double
) : Serializable