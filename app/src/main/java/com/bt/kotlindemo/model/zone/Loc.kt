/*
 * *
 *  * Created by Surajit on 16/6/20 8:44 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 16/6/20 8:44 PM
 *  *
 */

package com.bt.kotlindemo.model.zone


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Loc(
    @SerializedName("lat")
    val lat: Double,
    @SerializedName("lng")
    val lng: Double
):Serializable