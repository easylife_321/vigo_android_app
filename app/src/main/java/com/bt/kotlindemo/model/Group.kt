/*
 * *
 *  * Created by Surajit on 9/4/20 5:17 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 9/4/20 5:17 PM
 *  *
 */

package com.bt.kotlindemo.model


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Group(
    @SerializedName("groupId")
    val id: Int,
    @SerializedName("groupName")
    val name: String,
    @SerializedName("deviceCount")
    val deviceCount: Int
) : Serializable