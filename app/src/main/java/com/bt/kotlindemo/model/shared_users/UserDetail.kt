/*
 * *
 *  * Created by Surajit on 1/16/21 10:27 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/16/21 10:27 PM
 *  *
 */

package com.bt.kotlindemo.model.shared_users


import com.google.gson.annotations.SerializedName

data class UserDetail(
    @SerializedName("day")
    val day: Day?=null,
    @SerializedName("name")
    val name: String?=null,
    @SerializedName("phoneName")
    val phoneName: String?=null,
    @SerializedName("profilePic")
    val profilePic: String?=null,
    @SerializedName("shareType")
    val shareType: String?=null,
    //@SerializedName("shift")
    //val shift: Shift?=null,
    @SerializedName("userid")
    val userId: Int?=null,
    var isHeader:Boolean=false,
    var headerText:String=""


)