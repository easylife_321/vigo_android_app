package com.bt.kotlindemo.model

public data class Result(
    val code: Int,
    val custom: String,
    val msg: String
)

