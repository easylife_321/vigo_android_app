/*
 * *
 *  * Created by Surajit on 1/16/21 10:27 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/16/21 10:27 PM
 *  *
 */

package com.bt.kotlindemo.model.shared_users


import com.google.gson.annotations.SerializedName

data class Day(
    @SerializedName("fri")
    val fri: Boolean?,
    @SerializedName("mon")
    val mon: Boolean?,
    @SerializedName("sat")
    val sat: Boolean?,
    @SerializedName("sun")
    val sun: Boolean?,
    @SerializedName("thu")
    val thu: Boolean?,
    @SerializedName("tue")
    val tue: Boolean?,
    @SerializedName("wed")
    val wed: Boolean?
)