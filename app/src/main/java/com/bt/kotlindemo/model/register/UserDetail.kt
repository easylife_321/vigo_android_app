package com.bt.kotlindemo.model.register

import com.google.gson.annotations.SerializedName

data class UserDetail(
    @SerializedName("userId")
    val userId: Int,
    @SerializedName("fullName")
    val name: String,
    @SerializedName("mobileNo")
    val mobileNo: String,
    @SerializedName("emailId")
    val emailId: String?,
    @SerializedName("phoneToken")
    val firebaseToken: String,
    @SerializedName("accountId")
    val accountId: Int,
    @SerializedName("addressLine1")
    val addressLine1: String?,
    @SerializedName("addressLine2")
    val addressLine2: String?,
    @SerializedName("pin")
    val pin: String?,
    @SerializedName("profilePic")
    val profilePic: String?,
    @SerializedName("stateName")
    val stateName: String?,
    @SerializedName("stateId")
    val stateId: Int?
    )
