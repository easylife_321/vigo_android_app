/*
 * *
 *  * Created by Surajit on 29/6/20 11:51 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 29/6/20 11:51 PM
 *  *
 */

package com.bt.kotlindemo.model.device_history


import com.google.gson.annotations.SerializedName

data class History(
    @SerializedName("dist")
    val dist: Int,
    @SerializedName("gpsTime")
    val gpsTime: String,
    @SerializedName("_id")
    val id: String,
    @SerializedName("isIgnitionOn")
    val isIgnitionOn: Boolean,
    @SerializedName("isMoving")
    val isMoving: Boolean,
    @SerializedName("lat")
    val lat: Double,
    @SerializedName("lng")
    val lng: Double,
    @SerializedName("speed")
    val speed: Int,
    @SerializedName("stopTillTimestamp")
    val stopTillTimestamp: Long,
    @SerializedName("gpsTime_ts")
    val timestamp: Long
)