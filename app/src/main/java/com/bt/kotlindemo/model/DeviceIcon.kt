/*
 * *
 *  * Created by Surajit on 10/4/20 8:32 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 10/4/20 8:32 PM
 *  *
 */

package com.bt.kotlindemo.model

data class DeviceIcon(
    var text: String,
    var icon: Int,
    var id: Int
)