/*
 * *
 *  * Created by Surajit on 1/3/21 7:56 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/3/21 7:56 PM
 *  *
 */

package com.bt.kotlindemo.model


import com.google.gson.annotations.SerializedName

data class Country(
    @SerializedName("countryCode")
    val countryCode: Int?,
    @SerializedName("countryName")
    val countryName: String?
)