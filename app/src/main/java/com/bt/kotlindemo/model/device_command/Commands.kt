/*
 * *
 *  * Created by Surajit on 15/5/20 4:41 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 15/5/20 4:41 PM
 *  *
 */

package com.bt.kotlindemo.model.device_command


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Commands(
    @SerializedName("commandName")
    var commandName: String,
    @SerializedName("currentStatus")
    var currentStatus: Any,
    @SerializedName("datatypeOfReceivingValue")
    var datatypeOfReceivingValue: Int,
    @SerializedName("featureIcon")
    var featureIcon: String,
    @SerializedName("featureName")
    var featureName: String,
    @SerializedName("showCurrentStatus")
    var showCurrentStatus: Boolean,
    @SerializedName("commandDesc")
    var commandDesc: String,
    @SerializedName("accessoriesMacId")
    var accessoriesMacId: String

) : Serializable