package com.bt.kotlindemo.model

import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("userId") val userId: Int,
    @SerializedName("id") val id: Int,
    @SerializedName("title") val title: Int,
    @SerializedName("completed") val completed: Boolean
)