package com.bt.kotlindemo.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CountryList : Serializable {
    @SerializedName("country")
    @Expose
    var country: String = ""

    @SerializedName("country_code")
    @Expose
    var countryCode: String = ""

    @SerializedName("exit_code")
    @Expose
    var exitCode: String = ""

    @SerializedName("trunk_prefix")
    @Expose
    var trunkPrefix: String = ""

    @SerializedName("isd")
    @Expose
    var isd: String = ""

    @SerializedName("isd_2")
    @Expose
    var isoAlpha: String = ""

}
