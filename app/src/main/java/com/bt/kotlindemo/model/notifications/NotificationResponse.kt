/*
 * *
 *  * Created by Surajit on 1/24/21 1:21 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/24/21 1:21 AM
 *  *
 */

package com.bt.kotlindemo.model.notifications


import com.google.gson.annotations.SerializedName
import com.bt.kotlindemo.model.Result

data class NotificationResponse(
    @SerializedName("notifications")
    val notifications: List<Notification>?,
    @SerializedName("result")
    val result: Result
)