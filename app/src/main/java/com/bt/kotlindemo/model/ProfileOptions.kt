/*
 * *
 *  * Created by Surajit on 27/12/20 4:42 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/12/20 4:42 PM
 *  *
 */

package com.bt.kotlindemo.model

data class ProfileOptions(
    val title: String,
    val res: Int,
    val id: Int,
    var isOn:Boolean=false
)