/*
 * *
 *  * Created by Surajit on 14/4/20 5:59 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 14/4/20 5:59 PM
 *  *
 */

package com.bt.kotlindemo.model


import com.google.gson.annotations.SerializedName

data class NotificationSettings(
    @SerializedName("ntfName")
    val ntfName: String,
    @SerializedName("ntfTypeId")
    val ntfTypeId: Int,
    @SerializedName("NtfValue")
    var ntfValue: Int
)