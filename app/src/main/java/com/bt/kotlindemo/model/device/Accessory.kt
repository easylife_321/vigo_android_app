/*
 * *
 *  * Created by Surajit on 25/4/20 12:55 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 25/4/20 12:55 PM
 *  *
 */

package com.bt.kotlindemo.model.device


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Accessory(
    @SerializedName("assetName")
    val assetsName: String,
    @SerializedName("currentValue1")
    val currentValue1: String,
    @SerializedName("currentValue2")
    val currentValue2: String,
    @SerializedName("currentValue3")
    val currentValue3: String,
    @SerializedName("datatypeOfReceivingValue")
    val datatypeOfReceivingValue: Int,
    @SerializedName("iconPath")
    val iconPath: String,
    @SerializedName("isReadOnly")
    val isReadOnly: Boolean
):Serializable