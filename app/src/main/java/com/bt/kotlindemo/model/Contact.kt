/*
 * *
 *  * Created by Surajit on 23/7/20 12:43 AM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 23/7/20 12:43 AM
 *  *
 */

package com.bt.kotlindemo.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class Contact(
    var contactId: String? = null,
    @SerializedName("phoneName")
    var contactName: String? = "",
    @SerializedName("registeredName")
    var registeredName: String? = "",
    @SerializedName("mobileno")
    var contactNumber: String? = "",
    @SerializedName("userId")
    var userId: Int = 0,
    @SerializedName("profilePic")
    var profilePic: String? = "",
    @SerializedName("profilePicThumb")
    var profilePicThumb: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(contactId)
        parcel.writeString(contactName)
        parcel.writeString(registeredName)
        parcel.writeString(contactNumber)
        parcel.writeInt(userId)
        parcel.writeString(profilePic)
        parcel.writeString(profilePicThumb)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Contact> {
        override fun createFromParcel(parcel: Parcel): Contact {
            return Contact(parcel)
        }

        override fun newArray(size: Int): Array<Contact?> {
            return arrayOfNulls(size)
        }
    }
}