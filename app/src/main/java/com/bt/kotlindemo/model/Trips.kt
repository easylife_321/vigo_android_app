/*
 * *
 *  * Created by Surajit on 1/5/21 10:29 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/5/21 10:29 PM
 *  *
 */

package com.bt.kotlindemo.model


import com.google.gson.annotations.SerializedName

data class Trips(
    @SerializedName("deviceId")
    val deviceId: Int?,
    @SerializedName("endAddr")
    val endAddr: String?,
    @SerializedName("endLat")
    val endLat: Double?,
    @SerializedName("endTime")
    val endTime: String?,
    @SerializedName("endTimestamp")
    val endTimestamp: Long?,
    @SerializedName("endlng")
    val endlng: Double?,
    @SerializedName("isBusinessTrip")
    val isBusinessTrip: Boolean?,
    @SerializedName("startAddr")
    val startAddr: String?,
    @SerializedName("startLat")
    val startLat: Double?,
    @SerializedName("startLng")
    val startLng: Double?,
    @SerializedName("startTime")
    val startTime: String?,
    @SerializedName("startTimestamp")
    val startTimestamp: Long?,
    @SerializedName("status")
    val status: Boolean?,
    @SerializedName("totalDistance")
    val totalDistance: Int?,
    @SerializedName("tripId")
    val tripId: String?,
    @SerializedName("tripName")
    val tripName: String?,
    @SerializedName("travelTime")
    val travelTime: String?
)