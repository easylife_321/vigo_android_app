/*
 * *
 *  * Created by Surajit on 16/6/20 8:44 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 16/6/20 8:44 PM
 *  *
 */

package com.bt.kotlindemo.model.zone


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Zone(
    @SerializedName("geolocations")
    val loc: List<Loc>,
    @SerializedName("name")
    val name: String,
    @SerializedName("notifyAction")
    val notifyAction: Int,
    @SerializedName("notifyWith")
    val notifyWith: Int,
    @SerializedName("radius")
    val radius: Int,
    @SerializedName("shape")
    val shape: Int,
    @SerializedName("userId")
    val userId: Int,
    @SerializedName("zoneId")
    val zoneId: String
) : Serializable {
    val shapeText = if (shape == 0) "Circle" else "Polygon"
}