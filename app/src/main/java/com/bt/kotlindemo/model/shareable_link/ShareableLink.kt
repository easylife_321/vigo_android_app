/*
 * *
 *  * Created by Surajit on 2/6/20 11:29 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 2/6/20 11:29 PM
 *  *
 */

package com.bt.kotlindemo.model.shareable_link


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ShareableLink(
    @SerializedName("devices")
    val devices: List<Int>,
    @SerializedName("isActive")
    var isActive: Boolean,
    @SerializedName("isExpired")
    val isExpired: Boolean,
    @SerializedName("trackingLinkId")
    val trackingLinkId: Int,
    @SerializedName("trackingLinkName")
    val trackingLinkName: String,
    @SerializedName("trackingMessage")
    val trackingMessage: String,
    @SerializedName("trackingURL")
    val trackingURL: String,
    @SerializedName("validfrom")
    val validfrom: String,
    @SerializedName("validfromTimestamp")
    val validfromTimestamp: Long,
    @SerializedName("validtill")
    val validtill: String,
    @SerializedName("validtillTimestamp")
    val validtillTimestamp: Long

) : Serializable