/*
 * *
 *  * Created by Surajit on 28/10/20 5:26 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 28/10/20 5:26 PM
 *  *
 */
package com.bt.kotlindemo.model

data class Days(
    val day: String,
    var selected: Boolean
)