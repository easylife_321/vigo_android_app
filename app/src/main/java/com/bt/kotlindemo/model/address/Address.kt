/*
 * *
 *  * Created by Surajit on 3/28/22, 6:32 PM
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 3/28/22, 6:32 PM
 *  *
 */

package com.bt.kotlindemo.model.address


import com.google.gson.annotations.SerializedName

data class Address(
    @SerializedName("accountId")
    val accountId: Int?=null,
    @SerializedName("address")
    val address: String?=null,
    @SerializedName("cityName")
    val cityName: String?=null,
    @SerializedName("lat")
    val lat: Double?=null,
    @SerializedName("lng")
    val lng: Double?=null,
    @SerializedName("postalCode")
    val postalCode: String?=null,
    @SerializedName("stateName")
    val stateName: String?=null
)