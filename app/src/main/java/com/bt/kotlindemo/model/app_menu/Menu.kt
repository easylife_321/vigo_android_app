/*
 * *
 *  * Created by Surajit on 11/5/20 8:13 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 11/5/20 8:13 PM
 *  *
 */

package com.bt.kotlindemo.model.app_menu


import com.google.gson.annotations.SerializedName

data class Menu(
    @SerializedName("menuIcon")
    val menuIcon: String,
    @SerializedName("menuId")
    val menuId: Int,
    @SerializedName("menuName")
    val menuName: String,
    @SerializedName("menuUrl")
    val menuUrl: String,
    @SerializedName("Submenuids")
    val submenuids: List<SubMenu>,
    @SerializedName("redirectTo")
    val redirectTo: String
)