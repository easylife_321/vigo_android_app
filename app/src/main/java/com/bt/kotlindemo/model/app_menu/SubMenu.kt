/*
 * *
 *  * Created by Surajit on 11/5/20 8:13 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 11/5/20 8:13 PM
 *  *
 */

package com.bt.kotlindemo.model.app_menu


import com.google.gson.annotations.SerializedName

data class SubMenu(
    @SerializedName("subMenuIcon")
    val subMenuIcon: String,
    @SerializedName("subMenuId")
    val subMenuId: Int,
    @SerializedName("subMenuName")
    val subMenuName: String,
    @SerializedName("subMenuURL")
    val subMenuURL: String,
    @SerializedName("redirectTo")
    val redirectTo: String
)