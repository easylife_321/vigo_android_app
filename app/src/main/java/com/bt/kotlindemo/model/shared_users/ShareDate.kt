/*
 * *
 *  * Created by Surajit on 1/16/21 10:27 PM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/16/21 10:27 PM
 *  *
 */

package com.bt.kotlindemo.model.shared_users


import com.google.gson.annotations.SerializedName

data class ShareDate(
    @SerializedName("date")
    val date: String?,
    @SerializedName("userdetails")
    val userdetails: List<UserDetail>?
)