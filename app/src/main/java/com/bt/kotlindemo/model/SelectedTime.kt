/*
 * *
 *  * Created by Surajit on 27/10/20 10:44 PM
 *  * Copyright (c) 2020 . All rights reserved.
 *  * Last modified 27/10/20 10:44 PM
 *  *
 */

package com.bt.kotlindemo.model

data class SelectedTime(
    val startTime:Long,
    val endTime:Long
)