/*
 * *
 *  * Created by Surajit on 1/3/21 1:11 AM
 *  * Copyright (c) 2021 . All rights reserved.
 *  * Last modified 1/3/21 1:11 AM
 *  *
 */

package com.bt.kotlindemo.model


import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("typeId")
    val typeId: Int?
)