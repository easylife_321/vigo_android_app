package com.bt.kotlindemo.di

import com.bt.kotlindemo.ui.contacts.ContactsListViewModel
import com.bt.kotlindemo.ui.dashboard.device_list_view.ListViewViewModel
import com.bt.kotlindemo.ui.dashboard.map_view.MapViewViewModel
import com.bt.kotlindemo.ui.device_detail.DeviceDetailViewModel
import com.bt.kotlindemo.ui.device_detail.device_comands.DeviceCommandViewModel
import com.bt.kotlindemo.ui.device_history.DeviceHistoryViewModel
import com.bt.kotlindemo.ui.device_options.device_management.add_device.AddDeviceActivityViewModel
import com.bt.kotlindemo.ui.device_options.device_management.device_list.DeviceManagementViewModel
import com.bt.kotlindemo.ui.device_options.device_management.manage_shared_users.ManagedSharedUsersViewModel
import com.bt.kotlindemo.ui.device_options.device_management.share.TimeSpecificDeviceSharingViewModel
import com.bt.kotlindemo.ui.device_options.groups.GroupManagementActivityViewModel
import com.bt.kotlindemo.ui.device_options.notification_settings.NotificationSettingsActivityViewModel
import com.bt.kotlindemo.ui.device_options.poi_management.add_edit_poi.AddPoiViewModel
import com.bt.kotlindemo.ui.device_options.poi_management.poi_list.PoiListViewModel
import com.bt.kotlindemo.ui.device_options.shareable_link.add_new_link.AddShareableLinkViewModel
import com.bt.kotlindemo.ui.device_options.shareable_link.link_list.ShareableLInkListViewModel
import com.bt.kotlindemo.ui.device_options.trips_management.TripsListViewModel
import com.bt.kotlindemo.ui.device_options.zone_management.add_zone.AddZoneViewModel
import com.bt.kotlindemo.ui.device_options.zone_management.zone_list.ZoneListViewModel
import com.bt.kotlindemo.ui.feedback.FeedbackViewModel
import com.bt.kotlindemo.ui.notifications.NotificationViewModel
import com.bt.kotlindemo.ui.profile.change_mobile_number.ChangeMobileNumberViewModel
import com.bt.kotlindemo.ui.profile.profile_detail.ProfileDetailViewModel
import com.bt.kotlindemo.ui.profile.update_profile.UpdateProfileViewModel
import com.bt.kotlindemo.ui.sign_in_activity.RegistrationViewModel
import com.bt.kotlindemo.ui.splash_activity.SplashActivityViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {


    viewModel { RegistrationViewModel(get(), get(), get(), get(), get()) }
    viewModel {
        SplashActivityViewModel(
            get(),
            get()
        )
    }
    viewModel { MapViewViewModel(get(), get()) }
    viewModel { ListViewViewModel(get(), get()) }

    viewModel { AddDeviceActivityViewModel(get(), get(), get()) }
    viewModel { GroupManagementActivityViewModel(get(), get(),get()) }


    viewModel { NotificationSettingsActivityViewModel(get(), get()) }
    viewModel {
        DeviceManagementViewModel(
            get(),
            get(),
            get()
        )
    }
    viewModel { (id: Int) -> DeviceDetailViewModel(id, get(), get(),get(),get()) }
    viewModel { (id: Int) -> DeviceCommandViewModel(id, get(), get(), get()) }

    viewModel { (id: String?) -> AddPoiViewModel(id, get(), get(), get()) }
    viewModel { PoiListViewModel(get(), get()) }


    viewModel { AddShareableLinkViewModel(get(), get(), get()) }
    viewModel { ShareableLInkListViewModel(get(), get()) }

    viewModel { AddZoneViewModel(get(), get(), get()) }
    viewModel { ZoneListViewModel(get(), get(), get()) }

    viewModel { (id: Int) -> DeviceHistoryViewModel(id, get(), get()) }

    viewModel { ContactsListViewModel(get(), get(), get()) }

    viewModel { TimeSpecificDeviceSharingViewModel(get(), get(),get()) }

    viewModel { ProfileDetailViewModel(get(), get(),get(), get()) }

    viewModel { FeedbackViewModel(get(), get(),get()) }

    viewModel { UpdateProfileViewModel(get(), get(),get()) }

    viewModel { TripsListViewModel(get(), get(),get()) }

    viewModel { ChangeMobileNumberViewModel(get(), get(),get(),get()) }

    viewModel { ManagedSharedUsersViewModel(get(), get()) }

    viewModel { NotificationViewModel(get()) }
}