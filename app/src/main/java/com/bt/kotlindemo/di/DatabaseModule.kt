package com.bt.kotlindemo.di

import android.content.Context
import androidx.room.Room
import com.bt.kotlindemo.constants.DatabaseConstants
import com.bt.kotlindemo.data.database.AppDatabase
import com.bt.kotlindemo.data.database.dao.ContactsDao
import com.bt.kotlindemo.data.database.dao.DeviceDao
import com.bt.kotlindemo.data.database.dao.NotificationDao
import org.koin.dsl.module

var dataBaseModule = module {


    fun provideDatabase(context: Context): AppDatabase? {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, DatabaseConstants.DB_NAME
        )
            .fallbackToDestructiveMigration()
            .build()
    }


    fun provideDeviceDao(appDatabase: AppDatabase): DeviceDao {
        return appDatabase.getDeviceDao();
    }


    fun provideContactsDao(appDatabase: AppDatabase): ContactsDao {
        return appDatabase.getContactsDao();
    }


    fun provideNotificationDao(appDatabase: AppDatabase): NotificationDao {
        return appDatabase.getNotificationDao();
    }

    single { provideDatabase(get()) }
    single { provideDeviceDao(get()) }
    single { provideContactsDao(get()) }
    single { provideNotificationDao(get()) }


}