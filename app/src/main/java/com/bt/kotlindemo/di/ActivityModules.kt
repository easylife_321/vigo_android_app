package com.bt.kotlindemo.di

import android.content.Context
import com.bt.kotlindemo.model.CountryList
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.koin.dsl.module
import com.bt.kotlindemo.utils.MiscUtil
import java.io.IOException


var selectCountryModule = module {

    fun provideCountryList(gson: Gson, context: Context): ArrayList<CountryList> {
        val token: TypeToken<ArrayList<CountryList>> =
            object : TypeToken<ArrayList<CountryList>>() {}
        var countryList: ArrayList<CountryList> = ArrayList()
        try {
            countryList =
                gson.fromJson(MiscUtil.getJsonAsString("country_data.json", context), token.type)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return countryList
    }

    single(override = true) { provideCountryList(get(), get()) }
}