package com.bt.kotlindemo.di

import com.bt.kotlindemo.data.database.AppDatabase
import com.bt.kotlindemo.data.database.dao.ContactsDao
import com.bt.kotlindemo.data.database.dao.DeviceDao
import com.bt.kotlindemo.data.database.dao.NotificationDao
import com.bt.kotlindemo.data.datasource.*
import com.bt.kotlindemo.data.network.ApiService
import com.bt.kotlindemo.data.repository.*
import com.bt.kotlindemo.utils.AppPreferences
import org.koin.dsl.module

var repositoryModule = module {

    //app data
    fun provideAppDataSource(api: ApiService) = AppDataSource(api)
    fun provideAppRepository(dataSource: AppDataSource, preference: AppPreferences) =
        AppDataRepository(
            dataSource,
            preference
        )

    factory { provideAppDataSource(get()) }
    factory { provideAppRepository(get(), get()) }





    //registration/login(user)
    fun provideRegistrationDataSource(api: ApiService) = RegistrationDataSource(api)
    fun provideRegistrationRepository(
        dataSource: RegistrationDataSource,
        appPreferences: AppPreferences
    ) = RegistrationRepository(dataSource, appPreferences)
    factory { provideRegistrationDataSource(get()) }
    factory { provideRegistrationRepository(get(), get()) }


    //device
    fun provideDeviceDataSource(api: ApiService, deviceDao: DeviceDao) =
        DeviceDataSource(api, deviceDao)

    fun provideDeviceRepository(
        dataSource: DeviceDataSource,
        appPreferences: AppPreferences
    ) = DeviceRepository(dataSource, appPreferences)
    factory { provideDeviceDataSource(get(), get()) }
    factory { provideDeviceRepository(get(), get()) }




    //group
    fun provideGroupDataSource(api: ApiService) = GroupDataSource(api)
    fun provideGroupRepository(
        dataSource: GroupDataSource,
        appPreferences: AppPreferences
    ) = GroupRepository(dataSource, appPreferences)
    factory { provideGroupDataSource(get()) }
    factory { provideGroupRepository(get(), get()) }



    //notification settings
    fun provideNotificationSettingsDataSource(api: ApiService, notificationDao: NotificationDao) =
        NotificationDataSource(api, notificationDao)

    fun provideNotificationSettingsRepository(
        dataSource: NotificationDataSource,
        appPreferences: AppPreferences
    ) = NotificationRepository(dataSource, appPreferences)
    factory { provideNotificationSettingsDataSource(get(), get()) }
    factory { provideNotificationSettingsRepository(get(), get()) }



    //device command
    fun provideDeviceCommandDataSource(api: ApiService) = DeviceCommandDataSource(api)
    fun provideDeviceCommandRepository(
        dataSource: DeviceCommandDataSource,
        appPreferences: AppPreferences
    ) = DeviceCommandRepository(dataSource, appPreferences)
    factory { provideDeviceCommandRepository(get(), get()) }
    factory { provideDeviceCommandDataSource(get()) }


    //poi
    fun providePoiDataSource(api: ApiService) = PoiDataSource(api)
    fun providePoiRepository(
        dataSource: PoiDataSource,
        appPreferences: AppPreferences
    ) = PoiRepository(dataSource, appPreferences)
    factory { providePoiRepository(get(), get()) }
    factory { providePoiDataSource(get()) }


    //shareable link
    fun provideSharableLinkDataSource(api: ApiService) = ShareableLinkDataSource(api)
    fun provideShareableLinkRepository(
        dataSource: ShareableLinkDataSource,
        appPreferences: AppPreferences
    ) = ShareableLinkRepository(dataSource, appPreferences)
    factory { provideShareableLinkRepository(get(), get()) }
    factory { provideSharableLinkDataSource(get()) }


    //zone
    fun provideZoneDataSource(api: ApiService) = ZoneDataSource(api)
    fun provideZoneRepository(
        dataSource: ZoneDataSource,
        appPreferences: AppPreferences
    ) = ZoneRepository(dataSource, appPreferences)
    factory { provideZoneRepository(get(), get()) }
    factory { provideZoneDataSource(get()) }


    //trips
    fun provideTripDataSource(api: ApiService) = TripDataSource(api)
    fun provideTripRepository(
        dataSource: TripDataSource,
        appPreferences: AppPreferences
    ) = TripRepository(dataSource, appPreferences)
    factory { provideTripRepository(get(), get()) }
    factory { provideTripDataSource(get()) }


    //contacts
    fun provideContactDataSource(api: ApiService, contactsDao: ContactsDao) =
        ContactDataSource(api, contactsDao)

    fun provideContactRepository(
        dataSource: ContactDataSource,
        appPreferences: AppPreferences
    ) = ContactRepository(dataSource, appPreferences)
    factory { provideContactDataSource(get(), get()) }
    factory { provideContactRepository(get(), get()) }


    //settings
    fun provideSettingsDataSource(api: ApiService) =
        SettingsDataSource(api)

    fun provideSettingsRepository(
        dataSource: SettingsDataSource,
        appPreferences: AppPreferences,
        appDatabase: AppDatabase
    ) = SettingsRepository(dataSource, appPreferences, appDatabase)
    factory { provideSettingsDataSource(get()) }
    factory { provideSettingsRepository(get(), get(), get()) }


    //address
    fun provideAddressDataSource(api: ApiService) =
        AddressDataSource(api)

    fun provideAddressRepository(
        dataSource: AddressDataSource
    ) = AddressRepository(dataSource)
    factory { provideAddressDataSource(get()) }
    factory { provideAddressRepository(get()) }
}