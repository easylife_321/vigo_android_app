package com.bt.kotlindemo.di

import android.content.Context
import com.bt.kotlindemo.utils.AppPreferences
import com.bt.kotlindemo.utils.ConnectivityUtil
import com.bt.kotlindemo.utils.CryptoUtil
import com.jakewharton.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import io.michaelrocks.libphonenumber.android.PhoneNumberUtil
import okhttp3.OkHttpClient
import org.koin.dsl.module


var appModule = module {

    fun provideAppPreferences(context: Context): AppPreferences {
        return AppPreferences(context)
    }

    fun provideConnectivityUtil(context: Context) = ConnectivityUtil(context)
    fun provideCryptoUtil() = CryptoUtil()

    fun getPicassoDownloader(okHttpClient: OkHttpClient): OkHttp3Downloader {
        return OkHttp3Downloader(okHttpClient)
    }

    fun getPicasso(context: Context, downloader: OkHttp3Downloader): Picasso {
        return Picasso.Builder(context)
            .loggingEnabled(true)
            .build()

    }

    fun getPhoneLib(context: Context): PhoneNumberUtil {
        return PhoneNumberUtil.createInstance(context)

    }


    single { provideAppPreferences(get()) }
    single { provideConnectivityUtil(get()) }
    single { provideCryptoUtil() }
    single { getPicassoDownloader(get()) }
    single { getPicasso(get(), get()) }
    single { getPhoneLib(get()) }
    // single { getDataBindingAdapter(get()) }

}